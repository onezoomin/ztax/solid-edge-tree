# Note3

### IPLD-based datalog powering a local-first outliner app

This is our submission as part of HackFS 2023.

[**Open live app here**](https://note3.fission.app)\
(Note: Push is broken in the minified/deployed build because of a bug in w3ui, so you'll have to clone & run it for the full experience currently 😵‍💫 - see below)

we've built:

- a simple datalog engine with reactive queries
- that saves & syncs to IPLD (currently IndexDB & web3.storage, but other options planned)
- an outliner note-taking app inspired by Logseq, Workflowy & co
- option to publish and share your datalog stream and pull in the streams of others, enabling (far from perfect, but with high potential) CRDT-like change merging

### Clone & run

1. (optional) `direnv allow` / `nix develop`
2. `task dev` (or `pnpm i && pnpm dev`)

[Hack on gitpod](https://gitpod.io/#https://gitlab.com/onezoomin/note3)
