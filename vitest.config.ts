import { defineConfig } from 'vite'
import { configDefaults } from 'vitest/config'

export default defineConfig({
	plugins: [],
	build: {
		target: 'esnext',
	},
	test: {
		exclude: [
			...configDefaults.exclude, // https://vitest.dev/config/#exclude
			'**/.direnv/**',
			'**/.devenv/**',
			'**/.task/**',
		],

		deps: {
			optimizer: {
				web: {
					enabled: true,
				},
			},
		},
	},
})
