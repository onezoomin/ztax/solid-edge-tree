import { Type as T } from '@sinclair/typebox'
import { TypeSystem } from '@sinclair/typebox/system'
import { ApplogStream } from '../data/datalog/query/engine'

export const ApplogStreamTB = TypeSystem.Type<ApplogStream>('ApplogStream', (options, value) => {
	return value instanceof ApplogStream
})()
const { String: StringTB, Optional: OptionalTB, Boolean: BooleanTB, Object: ObjectTB, Number: NumberTB } = T
export const Str: typeof StringTB = StringTB.bind(T)
export const Num: typeof NumberTB = NumberTB.bind(T)
export const Obj: typeof ObjectTB = ObjectTB.bind(T)
export const Opt: typeof OptionalTB = OptionalTB.bind(T)
export const Bool: typeof BooleanTB = BooleanTB.bind(T)

export const STR: ReturnType<typeof T.String> = Str()
export const NUM: ReturnType<typeof T.Number> = Num()
export const BOOL: ReturnType<typeof T.Boolean> = Bool()

export type DefaultTrue = true | boolean
export const DefaultTrue: DefaultTrue = true

export type DefaultFalse = false | boolean
export const DefaultFalse: DefaultFalse = false

export type GenericObject = Record<string, any>
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>
export type PartialBy<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>
export type PromiseType<T extends Promise<any>> = T extends Promise<infer U> ? U : never

export type CoerceToString<T> = T extends string ? T : never

// https://stackoverflow.com/a/76276541
export type LastElementOf<T extends readonly unknown[]> = T extends readonly [...unknown[], infer Last] ? Last : never

export function checkParityTB() {
	/* Most examples are constantly calling Type.*() - needed to check if its really needed
    https://github.com/sinclairzx81/typebox/issues/587#issuecomment-1712457623
     */
	const s1 = Str()
	const s2 = Str()
	const n1 = Num()
	const n2 = Num()
	console.log({ s1, s2, n1, n2 })
}

/** solidjs Setter requires returning something, which I often don't */
export type GenericSetter<T> = (newValue: T) => void
