import '@shoelace-style/shoelace/dist/themes/dark.css'
import { useLocation, useNavigate, useParams, useSearchParams } from '@solidjs/router'
import AddIcon from '@suid/icons-material/Add'
import { Fab } from '@suid/material'
import { Logger } from 'besonders-logger/src'
import { createEffect, createMemo, createResource, Match, Show, Suspense, Switch } from 'solid-js'
import { ApplogView } from './components/ApplogView'
import { BlockTree } from './components/BlockTree'
import { Breadcrumbs } from './components/Breadcrumbs'
import { PubInfos, Spinner } from './components/mini-components'
import { getSubOrPub } from './data/AgentStore'
import { addBlock } from './data/block-utils'
import { useGlobalKeyhandlers } from './data/keybindings'
import { isPublication } from './data/local-state'
import { retrievePubDataWithExtras } from './ipfs/store-sync'
import { LazyRender } from './ui/lazy-render'
import { DBContext, useFocus, useRawDS, useRoots, withDS } from './ui/reactive'
import './ui/shoelace'
import { zoomToBlock } from './ui/ui-utils'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

let renderCount = 0
export default () => {
	;(renderCount++ > 0 ? WARN : DEBUG)('!!! MAINPAGE RENDER !!!', renderCount)
	// trace(true)

	const location = useLocation()
	const navigate = useNavigate()
	const rootDS = useRawDS()
	const params = useParams()
	const focus = useFocus()

	useGlobalKeyhandlers()

	const [searchParams, _setSearchParams] = useSearchParams()
	const pubFromUrl = createMemo(() => searchParams.pub)
	const previewFromUrl = createMemo(() => {
		DEBUG(`searchParams:`, { ...searchParams })
		return searchParams.preview // TODO: handle pub url properly
	})
	const selectedPubOrPreview = createMemo(() => pubFromUrl() || previewFromUrl())
	// TODO: pull in BG when pub

	// PREVIEW //
	const matchingSubOrPub = createMemo(() => selectedPubOrPreview() ? getSubOrPub(selectedPubOrPreview()) : null)
	const matchingIsPub = createMemo(() => !!matchingSubOrPub() && isPublication(matchingSubOrPub()))
	const previewOrMissingPub = createMemo(() => {
		if (previewFromUrl()) return previewFromUrl()
		if (pubFromUrl() && !matchingSubOrPub()) return pubFromUrl()
		return null
	})
	const [previewData, { refetch: refetchPreview }] = createResource(previewOrMissingPub, async (previewOrMissingPub) => {
		try {
			DEBUG(`[previewData] for`, previewOrMissingPub)
			// await new Promise(resolve => setTimeout(resolve, 700)) // ; throw new Error(`Eddi`)
			if (!previewOrMissingPub) return null
			return await retrievePubDataWithExtras(rootDS, previewOrMissingPub)
		} catch (err) {
			ERROR(`Failed to fetch preview`, previewOrMissingPub, err)
			throw new Error(`Failed to fetch preview: ${err.message}`)
		}
	})
	createEffect(() => DEBUG(`preview:`, /* previewDS(), previewDS, */ { state: previewData.state, err: previewData.error }))
	const [currentDS] = createResource(() => {
		DEBUG(`[currentDS] resource source:`, { previewData, previewOrMissingPub: previewOrMissingPub() })
		return [previewOrMissingPub(), (!!previewOrMissingPub()) && previewData.state === 'ready' && previewData()]
	}, async ([previewOrMissingPub, previewData]) => {
		DEBUG(`[currentDS] resource load`, { previewData, previewOrMissingPub, rootDS })
		// await sleep(1000) // HACK: defer datastream render to display spinner while loading
		if (previewOrMissingPub) {
			// if (!previewData) throw ERROR(`Invalid previewData state`, previewData)
			if (!previewData) return null
			return previewData.dataStream
		}
		return rootDS
	})
	createEffect(() => DEBUG(`currentDS state:`, currentDS.state))
	createEffect(() => DEBUG(`currentDS:`, currentDS()))

	const roots = createMemo(() => {
		DEBUG(`Querying roots of`, currentDS())
		return currentDS() && withDS(currentDS(), () => useRoots())
	})
	createEffect(() => DEBUG(`roots:`, roots(), currentDS()))
	const addRootNode = () => {
		const newBlockID = addBlock({ dataStream: currentDS(), asChildOf: null, focus: true })
		LOG(`New block created`, newBlockID, { focus: focus() })
		if (focus()) { // ? or always
			zoomToBlock({ id: newBlockID }, location, navigate)
		}
	}

	return (
		<main flex='1 ~ col items-center justify-start' w-full gap-8 pb-2>
			{
				/* <pre>urlP: {previewFromUrl()}</pre>
			<div key={previewFromUrl()}>
				<Suspense fallback={<spinner style='font-size: 3rem; --indicator-color: brown; --track-color: #000;'></spinner>}>
					<pre>DS: {currentDS()}</pre>
				</Suspense>
			</div> */
			}
			<Show when={!previewOrMissingPub() /* HACK: check if DataStream is writable/persisted */}>
				<div fixed bottom-3 right-5 z-500>
					<Fab color='primary' aria-label='add' onClick={addRootNode}>
						<AddIcon />
					</Fab>
				</div>
			</Show>
			{/* <pre>{JSON.stringify(roots(), undefined, 2)}</pre> */}
			<Suspense fallback={<Spinner size='3rem' color='orange' />}>
				<Switch fallback={'unknown state'}>
					<Match when={previewData.error}>
						<div flex='~ col' w-full gap-4>
							<sl-alert variant='danger' open>
								<div flex='~ items-center gap-4'>
									<div class='i-ph:warning-bold' />
									<strong>Failed to load stream</strong>
								</div>
								<span font-mono>{previewData.error.message ?? JSON.stringify(previewData.error, undefined, 4)}</span>
							</sl-alert>
						</div>
					</Match>
					<Match when={previewData.loading}>
						<div flex='~ col' w-full gap-4>
							<sl-alert variant='primary' open w-full>
								<div flex='~ items-center gap-2'>
									<div flex='shrink-0' class='i-ph:eye-bold' />
									<strong overflow-hidden text-ellipsis>
										Loading preview for datastream: <code>{previewOrMissingPub()}</code>
									</strong>
								</div>
							</sl-alert>
							<div flex='~ col items-center'>
								<Spinner size='3rem' color='purple' />
							</div>
						</div>
					</Match>
					<Match when={true}>
						{/* HACK: suspense necessary? */}
						<Suspense fallback={<sl-spinner style='font-size: 3rem; --indicator-color: pink; --track-color: #000;'></sl-spinner>}>
							<Show when={currentDS()} fallback={<span text-red>No currentDS</span>}>
								<DBContext.Provider value={currentDS}>
									{/* <pre>{JSON.stringify({f:focus(),r:roots()})}</pre> */}
									<div id='main-list' key={currentDS().name} flex='~ col grow-1' w-full gap-4>
										<Show when={previewOrMissingPub()}>
											<sl-alert variant='primary' open>
												<div flex='~ items-center gap-2'>
													<div flex='shrink-0' class='i-ph:eye-bold' />
													<strong flex-shrink-0>
														Previewing datastream:
													</strong>
													<span overflow-hidden text-ellipsis>
														<code>{previewOrMissingPub()}</code>
													</span>
												</div>

												<div mt-1 px-2 py-1 border='1 solid lightblue-700' rounded gap-2>
													<PubInfos info={previewData} />
												</div>

												<Show when={matchingSubOrPub()}>
													<div mt-2>
														This is{' '}
														<a
															href={`#/settings/${matchingIsPub() ? 'publications' : 'subscriptions'}/${matchingSubOrPub().id}`}
															visited:color-white
														>
															{matchingIsPub() ? 'your publication' : 'a subscription in your list'}
														</a>
													</div>
												</Show>
												<Show when={!matchingSubOrPub()}>
													<div flex='~ justify-end' mt-2 text-xs>
														<a href={`#/settings/subscriptions/?pub=${previewOrMissingPub()}`}>
															<sl-button size='small' variant='primary'>
																<div class='i-ph:plus' slot='prefix' />
																Add subscription
															</sl-button>
														</a>
													</div>
												</Show>
											</sl-alert>
										</Show>
										<Breadcrumbs
											focus={focus}
											showOverview={() => !!(params.blockID /* && roots.get().length > 1 */)}
										/>
										<LazyRender items={params.blockID ? [params.blockID] : roots()} fallback={<div>No roots - reload?</div>}>
											{(id, isInitial) => (
												<div
													key={id}
													class={isInitial ? '' : 'animate-[fade-in-up-small_1s_ease-in-out] animate-duration-[300ms]'}
												>
													{/* <pre>{JSON.stringify(id)}</pre> */}
													<BlockTree root={id} />
												</div>
											)}
										</LazyRender>
									</div>
									<div w-full>
										<ApplogView />
									</div>
								</DBContext.Provider>
							</Show>
						</Suspense>
					</Match>
				</Switch>
			</Suspense>
		</main>
	)
}
