import { useNavigate, useParams } from '@solidjs/router'
import { Logger } from 'besonders-logger/src'
import { createEffect, createMemo } from 'solid-js'
import { AgentSettings } from './components/settings/AgentSettings'
import { PublicationsSettings } from './components/settings/PublicationsSettings'
import { StorageSettings } from './components/settings/StorageSettings'
import { SubscriptionsSettings } from './components/settings/SubscriptionsSettings'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export default () => {
	const params = useParams()
	const navigate = useNavigate()
	DEBUG('!!! <SETTINGSPAGE> RENDER !!!', params)
	// trace(true)

	const active = createMemo(() => {
		const [section, ...sectionParams] = params.section?.split('/')
		DEBUG(`Section?`, params.section, '->', { section, sectionParams })
		return { section, params: sectionParams }
	})
	const activeParams = createMemo(() => active().params) // ? possibly simpler
	const activeSection = createMemo(() => active().section)
	const setActive = (evt: CustomEvent) => {
		DEBUG(`Tab activated`, evt, active())
		if (active().section !== evt.detail.name) {
			navigate(`/settings/${evt.detail.name}`)
		}
	}
	createEffect(() => DEBUG(`active settings section:`, { ...active() }))

	let tabGroupRef
	// (i) we need both: the effect in case we have a section change while the component is mounted, and the active={} on the tab for the initial page load
	createEffect(() => tabGroupRef?.show(active().section))

	return (
		<div flex='~ col' w-full>
			<sl-tab-group onsl-tab-show={setActive} ref={tabGroupRef}>
				<sl-tab slot='nav' panel='agent' active={active().section === 'agent'}>Agent</sl-tab>
				<sl-tab slot='nav' panel='storage' active={active().section === 'storage'}>Storage</sl-tab>
				<sl-tab slot='nav' panel='publications' active={active().section === 'publications'}>Publications</sl-tab>
				<sl-tab slot='nav' panel='subscriptions' active={active().section === 'subscriptions'}>Subscriptions</sl-tab>

				<sl-tab-panel name='agent'>
					{/* <Show when={active().section === 'agent'}> breaks keyring */}
					<AgentSettings />
					{/* </Show> */}
				</sl-tab-panel>
				<sl-tab-panel name='storage'>
					{/* <Show when={active().section === 'storage'}> breaks keyring */}
					<StorageSettings />
					{/* </Show> */}
				</sl-tab-panel>
				<sl-tab-panel name='publications'>
					{/* <Show when={active().section === 'publications'}> */}
					<PublicationsSettings navigate={navigate} activeSection={activeSection} params={activeParams} />
					{/* </Show> */}
				</sl-tab-panel>
				<sl-tab-panel name='subscriptions'>
					<SubscriptionsSettings navigate={navigate} activeSection={activeSection} params={activeParams} />
				</sl-tab-panel>
			</sl-tab-group>
		</div>
	)
}
