import { SlSelect } from '@shoelace-style/shoelace'
import type { useNavigate } from '@solidjs/router'
import { Logger } from 'besonders-logger/src'
import { computed, toJS } from 'mobx'
import { Collapse } from 'solid-collapse'
import { Accessor, Component, createEffect, createMemo, createSignal, For, Match, Show, Switch } from 'solid-js'
import { boundInput, getAgentString, useAgent } from '../../data/AgentStore'
import {
	getPublicationData,
	makeBlocksSelector,
	RE_SELECTOR_BLOCKS,
	RE_SELECTOR_WITHOUT_HISTORY,
	toggleWithoutHistorySelector,
} from '../../data/block-utils'
import { isoDateStrCompare } from '../../data/datalog/utils'
import { IPublication } from '../../data/local-state'
import { publishDatalog } from '../../ipfs/store-sync'
import { addPublication, updatePubEncryption } from '../../ipfs/sub-pub'
import { useW3Uploader } from '../../ipfs/w3ui'
import { useBlockAt, useRawDS, useRoots, withDS } from '../../ui/reactive'
import { createAsyncButtonHandler, makeNote3Url, singleBlockOfPublication, stopPropagation } from '../../ui/ui-utils'
import { BackLink } from '../BackLink'
import { CopyableID, DeleteWithConfirm, Iconify, KidCount } from '../mini-components'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export const PublicationsSettings: Component<{
	navigate: ReturnType<typeof useNavigate> // HACK: context is broken by w3 keyring provider
	activeSection?: Accessor<string>
	params?: Accessor<string[]>
}> = (
	{ navigate, activeSection, params },
) => {
	DEBUG(`Creating <PublicationsPanel>`, { activeSection, params })
	const focussedPub = createMemo(() => params()?.[0])
	const agent = useAgent()

	return (
		// <Show when={activeSection().startsWith('publications')}>
		<div w-full flex='~ col' gap-4>
			<Show when={!agent.hasStorageSetup}>
				<sl-alert variant='warning' open>
					<div flex='~ items-center gap-4'>
						<div class='i-ph:warning-bold' />
						<strong>No storage set up</strong>
					</div>
					<p mb-0>
						You need to <a href='#/settings/storage'>set up storage</a> before you can publish
					</p>
				</sl-alert>
			</Show>
			<Switch>
				<Match when={!focussedPub()}>
					<AddPublicationButton {...{ navigate }} />
				</Match>
				<Match when={focussedPub()}>
					<h4 m-0 flex='inline items-center gap-4'>
						<BackLink text='Back to list' target='/settings/publications' />
					</h4>
				</Match>
			</Switch>
			<PublicationsList {...{ focussedPub, navigate }} />
		</div>
		// </Show>
	)
}

export const PublicationsList: Component<{
	focussedPub?: Accessor<string>
	navigate: ReturnType<typeof useNavigate> // HACK: context is broken by w3 keyring provider
}> = ({ focussedPub, navigate }) => {
	DEBUG('create <PublicationsList>')
	const agent = useAgent()
	const { publications } = agent

	const focusedOrList = createMemo(() => {
		let list = focussedPub() ? publications.filter(p => p.id === focussedPub()) : publications
		list = list.slice().sort((a, b) => isoDateStrCompare(a.createdAt, b.createdAt, 'desc')) // newest first
		DEBUG(`focusedOrList:`, list)
		return list
	})
	createEffect(() => DEBUG(`focussed:`, focussedPub()))
	createEffect(() => DEBUG(`pubs:`, toJS(publications)))

	return (
		<div flex='~ col items-stretch' gap-2>
			<For
				each={focusedOrList()}
				fallback={
					<Switch>
						<Match when={focussedPub()}>
							<sl-alert variant='danger' open>
								<div flex='~ items-center gap-4' text-xl>
									<div class='i-ph:warning-bold' />
									<strong>Publication not found:</strong>
								</div>
								<pre mb-0>{focussedPub()}</pre>
							</sl-alert>
						</Match>
						<Match when={!focussedPub()}>
							No publications (yet)
						</Match>
					</Switch>
				}
			>
				{(pub: IPublication, _nodeIdx) => {
					const isFocussed = createMemo(() => focussedPub() === pub.id)
					return <SinglePublicationPanel pub={pub} {...{ isFocussed, navigate }} />
				}}
			</For>
			<style>
				{`.card-overview::part(base){ height: 100%;}`}
			</style>
		</div>
	)
}
export const AddPublicationButton: Component<{
	navigate: ReturnType<typeof useNavigate> // HACK: context is broken by w3 keyring provider
}> = ({ navigate }) => {
	const addButtonProps = createAsyncButtonHandler(async () => {
		const pub = await addPublication()
		navigate(`/settings/publications/${pub.id}`)
	})

	return (
		<div>
			<sl-button variant='primary' {...addButtonProps()}>
				<div class='i-ph:plus-bold' slot='prefix'></div>
				Create publication
			</sl-button>
			{
				/* Publish all {/* db.count() * /}applogs {encryptFor() ? `for: ${encryptFor()}` : 'unencrypted'}
			<br /> to: <br />
			<ChoosePubPulldown getPub={pub} setPub={setPub} />
			{/* <EditPubPulldown /> * /}
			<br />to IPNS (hosted in your Web3.storage):
			{/* <code dir='rtl' max-w-full overflow-hidden text='3 right'>{currentPub() ? `${currentPub().id}` : 'No w3name set up'}</code> * /}
			<br />

			<div flex='~ justify-end'>
				Push All Applogs
				<sl-button mt-2 variant='primary' onclick={null}>
					{/* loading={uploading()} disabled={!getW3NamePublic()} * /}
					<div class='i-ph:upload-simple-bold' slot='prefix'></div>
					Push
				</sl-button>
			</div> */
			}
		</div>
	)
}
export const SinglePublicationPanel: Component<{
	pub: IPublication
	isFocussed?: Accessor<boolean> | undefined
	navigate: ReturnType<typeof useNavigate> // HACK: context is broken by w3 keyring provider
}> = (
	{ pub, isFocussed, navigate },
) => {
	const rawDS = useRawDS()
	const [uploadState, uploader] = useW3Uploader()
	const agent = useAgent()
	const [isExpanded, setIsExpanded] = createSignal(isFocussed?.())
	createEffect(() => {
		if (isFocussed?.()) setIsExpanded(true) // if it was in the list, but not focussed - it will not be recreated
	})
	const encryptedFor = createMemo(() => pub.sharedAgents?.join(',') ?? '')
	async function deletePub(id) {
		await agent.deletePub(id)
		if (isFocussed?.()) navigate(`/settings/publications`)
	}
	const pushButtonProps = createAsyncButtonHandler(async () => {
		DEBUG(`Pushing '${pub.name}'`, { pub, rawDS, uploader })
		await publishDatalog(rawDS, uploader, pub)
	})

	const rootBlockIDs = createMemo(() => {
		const selector = pub.selectors?.find(s => s.startsWith('blocks('))
		if (!selector) return []
		return selector.split(/[()]/, 3)[1].split(',')
	})
	function onRootIDsChanged(e: CustomEvent) {
		const newRoots = (e.target as SlSelect).value as string[] // HACK: event doesn't seem to have that info
		DEBUG(`RootIDs changed:`, e, newRoots)
		const prevSelectors = pub.selectors ?? []
		const prevSelector = prevSelectors.find(s => !!s.match(RE_SELECTOR_BLOCKS))
		const selectorIdx = prevSelector && prevSelectors.indexOf(prevSelector)
		let newSelector = newRoots.length
			? (prevSelector ? prevSelector.replace(RE_SELECTOR_BLOCKS, makeBlocksSelector(newRoots)) : makeBlocksSelector(newRoots))
			: null
		let newSelectors = [...prevSelectors]
		if (prevSelector) newSelectors.splice(selectorIdx)
		if (newSelector) newSelectors.push(newSelector)
		DEBUG(`[pub] updating selectors`, pub.id, prevSelectors, newSelectors, { prevSelector, newSelector, newRoots })
		agent.updatePub(pub.id, { selectors: newSelectors })
	}
	const isWithoutHistory = createMemo(() => {
		return !!pub.selectors?.find(s => s.match(RE_SELECTOR_WITHOUT_HISTORY))
	})
	const onToggleWithHistory = (e) => {
		const withoutHistory = e.target.checked
		const prevSelectors = pub.selectors ?? []
		let newSelectors: string[]
		if (!prevSelectors.length) newSelectors = withoutHistory ? ['withoutHistory'] : []
		else {
			newSelectors = prevSelectors.map(s => toggleWithoutHistorySelector(s, withoutHistory)).filter(s => !!s)
		}
		DEBUG(`[pub] Setting new selectors`, pub.id, newSelectors, { prevSelectors })
		return agent.updatePub(pub.id, { selectors: newSelectors })
	}
	const onToggleAutopush = (e) => agent.updatePub(pub.id, { autopush: e.target.checked })
	const isAutopush = createMemo(() => {
		return pub.autopush
	})

	const allRoots = useRoots()
	const pubData = computed(() => getPublicationData(rawDS, pub))
	const pubRoots = computed(() => pubData.get() && withDS(pubData.get(), () => useRoots()))
	const knownAgentMap = agent.getKnownAgents(rawDS)
	const setEncryptionTargets = (evt: CustomEvent) => {
		const { value: selectedAgentIDs } = evt.target as SlSelect
		const pubUpdate: Parameters<typeof updatePubEncryption>[0] = { id: pub.id }
		if (selectedAgentIDs?.length) {
			pubUpdate.sharedAgents = selectedAgentIDs as string[]
			pubUpdate.encryptedFor = null // TODO fully deprecate
			// pubUpdate.encryptedWith = null
			DEBUG('updating', { pub, pubUpdate })
			updatePubEncryption(pubUpdate)
		} else {
			pubUpdate.sharedAgents = null
			pubUpdate.encryptedFor = null
			// pubUpdate.encryptedWith = null
			updatePubEncryption(pubUpdate)
		}
		LOG('setEncryptionTargets', { evt, selectedAgentIDs, pubUpdate })
	}

	const encryptionSelectValue = () => pub?.sharedAgents ?? ''
	return (
		<div flex='~ col' rounded border='solid 1' border-gray-400>
			<div
				px-3
				py-2
				flex='~ wrap space-between items-center justify-between'
				gap-2
				bg-white-200
				cursor-pointer
				onclick={() => setIsExpanded(!isExpanded())}
			>
				<div onclick={stopPropagation}>
					<span text-lg font-bold mr-2>
						{boundInput('publicationsMap', [], { pubID: pub.id, padding: 'py-1 px-2' })}
					</span>
				</div>
				<div flex='~ items-center grow-1 justify-end' gap-2>
					{
						/* <span>
						({pub.encryptedFor ? `for ${pub.encryptedFor}` : 'unencrypted'}, last push:{' '}
						{pub.lastPush ? <sl-relative-time date={pub.lastPush} /> : 'never'})
					</span> */
					}
					<div flex='~ items-center' gap-2>
						<div flex='~ items-center' gap-2 onclick={stopPropagation}>
							<CopyableID id={pub.id} label='Copy IPNS ID' />
							<sl-copy-button
								m='l--2'
								value={makeNote3Url({ pub: pub.id, block: singleBlockOfPublication(pub) })}
								copy-label='Copy Note3 URL'
							>
								<Iconify name='link' size='5' slot='copy-icon' />
							</sl-copy-button>
							<sl-tooltip content='Open preview (new window)'>
								<a
									href={makeNote3Url({ pub: pub.id, previewPub: true })}
									target='_blank'
									color='white visited:white'
									flex='inline justify-center'
								>
									<Iconify class='ml--1' size='5' name='eye' />
								</a>
							</sl-tooltip>
							<sl-tooltip placement='left'>
								<Iconify class='mx-1' size='5' name={encryptedFor() ? 'lock-key-fill' : 'lock-simple-open-light'} />
								<div slot='content'>
									{encryptedFor() ? `for ${encryptedFor()}` : 'unencrypted'}
								</div>
							</sl-tooltip>
							<DeleteWithConfirm handler={() => deletePub(pub.id)} />
							<sl-tooltip placement='left' content={`last push:` + pub.lastPush ? <sl-relative-time date={pub.lastPush} /> : 'never'}>
								<sl-button
									variant='primary'
									size='small'
									{...pushButtonProps()}
									disabled={false /* HACK: as long as login is broken */ && !agent.hasStorageSetup}
								>
									<div class='i-ph:upload-simple-bold' w-4 h-4 slot='prefix' />
								</sl-button>
							</sl-tooltip>
						</div>
						<div
							class={'i-ph:caret-down-bold h-5 w-5 cursor-pointer ' + `${isExpanded() ? 'rotate-180' : 'rotate-0'}`}
							duration-300
							transition-property='[transform]'
						/>
					</div>
				</div>
			</div>
			<Collapse value={isExpanded()} class='transition-property-[height] duration-300'>
				<div px-3 text-sm>
					Created: <sl-relative-time date={pub.createdAt} />
					{/* <pre>{JSON.stringify(omit(pub, 'pk'), undefined, 4)}</pre> */}
				</div>
				<div flex='~ wrap justify-items-stretch' px-3 p-2 gap-2>
					<sl-card class='card-overview min-h-32 md:min-w-md' flex='grow-1'>
						<div slot='header' flex='~ justify-between'>
							<strong>Push</strong>
							<div text-sm>
								Autopush <sl-switch checked={isAutopush()} onsl-change={onToggleAutopush} size='small' mr--1 />
							</div>
						</div>
						<sl-select
							size='small'
							label={`Select agent to encrypt for: (Currently: ${encryptionSelectValue()})`}
							value={encryptionSelectValue()}
							defaultValue={encryptionSelectValue()}
							clearable
							multiple
							onsl-after-hide={setEncryptionTargets}
						>
							<For each={[...knownAgentMap.get().values()]}>
								{({ ag: eachAg, jwkd: eachJwkString }, _logIdx) => {
									const agString = getAgentString(eachAg)
									return (
										<sl-option value={`${eachAg}`} text-xs>
											{`[${eachAg} : ${agString}] : ...${(eachJwkString as string).slice(-8)}`}
										</sl-option>
									)
								}}
							</For>
						</sl-select>
					</sl-card>
					<sl-card class='card-overview min-h-32 min-w-sm flex-auto'>
						<div slot='header' flex='~ justify-between'>
							<strong>Selectors</strong>
							<div text-sm>
								Without history&ensp;<sl-switch checked={isWithoutHistory()} onsl-change={onToggleWithHistory} size='small' mr--1 />
							</div>
						</div>
						<div class='flexcol' gap-2>
							<sl-select
								class='min-w-xs'
								label='Blocks to publish, including children (select none to publish all data):'
								value={rootBlockIDs()}
								multiple
								clearable
								onsl-input={onRootIDsChanged}
							>
								<For each={allRoots}>
									{(rootID) => {
										const content = useBlockAt(rootID, 'content')
										return <sl-option value={rootID}>{content.get()}</sl-option>
									}}
								</For>
							</sl-select>
							<sl-input placeholder='Search for a block to "track"' size='medium' disabled>
								<div w-8 h-8 slot='prefix' class='i-ph:dot-duotone scale-115' />
								<div w-4 h-4 slot='suffix' class='i-ph:magnifying-glass' />
							</sl-input>
							<pre m-0>{JSON.stringify(pub.selectors)}</pre>
						</div>
					</sl-card>
					<sl-card class='card-overview min-h-32 md:min-w-20 flex-auto'>
						<Show when={pubData.get()} fallback={<div color-red>Publication contains no data</div>}>
							<div slot='header' flex='~ justify-between'>
								<span>
									Preview ({pubData.get().size} Applogs,&ensp;<sl-relative-time date={pubData.get().firstLog.ts} />
									➜<sl-relative-time date={pubData.get().latestLog.ts} />)
								</span>
								<Show when={!!pub.lastCID}>
									<a href={makeNote3Url({ pub: pub.id, previewPub: true })} target='_blank'>
										<sl-button size='small'>
											<div class='i-ph:eye-bold' slot='prefix' />
										</sl-button>
									</a>
								</Show>
							</div>
							<For each={pubRoots.get()}>
								{(rootID) => {
									const content = useBlockAt(rootID, 'content')
									return (
										<div>
											- {content.get() as string} (<KidCount block={rootID} /> kids)
										</div>
									)
								}}
							</For>
						</Show>
					</sl-card>
				</div>
			</Collapse>
		</div>
	)
}
