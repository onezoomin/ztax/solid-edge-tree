// import { useKeyring } from '@w3ui/solid-uploader'
import { Logger } from 'besonders-logger/src'
import { Component, Show } from 'solid-js'
import { useAgent } from '../../data/AgentStore'
import { AgentEditor } from '../AgentEditor'
import { MnemDialog } from '../MnemDialog'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export const AgentSettings: Component<{}> = () => {
	DEBUG('Creating <AgentSettings>') // HACK used to render twice and sideEffects should only happen once if possible
	const agent = useAgent() // ? are there reactivity reasons not to destructure here?

	let mnemDialogRef
	return (
		<div flex='~ col' gap-4>
			<sl-card class='card-header' style='--border-color: #ffffff77; --padding: var(--sl-spacing-medium)'>
				<div slot='header'>
					Your agent {`(${agent.ag})`}
				</div>

				<div mb-2>Set up your AppAgent string</div>
				<AgentEditor />

				<Show when={!agent.hasStorageSetup}>
					<sl-alert mt-4 variant='warning' open>
						<div flex='~ items-center gap-4'>
							<div class='i-ph:warning-bold' />
							<strong>No storage set up</strong>
						</div>
						<p mb-0>
							You need to <a href='#/settings/storage'>set up storage</a> to have a complete DID
						</p>
					</sl-alert>
				</Show>

				<div slot='footer' flex='~ justify-end md:justify-start' gap-2>
					<MnemDialog mnemDialogRef={mnemDialogRef} />
				</div>
			</sl-card>
		</div>
	)
}
