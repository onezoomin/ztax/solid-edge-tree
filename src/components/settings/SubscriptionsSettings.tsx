import { useNavigate } from '@solidjs/router'
import { Logger } from 'besonders-logger/src'
import { toJS } from 'mobx'
import { Collapse } from 'solid-collapse'
import { Accessor, Component, createEffect, createMemo, createSignal, For, Match, Show, Switch } from 'solid-js'
import { boundInput, getSubOrPub, useAgent } from '../../data/AgentStore'
import { getApplogDB } from '../../data/ApplogDB'
import { isoDateStrCompare } from '../../data/datalog/utils'
import { ISubscription } from '../../data/local-state'
import { dateNowIso } from '../../data/Utils'
import { retrieveDatalog } from '../../ipfs/store-sync'
import { upsertSubscription } from '../../ipfs/sub-pub'
import { parseW3Name } from '../../ipfs/web3storage'
import { useRawDS } from '../../ui/reactive'
import { createAsyncButtonHandler, makeNote3Url, searchParamsFromHash, stopPropagation, tryParseNote3URL } from '../../ui/ui-utils'
import { BackLink } from '../BackLink'
import { CidInput } from '../CidInput'
import { CopyableID, DeleteWithConfirm, Iconify, PubInfoBox } from '../mini-components'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

const examplePullHash = 'k51qzi5uqu5dkfh2f4gkcxoi2vlzacegh9oo3baalz203j09y1xa3c82cu9qv2'

export const SubscriptionsSettings: Component<{
	navigate: ReturnType<typeof useNavigate> // HACK: context is broken by w3 keyring provider
	activeSection?: Accessor<string>
	params?: Accessor<string[]>
}> = (
	{ navigate, activeSection, params },
) => {
	DEBUG('create <SubscriptionSettings>', { params })
	const agentState = useAgent()
	const focussedSub = createMemo(() => params()?.[0])

	return (
		<div flex='~ col' gap-4>
			<Switch>
				<Match when={!focussedSub()}>
					<SubscriptionAdder {...{ navigate }} />
				</Match>
				<Match when={focussedSub()}>
					<h4 m-0 flex='inline items-center gap-4'>
						<BackLink text='Back to list' target='/settings/subscriptions' />
					</h4>
				</Match>
			</Switch>
			<SubscriptionsList {...{ focussedSub, navigate }} />
		</div>
	)
}

export const SubscriptionsList: Component<{
	focussedSub?: Accessor<string>
	navigate: ReturnType<typeof useNavigate> // HACK: context is broken by w3 keyring provider
}> = ({ focussedSub, navigate }) => {
	DEBUG('create <SubscriptionsList>')
	const agent = useAgent()
	const { subscriptions } = agent

	const focusedOrList = createMemo(() => {
		let list = focussedSub() ? subscriptions.filter(p => p.id === focussedSub()) : subscriptions
		list = list.slice().sort((a, b) => isoDateStrCompare(a.createdAt, b.createdAt, 'desc')) // newest first
		DEBUG(`focusedOrList:`, list)
		return list
	})
	createEffect(() => DEBUG(`focussed:`, focussedSub()))
	createEffect(() => DEBUG(`subs:`, toJS(subscriptions)))

	return (
		<div flex='~ col items-stretch' gap-2>
			<For
				each={focusedOrList()}
				fallback={
					<Switch>
						<Match when={focussedSub()}>
							<sl-alert variant='danger' open>
								<div flex='~ items-center gap-4' text-xl>
									<div class='i-ph:warning-bold' />
									<strong>Subscription not found:</strong>
								</div>
								<pre mb-0>{focussedSub()}</pre>
							</sl-alert>
						</Match>
						<Match when={!focussedSub()}>
							No subscriptions (yet)
						</Match>
					</Switch>
				}
			>
				{(sub: ISubscription, _nodeIdx) => {
					const isFocussed = createMemo(() => focussedSub() === sub.id)
					return <SingleSubscriptionPanel sub={sub} {...{ isFocussed, navigate }} />
				}}
			</For>
			<style>
				{`.card-overview::part(base){ height: 100%;}`}
			</style>
		</div>
	)
}

export const SubscriptionAdder: Component<{
	navigate: ReturnType<typeof useNavigate> // HACK: context is broken by w3 keyring provider
}> = (
	{ navigate },
) => {
	DEBUG('create <SubscriptionAdder>')
	// const [urlSearchParams, _setSearchParams] = useSearchParams()
	// const { pub: pubToPull } = urlSearchParams
	const pubToPull = searchParamsFromHash()?.get('pub') // HACK: useSearchParams is broken because of keyring provider

	const [retrieveID, setRetrieveID] = createSignal(pubToPull || '')
	const [retrieving, setRetrieving] = createSignal(false)
	const [subExtras, setSubExtras] = createSignal({})

	const sanitizedID = createMemo(() => {
		const parsed = tryParseNote3URL(retrieveID())
		if (parsed && parsed.publication) {
			if (parsed.focus) setSubExtras({ selector: `blocks(${parsed.focus})` })
			return parsed.publication.toString()
			// ? overwrite retrieveID
		}
		return retrieveID()
	})
	const matchingSubOrPub = createMemo(() => sanitizedID() ? getSubOrPub(sanitizedID()) : null)
	const matchingIsPub = createMemo(() => !!matchingSubOrPub() && (matchingSubOrPub() as any).lastPush !== undefined)

	async function pullAndSave(sub: ISubscription | null, save: boolean = false, retries = 0) {
		// const [encryptedBy, derivationJWKstring] = (encryptAgentSelectRefPull?.value ?? '').split('_|_') ?? []
		setRetrieving(true)
		try {
			try {
				const ipns = parseW3Name(sub ? sub.id : (sanitizedID() || examplePullHash))
				DEBUG('retrieving:', { ipns, sub /* encryptedBy, derivationJWKstring,  */ })
				if (!sub) {
					if (matchingIsPub()) throw ERROR(`trying to retrieve from own pub`, sub)
					sub = matchingSubOrPub() as (ISubscription | null)
				}
				// const sub = sub as ISubscription|null

				const { cid, encryptedFor } = await retrieveDatalog(getApplogDB(), ipns, sub)
				const updatedFields = { lastCID: cid, lastPull: dateNowIso(), encryptedFor, ...subExtras }
				DEBUG('retrieved:', cid, 'save?', !sub && save, updatedFields)
				if (!sub && save) {
					const sub = await upsertSubscription(ipns, updatedFields)
					setRetrieveID('')
					navigate('/settings/subscriptions/' + sub.id)
				}
			} catch (err) {
				// throw new Error(`failed to retrieve ${sanitizedID()}`, { cause: err })
				ERROR(`failed to retrieve ${sanitizedID()}`, err)
				if (retries++ < 3) await pullAndSave(sub, save, retries)
			}
		} finally {
			setRetrieving(false)
		}
	}

	return (
		<div flex='~ col'>
			<sl-card class='card-header' w-full style='--border-color: #ffffff77; --padding: var(--sl-spacing-medium)'>
				<div slot='header'>
					Add subscription
				</div>
				<div flex='~ col' gap-4>
					<Switch>
						<Match when={!retrieving()}>
							<Show when={pubToPull && sanitizedID() === pubToPull}>
								<p m-0 text-sm>Publication ID from URL:</p>
							</Show>
							<CidInput
								w-full
								placeholder='IPNS (k51q...),  #TODO: IPFS (bafy...), git@..., etc.'
								value={retrieveID}
								onInput={e => setRetrieveID((e.target as HTMLInputElement).value)}
							/>

							<Show when={sanitizedID()}>
								<div mt--2>
									<PubInfoBox pubID={sanitizedID()} />
								</div>
								<Show
									when={matchingSubOrPub()}
								>
									<div>
										This is an&ensp;
										<a href={`#/settings/${matchingIsPub() ? 'publications' : 'subscriptions'}/${matchingSubOrPub().id}`}>
											existing {matchingIsPub() ? 'publication' : 'subscription'}
										</a>
									</div>
								</Show>

								<div flex='~ justify-end' gap-2>
									<a href={sanitizedID() ? makeNote3Url({ pub: sanitizedID(), previewPub: true }) : undefined} target='_blank'>
										<sl-button size='small' variant='primary' disabled={!sanitizedID()}>
											<div class='i-ph:eye-bold' slot='prefix'></div>
											Preview
										</sl-button>
									</a>
									{/* <sl-button-group label='Pull buttons'> */}
									<sl-button
										size='small'
										variant='primary'
										onclick={() => pullAndSave(null, true)}
										loading={retrieving()}
										disabled={!sanitizedID() || matchingSubOrPub()}
									>
										<div class='i-ph:download-simple-bold' slot='prefix'></div>
										Pull & Save
									</sl-button>
									{
										/* <sl-dropdown placement='bottom-end'>
										<sl-button slot='trigger' variant='primary' caret>
											<sl-visually-hidden>More pull options</sl-visually-hidden>
										</sl-button>
										<sl-menu>
											<sl-menu-item onclick={() => retrieve(null, false)}>Pull only</sl-menu-item>
											<sl-menu-item disabled>Preview (TODO)</sl-menu-item>
										</sl-menu>
									</sl-dropdown>
								</sl-button-group> */
									}
								</div>
							</Show>
						</Match>
						<Match when={retrieving()}>
							Retrieving
							<code text-xs text-ellipsis>{sanitizedID()}</code>
						</Match>
					</Switch>
				</div>
			</sl-card>
		</div>
	)
}

export const SingleSubscriptionPanel: Component<{
	sub: ISubscription
	isFocussed?: Accessor<boolean> | undefined
	navigate: ReturnType<typeof useNavigate> // HACK: context is broken by w3 keyring provider
}> = (props) => {
	const { isFocussed, navigate, sub } = props
	const rawDS = useRawDS()
	const agent = useAgent()

	const [isExpanded, setIsExpanded] = createSignal(isFocussed?.())
	createEffect(() => {
		if (isFocussed?.()) setIsExpanded(true) // if it was in the list, but not focussed - it will not be recreated
	})

	async function deleteSub(id) {
		await agent.deleteSub(id)
		if (isFocussed?.()) navigate(`/settings/subscriptions`)
	}
	const pullButtonProps = createAsyncButtonHandler(async () => {
		DEBUG(`Pulling`, props.sub.name, { sub: props.sub, rawDS })
		await retrieveDatalog(getApplogDB(), parseW3Name(props.sub.id), props.sub)
	})

	const onToggleAutopull = (e) => agent.updateSub(props.sub.id, { autopull: e.target.checked })
	const isAutopull = createMemo(() => {
		return props.sub.autopull
	})

	// TODO: sub data?
	// const subData = /* computed(() =>  */ getPublicationData(rawDS, sub) /* ) */
	// const subRoots = withDS(subData, () => useRoots())
	// const knownAgentPKlogs = rollingFilter(rawDS, { at: 'agent/jwkd' })

	return (
		<div flex='~ col' rounded border='solid 1' border-gray-400>
			<div
				px-3
				py-2
				flex='~ wrap space-between items-center justify-between'
				gap-2
				bg-white-200
				cursor-pointer
				onclick={() => setIsExpanded(!isExpanded())}
			>
				<div onclick={stopPropagation}>
					<span text-lg font-bold mr-2>
						{boundInput('subscriptionsMap', [], { subID: props.sub.id, padding: 'py-1 px-2' })}
					</span>
				</div>
				<div flex='~ items-center grow-1 justify-end' gap-2>
					<CopyableID id={props.sub.id} label='Copy IPNS ID' />
					<sl-copy-button
						onclick={stopPropagation}
						m='l--2'
						value={makeNote3Url({ pub: props.sub.id /* block: singleBlockOfSubscription(sub) */ })}
						copy-label='Copy Note3 URL'
					>
						<Iconify name='link' size='5' slot='copy-icon' />
					</sl-copy-button>

					<sl-tooltip onclick={stopPropagation} content='Open preview (new window)'>
						<a
							href={makeNote3Url({ pub: props.sub.id, previewPub: true })}
							target='_blank'
							color='white visited:white'
							flex='inline justify-center'
						>
							<Iconify class='ml--1' size='5' name='eye' />
						</a>
					</sl-tooltip>
					<sl-tooltip placement='left'>
						<Iconify class='mx-1' size='5' name={props.sub.encryptedFor ? 'lock-key-fill' : 'lock-simple-open-light'} />
						<div slot='content'>
							{props.sub.encryptedFor ? `for ${props.sub.encryptedFor}` : 'unencrypted'}
						</div>
					</sl-tooltip>

					<div flex='~ items-center' gap-2>
						<DeleteWithConfirm handler={() => deleteSub(props.sub.id)} />
						<sl-tooltip placement='left' content={`last push:` + sub.lastPull ? <sl-relative-time date={sub.lastPull} /> : 'never'}>
							<sl-button variant='primary' size='small' {...pullButtonProps()}>
								<div class='i-ph:download-simple-bold' w-4 h-4 slot='prefix' />
							</sl-button>
						</sl-tooltip>
						<div
							class={'i-ph:caret-down-bold h-5 w-5 cursor-pointer ' + `${isExpanded() ? 'rotate-180' : 'rotate-0'}`}
							duration-300
							transition-property='[transform]'
							onclick={() => setIsExpanded(!isExpanded())}
						/>
					</div>
				</div>
			</div>
			<Collapse value={isExpanded()} class='transition-property-[height] duration-300'>
				<div px-3 text-sm>
					Created: <sl-relative-time date={props.sub.createdAt} />
				</div>
				<div flex='~ wrap justify-items-stretch' px-3 p-2 gap-2>
					<sl-card class='card-overview min-h-32'>
						<div slot='header' flex='~ justify-between'>
							<strong>Pull settings</strong>
						</div>
						<div text-sm>
							Autopull&ensp;<sl-switch checked={isAutopull()} onsl-change={onToggleAutopull} size='small' mr--1 />
						</div>
					</sl-card>

					<Show when={isExpanded()}>
						<PubInfoBox label='Data of last pull' pubID={props.sub.id} pinCID={props.sub.lastCID} />
					</Show>
					{
						/* <sl-card class='card-overview min-h-32 md:min-w-20 flex-auto'>
						<div slot='header' flex=''>
							Preview ({subData.size} Applogs,&ensp;<sl-relative-time date={subData.firstLog.ts} />
							➜<sl-relative-time date={subData.latestLog.ts} />)
							<Show when={!!sub.lastCID}>
								<a href={makeNote3Url({ pub: sub.id, previewPub: true })} target='_blank'>
									<sl-button size='small'>
										<div class='i-ph:eye-bold' slot='prefix' />
									</sl-button>
								</a>
							</Show>
						</div>
						<For each={subRoots}>
							{(rootID) => {
								const content = useBlockAt(rootID, 'content')
								return (
									<div>
										- {content.get() as string} (<KidCount block={rootID} /> kids)
									</div>
								)
							}}
						</For>
					</sl-card> */
					}
				</div>
			</Collapse>
		</div>
	)
}
