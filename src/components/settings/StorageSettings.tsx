// import { useKeyring } from '@w3ui/solid-uploader'
import { Logger } from 'besonders-logger/src'
import { Component, createSignal, Match, Show, Switch } from 'solid-js'
import { useW3Keyring } from '../../ipfs/w3ui'
import { createHandleAuthFx } from '../../ipfs/web3storage'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export const StorageSettings: Component<{}> = () => {
	DEBUG('Creating <StorageSettings>') // HACK used to render twice and sideEffects should only happen once if possible
	const [keyring, w3] = useW3Keyring() /* Example from: https://beta.ui.web3.storage/keyring */

	const [email, setEmail] = createSignal('')
	const [submitted, setSubmitted] = createSignal(false)
	const [registeringSpace, setRegisteringSpace] = createSignal(false)
	const [keyringSpaceBuster, setKeyringBuster] = createSignal({})

	const unloadAgent = async () => {
		w3.unloadAgent() // FIXME: broken & needs reload
		localStorage.removeItem('w3ui-account-email') // HACK
	}
	const tryRegisterSpace = async () => {
		if (keyring.account && !keyring.space?.registered()) {
			const keyringBuster = { did: keyring.space?.did(), registered: keyring.space?.registered() }
			setKeyringBuster(keyringBuster) // ? huh ?
			DEBUG('Space is unregistered, registering', keyringSpaceBuster(), keyring.space, { keyring })
			await w3.registerSpace(keyring.account, { /* space: keyring.space, */ provider: 'did:web:web3.storage' })
			DEBUG('Done!', keyring.space)
		}
	}
	const handleAuthorizeSubmit = createHandleAuthFx({ w3, email, setRegisteringSpace, tryRegisterSpace, setSubmitted, keyring })

	return (
		<div flex='~ col' gap-4>
			{/* <span pt='4' border='b-1 b-solid gray-400' mb='4' /> */}
			<sl-card class='card-header' style='--border-color: #ffffff77; --padding: var(--sl-spacing-medium)'>
				<div slot='header'>
					Your storage
					{/* <sl-icon-button name="gear" label="Settings"></sl-icon-button> */}
				</div>

				<Switch>
					<Match when={!keyring.account && !submitted()}>
						<form flex='~ col' gap-4 onSubmit={handleAuthorizeSubmit}>
							<p my-0>
								Log in / Sign up with <a href='https://web3.storage'>web3.storage</a>{' '}
								as one way to store your data. (and currently the only way, but that will change)
							</p>
							<sl-input type='email' placeholder='Email' required filled value={email()} onInput={e => setEmail(e.target.value)} max-w-md />
							<div slot='footer' flex='~ justify-end md:justify-start' gap-2>
								<sl-button
									size='small'
									type='submit'
									disabled={!email() || submitted()}
									variant='primary'
								>
									Log in / Sign up
								</sl-button>
							</div>
						</form>
					</Match>
					<Match when={submitted() && !registeringSpace()}>
						{/* <h3>Verify your email address!</h3> */}
						<p>Click the link in the email we sent to {keyring.account} to sign in.</p>
						<p>This page should update automatically a few seconds after you've confirmed.</p>
						<div slot='footer'>
							<sl-button
								size='small'
								variant='warning'
								onclick={w3.cancelAuthorize}
							>
								Cancel
							</sl-button>
						</div>
					</Match>
					<Match when={submitted() && registeringSpace()}>
						<p>Trying to register a space, you might have to confirm in another email because of a bug.</p>
						<p>This page should update automatically a few seconds after you've confirmed.</p>

						<div slot='footer' flex='~ justify-end' gap-2>
							<sl-button
								size='small'
								variant='warning'
								onclick={w3.cancelAuthorize}
							>
								Cancel
							</sl-button>
						</div>
					</Match>
					<Match when={keyring.account}>
						{/* TODO setup a collapse tree here */}
						<p mt-2 mb-6>web3storage: {keyring.account}!</p>
						<p mt-2 mb-6>
							Your space is<br />
							<code text-xs>
								{keyringSpaceBuster() && keyring.space
									? keyring.space.did() + (keyring.space.registered() ? ' (registered)' : ' (unregistered)')
									: 'missing!' + `(${keyring.spaces.length} available)`}
							</code>
							<br />
							<Show when={!keyring.space}>(note that this display is sometimes broken even though Pushing works... 🤷)</Show>
						</p>
						{/* FIXME WTF, no reactivity? */}
						<pre hidden>{JSON.stringify(keyringSpaceBuster(),undefined,2)}</pre>
						<div slot='footer' flex='~ justify-end' gap-2>
							<a href='https://web3.storage/account/' target='_blank'>
								<sl-button size='small'>
									{/* <sl-icon slot='suffix' name='box-arrow-up-right'></sl-icon> */}
									<div slot='suffix' class='i-ph:arrow-square-out'></div>
									web3.storage account
								</sl-button>
							</a>
							<Show when={keyring.space && !keyring.space.registered()}>
								<sl-button
									size='small'
									variant='warning'
									onclick={tryRegisterSpace}
									mb-2
								>
									TryFix
								</sl-button>
							</Show>
							<sl-button
								size='small'
								variant='danger'
								onclick={unloadAgent}
							>
								Log out
							</sl-button>
						</div>
					</Match>
				</Switch>
			</sl-card>
		</div>
	)
}
