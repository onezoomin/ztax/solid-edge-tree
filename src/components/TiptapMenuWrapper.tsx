import { BubbleMenuPlugin, BubbleMenuPluginProps } from '@tiptap/extension-bubble-menu'
// import { nanoid } from 'nanoid'
import { Component, JSX, onMount } from 'solid-js'

type TiptapMenuWrapperProps =
	& Omit<
		BubbleMenuPluginProps,
		'element' | 'pluginKey' | 'shouldShow'
	>
	& {
		class?: string
		children?: JSX.Element
		shouldShow?: BubbleMenuPluginProps['shouldShow']
	}

const TiptapMenuWrapper: Component<TiptapMenuWrapperProps> = (props) => {
	let ref
	const pluginKey = 'note3-menu' // ? nanoid()

	onMount(() => {
		const { editor, shouldShow, tippyOptions } = props

		// if (ref) {
		editor.registerPlugin(
			BubbleMenuPlugin({
				editor,
				pluginKey,
				shouldShow, /* : (props) => {
					if (shouldShow) {
						return shouldShow(props)
					}

					return false
				} */
				element: ref,
				tippyOptions,
			}),
		)
		// }
	})

	return (
		<div
			ref={ref}
			class={props.class}
			style={{ visibility: 'hidden' }}
		>
			{props.children}
		</div>
	)
}

export { TiptapMenuWrapper }
export type { TiptapMenuWrapperProps }
