import { Logger } from 'besonders-logger/src'
import { Component, createEffect, createSignal } from 'solid-js'
import { useAgent } from '../data/AgentStore'
import { createMnemonic, encHashedMnemonic, getECDHkeypairFromHashArray, testStats } from '../data/mnemonic'
import { buf2hex } from '../data/Utils'
import { notifyToast } from '../ui/ui-utils'
import { Iconify } from './mini-components'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

const inputStyle = {
	'--sl-input-font-size-small': '11px',
}

export const MnemDialog: Component<{
	mnemDialogRef: any
}> = function MnemDialog({ mnemDialogRef }) {
	const [mnemToTest, setMnem] = createSignal(
		'basket,chief,toad,chemical,little,analysis,admonish,cupcake,diatribe,obedient,craving,nightcap,hopeful,street,raggedy,stymie,mechanic,papyrus,sorcerer,plushy,rickshaw,taffy,nocturne,unicycle',
	)
	const [mnemArray, setMnemArr] = createSignal([])
	createEffect(() => {
		const cleanedMnemString = mnemToTest().replaceAll(' ', ',').replaceAll(',,,', ',').replaceAll(',,', ',')
		setMnemArr(cleanedMnemString.split(','))
		VERBOSE('cleaned array', mnemArray())
	})
	const onInput = (evt: InputEvent) => {
		setMnem((evt.target as HTMLInputElement).value)
	}
	const comparePubDerivationKey = async (hexPubKey, newMnemArr?: string[]) => {
		const { publicKey } = await useAgent().getDerivationKeypair(newMnemArr)
		const exportedPubKeyHex = buf2hex(await window.crypto.subtle.exportKey('raw', publicKey))
		const isMatching = !!(hexPubKey === exportedPubKeyHex)
		DEBUG({ isMatching, hexPubKey, exportedPubKeyHex })
		return isMatching
	}
	const importDerivationKey = async (newMnemArr?: string[]) => {
		const { publicKey } = await useAgent().getDerivationKeypair(newMnemArr)
		const exportedPubKeyHex = buf2hex(await window.crypto.subtle.exportKey('raw', publicKey))
		DEBUG({ exportedPubKeyHex })
	}
	return (
		<>
			<sl-button
				max-w-fit
				variant='primary'
				onclick={() => mnemDialogRef?.show()}
				size='small'
			>
				<div flex='~ items-center' gap-1>
					<div slot='prefix' w-4 h-4 class='i-ph:gear' />
					MnemKey
				</div>
			</sl-button>
			<sl-dialog ref={mnemDialogRef}>
				<p>Enter your mnemonic for testing:</p>
				<sl-input
					placeholder='list of words like foo,bar,baz (separated by single commas or single spaces)'
					value={mnemToTest()}
					onInput={onInput}
					filled
					font-mono
					size='small'
					max-w-full
					min-w-fit
					mb-4
					spellcheck={false}
					style={inputStyle}
				/>
				<div>
					<sl-button
						max-w-fit
						variant='primary'
						onclick={async () => {
							const { publicKey } = await getECDHkeypairFromHashArray(encHashedMnemonic(mnemArray()))
							LOG({ publicKey }, mnemArray())
							const isMatching = await comparePubDerivationKey(buf2hex(await window.crypto.subtle.exportKey('raw', publicKey)))
							if (isMatching) {
								notifyToast(`Looking good, your public key in IDB matches the one derived from the mnemonic you entered`, 'success', 10000)
							} else {
								notifyToast(`UhOh, your pubkey DOES NOT MATCH the one derived from the mnemonic`, 'danger', 10000)
							}
						}}
						size='small'
					>
						<div flex='~ items-center' gap-1>
							<div slot='prefix' w-4 h-4 class='i-ph:check' />
							Check
						</div>
					</sl-button>
					<sl-button
						max-w-fit
						ml-4
						variant='primary'
						onclick={async () => {
							setMnem(createMnemonic(24).join(','))
							notifyToast('This is not saved anywhere (yet) just for testing')
							testStats(256, 10)
						}}
						size='small'
					>
						<div flex='~ items-center' gap-1>
							<div slot='prefix' w-4 h-4 class='i-ph:plus-circle' />
							New
						</div>
					</sl-button>
					<sl-button
						max-w-fit
						ml-4
						variant='primary'
						onclick={async () => {
							importDerivationKey(mnemArray())
							notifyToast('This is not saved anywhere (yet) just for testing')
							testStats(256, 10)
						}}
						size='small'
					>
						<div flex='~ items-center' gap-1>
							<Iconify slot='prefix' name='shield-plus' />
							Import
						</div>
					</sl-button>
				</div>
			</sl-dialog>
		</>
	)
}
