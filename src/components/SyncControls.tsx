// // import { useKeyring } from '@w3ui/solid-uploader'
// import { useSearchParams } from '@solidjs/router'
import { Logger } from 'besonders-logger/src'
import { Component, Show } from 'solid-js'
import { useAgent } from '../data/AgentStore'
import { syncState, triggerSync } from '../ipfs/sync-service'
// import { updateAgentState } from '../data/AgentStore'
// import { createHandleAuthFx } from '../ipfs/web3storage'
// import { SetupDialog } from './SetupDialog'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG, { prefix: '[w3]' }) // eslint-disable-line no-unused-vars

export const SyncButton: Component<{}> = (props) => {
	const agentState = useAgent()
	// const [isFetching] = createDeferredResource(async () => {
	// 	DEBUG(`SyncButton.syncing`, props)
	// 	// if (!props.pubID) return null
	// 	// if (matchingSubOrPub()) return null
	// 	// return await retrievePubDataWithExtras(baseDS, props.pubID, props.pinCID)
	// 	await sleep(777)
	// 	return 1
	// })

	return (
		<sl-tooltip
			// hoist
			content={
				<div>
					<div>Interval: {syncState.syncInterval}s</div>
					<Show when={syncState.errors.length}>
						<div text-red whitespace-pre>
							<br />
							<strong>Errors:</strong>
							<br />
							{syncState.errors.map(e => '- ' + e.message).join('\n\n')}
						</div>
					</Show>
				</div>
			}
			placement='bottom-end'
		>
			<sl-button
				variant='text'
				onClick={() => triggerSync()}
				onAuxClick={() => {
					if (syncState.syncInterval > 0) syncState.syncInterval = 0
					else syncState.syncInterval = 300
				}}
			>
				<div
					class={`${
						syncState.errors.length
							? 'text-red'
							: (syncState.syncing
								? 'text-blue animate-[spin_2s_linear_infinite]'
								: (syncState.syncInterval > 0 ? 'text-white' : 'text-gray-600'))
					} i-ph:arrows-down-up-fill`}
					w-6
					h-6
				/>
			</sl-button>
		</sl-tooltip>
	)
}
