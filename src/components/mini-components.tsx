import { Logger } from 'besonders-logger/src'
import classNames from 'classnames'
import { Component, createEffect, createMemo, createResource, JSX, ParentComponent, Resource, Show, splitProps, Suspense } from 'solid-js'
import { iconNames } from '../../unocss.safelist'
import { useAgent } from '../data/AgentStore'
import { Applog } from '../data/datalog/datom-types'
import { BlockVM } from '../data/VMs/BlockVM'
import { retrievePubDataWithExtras } from '../ipfs/store-sync'
import { PromiseType } from '../types/util-types'
import { useRawDS } from '../ui/reactive'
import {
	createAsyncButtonHandler,
	explorerUrl,
	isColorDark,
	makeGlobalHover,
	mergeStyles,
	stopPropagation,
	stringToColor,
} from '../ui/ui-utils'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export const ShortID: Component<{ id: string }> = ({ id }) => {
	return (
		<span flex='inline items-center'>
			{/* max-w-5 truncate */}
			<code title={id}>{id.slice(-7)}</code>
		</span>
	)
}

export const CopyableID: Component<{ id: string; label?: string }> = ({ id, label }) => {
	return (
		<div flex='~ items-center' onclick={stopPropagation}>
			<code p='t-[2px] r-1'>{id.slice(-7)}</code>
			<sl-copy-button
				value={id}
				copy-label={label ?? undefined}
			>
				<Iconify name='copy-light' size='5' slot='copy-icon' />
			</sl-copy-button>
		</div>
	)
}
export const Spinner: Component<{ size?: string; color?: string }> = (props) => {
	return (
		<div>
			<sl-spinner
				style={`${props.size ? `font-size: ${props.size}; ` : ''}${
					props.color ? `--indicator-color: ${props.color}; --track-color: #000;` : ''
				}`}
			>
			</sl-spinner>
		</div>
	)
}
export const DynamicColored: ParentComponent<
	{
		text: string
	} & JSX.HTMLAttributes<HTMLSpanElement>
> = (props) => {
	const [ourProps, otherProps] = splitProps(props, ['text', 'children'])
	const color = createMemo(() => stringToColor(ourProps.text))

	return (
		<span
			{...otherProps}
			style={mergeStyles(otherProps.style, {
				color: isColorDark(color()) ? 'white' : 'black',
				'background-color': color(),
			})}
		>
			{ourProps.children || ourProps.text}
		</span>
	)
}

export const [/* _idHover */, useIdHover] = makeGlobalHover((active: boolean, myID: boolean) => ({
	'data-test': 123,
	class: classNames(
		'transition-property-[opacity] transition-duration-300',
		(active || myID) ? 'opacity-100' : 'opacity-40',
		myID && 'underline font-bold',
	),
}))
// TODO: useChangeHighlight (?)
export const IdHover: ParentComponent<
	{
		id: string
	} & JSX.HTMLAttributes<HTMLSpanElement>
> = (props) => {
	const [ourProps, otherProps] = splitProps(props, ['id', 'children'])
	return (
		<span
			{...(useIdHover('?', ourProps.id))}
			{...otherProps}
		>
			{ourProps.children}
		</span>
	)
}

export const Iconify: Component<{
	name: typeof iconNames[number]
	prefix?: string
	size?: number | string
	scale?: number | string
	class?: string
	style?: JSX.CSSProperties
	[x: string]: any
}> = (fullProps) => {
	const [props, otherProps] = splitProps(fullProps, ['prefix', 'class', 'size', 'scale', 'name', 'style'])

	const sizes = createMemo(() =>
		props.size && (
			(typeof props.size === 'string' && props.size.includes(' '))
				? props.size.split(' ')
				: [props.size, props.size]
		)
	)

	return (
		<div
			class={classNames(
				props.class,
				(props.prefix ? `${props.prefix}:` : 'i-ph:') + props.name,
				sizes() && `w-${sizes()[0]} h-${sizes()[1]}`,
			)}
			style={props.scale
				? { ...(props.style ?? {}), '-webkit-mask-size': `${props.scale}%`, '-webkit-mask-position': 'center' }
				: props.style}
			{...otherProps}
		>
		</div>
	)
}
export const KidCount: Component<{ block: string }> = (props) => {
	const rawDS = useRawDS()
	const [kidCount] = createDeferredResource(async () => {
		// if (Math.random() > 0.5) throw new Error(`TEST err`)
		for (let index = 0; index < 1000000; index++) {
			Math.random()
			// DEBUG(`T`)
		}
		return BlockVM.get(props.block, rawDS).recursiveKidCount
	})
	return (
		<ResourceSpinner resource={kidCount}>
			<span>{kidCount()}</span>
		</ResourceSpinner>
	)
}

export const ResourceSpinner: ParentComponent<{ resource: Resource<any> }> = (props) => {
	createEffect(() => props.resource.error && ERROR(`ResourceSpinner error:`, props.resource.error)) // otherwise, we just render but don't log
	return (
		<Suspense
			fallback={<Spinner />}
		>
			<Show
				when={!props.resource.error}
				fallback={
					<span color-yellow>
						<div class='i-ph:warning-bold' />
						{props.resource.error.message}
					</span>
				}
			>
				{props.children}
			</Show>
		</Suspense>
	)
}

export function createDeferredResource<R>(fetcher: () => Promise<R>) {
	return createResource(async () => {
		await new Promise<void>(resolve => queueMicrotask(resolve)) // i.e. defer
		// await new Promise<void>(resolve => setTimeout(resolve, 1000)) // for testing
		return fetcher()
	})
}

export const DeleteWithConfirm: Component<{ slot?: string; handler: () => Promise<void> | void }> = ({ slot, handler }) => {
	const deleteButtonProps = createAsyncButtonHandler(handler)
	return (
		<sl-dropdown onClick={stopPropagation} placement='bottom-start' slot={slot}>
			<sl-button slot='trigger' size='small' variant='danger'>
				<div w-4 h-4 class='i-ph:trash-duotone' slot='prefix'></div>
			</sl-button>
			<div class='darkdropdown p-4 outline-rounded-white' flex='~ col gap-4'>
				<div>You sure bout' dat?</div>
				<sl-button class='' slot='trigger' size='small' variant='danger' {...deleteButtonProps()}>
					<div w-4 h-4 class='i-ph:trash-duotone' />
					Yes, Delete
				</sl-button>
			</div>
		</sl-dropdown>
	)
}

export const PubInfoBox: Component<{ pubID: string; pinCID?: string; label?: string }> = (props) => {
	const baseDS = useRawDS()

	const [fetchedPub] = createDeferredResource(async () => {
		DEBUG(`PubInfoBox.fetch`, props)
		if (!props.pubID) return null
		// if (matchingSubOrPub()) return null
		// throw new Error(`TEST pubinfo err`)
		return await retrievePubDataWithExtras(baseDS, props.pubID, props.pinCID)
	})

	return (
		<ResourceSpinner resource={fetchedPub}>
			<Show when={fetchedPub()}>
				<div p-2 border='1 solid lightblue-700' rounded gap-2>
					<PubInfos info={fetchedPub} label={props.label} />
				</div>
			</Show>
		</ResourceSpinner>
	)
}

export const PubInfos: Component<{
	info: Resource<PromiseType<ReturnType<typeof retrievePubDataWithExtras>>>
	label?: string
}> = (props) => {
	const { info } = props
	const agent = useAgent()
	const encryptedApplogCount = info().maybeEncryptedApplogs.filter(log => !log?.ag).length // encrypted logs are byte arrays, so they have no .ag prop // HACKish
	// const encryptedFor= filterAndMap(withoutHistory(info().dataStream), { en: publicationNameString, at: 'pub/encryptedFor' }, 'vl') // no need for a filter just doing a find below instead
	let encryptedFor = info().dataStream.applogs.find(log => log.at === 'pub/encryptedFor')?.vl as string ?? ''
	const sharedKeyLog: Applog = agent.getSharedKeyApplogForCurrentAgent(info().dataStream.applogs)
	if (sharedKeyLog) encryptedFor = sharedKeyLog.en
	// TODO check agents for if we know their agent string. have any atoms from them, and have a public derivation key from them
	return (
		<Show when={info()}>
			<div pt-1 flex='~ items-center gap-2' text-xs>
				{props.label ? `${props.label}: ` : ''}
				{info().dataStream.size} Applogs (of which {info().newLogs} are not in the local data) <br />
				<Show when={encryptedApplogCount}>
					(encrypted logs for: {encryptedFor === agent.ag ? ' you - ' : ' NOT you - '}
					{encryptedFor && `${encryptedFor || '?unknown?'}:${agent.getKnownAgents().get()?.get(encryptedFor)?.agString}`})
				</Show>
			</div>
			<div pt-1 flex='~ items-center gap-2' text-xs>
				<strong>
					Agents:
				</strong>
				{info().agents.join(', ') /* TODO sharedKeyLogs  */}
			</div>
			<div pt-1 flex='~ items-center gap-2' text-xs>
				<strong>
					Timerange:
				</strong>
				<Show when={info().dataStream.size} fallback='Empty Stream'>
					<sl-relative-time date={info().dataStream.firstLog?.ts} />
					➜<sl-relative-time date={info().dataStream.latestLog?.ts} />
				</Show>
			</div>
			<div pt-1 flex='~ items-center gap-2' text-xs>
				<strong>
					CID:
				</strong>
				<a href={explorerUrl(info().cid)} target='_blank' overflow-hidden text-ellipsis>
					<code>{info().cid}</code>
				</a>
			</div>
			<div pt-1 flex='~ items-center gap-2' text-xs class={info().entityOverlapCount.get() ? 'color-yellow' : ''}>
				Related to <strong>{info().entityOverlapCount.get()} entities</strong> in your local data
			</div>
		</Show>
	)
}
