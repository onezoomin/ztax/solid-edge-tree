import { Component, createContext, createEffect, JSX, ParentComponent, Show, Switch } from 'solid-js'

import { Logger } from 'besonders-logger/src'
import classNames from 'classnames'
import { Collapse } from 'solid-collapse'
import { createSignal, Match, Suspense } from 'solid-js'
import Sortable, { SortableEvent } from 'solid-sortablejs'
import { BlockPanel, BlockPanelDef, useDivergencePanel } from '../data/block-panels'
import { handleBlockDrag, useBlockContext } from '../data/block-ui-helpers'
import { REL_DEF } from '../data/data-types'
import type { EntityID } from '../data/datalog/datom-types'
import { RelationModelDef } from '../data/Relations'
import { useCurrentDS, useEntityAt, useKidRelations } from '../ui/reactive'
import { BlockItem } from './BlockContent'
import { Spinner } from './mini-components'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

const MAX_DEPTH = 12 // TODO: properly handle max depth UX

const indentGuideStyle = {
	'--indent-guide-width': '2px',
	'--indent-guide-color': '',
}

export interface BlockContextType {
	// ! context values are not reactive per se - use signals/resources/obvervables if reactivity is needed
	id: EntityID
	depth: number
	relationToParent: EntityID
	parentContext: BlockContextType | null
	setPanel: (panel: BlockPanelDef) => void
}
export const BlockContext = createContext<BlockContextType>(null)

// Root-level blocks need to be sortables to be able to be dragged, but cannot be drag targets, so we disable them while something is being dragged
const [draggingBlock, setDraggingBlock] = createSignal(null)

export const BlockTree: Component<{ root: /* Accessor< */ EntityID /* | null> */ }> = ({ root: rootID }) => {
	DEBUG('[BlockTree] created', rootID)
	const handleSort = (newRels: RelationModelDef[]) => WARN(`TODO: handleSort in BlockTree`, newRels)
	return (
		<Suspense fallback={<Spinner />}>
			{/* FIXME: BlockTree render is sync/blocking */}
			<SortableBlocks
				class='border-1 border-solid border-gray-500 rounded py-2 pr-1'
				blockID={rootID}
				kidRelations={null}
				disabled={draggingBlock() && draggingBlock() !== rootID /* so we can drop it back in its' home, but not on next to other roots */}
			>
				{({ block }) => <TreeItem blockID={block} />}
			</SortableBlocks>
		</Suspense>
	)
}

const TreeItem: ParentComponent<{
	blockID: string
	relation?: RelationModelDef | null
	parentRelation?: RelationModelDef | null
	// className?: string
}> = (
	{ blockID, relation = null, parentRelation = null /* , className = '',  children */ },
) => {
	if (!blockID) throw ERROR(`<TreeItem> with falsy blockID`, blockID)
	VERBOSE(`[TreeItem#${blockID}] created`)

	// const location = useLocation()
	// const navigate = useNavigate()
	const parentContext = useBlockContext()
	const depth = (parentContext?.depth ?? 0) + 1
	// const block = useBlockVM(blockID)
	const kidRelations = useKidRelations(blockID)

	const [isExpanded, setExpanded] = relation
		? useEntityAt<boolean>(relation.en, REL_DEF.isExpanded, true)
		: createSignal(true) // HACK: isExpanded on root nodes?

	// const mirrors = useBlockAt(blockID, 'mirrors') // ? specifise query // useBlockVM
	// const pullUrl = useBlockAt(blockID, 'pullUrl')
	// const {mirrors, pullUrl, ts} = useBlockAt(blockID, ['mirrors', 'pullUrl'])
	// const mirrorID = createMemo(() => block.get()?.mirrors)
	// const mirroredBlock = createMemo(() => mirrorID() ? useBlock(mirrorID()).get() : null)

	const [panel, setPanelValue] = createSignal<BlockPanelDef>(null)
	const setPanel = (newPanel: BlockPanelDef) => {
		if (panel()) WARN(`[setPanel] but we already have a panel:`, { prev: panel(), new: newPanel })
		setPanelValue(newPanel) // HACK: handle double panel
	}
	useDivergencePanel(blockID, setPanel)

	// let blockSpan
	// onMountFx(() => {
	// 	// if (!blockSpan) {
	// 	// 	WARN('[EditableContent] contentElem is not set', blockSpan, { blockVM })
	// 	// 	setTimeout(() => {
	// 	// 		if (blockSpan) WARN('It was a timing issue!', blockSpan, { blockVM }) // probably not, but worth a shot.
	// 	// 	}, 1000)
	// 	// 	return
	// 	// }
	// 	// interact(blockSpan)
	// 	// 	.draggable({ inertia: true })
	// 	// 	.on('dragend', function(event) {
	// 	// 		const prevSpeed = event._interaction.prevEvent.speed
	// 	// 		event._interaction.prevEvent.speed = 601
	// 	// 		event.swipe = event.getSwipe()
	// 	// 		event.swipe.speed = prevSpeed
	// 	// 		if (!event.swipe) {
	// 	// 			DEBUG('no swipe', event, event.getSwipe())
	// 	// 			return
	// 	// 		}
	// 	// 		const { angle, speed, right, left } = event.swipe
	// 	// 		LOG('swipe', { angle, speed, right, left })
	// 	// 		if (right) indentBlk(relation)
	// 	// 		if (left) outdentBlk(relation, parentRelation)
	// 	// 	})
	// 	// 	.on('doubletap', function(event: PointerEvent) {
	// 	// 		stopPropagation(event)
	// 	// 		LOG('double', event, { setRadialPos, traceSet })
	// 	// 		const { clientX: screenX, clientY: screenY } = event
	// 	// 		traceSet({ screenX, screenY })
	// 	// 	})
	// })()

	return (
		<BlockContext.Provider value={{ id: blockID, relationToParent: relation?.en, setPanel, parentContext, depth }}>
			<div pl-1 flex='~ col' rounded>
				<Switch>
					<Match when={panel()}>
						<BlockPanel blockID={blockID} relation={relation} panel={panel} />
					</Match>
					<Match when={true /* = else */}>
						<BlockItem {...{ blockID, isExpanded, setExpanded }} relationID={relation?.en} hasKids={kidRelations.length > 0} />

						<Show when={kidRelations.length && depth < MAX_DEPTH}>
							<Collapse value={isExpanded()} class='transition-property-[height] duration-300'>
								<SortableBlocks
									class='mt-2 ml-7 border-l-1 border-l-gray-700 border-l-solid rounded-b-lg'
									blockID={blockID}
									kidRelations={kidRelations}
								>
									{rel => <TreeItem blockID={rel.block} relation={rel} parentRelation={relation} />}
								</SortableBlocks>
							</Collapse>
						</Show>

						<Show when={isExpanded() && depth >= MAX_DEPTH}>
							<div flex='~ justify-center' text-red>Children hidden</div>
						</Show>
					</Match>
				</Switch>
			</div>
			{
				/*
				<Match when={!block.get() || (mirrorID() && !mirroredBlock())}>
					<SimpleTreeItem blockID={blockID} mirroring={mirrorID()} missing={true} relation={relation} />
				</Match>
				<Match when={block.get().mirrors}>
					<EditableTreeItem blockID={block.get().mirrors as string} mirroring={blockID}></EditableTreeItem>
				</Match>
				<Match when={!block.get().mirrors}>
					<EditableTreeItem blockID={blockID} relation={relation} parentRelation={parentRelation}></EditableTreeItem>
				</Match>
			</Switch> */
			}
		</BlockContext.Provider>
	)
}

const SortableBlocks: Component<{
	blockID: string
	kidRelations: Array<RelationModelDef> | null
	disabled?: boolean
	class?: string
	children: (item: RelationModelDef) => JSX.Element
}> = (props) => {
	const currentDS = useCurrentDS()
	const handleDrag = (event: SortableEvent) => {
		const { item } = event
		const relID = item.dataset.id
		let sourceRelation = null, blockID = null
		if (relID.startsWith('FakeDragRelation-')) {
			blockID = props.blockID
		} else {
			sourceRelation = props.kidRelations.find(({ en }) => en === relID)
			blockID = sourceRelation.block
		}
		const newParentID = event.to.dataset.block
		// const afterRelID = event.newIndex === 0 ? null : event.to.dataset.relations.split(',')[event.newIndex - 1]
		const kidsOfTargetParent = event.to.dataset.kids.split(',')
		let newIndex = event.newIndex
		let selfIndex = kidsOfTargetParent.indexOf(blockID)
		DEBUG(`[handleDrag]`, { kidsOfTargetParent, newIndex, selfIndex, newParentID, sourceRelation, event })
		if (selfIndex > -1) {
			// we might be dragging into the same parent as we also are now, but the index from sortablejs already took our removal into account
			kidsOfTargetParent.splice(selfIndex, 1)
		}
		const newAfterID = newIndex === 0 ? null : kidsOfTargetParent[newIndex - 1]
		if (newAfterID === blockID) throw ERROR(`[handleBlockDrag] newAfterID === blockID`)
		if (newParentID === blockID) throw ERROR(`[handleBlockDrag] newParentID === blockID`) // TODO: this can happen if we drop the block back in place

		if (
			!sourceRelation
			|| newParentID !== sourceRelation.childOf
			|| newAfterID !== sourceRelation.after
		) {
			DEBUG(`Calling handleBlockDrag`, { newAfterID, newIndex, kidsOfTargetParent, event })
			handleBlockDrag(
				currentDS,
				blockID,
				sourceRelation?.en,
				newParentID,
				newAfterID,
			)
		}
	}
	createEffect(() => VERBOSE(`[Sortable#${props.blockID}] kids changed:`, props.kidRelations && [...props.kidRelations]))

	// ℹ OPTION DOCS: https://github.com/SortableJS/Sortable#options
	return (
		<Sortable<{ en: EntityID; block: EntityID }>
			class={classNames(props.class, 'flex flex-col gap-2')}
			items={props.kidRelations ?? [{ en: 'FakeDragRelation-' + props.blockID, block: props.blockID }]}
			idField={'en'}
			// setItems={handleSort}
			group='blocks'
			handle='.drag-handle'
			disabled={props.disabled}
			swapThreshold={0.5}
			delay={200 /* if drag is shorter than this, ignore it */}
			delayOnTouchOnly={true}
			touchStartThreshold={10 /* non-intuitive option - see docs */}
			onChoose={(event) => {
				setDraggingBlock(props.blockID)
				VERBOSE(`[Sortable#${props.blockID}] onChoose`, event)
				return false
			}}
			onUnchoose={(event) => {
				setDraggingBlock(null)
				VERBOSE(`[Sortable#${props.blockID}] onUnchoose`, event)
			}}
			onStart={(event) => VERBOSE(`[Sortable#${props.blockID}] onStart`, event)}
			onEnd={(event) => {
				VERBOSE(`[Sortable#${props.blockID}] onEnd`, event)
				handleDrag(event)
			}}
			setItems={() => {/* required prop */}}
			data-block={props.blockID}
			data-relations={props.kidRelations && props.kidRelations.map(({ en }) => en)}
			data-kids={props.kidRelations && props.kidRelations.map(({ block }) => block)}
		>
			{props.children}
		</Sortable>
	)
}

// const SimpleTreeItem: ParentComponent<{
// 	blockID: string
// 	relation: RelationModelDef | null
// 	mirroring?: EntityID
// 	missing?: boolean
// }> = (props) => {
// 	VERBOSE('[SimpleTreeItem] created for', {
// 		blockID: props.blockID,
// 		mirroring: props.mirroring,
// 		missing: props.missing,
// 	})

// 	const [isExpanded, setExpanded] = createSignal(true)
// 	const icon = createMemo(() => {
// 		const classes = []
// 		if (props.missing) {
// 			classes.push(
// 				'color-red',
// 				props.mirroring
// 					? 'i-ph:warning-diamond-bold scale-50'
// 					: 'i-ph:warning scale-50',
// 			)
// 		}
// 		return classes
// 	})
// 	const pullUrl = computed(() => useBlockAt(props.blockID, BLOCK_DEF.pullUrl).get())

// 	const debugJson = createMemo(() => JSON.stringify({ en: props.blockID }, undefined, 2))
// 	return (
// 		<sl-tree-item expanded={isExpanded()} title={debugJson()}>
// 			<div class='i-ph:caret-right-bold' slot='expand-icon' onclick={() => setExpanded(true)}></div>
// 			<div class='i-ph:caret-right-bold' slot='collapse-icon' onclick={() => setExpanded(false)}></div>

// 			<BulletMenu blockID={props.blockID} iconClassSignal={icon} relation={props.relation} />
// 			<Switch fallback={'unhandled SimpleTreeItem usage'}>
// 				<Match when={props.missing}>
// 					<div text-red flex='~ col' cursor-default>
// 						<div>
// 							{props.mirroring ? 'Mirror target' : 'Block'} not found: <code>{props.blockID}</code>
// 						</div>
// 						<Show when={props.mirroring && pullUrl.get()}>
// 							<div overflow-hidden text-ellipsize text-xs>
// 								Publication: <code>{pullUrl.get() as string}</code>
// 							</div>
// 						</Show>
// 					</div>
// 				</Match>
// 			</Switch>
// 		</sl-tree-item>
// 	)
// }

// This is a hacky way to capture the event before it reaches sl-tree handlers (which e.g. collapse or capture spacebar)
// const stopTriggers = (evt: PointerEvent) => { // | {target.$$contextmenu:(e)=>void}
// 	const contextMenuRef = evt.target.$$contextmenu?.(evt)
// 	stopPropagation(evt, 'trigger')
// }

// const AsOfSlider: Component<{
// 	className?: string
// 	blockVM: BlockVM
// 	onChange: (historicalContent: string) => void
// }> = ({ className = '', blockVM, onChange }) => {
// 	const { contentHistoryMemo: contentHistory } = blockVM
// 	// DEBUG({ blockVM, contentHistory })
// 	let sliderRef, handleChange, formatTooltip
// 	// onMountFx(() => {
// 	// 	// sliderRef.addEventListener('sl-change', event => handleChange(event))
// 	// 	// sliderRef.tooltipFormatter = formatTooltip
// 	// })()
// 	formatTooltip = val => {
// 		const historyIndex = (contentHistory().length - 1) + val // will be negative
// 		const { content, ts } = contentHistory().reverse()[historyIndex] ?? {} // needs to be reversed as contentHistory is newest first

// 		VERBOSE({ thisref: this, content, sliderRef, blockVM, contentHistory: contentHistory() })

// 		// (i) this pattern causes 'will never be disposed' warnings - #egal
// 		onChange(content) // this sets the vintage content temporarily, to be persisted onBlur
// 		// above is not about listening: we should probably listen to sl-input event instead - https://shoelace.style/components/range?id=events

// 		return (
// 			<div style='white-space:nowrap; text-center'>
// 				{new Date(ts).toLocaleString()}
// 				{/* <em style='font-size: 0.7em'>{ts}</em><br />{content} */}
// 			</div>
// 		)
// 	}
// 	const formatTooltipMemo = createMemo(() => formatTooltip)
// 	// createEffect(() => {
// 	// 	sliderRef.tooltipFormatter = formatTooltip()
// 	// })
// 	handleChange = () => evt => {
// 		const historyIndex = (contentHistory()?.length - 1) + sliderRef.value // will be negative
// 		const content = contentHistory()?.[historyIndex]?.content
// 		// DEBUG({ content, sliderRef, evt, blockVM, contentHistory() })
// 		onChange(content)
// 	}
// 	return (
// 		<sl-range
// 			ref={sliderRef}
// 			tooltip='top'
// 			tooltip-formatter={formatTooltipMemo()}
// 			// on-sl-change={handleChange()}
// 			min={1 - contentHistory()?.length}
// 			max='0'
// 			step='1'
// 			className={className}
// 			ondoubleclick={() => DEBUG('dc on range')}
// 		/>
// 	)
// }
