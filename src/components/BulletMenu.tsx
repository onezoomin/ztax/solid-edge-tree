import { useLocation, useNavigate } from '@solidjs/router'
import { Logger } from 'besonders-logger/src'
import { Component, Show } from 'solid-js'
import { iconNames } from '../../unocss.safelist'
import { useAgent } from '../data/AgentStore'
import { copyToClipboard, useBlockContext } from '../data/block-ui-helpers'
import { getRecursiveKidsJSON, getRecursiveKidsOPML, indentBlk, outdentBlk, removeBlockRelAndMaybeDelete } from '../data/block-utils'
import { EntityID } from '../data/datalog/datom-types'
import { useRel } from '../data/VMs'
import { useCurrentDS, useRelation } from '../ui/reactive'
import { isTouchDevice, makeNote3Url, zoomToBlock } from '../ui/ui-utils'
import { Iconify } from './mini-components'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export const BulletMenu: Component<{
	blockID: EntityID
	icon: typeof iconNames[number]
	readOnly: boolean
}> = (props) => {
	const { blockID } = props // ! non-reactive
	if (!blockID) return null
	const location = useLocation()
	const navigate = useNavigate()
	const handlers = clickHandlersForBlockBullet()
	const { onCopyLink, onCopyContent, onCopyID, onHistory, onFocus, onDelete, onIndent, onOutent } = handlers
	// const navigate = useNavigate()

	let triggerRef, menuRef
	return (
		<sl-dropdown hoist placement='bottom-start' ref={triggerRef}>
			<Iconify
				slot='trigger'
				size={6}
				scale={150}
				class={props.readOnly ? '' : 'drag-handle cursor-grab'}
				name={props.icon}
				flex='shrink-0'
				onClick={e => DEBUG(`BULLET CLICK`, e)}
				onAuxClick={e => {
					if (e.button === 1) {
						e.preventDefault()
						zoomToBlock({ id: blockID }, location, navigate)
					}
				}}
				onContextMenu={e => {
					if (isTouchDevice()) return
					e.preventDefault()
					e.stopPropagation()
					e.target.parentElement.show()
				}}
			/>
			<sl-menu
				class='menu-tight'
				ref={menuRef}
			>
				{/* <sl-menu-item onClick={onCopyLink}>Copy note3:// link</sl-menu-item> */}
				<sl-menu-item onClick={onCopyID}>Copy ID</sl-menu-item>
				<sl-menu-item onClick={onCopyLink}>
					Copy Link
					<sl-menu slot='submenu'>
						{/* TODO: <For each={matchingPub}><sl-menu-item value="copy-..">via ...</sl-menu-item></For> */}
						<sl-menu-item value='copy-link-public'>Public Link</sl-menu-item>
						<sl-menu-item value='copy-link-select'>Select publication</sl-menu-item>
						<sl-menu-item value='copy-link-quick'>Quick share</sl-menu-item>
						<sl-menu-item value='copy-link-internal'>Internal (only reference)</sl-menu-item>
					</sl-menu>
				</sl-menu-item>
				<sl-menu-item>
					Copy Content
					<sl-menu slot='submenu'>
						<sl-menu-item onClick={(e) => onCopyContent(e, 'html')} value='copy-html'>
							Copy html ul tag
						</sl-menu-item>
						<sl-menu-item onClick={(e) => onCopyContent(e, 'md')} value='copy-md'>
							Copy md (for pasting into logseq, zettlr etc)
						</sl-menu-item>
						<sl-menu-item onClick={(e) => onCopyContent(e, 'opml')} value='copy-opml'>
							Copy OPML (for pasting into WF, note3, etc)
						</sl-menu-item>
					</sl-menu>
				</sl-menu-item>
				<Show when={!props.readOnly}>
					<sl-menu-item onClick={onIndent}>
						<Iconify name='text-indent' slot='prefix' />
						{'Indent'}
					</sl-menu-item>
					<sl-menu-item onClick={onOutent}>
						<Iconify name='text-outdent' slot='prefix' />
						{'Outdent'}
					</sl-menu-item>
					<sl-menu-item onClick={onDelete}>
						<Iconify name='trash-bold' slot='prefix' />
						Delete
					</sl-menu-item>
					<sl-menu-item onClick={onFocus}>
						Zoom To
						<div text-xs mt--1 opacity-60>Middle click</div>
					</sl-menu-item>
				</Show>
				<sl-divider />
				<sl-menu-item onClick={onHistory}>
					<div class='i-ph:clock-counter-clockwise-bold scale-85' />
					TODO History Asof
				</sl-menu-item>
			</sl-menu>
		</sl-dropdown>
	)
}

const clickHandlersForBlockBullet = () => {
	const currentDS = useCurrentDS()
	const blockContext = useBlockContext()
	const relation = blockContext.relationToParent && useRelation(blockContext.relationToParent)
	const { parentContext } = blockContext

	const grandParentRelID = parentContext?.relationToParent ?? null
	// const grandParentVM = grandParentID ? BlockVM.get(grandParentID, rawDS) : null
	const [grandParentRelVM] = useRel(grandParentRelID, currentDS)
	const grandParentID = grandParentRelVM?.childOf

	const blockID = blockContext.id
	const location = useLocation()
	const navigator = useNavigate()

	// VERBOSE({ setAsof })
	const onCopyID = (evt: MouseEvent) => copyToClipboard(blockID)
	const onCopyLink = (evt: MouseEvent) => {
		const pub = useAgent().w3NamePublic // TODO: smart-select pub & UX for other cases
		// const n3url = `note3://publication/${pub}/?focus=${blockID}`
		const n3url = makeNote3Url({ pub, block: blockID })
		DEBUG('Note3 url', n3url)
		copyToClipboard(n3url)
	}

	const onCopyContent = (evt: MouseEvent, mode: 'opml' | 'md' | 'html' = 'opml') => {
		const formattedContent = mode === 'opml'
			? getRecursiveKidsOPML(blockID)
			: mode === 'md'
			? getRecursiveKidsJSON(blockID, 'md')
			: getRecursiveKidsJSON(blockID, 'html')
		DEBUG('Note3 content', blockID, { evt, formattedContent })
		copyToClipboard(formattedContent)
	}
	const onHistory = (_evt: MouseEvent) => blockContext.setPanel({ type: 'history' }) //
	const onFocus = (_evt: MouseEvent) => zoomToBlock({ id: blockID }, location, navigator) //
	const onDelete = (_evt: MouseEvent) => removeBlockRelAndMaybeDelete(currentDS, blockID, relation?.get().en)
	const onIndent = (_evt: MouseEvent) => indentBlk(currentDS, relation?.get())
	const onOutent = (_evt: MouseEvent) => outdentBlk(currentDS, relation?.get(), grandParentID)

	return { onCopyLink, onCopyContent, onCopyID, onHistory, onFocus, onDelete, onIndent, onOutent }
}
