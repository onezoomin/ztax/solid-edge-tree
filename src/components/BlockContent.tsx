import { Editor, Extension } from '@tiptap/core'
import Link from '@tiptap/extension-link'
import Underline from '@tiptap/extension-underline'
import StarterKit from '@tiptap/starter-kit'
import { Logger } from 'besonders-logger/src'
import { pick } from 'lodash-es'
import { untracked } from 'mobx'
import { Accessor, Component, createEffect, createSignal, JSX, on, onCleanup, onMount, Setter, Show, splitProps } from 'solid-js'
import { createTiptapEditor, useEditorJSON } from 'solid-tiptap'
import { useAgent } from '../data/AgentStore'
import { copyToClipboard, createTreeItemKeydownHandler, createTreeItemPasteHandler } from '../data/block-ui-helpers'
import { compareBlockContent as blockContentEqual } from '../data/block-utils'
import type { EntityID } from '../data/datalog/datom-types'
import { useBlockKeyhandlers } from '../data/keybindings'
import { REL } from '../data/VMs'
import { useBlockVM, useRelationVM } from '../ui/reactive'
import { devMode } from '../ui/ui-utils'
import { BulletMenu } from './BulletMenu'
import { DynamicColored, Iconify, useIdHover } from './mini-components'
import { TiptapMenuWrapper } from './TiptapMenuWrapper'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

export const BlockItem: Component<{
	blockID: EntityID
	relationID: EntityID | null
	readOnly: boolean
	hasKids: boolean
	isExpanded: Accessor<boolean>
	setExpanded: Setter<boolean> | ((newVal: boolean) => any[])
}> = (props) => {
	if (!props.blockID) throw ERROR(`<Block> with falsy blockID`, props.blockID)
	const { isExpanded, setExpanded } = props
	// const relation = props.relationID && useRelation(props.relationID)
	const relVM = useRelationVM(props.relationID)
	let blockRef: HTMLDivElement
	useBlockKeyhandlers(() => blockRef)
	const ag = useAgent().ag
	// TODO needs reactivity support:
	const pasteHandler = createTreeItemPasteHandler(props.blockID, relVM)
	const keyDownHandler = createTreeItemKeydownHandler(props.blockID, props.relationID, isExpanded, setExpanded, ag)
	return (
		<div ref={blockRef} flex='~ items-center' data-block-id={props.blockID}>
			<Show when={!props.readOnly}>
				<Iconify
					size={'4 6'}
					scale={75}
					name={props.hasKids ? 'caret-right-bold' : null}
					class={'cursor-pointer color-gray-400 duration-300 transition-property-[transform]' +
						(isExpanded() ? ' rotate-90' : ' rotate-0')}
					flex='shrink-0'
					onClick={() => setExpanded(!isExpanded())}
				/>
			</Show>
			<BulletMenu
				blockID={props.blockID}
				icon={props.hasKids ? 'dot-duotone' : 'dot-bold'}
				readOnly={props.readOnly}
			/>
			<EditableContent
				readOnly={props.readOnly}
				blockID={props.blockID}
				relationID={props.relationID}
				onPaste={pasteHandler}
				onKeyDown={keyDownHandler}
				ml-1
				flex='grow-1 shrink-1'
			/>
			<Show when={devMode()}>
				<span
					flex='inline col items-stretch'
					ml-1
					text='[0.6em]'
					font-mono
					text-right
				>
					<div {...(useIdHover('block', props.blockID))} flex='~ items-stretch' transition='[opacity] duration-300'>
						B&thinsp;
						<DynamicColored
							text={props.blockID}
							class='cursor-pointer'
							onClick={() => copyToClipboard(props.blockID)}
							title='click to copy'
						/>
					</div>
					<Show when={props.relationID}>
						<div {...(useIdHover('block', props.relationID))} transition='[opacity] duration-300' flex='~ items-stretch'>
							R&thinsp; <DynamicColored text={props.relationID} title={JSON.stringify(pick(relVM, Object.values(REL)), undefined, 2)} />
						</div>
						<div {...(useIdHover('block', relVM?.after))} transition='[opacity] duration-300' flex='~ items-stretch'>
							↑&thinsp; <DynamicColored text={relVM?.after ?? 'null'} />
						</div>
					</Show>
				</span>
			</Show>
		</div>
	)
}

// HACK: I'm not sure how to get from dom node to Editor in another way
export const editorMap = new Map<HTMLDivElement, Editor>()

const CustomShortcuts = Extension.create({
	addKeyboardShortcuts() {
		return {
			// HACK: move default Enter behaviour to Shift-Enter (as our Enter creates a new block)
			'Shift-Enter': ({ editor }) =>
				// from: https://github.com/ueberdosis/tiptap/blob/39cf6979c49e953118bbbe4b894a1dc296128932/packages/core/src/extensions/keymap.ts#L49C25-L54C7
				editor.commands.first(({ commands }) => [
					() => commands.newlineInCode(),
					() => commands.createParagraphNear(),
					() => commands.liftEmptyBlock(),
					() => commands.splitBlock(),
				]),
		}
	},
})

export const EditableContent: Component<
	{
		blockID: EntityID
		relationID: EntityID
		readOnly: boolean
		// blockVM: BlockVM
		// block: typeof BLOCK
		// vintageContent: Accessor<string>
		onKeyDown: (evt: KeyboardEvent) => Promise<void>
		onPaste: (evt: ClipboardEvent) => Promise<void>
	} & JSX.HTMLAttributes<HTMLSpanElement>
> = (fullProps) => {
	const [props, otherProps] = splitProps(fullProps, ['blockID', 'relationID', 'onKeyDown', 'onPaste', 'readOnly'])
	VERBOSE(`<EditableContent#${props.blockID}> created`, { props, otherProps })
	const blockVM = useBlockVM(props.blockID)
	const [isEditing, setEditing] = createSignal(null)
	let ref!: HTMLDivElement
	let menuRef!: HTMLDivElement

	const initialContent = untracked(() => blockVM.content)
	VERBOSE(`[Content#${props.blockID}] initial`, initialContent)
	const editor = createTiptapEditor(() => ({
		element: ref!,
		editable: !props.readOnly,
		editorProps: {
			attributes: {
				//   class: 'prose prose-sm sm:prose lg:prose-lg xl:prose-2xl mx-auto focus:outline-none',
				class: 'focus:outline-none -my-4',
			},
		},
		extensions: [
			StarterKit.configure({
				hardBreak: false,
				// paragraph: {
				// 	HTMLAttributes: {
				// 		// class: 'min-h-[1rem]'
				// 	},
				// }),
			}),
			Underline,
			Link.configure({
				protocols: ['note3', 'mailto', 'ftp'],
			}),
			CustomShortcuts,
			// BubbleMenu.configure({
			// 	element: menuRef!,
			// }),
		],
		content: initialContent,
	}))
	onMount(() => {
		editorMap.set(ref!, editor())
	})
	onCleanup(() => {
		editorMap.delete(ref!)
	})

	const contentJson = useEditorJSON(editor)
	createEffect(on(() => contentJson(), newContent => {
		DEBUG(`[EditableContent.htmlUpdate]`, newContent, { blockVM })
		// if (blockVM.content === null) return // HACK: not loaded yet
		if (props.readOnly) return WARN(`content update but readonly`, newContent, { blockVM })
		if (!blockContentEqual(newContent, blockVM.content)) {
			LOG('[EditableContent.htmlUpdate] changed', { old: blockVM.content })
			// blockVM.editingPos = getCursorPositionInContentEditable(evt.target)
			blockVM.content = newContent
			setEditing(setTimeout(() => setEditing(null), 1000)) // HACK: unify with BlockVM.isEditing
			// persistBlockContent(block.en, spanText)
		}
	}))
	createEffect(on(() => blockVM.content, newContent => {
		DEBUG(`[EditableContent#${props.blockID}] newContent:`, newContent, { ref, isEditing: blockVM.isEditing })
		if (!isEditing()) {
			if (!blockContentEqual(contentJson(), newContent)) {
				editor().commands.setContent(newContent)
			}
		}
	}))

	// const onBlur = async (evt: FocusEvent) => {
	// 	// if (blockVM.content === null) return // not loaded yet
	// 	// // TODO: also debounced on input?
	// 	// const spanText = (evt.target as HTMLSpanElement).innerText
	// 	// if (spanText != blockVM.content) {
	// 	// 	VERBOSE('[EditableContent] onBlur', { blockVM, evt, spanText })
	// 	// 	blockVM.content = spanText
	// 	// 	// persistBlockContent(block.en, spanText)
	// 	// }
	// }
	// const onType = action((evt: KeyboardEvent) => {
	// 	LOG(`[EditableContent.onType]`, { evt, blockVM })
	// 	if (blockVM.content === null) return // HACK: not loaded yet
	// 	const spanText = (evt.target as HTMLSpanElement).innerText
	// 	if (spanText !== blockVM.content) {
	// 		LOG('[EditableContent] onType', { blockVM, evt, spanText })
	// 		blockVM.editingPos = getCursorPositionInContentEditable(evt.target)
	// 		blockVM.content = spanText
	// 		setEditing(setTimeout(() => setEditing(null), 1000))
	// 		// persistBlockContent(block.en, spanText)
	// 	}
	// })
	// // const keydownListener = createMemo(() => (evt) => {
	// // 	DEBUG(`[EditableContent.keydown]`, evt)
	// // 	// local.onKeyDown(evt)
	// // 	stopPropagation(evt)
	// // })
	// onMountFxDeprecated(() => {
	// 	if (!contentElem) {
	// 		WARN('[EditableContent] contentElem is not set', contentElem, { blockVM })
	// 		setTimeout(() => {
	// 			if (contentElem) WARN('It was a timing issue!', contentElem, { blockVM }) // probably not, but worth a shot.
	// 		}, 1000)
	// 		return
	// 	}
	// 	interact(contentElem)
	// 		.on('doubletap', function(event: PointerEvent) {
	// 			stopPropagation(event)
	// 			LOG('double', event, { setRadialPos, traceSet })
	// 			const { clientX: screenX, clientY: screenY } = event
	// 			traceSet({ screenX, screenY }, local.blockID, local.relationID)
	// 		})
	// 	contentElem.addEventListener('click', stopPropagation, true)
	// 	// true to use capturing
	// 	// contentElem.addEventListener('keydown', keydownListener, true) // true to use capturing
	// 	// make sure to clean up when the component is unmounted
	// })()
	// onCleanup(() => {
	// 	// contentElem.removeEventListener('keydown', keydownListener, true)
	// 	contentElem.removeEventListener('click', stopPropagation, true) // true to use capturing
	// })

	return (
		<>
			<div
				{...otherProps}
				onKeyDown={props.onKeyDown}
				ref={ref}
				id={'block-' + blockVM.en}
				p-1
				flex='grow-1'
				style='width: 0' // HACK to get break-words to work
			>
			</div>
			<TiptapMenuWrapper
				editor={editor()}
				tippyOptions={{ duration: 300 }}
			>
				{/* shouldShow={({ editor, state, view, from, to }) => { }} */}
				<sl-button-group label='Formatting'>
					<sl-tooltip content='Bold'>
						<sl-button size='small' onclick={() => editor().chain().focus().toggleBold().run()}>
							<Iconify slot='prefix' name='text-b-bold' />
						</sl-button>
					</sl-tooltip>
					<sl-tooltip content='Italic'>
						<sl-button size='small' onclick={() => editor().chain().focus().toggleItalic().run()}>
							<Iconify slot='prefix' name='text-italic-bold' />
						</sl-button>
					</sl-tooltip>
					<sl-tooltip content='Underline'>
						<sl-button size='small' onclick={() => editor().chain().focus().toggleUnderline().run()}>
							<Iconify slot='prefix' name='text-underline-bold' />
						</sl-button>
					</sl-tooltip>
					<sl-tooltip content='Strikethrough'>
						<sl-button size='small' onclick={() => editor().chain().focus().toggleStrike().run()}>
							<Iconify slot='prefix' name='text-strikethrough-bold' />
						</sl-button>
					</sl-tooltip>
					{/* TODO: divider */}
					<sl-tooltip content='Link'>
						<sl-button size='small' onclick={() => editor().chain().focus().toggleLink().run()}>
							<Iconify slot='prefix' name='link-bold' />
						</sl-button>
					</sl-tooltip>
				</sl-button-group>
			</TiptapMenuWrapper>
		</>
	)
	// return (
	// 	<span
	// 		{...otherProps}
	// 		focus-outline-none
	// 		role='textbox'
	// 		onBlur={onBlur}
	// 		onKeyUp={onType}
	// 		onKeyDown={local.onKeyDown}
	// 		onPaste={local.onPaste}
	// 		contenteditable
	// 		p-1
	// 		cursor-text
	// 		min-w-16
	// 		ref={contentElem}
	// 		id={'block-' + blockVM.enID}
	// 		break-all // TODO: should be only break-words, but doesn't work
	// 		hyphens-auto
	// 	>
	// 		{/* w-full w-min-content */}
	// 		{untracked(() => blockVM.content)}
	// 	</span>
	// )
}
