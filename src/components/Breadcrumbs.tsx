import { useLocation, useNavigate } from '@solidjs/router'
import { Logger } from 'besonders-logger/src'
import { Accessor, Component, createMemo, For, Match, Show, Switch } from 'solid-js'
import type { EntityID } from '../data/datalog/datom-types'
import { useBlockVM, useParent } from '../ui/reactive'
import { zoomToBlock } from '../ui/ui-utils'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

const MAX_DEPTH = 10

export const Breadcrumbs: Component<{ focus: Accessor<EntityID>; showOverview?: Accessor<boolean> }> = (
	{ focus, showOverview },
) => {
	// testing of context router within show
	// const [{ pub: pubFromUrl }, _setSearchParams] = useSearchParams()
	// DEBUG('useSearchParams in Breadcrumbs in show', { pubFromUrl })
	const location = useLocation()
	const navigate = useNavigate()

	const crumbs = createMemo(() => {
		let block = focus()
		const crumbs = []
		if (block) {
			let depth = 0
			while (depth < MAX_DEPTH) {
				depth++
				const parent = useParent(block).get()
				VERBOSE('Parent of', block, 'is', parent)
				if (!parent) {
					break
				}
				if (crumbs.includes(parent)) throw new Error(`Breadcrumbs loop: ${parent}`)
				crumbs.push(parent)
				block = parent
			}
		}
		DEBUG(`Breadcrumbs:`, crumbs, { focus: focus(), block })
		return crumbs.reverse()
	})

	return (
		<Show when={crumbs()?.length || showOverview?.()}>
			<sl-breadcrumb>
				<span mt='-1' px-2 w-4 h-4 class='i-ph:caret-right-bold' slot='separator'></span>
				<Show when={showOverview?.()}>
					<sl-breadcrumb-item
						cursor-pointer
						onClick={e => e.button === 0 && zoomToBlock({ id: null }, location, navigate)}
					>
						<span mt='-1' class='i-ph:house-bold'></span>
					</sl-breadcrumb-item>
				</Show>
				<For
					each={crumbs()}
				>
					{(blockID) => {
						const blockVM = useBlockVM(blockID)
						return (
							<sl-breadcrumb-item
								cursor-pointer
								onClick={e => e.button === 0 && zoomToBlock({ id: blockID }, location, navigate)}
								flex='~ items-center'
							>
								<div max-w-xs truncate>
									<Switch fallback={<code>{blockID}</code>}>
										<Match when={blockVM.content}>
											<span>{blockVM.contentPlaintext}</span>
										</Match>
									</Switch>
								</div>
							</sl-breadcrumb-item>
						)
					}}
				</For>
			</sl-breadcrumb>
		</Show>
	)
}
