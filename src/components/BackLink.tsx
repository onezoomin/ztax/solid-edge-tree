import { A } from '@solidjs/router'
import { Component, Show } from 'solid-js'

export const BackLink: Component<{ text?: string; target?: string }> = function BackLink({ text, target }) {
	return (
		<A
			text='white visited:white'
			href={target || '/'}
			flex='inline items-center'
			class='gap-2 no-underline'
		>
			<div text='white visited:white' class='i-ph:arrow-left-bold' />
			<Show when={text}>
				<span>{text || 'Back to home'}</span>
			</Show>
		</A>
	)
}
