import { Logger } from 'besonders-logger/src'
import { boundInput, useAgent } from '../data/AgentStore'
const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars
const persist = ['username', 'devicename', 'agentcode'] // will sync to localStorage
export function AgentEditor() {
	const globalAgent = useAgent()
	const traceW3situation = (e) => {
		globalAgent.loadW3().then(() => {
			DEBUG(globalAgent)
		})
	}
	return (
		<div w-full overflow-auto text='xs md:base' box-border py-1>
			{boundInput('username', persist)}
			.
			<a onClick={traceW3situation}>{globalAgent.shortDID}</a>@{
				globalAgent.app //
			}.{
				boundInput('agentcode', persist) //
			}.{
				boundInput('devicename', persist) //
			}
			<span onClick={() => globalAgent.ensureAgentAtoms()} ml='4' mt='-1' w='4' h='4' class='i-ph:checks-bold' />
		</div>
	)
}
