import { useLocation, useNavigate } from '@solidjs/router'
import { Logger } from 'besonders-logger/src'
import { startCase } from 'lodash-es'
import { autorun } from 'mobx'
import { Accessor, Component, createEffect, createResource, createSignal, For, Match, onCleanup, Setter, Suspense, Switch } from 'solid-js'
import { getSubOrPub } from '../data/AgentStore'
import { parseBlockContentValue, tiptapToPlaintext } from '../data/block-utils'
import { BLOCK_DEF } from '../data/data-types'
import type { EntityID } from '../data/datalog/datom-types'
import { observableArrayMap } from '../data/datalog/mobx-utils'
import { ApplogStreamOnlyCurrent, assertOnlyCurrent } from '../data/datalog/query/engine'
import { query } from '../data/datalog/query/queries'
import { isPublication } from '../data/local-state'
import { useCurrentDS } from '../ui/reactive'
import { makeNote3Url, tryParseNote3URL, zoomToBlock } from '../ui/ui-utils'
import { ShortID, Spinner } from './mini-components'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

type Result =
	| { type: 'block'; id: EntityID; content: string }
	| { type: 'subscription' | 'publication'; id: string }
	| { type: 'preview'; id: string; focus: EntityID | null }

async function performSearch({ search, currentDS }: { search: string; currentDS: ApplogStreamOnlyCurrent }) {
	VERBOSE(`Search:`, search)
	if (!search) return null
	assertOnlyCurrent(currentDS)
	!VERBOSE.isDisabled || DEBUG(`Search:`, search)

	const blocksByIDQuery = query(currentDS, [
		{ en: search, at: BLOCK_DEF.content },
	])
	const blocksQuery = query(currentDS, [
		// HACK: how to properly scan structured tiptap content as plaintext?
		{ at: BLOCK_DEF.content, vl: v => typeof v === 'string' && v.toLocaleLowerCase().includes(search.toLocaleLowerCase()) },
	])
	// autorun(() => DEBUG(`Search result computation updated:`, { applogSets: result.applogSets, logs: result.allApplogs }))
	// autorun(() => DEBUG(`Search result applogSets updated:`, { applogSets: result.applogSets }))
	// autorun(() => DEBUG(`Search result allApplogs updated:`, { logs: result.allApplogs }))
	autorun(() => DEBUG(`Search result by ID:`, blocksByIDQuery.applogSets))

	// Recalculate results in case blocksQuery or publications/subs changes
	return observableArrayMap(() => {
		let results = [] as Result[]

		const parsedUrl = tryParseNote3URL(search)
		const subOrPub = getSubOrPub(parsedUrl ? parsedUrl.publication.toString() : search.trim())
		DEBUG(`url/subOrPub?`, { subOrPub, parsedUrl })
		if (subOrPub) {
			results.push({ type: isPublication(subOrPub) ? 'publication' : 'subscription', id: subOrPub.id })
		} else if (parsedUrl?.publication) {
			// if (parsedUrl.focus)
			// const block = BlockVM.get(parsedUrl.focus)
			results.push({ type: 'preview', id: parsedUrl.publication.toString(), focus: parsedUrl.focus })
		}

		// ? if (!results.length) {
		if (!blocksByIDQuery.isEmpty) {
			results.push({
				type: 'block' as const,
				id: blocksByIDQuery.applogSets[0][0].en,
				content: blocksByIDQuery.applogSets[0][0].vl as string,
			})
		}
		DEBUG(`Adding block results`, { blocksByIDQuery, blocksQuery })
		results.push(...blocksQuery.applogSets.map(logs => {
			if (logs.length != 1) WARN(`Block matching search returned node with logs count != 1:`, { logs, blocksQuery })
			return {
				type: 'block' as const,
				id: logs[0].en,
				content: logs[0].vl as string,
			}
		}))
		DEBUG(`Search results:`, results)
		return results
	})
}

export const SearchBar: Component<{
	open: Accessor<boolean>
	setOpen: Setter<boolean>
}> = (
	{ open, setOpen },
) => {
	const location = useLocation()
	const navigate = useNavigate()
	// let setOpenInternal
	// if (open === undefined) {
	// 	;[open, setOpenInternal] = createSignal(false)
	// 	const setOpenOriginal = setOpen
	// 	setOpen = (state: boolean) => {
	// 		setOpenInternal(state)
	// 		setOpen(state)
	// 	}
	// }
	const [search, setSearch] = createSignal('')

	const close = () => {
		setOpen(false)
		setTimeout(() => setSearch(''), 300) // prevent glitch during fadeout
	}

	const handleKeyDown = (event: KeyboardEvent) => {
		DEBUG(`[handleKeyDown]`, event)
		if (event.ctrlKey && event.key === '/') {
			if (open()) close()
			else setOpen(true)
		} else if (open()) {
			if (event.key === 'Escape') {
				close()
			}
		}
	}
	document.addEventListener('keydown', handleKeyDown, true)
	onCleanup(() => {
		document.removeEventListener('keydown', handleKeyDown, true)
	})

	const handleClickToClose = (event: MouseEvent) => {
		DEBUG(`[handleClickToClose]`, event)
		// FIXME: if I drag from the input to outside, this is triggered
		if ((event.target as HTMLElement).dataset.clickaway !== undefined) {
			close()
		}
	}
	let inputRef, resultsContainerRef
	const handleArrowKeys = (event: KeyboardEvent) => {
		DEBUG(`[handleArrowKeys]`, event)
		if (['ArrowUp', 'ArrowDown'].includes(event.key)) {
			let target = event.target as HTMLElement
			if (target.getRootNode() instanceof ShadowRoot) {
				// Get the host element of the shadow root
				target = (target.getRootNode() as ShadowRoot).host as HTMLElement
			}
			const current = target.closest('sl-input, div[data-block-id]') as HTMLElement
			DEBUG(`[handleArrowKeys] current`, current)
			if (!current) return
			let newTarget = (event.key === 'ArrowUp' ? current.previousElementSibling : current.nextElementSibling) as HTMLElement
			if (!newTarget && current.parentElement == resultsContainerRef && event.key === 'ArrowUp') {
				newTarget = inputRef
			}
			if (newTarget == resultsContainerRef) {
				newTarget = newTarget.firstElementChild as HTMLElement
			}
			DEBUG(`[handleArrowKeys] newTarget`, newTarget)
			if (newTarget) {
				newTarget.focus()
				event.preventDefault()
			}
		}
	}

	const currentDS = useCurrentDS()
	const [results] = createResource(() => ({ search: search(), currentDS }), performSearch)

	createEffect(() => {
		if (open()) {
			DEBUG(`Focussing search field:`, inputRef)
			inputRef?.focus()
		}
	})

	const openResult = (result: Result) => {
		close()
		if (result.type === 'block') zoomToBlock({ id: result.id }, location, navigate)
		else if (result.type === 'preview') navigate(makeNote3Url({ pub: result.id, block: result.focus, previewPub: true, relative: true }))
		else navigate(`/settings/${result.type}s/${result.id}`)
	}

	return (
		// <Show when={open()}>
		<div
			class={open() ? 'opacity-100' : 'opacity-0 pointer-events-none'}
			transition-300
			transition-property-opacity
			absolute
			flex='~ col items-center'
			bg='black opacity-60'
			inset-0
			h-100vh
			z-300
			box-border
			p-2
			data-clickaway
			onClick={handleClickToClose}
			backdrop='filter blur-3'
		>
			<div
				flex='shrink-1 ~ col items-stretch justify-start'
				h-full
				w-full
				max-w-4xl
				color-white
				gap-4
				data-clickaway
				onKeyDown={handleArrowKeys}
			>
				<sl-input
					ref={inputRef}
					placeholder='Search'
					value={search()}
					onInput={e => setSearch(e.target.value)}
					border='2 solid white'
					rounded
					tabindex='0'
					onKeyDown={({ key }) => key === 'Enter' && openResult(results()?.[0])}
				/>
				<div ref={resultsContainerRef} flex='shrink-1 ~ col items-stretch justify-start' w-full gap-2 overflow-y-auto>
					<Suspense fallback={<Spinner size='2rem' />}>
						<For each={results()}>
							{(result, index) => (
								<div
									tabindex='0'
									bg='gray-900 hover:bluegray-700 focus:bluegray-800'
									flex='1'
									rounded
									border='1 solid white focus:blue'
									text-xs
									p-2
									data-block-id={result.type === 'block' ? result.id : undefined}
									onKeyDown={({ key }) => key === 'Enter' && openResult(result)}
									onclick={() => openResult(result)}
								>
									{/* <pre>{JSON.stringify(applog,undefined,4)}</pre> */}
									<Switch>
										<Match when={result.type === 'block'}>
											{/* @ts-expect-error TS inference doesn't fully work in solid */}
											{tiptapToPlaintext(parseBlockContentValue(result.content))}
											{/* TODO proper block rendering postponed because of search re-think ideas */}
										</Match>
										<Match when={true}>
											{startCase(result.type)}: <ShortID id={result.id} />
										</Match>
									</Switch>
								</div>
							)}
						</For>
					</Suspense>
				</div>
			</div>
		</div>
		// </Show>
	)
}
