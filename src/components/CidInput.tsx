import { Accessor, Component } from 'solid-js'

const inputStyle = {
	'--sl-input-font-size-small': '14px',
}

export const CidInput: Component<{
	value: Accessor<string>
	onInput: (evt: InputEvent) => any // TODO type seems to be evt based on consumer usage
	placeholder?: string
}> = function CidInput({ value, onInput, placeholder = 'IPNS (k51q...), #TODO: CID (bafy...) ' }) {
	return (
		<sl-input
			placeholder={placeholder}
			value={value()}
			onInput={onInput}
			filled
			font-mono
			size='small'
			max-w-full
			min-w-fit
			spellcheck={false}
			style={inputStyle}
		/>
	)
}
