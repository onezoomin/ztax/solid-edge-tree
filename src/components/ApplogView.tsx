import { A } from '@solidjs/router'
import { createMemo, createSignal, For, Match, Show, Switch } from 'solid-js'
import { ApplogStream } from '../data/datalog/query/engine'
import { tsNearlySame } from '../data/datalog/utils'
import { dateNowIso, formatIfJson } from '../data/Utils'
import { useRawDS } from '../ui/reactive'
import { explorerUrl } from '../ui/ui-utils'
import { DynamicColored, IdHover } from './mini-components'

export function ApplogView({ ds }: { ds?: ApplogStream } = {}) {
	if (!ds) ds = useRawDS()

	const [showAll, setShowAll] = createSignal(false)
	const reversed = createMemo(() =>
		ds.applogs
			.slice(showAll() ? undefined : -50) // TODO: make it a virtual container - e.g. https://github.com/minht11/solid-virtual-container
			.reverse()
	)

	return (
		<div w-full max-h-42vh overflow-auto text-xs border='1 solid rd-1 gray-500' box-border p-1 color-gray-400>
			{
				/* <For each={applogs().reverse()} fallback={<div>No Applogs</div>}>
				{(item, index) => <pre data-index={index()} m-0 text-xs>{JSON.stringify(item)}</pre>}
			</For> */
			}
			<div w-full text-center mb-2 truncate>
				<strong>{ds.size} Applogs</strong>&emsp;–&emsp;<code>{ds.name}</code>
			</div>
			<table w-full cellpadding='1' style='border-collapse:collapse;'>
				{/* cellspacing='0' cellpadding='0' */}
				<thead>
					<tr text-left font-mono>
						<th class='pr-0.5'>Time</th>
						<th class='px-0.5'>Agent</th>
						<th class='px-0.5'>Entity</th>
						<th class='px-0.5'>Attribute</th>
						<th class='px-0.5'>Value</th>
						<th class='pl-0.5'>Previous</th>
					</tr>
				</thead>
				<tbody>
					<For
						each={reversed()}
						fallback={
							<tr>
								<td colspan='5'>
									No Applogs
								</td>
							</tr>
						}
					>
						{(item, index) => {
							const clumpingClass = createMemo(() =>
								reversed()[index() + 1]?.ts && tsNearlySame(reversed()[index() + 1]?.ts, item.ts) ? '' : 'pb-2'
							)
							return (
								<tr
									data-index={index()}
									whitespace-nowrap
									font-mono
									bg='hover:black/40'
									data-cid={item.cid}
								>
									<td text-right class={`pr-0.5 ` + clumpingClass()} title={item.ts}>
										{
											item.ts
												.replace(dateNowIso().slice(0, 11), '') // remove date if it's today
												.slice(0, -5) // remove ms
										}
									</td>
									<td title={'TODO: agent info'} class={`px-0.5 ` + clumpingClass()}>
										<IdHover id={item.ag}>
											<DynamicColored text={item.ag} />
										</IdHover>
									</td>
									<td max-w-5em truncate title={item.en} overflow-hidden class={`px-0.5 ` + clumpingClass()}>
										<IdHover id={item.en}>
											<DynamicColored text={item.en} />
										</IdHover>
									</td>
									<td title={item.cid} class={clumpingClass()}>{item.at}</td>
									<td
										title={typeof item.vl === 'string' ? formatIfJson(item.vl) : JSON.stringify(item.vl, undefined, 4)}
										class={`px-0.5 ` + clumpingClass()}
										max-w-md
										truncate
									>
										<Switch>
											<Match when={typeof item.vl === 'string' && item.vl.length === 8}>
												{/* HACK: heuristic to check for EntityID */}
												<IdHover id={item.vl.toString()}>
													<DynamicColored text={item.vl.toString()} />
												</IdHover>
											</Match>
											<Match when={true}>
												{typeof item.vl === 'string' && item.vl ? item.vl : JSON.stringify(item.vl)}
											</Match>
										</Switch>
									</td>
									<td class={`pl-0.5 ` + clumpingClass()}>
										<Show when={!!item.pv}>
											<A target='_blank' href={explorerUrl(item.pv)}>{item.pv.slice(-7)}</A>
										</Show>
									</td>
								</tr>
							)
						}}
					</For>
				</tbody>
			</table>
			<sl-button
				onclick={() => setShowAll(!showAll())}
			>
				Show {showAll() ? 'fewer' : 'all'}
			</sl-button>
		</div>
	)
}
