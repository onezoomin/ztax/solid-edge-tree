// import { createEffect } from 'solid-js'
// import { DialogLabel } from '../icons/Web3StorageIconSVG'

// export const SetupDialog = (
// 	{
// 		submitted,
// 		handleAuthorizeSubmit,
// 		email,
// 		setEmail,
// 		registeringSpace,
// 		tryRegisterSpace,
// 		unloadAgent,
// 		keyringSpaceBuster,
// 		keyring,
// 		w3,
// 	},
// ) => {
// 	let dialogRef

// 	createEffect(async () => {
// 		const agentProps = ['agentcode', 'devicename', 'username']
// 		let isAgentStringSetup = false
// 		for (const prop of agentProps) {
// 			if (localStorage.getItem(`agent.${prop}`)) isAgentStringSetup = true
// 		}
// 		// console.log({ isAgentStringSetup, dialogRef })
// 		if (!isAgentStringSetup && dialogRef) dialogRef.show()
// 	})

// 	return (
// 		<>
// 			<sl-button
// 				variant='primary'
// 				onclick={() => {
// 					dialogRef?.show()
// 				}}
// 				size='small'
// 			>
// 				<div flex='~ items-center' gap-1>
// 					<div slot='prefix' w-4 h-4 class='i-ph:gear' />
// 					{keyring?.account ? 'web3' : 'Set up!'}
// 				</div>
// 			</sl-button>
// 			<sl-dialog ref={dialogRef} class='dialog-header-actions'>
// 				<DialogLabel />
// 			</sl-dialog>
// 		</>
// 	)
// }
