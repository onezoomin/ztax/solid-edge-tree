import { Logger } from 'besonders-logger/src'
const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

/* eslint-disable no-fallthrough */
export const overlayHack = () => {
	// hack to avoid overlay.ts's dom assumptions
	// @ts-expect-error
	self.HTMLElement = function() {
		return {}
	}

	self.customElements = {
		// @ts-expect-error
		get() {
			return []
		},
	}
}
export const checkWorker = (...msg) => {
	if (self.document === undefined) {
		return ['WW:', ...msg]
	} else {
		return [...msg] // ' sad trombone. not worker ',
	}
}

const areDeduplicatedArraysEqualUnordered = (a1: any[] | Set<any>, a2: any[] | Set<any>) => {
	a1 = new Set(a1)
	a2 = new Set(a2)
	VERBOSE('checking arrays as Sets', { a1, a2 })
	return !!(a1.size === a2.size && [...a1].every(v => (a2 as Set<any>).has(v)))
}
const areDeduplicatedArraysEqualOrdered = (a1: any[], a2: any[]) => {
	;(a1 = [...(new Set(a1))] as any[]) && (a2 = [...(new Set(a2))] as any[]) && VERBOSE('checking deduplicated arrays', { a1, a2 })
	return !!(a1.length === a2.length && a1.every((v: any, i) => a2[i] === v))
}

export const frozen = async (obj: any) => JSON.parse(JSON.stringify(obj))
const appStartTimeStamp = Date.now()
const p = performance.now()
let precision = 0
// milliseconds since epoch (100nanosecond "precision")
export function utcMsTs(): number {
	const now = new Date()
	const newPrecision = Math.round(performance.now() - p)
	precision = newPrecision === precision ? newPrecision + 1 : newPrecision // ensure 1ms difference - basically
	return appStartTimeStamp + precision + (now.getTimezoneOffset() * 60 * 1000)
}
export function dateNowIso(): string {
	const now = new Date()
	return now.toISOString()
}

export const BYGONZ_MUTATION_EVENT_NAME = 'bygonzmutation'

export const allPromises = Promise.all.bind(Promise) // https://stackoverflow.com/a/48399813/2919380

export const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms)) // https://stackoverflow.com/a/39914235/2919380
/**
 * JS Implementation of MurmurHash3 (r136) (as of May 20, 2011)
 *
 * https://github.com/garycourt/murmurhash-js/blob/master/murmurhash3_gc.js
 *
 * @author <a href="mailto:gary.court@gmail.com">Gary Court</a>
 * @see http://github.com/garycourt/murmurhash-js
 * @author <a href="mailto:aappleby@gmail.com">Austin Appleby</a>
 * @see http://sites.google.com/site/murmurhash/
 *
 * @param {string} key ASCII only
 * @param {number} seed Positive integer only
 * @return {number} 32-bit positive integer hash
 */
export function murmurHash(
	key: string,
	seed: number,
	_rippedFrom = 'https://github.com/garycourt/murmurhash-js/blob/master/murmurhash3_gc.js',
) {
	let remainder, bytes, h1, h1b, c1, c2, k1, i

	remainder = key.length & 3 // key.length % 4
	bytes = key.length - remainder
	h1 = seed
	c1 = 0xcc9e2d51
	c2 = 0x1b873593
	i = 0

	while (i < bytes) {
		k1 = (key.charCodeAt(i) & 0xff)
			| ((key.charCodeAt(++i) & 0xff) << 8)
			| ((key.charCodeAt(++i) & 0xff) << 16)
			| ((key.charCodeAt(++i) & 0xff) << 24)
		;++i

		k1 = (((k1 & 0xffff) * c1) + ((((k1 >>> 16) * c1) & 0xffff) << 16)) & 0xffffffff
		k1 = (k1 << 15) | (k1 >>> 17)
		k1 = (((k1 & 0xffff) * c2) + ((((k1 >>> 16) * c2) & 0xffff) << 16)) & 0xffffffff

		h1 ^= k1
		h1 = (h1 << 13) | (h1 >>> 19)
		h1b = (((h1 & 0xffff) * 5) + ((((h1 >>> 16) * 5) & 0xffff) << 16)) & 0xffffffff
		h1 = ((h1b & 0xffff) + 0x6b64) + ((((h1b >>> 16) + 0xe654) & 0xffff) << 16)
	}

	k1 = 0

	switch (remainder) {
		case 3:
			k1 ^= (key.charCodeAt(i + 2) & 0xff) << 16
		case 2:
			k1 ^= (key.charCodeAt(i + 1) & 0xff) << 8
		case 1:
			k1 ^= key.charCodeAt(i) & 0xff
			k1 = (((k1 & 0xffff) * c1) + ((((k1 >>> 16) * c1) & 0xffff) << 16)) & 0xffffffff
			k1 = (k1 << 15) | (k1 >>> 17)
			k1 = (((k1 & 0xffff) * c2) + ((((k1 >>> 16) * c2) & 0xffff) << 16)) & 0xffffffff
			h1 ^= k1
	}

	h1 ^= key.length

	h1 ^= h1 >>> 16
	h1 = (((h1 & 0xffff) * 0x85ebca6b) + ((((h1 >>> 16) * 0x85ebca6b) & 0xffff) << 16)) & 0xffffffff
	h1 ^= h1 >>> 13
	h1 = (((h1 & 0xffff) * 0xc2b2ae35) + ((((h1 >>> 16) * 0xc2b2ae35) & 0xffff) << 16)) & 0xffffffff
	h1 ^= h1 >>> 16

	return h1 >>> 0
}

/*
				cyrb53 (c) 2018 bryc (github.com/bryc)
				A fast and simple hash function with decent collision resistance.
				Largely inspired by MurmurHash2/3, but with a focus on speed/simplicity.
				Public domain. Attribution appreciated.

	ripped from https://github.com/bryc/code/blob/mast`er/jshash/experimental/cyrb53.js
*/
export const cyrb53hash = function(
	str: string,
	seed = 13,
	strLength: number, /* = 0 */
) {
	if (!str?.length) {
		throw new Error(`Empty string: ${str}`)
	}

	let h1 = 0xdeadbeef ^ seed
	let h2 = 0x41c6ce57 ^ seed
	for (let i = 0, ch; i < str.length; i++) {
		ch = str.charCodeAt(i)
		h1 = Math.imul(h1 ^ ch, 2654435761)
		h2 = Math.imul(h2 ^ ch, 1597334677)
	}
	h1 = Math.imul(h1 ^ (h1 >>> 16), 2246822507) ^ Math.imul(h2 ^ (h2 >>> 13), 3266489909)
	h2 = Math.imul(h2 ^ (h2 >>> 16), 2246822507) ^ Math.imul(h1 ^ (h1 >>> 13), 3266489909)
	// if (strLength) {
	const asHex = (4294967296 * (2097151 & h2) + (h1 >>> 0)).toString(16)
	return asHex.slice(-strLength).padStart(strLength, '0')
	// }
	// // if not specified return as 16 digit integer
	// return 4294967296 * (2097151 & h2) + (h1 >>> 0)
}

export function notNull<TValue>(value: TValue | null): value is TValue {
	return value !== null
}

export function notUndefined<TValue>(value: TValue | undefined): value is TValue {
	return value !== undefined
}

export function fnBrowserDetect() {
	const userAgent = navigator.userAgent
	let browserName

	if (userAgent.match(/chrome|chromium|crios/i)) {
		browserName = 'chrome'
	} else if (userAgent.match(/firefox|fxios/i)) {
		browserName = 'firefox'
	} else if (userAgent.match(/safari/i)) {
		browserName = 'safari'
	} else if (userAgent.match(/opr\//i)) {
		browserName = 'opera'
	} else if (userAgent.match(/edg/i)) {
		browserName = 'edge'
	} else {
		browserName = 'No browser detection'
	}
	return browserName
}

/*
Convert an ArrayBuffer into a string
from https://developer.chrome.com/blog/how-to-convert-arraybuffer-to-and-from-string/
*/
export function ab2str(buf) {
	return String.fromCharCode.apply(null, new Uint8Array(buf))
}
export function str2ab(str) {
	var buf = new ArrayBuffer(str.length * 2) // 2 bytes for each char
	var bufView = new Uint16Array(buf)
	for (var i = 0, strLen = str.length; i < strLen; i++) {
		bufView[i] = str.charCodeAt(i)
	}
	return bufView // ? consider https://developer.chrome.com/blog/easier-arraybuffer-string-conversion-with-the-encoding-api/
}

function buf2hexSlower(buffer: ArrayBuffer) { // buffer is an ArrayBuffer
	return [...new Uint8Array(buffer)]
		.map(x => x.toString(16).padStart(2, '0'))
		.join('')
}

const byteToHex = []
function preComputeHex() {
	if (byteToHex.length === 0) {
		for (let n = 0; n <= 0xff; ++n) {
			const hexOctet = n.toString(16).padStart(2, '0')
			byteToHex.push(hexOctet)
		}
	}
}
export function buf2hex(arrayBuffer: ArrayBuffer) {
	if (byteToHex.length === 0) preComputeHex()
	const buff = new Uint8Array(arrayBuffer)
	const hexOctets = [] // new Array(buff.length) is even faster (preallocates necessary array size), then use hexOctets[i] instead of .push()

	for (let i = 0; i < buff.length; ++i) {
		hexOctets.push(byteToHex[buff[i]])
	}

	return hexOctets.join('')
}
export function arrayBufferToBase64(buffer) {
	var binary = ''
	var bytes = new Uint8Array(buffer)
	var len = bytes.byteLength
	for (var i = 0; i < len; i++) {
		binary += String.fromCharCode(bytes[i])
	}
	return btoa(binary)
}
export function base64ToArray(base64) {
	var binaryString = atob(base64)
	var bytes = new Uint8Array(binaryString.length)
	for (var i = 0; i < binaryString.length; i++) {
		bytes[i] = binaryString.charCodeAt(i)
	}
	return bytes
}
export function base64ToArrayBuffer(base64) {
	return base64ToArray(base64).buffer
}

export function bufToBase64(arrayBuffer: ArrayBuffer) {
	return btoa(arrayBuffer)
}

export function base64ToUrlBase64(str) {
	return str.replace(/\+/g, '-').replace(/\//g, '_').replace(/=/g, '')
}
export function bnToB64(bn, url = false) {
	var hex = BigInt(bn).toString(16)
	if (hex.length % 2) hex = '0' + hex

	var bin = []
	var i = 0
	var d
	var b
	while (i < hex.length) {
		d = parseInt(hex.slice(i, i + 2), 16)
		b = String.fromCharCode(d)
		bin.push(b)
		i += 2
	}

	return url ? base64ToUrlBase64(btoa(bin.join(''))) : btoa(bin.join(''))
}

export function formatIfJson(str: string) {
	try {
		return JSON.stringify(JSON.parse(str), undefined, 4)
	} catch (err) {
		return str
	}
}

export const unwrapIfSingle = (arr: any[]) => arr.length === 1 ? arr[1] : arr

export function arraysContainSameElements(arr1, arr2) {
	if (arr1.length !== arr2.length) {
		return false
	}

	const sortedArr1 = [...arr1].sort()
	const sortedArr2 = [...arr2].sort()

	for (let i = 0; i < sortedArr1.length; i++) {
		if (sortedArr1[i] !== sortedArr2[i]) {
			return false
		}
	}

	return true
}
