import type odd from '@oddjs/odd'
import { importRsaKey } from '@oddjs/odd/components/crypto/implementation/browser'
import { Logger } from 'besonders-logger/src'
import { action, comparer, computed, flow, makeObservable, observable, runInAction } from 'mobx'
import stringify from 'safe-stable-stringify'

import type { GenericObject } from '../types/util-types'
import type { ApplogForInsert } from './datalog/datom-types'

import { useW3Keyring } from '../ipfs/w3ui'
import { notifyToast, stopPropagation } from '../ui/ui-utils'
import { getApplogDB, insertApplogs } from './ApplogDB'
import { type ApplogStream, rollingFilter } from './datalog/query/engine'
import { filterAndMap, withoutHistory } from './datalog/query/queries'
import { type IPublication, type ISubscription, stateDB } from './local-state'
import { createMnemonic, getECDHkeypairFromHashArray, hashMnemonic } from './mnemonic'
import { arrayBufferToBase64, cyrb53hash, fnBrowserDetect } from './Utils'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

export interface AgentState {
	app: string
	username: string
	devicename: string
	agentcode: string

	publications: IPublication[]
	subscriptions: ISubscription[]
	crypto?: odd.Crypto.Implementation & { ecdh?: Partial<CryptoKeyPair> }
	verifier?: any
	w3NamePublic?: any
	_cryptoDerivationPublicECDH?: string
	_cryptoDerivationPublicJWK?: globalThis.JsonWebKey
	// pubkey: string
	// agentstring: string
	// shortHash: string
	// ensureAgentAtoms: () => Promise<void>
}
let /* keyring, w3, agentFromKeyring, space, account,  */ isLoading, isLoaded
export class AgentStateClass implements AgentState {
	loading = true
	app = 'dev.note3'
	username = localStorage.getItem('agent.username') ?? 'userhandle'
	devicename = localStorage.getItem('agent.devicename') ?? 'laptop'
	agentcode = localStorage.getItem('agent.agentcode') ?? fnBrowserDetect() ?? 'browser'

	readonly publications = observable.array([] as IPublication[], { deep: true, equals: comparer.structural })
	readonly subscriptions = observable.array([] as ISubscription[], { deep: true, equals: comparer.structural })
	crypto: odd.Crypto.Implementation & { ecdh?: Partial<CryptoKeyPair> } = null
	verifier: any = null // needs to be set before getters will work
	w3NamePublic: string | null = null // needs to be set before getters will work

	_cryptoDerivationPublicJWK?: globalThis.JsonWebKey
	_cryptoDerivationPublicECDH?: string // extracted from the IDB keystore on first request then cached in memory here

	constructor() {
		makeObservable(this, {
			loading: observable,
			app: observable,
			username: observable,
			devicename: observable,
			agentcode: observable,
			crypto: observable.ref,
			verifier: observable.ref,
			w3NamePublic: observable,

			publicationsMap: computed,
			subscriptionsMap: computed,
			hasStorageSetup: computed,
			mostRecentPublication: computed,
			pubkeyDID: computed,
			shortDID: computed,
			appAgent: computed,
			ag: computed,
			// addPub: action,
			// addSub: action,
			// publications: observable, - already observable.array
			// subscriptions: observable,
		})
	}

	loadW3 = async () => {
		const [keyring, w3] = useW3Keyring()
		if (!this.verifier) {
			DEBUG('loading verifier from w3', { keyring, w3 })
			const { agent: agentFromKeyring, space, account } = keyring
			const verifier = agentFromKeyring?.verifier
			if (account && verifier) {
				DEBUG('[loadW3] setting verifier ', { verifier, agentFromKeyring, space, account })
				this.verifier = verifier
				// setEmail(account)
			} else {
				WARN('[loadW3] no verifier/account', { verifier, agentFromKeyring, space, account })
			}
			// createEffect(async () => {
			// 	const keyringBuster = { did: space?.did(), registered: space?.registered() }
			// 	setKeyringBuster(keyringBuster) // ? huh ?
			// 	DEBUG('W3UP effect', { keyringBuster })
			// })
			isLoaded = true
			isLoading = false
		}
	}
	loadIDB = async () => {
		// TODO: dexie doesn't support nullable index/sorting - so do it manually
		const publications = (await stateDB.publications.orderBy(/* 'lastPush',  */ 'createdAt').toArray()).filter(p => !p.isDeleted)
		const subscriptions = (await stateDB.subscriptions.orderBy(/* 'lastPull',  */ 'createdAt').toArray()).filter(p => !p.isDeleted)
		runInAction(() => {
			this.publications.replace(publications)
			this.subscriptions.replace(subscriptions)
			DEBUG(`Loaded from IDB:`, { pubs: this.publications, subs: this.subscriptions, stateDB })
			this.loading = false
		})
	}
	get publicationsMap() {
		return new Map(this.publications.map(pub => [pub.id, pub]))
	}
	get subscriptionsMap() {
		return new Map(this.subscriptions.map(sub => [sub.id, sub]))
	}
	get mostRecentPublication() {
		if (!this.publications.length) return null
		return this.publications[this.publications.length - 1]
	}

	addPub = flow(function*(this: AgentStateClass, pub: IPublication) {
		LOG(`Adding publication:`, pub)
		// (i) dexie does some type checking, so we do it first & await
		yield stateDB.publications.add(pub)
		this.publications.push(pub)
	})
	addSub = flow(function*(this: AgentStateClass, sub: ISubscription) {
		LOG(`Adding subscription:`, sub)
		yield stateDB.subscriptions.add(sub)
		this.subscriptions.push(sub)
	})
	updateSub = flow(function*(this: AgentStateClass, id: string, changes: Partial<Omit<ISubscription, 'id'>>) {
		const sub = this.subscriptions.find(p => p.id === id)
		if (!sub) throw ERROR(`[updateSub] id not found:`, id, changes)
		LOG(`Updating subscription:`, id, changes, sub)
		yield stateDB.subscriptions.update(id, changes)
		Object.assign(sub, changes)
	})
	updatePub = flow(function*(this: AgentStateClass, id: string, changes: Partial<Omit<IPublication, 'id'>>) {
		const pub = this.publications.find(p => p.id === id)
		if (!pub) throw ERROR(`[updatePub] id not found:`, id, changes)
		LOG(`Updating publication:`, id, changes, pub)
		Object.assign(pub, changes)
		void stateDB.publications.update(id, changes)
	})
	deletePub = flow(function*(this: AgentStateClass, pubID: string) {
		const pub = this.publications.find(p => p.id === pubID)
		LOG(`Deleting publication:`, pubID, pub)
		if (!pub) throw ERROR(`Trying to delete non-existing publication:`, pubID)
		yield stateDB.publications.update(pubID, { isDeleted: true })
		this.publications.remove(pub)
	})
	deleteSub = flow(function*(this: AgentStateClass, subID: string) {
		const sub = this.subscriptions.find(p => p.id === subID)
		LOG(`Deleting subscription:`, subID, sub)
		if (!sub) throw ERROR(`Trying to delete non-existing subscription:`, subID)
		yield stateDB.subscriptions.update(subID, { isDeleted: true })
		this.subscriptions.remove(sub)
	})

	decryptWithKeystore = async (encByteArray: Uint8Array) => {
		const dec = new TextDecoder('utf-8')
		const deencDecodedPayload = dec.decode(await this.crypto?.keystore.decrypt(encByteArray))
		return deencDecodedPayload
	}

	getEncryptionKeyCrypto = async () => {
		// TODO consider caching and utilifying
		const pubEncryptionKeyBytes = await this.crypto?.keystore?.publicExchangeKey()
		const pubKeyCryptoReimported = await importRsaKey(pubEncryptionKeyBytes, ['encrypt'])
		// VERBOSE('[odd]', { pubKeyCryptoReimported })

		return pubKeyCryptoReimported
	}

	get pubkeyDID() {
		// TODO coordinate odd did with web3 and investigate UCAN
		return this.verifier?.toDIDKey() ?? '' // TODO: create a verifier/did without web3storage login?
	}
	getVerifierJWK = async () => {
		const publicKey = await this.verifier?.publicKey
		const exportedPubKey = await window.crypto.subtle.exportKey('jwk', publicKey)
		VERBOSE('[agentAtoms] getPubkeyJWK', { publicKey, exportedPubKey })

		return exportedPubKey
	}
	get shortDID() {
		return this.pubkeyDID?.substring(24, 32) || 'noDIDyet'
	} // first 24 from w3ui "always" seem to be did:key:z4MXj1wBzi9jUsty
	get appAgent() {
		return `${this.username}.${this.shortDID}@${this.app}.${this.agentcode}.${this.devicename}`
	}
	get ag() {
		return cyrb53hash(this.appAgent, 31, 7) as string
	}
	get hasStorageSetup() {
		const [keyring] = useW3Keyring()
		DEBUG(`[hasStorageSetup]`, keyring?.space?.registered(), keyring)
		return keyring?.space?.registered()
	}

	ensureAgentAtoms = flow(function*() {
		// TODO: if (!this.verifier) throw new Error('ensure not possible without verifier')
		if (!this?.verifier) return WARN('ensure not possible without verifier')
		if (!this?.crypto?.ecdh) yield this.getDerivationKeypair()
		if (!this?.crypto?.ecdh) return WARN('ensure not possible without ecdh', this?.crypto)
		const { appAgent, ag, pubkeyDID } = this
		let ds: ApplogStream
		try {
			ds = getApplogDB()
		} catch (err) {
			return WARN('ensure not possible without db', err)
		}
		const agentAtoms = [] as ApplogForInsert[]

		// const appAgentResult = db.enatIndex[`${ag}_|_agent/appAgent`]?.[0] // as ag is a unique hash, this should always be the same

		// const deriverLog = rollingFilter(ds, { en: ag, at: 'agent/jwkd' })
		const deriverJWKD = filterAndMap(withoutHistory(ds), { en: ag, at: 'agent/jwkd' }, 'vl')[0]
		const deriverECDH64 = filterAndMap(withoutHistory(ds), { en: ag, at: 'agent/ecdh' }, 'vl')[0]
		const appAgentFromLogs = getAgentString(ag)
		if (appAgentFromLogs === appAgent) {
			VERBOSE('[agentAtoms] matching appAgent log exists for current ag, assuming its fine', { appAgentFromLogs })
		} else {
			WARN('[agentAtoms] appAgentResults missing or non matching', {
				appAgentFromLogs,
				deriverECDH64,
				deriverJWKD,
			})

			agentAtoms.push(
				{ en: ag, at: 'agent/appAgent', vl: appAgent, ag },
			)
		}
		// array of all atoms for this specific agent - can be more than one if the key has been "rotated"
		if (deriverJWKD && deriverECDH64) {
			VERBOSE('[agentAtoms] deriver jwk and 64 both exist, assuming its fine', { deriverECDH64, deriverJWKD })
			// TODO stop assuming and start checking
			// if (deriverJWKD.size > 1) WARN('//TODO deal with multiple jwkd atoms')
		} else {
			if (!deriverJWKD) {
				const newDeriverJWK = yield this.getDerivationJWK()
				WARN('[agentAtoms] ECDH deriverLog missing or non matching', { newDeriverJWK })
				agentAtoms.push({ en: ag, at: 'agent/jwkd', vl: stringify(newDeriverJWK), ag })
			}
			if (!deriverJWKD) {
				const newDeriverECDH = yield this.getDerivationECDH()
				WARN('[agentAtoms] ECDH deriverLog missing or non matching', { newDeriverECDH })
				agentAtoms.push({ en: ag, at: 'agent/ecdh', vl: newDeriverECDH, ag })
			}
		}

		const nonExistingAgentAtoms = agentAtoms.filter(a => !ds.hasApplogWithDiffTs(a))
		if (nonExistingAgentAtoms.length) {
			VERBOSE('[agentAtoms] adding', { agentAtoms, nonExistingAgentAtoms }) // double check that the above did not produce any duplicates
			yield insertApplogs(nonExistingAgentAtoms)
		} else {
			DEBUG('[agentAtoms] alles klar miene shatzie', { deriverJWKD, deriverECDH64, appAgentFromLogs })
		}
	})

	// TODO: mobx-ify those:
	getDerivationJWK = async () => {
		if (!this._cryptoDerivationPublicJWK) {
			this._cryptoDerivationPublicJWK = await window.crypto.subtle.exportKey('jwk', (await this.getDerivationKeypair()).publicKey)
		}
		return this._cryptoDerivationPublicJWK
	}
	getDerivationECDH = async () => {
		if (!this._cryptoDerivationPublicECDH) {
			const exportedKey = await window.crypto.subtle.exportKey('raw', (await this.getDerivationKeypair()).publicKey)
			const keyBase64 = arrayBufferToBase64(exportedKey)
			DEBUG('sharedKey', { exportedKey, keyBase64 })
			this._cryptoDerivationPublicECDH = keyBase64
		}
		return this._cryptoDerivationPublicECDH
	}

	getSharedKeyApplogForCurrentAgent = (applogs) => {
		DEBUG('looking for sharedKeyLog for ', this.ag, 'in', { applogs })
		return applogs.find(eachLog => (
			(eachLog?.at as string) === 'pub/sharedKey'
			&& (eachLog?.en as string) === this.ag
		))
	}

	getDerivationKeypair = async (newMnemonic?: string[]) => {
		if (!this.crypto?.ecdh) {
			if (!this.crypto) throw new Error('getDerivationKeypair called before odd init')
			this.crypto.ecdh = (await stateDB.cryptokeys.get([this.ag, 'derivation']))?.keys ??
				await this.initializeDerivationKeypair(newMnemonic)
		}
		return this.crypto.ecdh as CryptoKeyPair
	}
	initializeDerivationKeypair = async (newMnemonic?: string[]) => {
		newMnemonic = newMnemonic ?? createMnemonic(24)
		notifyToast(`mnemonic for ${this.ag}\n${newMnemonic}`, 'success', 300000) // TODO more elegant ux needed
		const hashedMnemonic = hashMnemonic(newMnemonic)
		const encHashedMnu = (new TextEncoder()).encode(hashedMnemonic)
		const keypairFromMnemonic = await getECDHkeypairFromHashArray(encHashedMnu)
		this.crypto.ecdh = keypairFromMnemonic
		await stateDB.cryptokeys.put({ ag: this.ag, type: 'derivation', keys: keypairFromMnemonic })
		return keypairFromMnemonic
	}
	writeAgentCryptoAtoms = action(async (existingDerivationKeypair, skipToast = false) => {
		existingDerivationKeypair = existingDerivationKeypair ?? this.initializeDerivationKeypair()
		// TODO use derivation key
		const pubEncryptionKeyBytes = await this.crypto?.keystore?.publicExchangeKey()
		const pubEncryptionKeyb64 = btoa(String.fromCharCode(...pubEncryptionKeyBytes))
		// const existingLog = getApplogDB().valueIndex[pubEncryptionKeyb64]?.find(l => l.en === this.ag)
		const existingLog = rollingFilter(getApplogDB(), { en: this.ag, vl: pubEncryptionKeyb64 })[0]
		if (existingLog) {
			notifyToast(`Applog already exists for ${this.ag}--${pubEncryptionKeyb64}`, 'warning', 15000)
		} else {
			await insertApplogs([
				{ en: this.ag, at: 'agent/derivationKey', vl: pubEncryptionKeyb64, ag: this.ag },
			])
			notifyToast(`Applog added for ${this.ag}--${pubEncryptionKeyb64}`, 'success')
		}
		// TODO add signing key also
	})

	getKnownAgents(dataStream: ApplogStream = getApplogDB()) {
		const knownAgentPKlogs = rollingFilter(withoutHistory(dataStream), { at: 'agent/jwkd' })
		const knownAgentECDHlogs = rollingFilter(withoutHistory(dataStream), { at: 'agent/ecdh' })

		return computed(() => {
			const mappedAgentLogs = knownAgentPKlogs.applogs.map(
				(log, _logIdx) => {
					const { en: eachAg, vl: jwkd } = log
					const agString = getAgentString(eachAg)
					return [eachAg, { log, ag: eachAg, agString, jwkd }] as const
				},
			)
			const agentsMap = new Map(mappedAgentLogs) // TODO ts
			DEBUG('agentMapMemo', { mappedAgentLogs, agentsMap, knownAgentECDHlogs, knownAgentPKlogs })
			return agentsMap
		})
	}
}

export const agentState = new AgentStateClass()

// const [state, setState] = createSignal(AgentState) // create signal with defaults
// export const createAgentStateGetter = () => new AgentStateGetters(state()) // create getter with current state
// export const [globalState, setGlobalState] = createSignal(useAgent()) // set globalState to instance of AgentStateGetters

export const updateAgentState = action((newState: Partial<AgentState>) => {
	// TODO persist changes to IDB

	DEBUG('[AgentState] before', { agentState: JSON.parse(JSON.stringify(agentState)), newState })
	Object.assign(agentState, newState)
	DEBUG('[AgentState] after', JSON.parse(JSON.stringify(agentState)))
})

// usage: const [globalAgentSignal, setAgentState] = useGlobalAgentSignal()
// ... globalAgentSignal().ag

export function useAgent() {
	return agentState
}

export const getAgentString = (agentEntityHash) => filterAndMap(getApplogDB(), { en: agentEntityHash, at: 'agent/appAgent' }, 'vl')[0]
// (getApplogDB().getSortedApplogsFromIndex({ whichID: `${agentEntityHash}_|_agent/appAgent`, whichIndex: 'enatIndex' }))?.[0]?.vl
/******
 * agentString =  ${username}.${pubkey} @ ${agentcode}.${devicename}
 * agentString =  ${w3ui-verifiedEmailAddress}.${fullDID} @ ${agentcode}.${devicename}

	agentString =  ${userhandle}.${shortDID} @ ${app}.${agentcode}.${devicename}
	agentStringEx =  	myuser.PVZxrKoX@dev.note3.chromium.laptop
	ag = hash(agentString)// used for every appLog

	atoms needed
	ag=>did mapping
	ag=>agentstring
	ag=>signed agentsring

	// ??
	ag=>ucan mapping
	ag=>exported json public key // only if did extraction

********/
const syncToLocalStorageDefaults = ['username', 'devicename', 'agentcode']
export const valuesAsofBinding = new Map()
const acceptableExtraAttr: { pubID?: string; subID?: string; padding?: string; extraClasses?: string } = {}
export function boundInput(prop: keyof AgentStateClass, syncToLocalStorage = syncToLocalStorageDefaults, extraAttrs = acceptableExtraAttr) {
	const pubOrSubID = extraAttrs.pubID || extraAttrs.subID
	const val = pubOrSubID
		? (agentState[prop] as Map<string, GenericObject>)?.get(pubOrSubID as string)?.name ?? 'unknown'
		: agentState[prop]
	// VERBOSE('[agent] binding', { [prop]: val })
	valuesAsofBinding.set(prop, val)
	// value="${username}" onkeydown="${handleKeydown}" oninput="${setContents}
	const onkeydown = (evt: any) => {
		// const { agentstring: agentStringDirect } = state()
		// const val = evt.target.textContent
		DEBUG('[onkeydown]', { val, evt })
	}
	const oninput = (evt: any) => {
		// const val = evt.target.textContent
		// // host[prop] = val - breaks UX
		// store.set(AgentState, { [prop]: val })
		// DEBUG('[oninput]', val, { host, evt })
		// if (['username', 'devicename'].includes(prop)) {
		// 	localStorage.setItem(`agent.${prop}`, val)
		// }
	}
	const onblur = (evt: any) => {
		const newVal = evt.target.textContent
		// VERBOSE('[onblur]', prop, 'newVal:', newVal)
		if (newVal !== val) {
			if (pubOrSubID) {
				if (!['publicationsMap', 'subscriptionsMap'].includes(prop)) throw new Error('invalid prop for pubsub')
				const currentPubOrSub = (agentState[prop as 'publicationsMap' | 'subscriptionsMap'] /* as Map<string, GenericObject> */)?.get(
					pubOrSubID as string,
				)
				DEBUG('[onblur] with subPath', prop, { currentPub: currentPubOrSub, newVal })
				if (!currentPubOrSub) throw ERROR(`Invalid pubOrSubID`, pubOrSubID)
				if (prop === 'publicationsMap') {
					agentState.updatePub(currentPubOrSub.id, { name: newVal })
				} else {
					agentState.updateSub(currentPubOrSub.id, { name: newVal })
				}
			} else {
				const newState = updateAgentState({ [prop]: newVal })
				VERBOSE('[onblur]', prop, 'newState:', newState)

				if (syncToLocalStorage.includes(prop)) {
					localStorage.setItem(`agent.${prop}`, newVal)
				}
			}
		}
	}

	// TODO grok how to penetrate shadow dom situation
	return (
		<span
			w-fit
			focus-outline-none
			rounded-sm
			b-b='2 solid white hover:blue focus:blue-600'
			bg='white opacity-10 hover:opacity-15'
			leading-loose
			hover-bg-white-200
			focus-bg-white-100
			role='textbox'
			contenteditable
			spellcheck={false}
			class={[extraAttrs.padding ?? 'py-0.5 px-[0.15rem]', extraAttrs.extraClasses].join(' ')}
			style='margin-bottom: calc(-0.125rem - 2px)'
			{...{ onblur, onkeydown, oninput, onclick: stopPropagation }}
		>
			{val}
		</span>
	)
}

export const getSubOrPub = (id) => {
	return agentState.subscriptions.find(s => s.id === id)
		?? agentState.publications.find(s => s.id === id)
}
