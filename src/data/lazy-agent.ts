import type { AgentStateClass } from './AgentStore'

let useAgent: useAgentT
async function getUseAgent() {
	if (!useAgent) useAgent = (await import('./AgentStore')).useAgent
	return useAgent
}
type useAgentT = () => AgentStateClass
export { getUseAgent, type useAgentT }
