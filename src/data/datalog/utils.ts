import { Logger } from 'besonders-logger/src'
import { isBefore } from 'date-fns'
import { action, comparer, untracked } from 'mobx'
import stringify from 'safe-stable-stringify'
import { cyrb53hash } from '../Utils'
import type {
	Applog,
	ApplogValue,
	DatalogQueryPattern,
	DatalogQueryResultEntry,
	ResultContext,
	SearchContext,
	ValueOrMatcher,
} from './datom-types'
import { ApplogEventMapper, ApplogStream, isInitEvent, MappedApplogStream } from './query/engine'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export const isoDateStrCompare = (strA: string, strB: string, dir: 'asc' | 'desc' = 'asc') =>
	dir === 'asc'
		? strA.localeCompare(strB, 'en-US')
		: strB.localeCompare(strA, 'en-US')
/** Sort by TS (modifies the array, but also returns for ease of use)  */
export function sortApplogsByTs(appLogArray: Applog[], dir: 'asc' | 'desc' = 'asc') {
	return appLogArray.sort((a, b) => isoDateStrCompare(a.ts, b.ts, dir))
}
export const isTsBefore = (log: Applog, logToCompare: Applog) => isBefore(new Date(log.ts), new Date(logToCompare.ts))
export const uniqueEnFromAppLogs = (appLogArray: Applog[]) => [...new Set(appLogArray.map(eachLog => eachLog.en))]
export const areApplogsEqual = (logA: Applog, logB: Applog) => comparer.structural(logA, logB)

export function joinStreams(streams: ApplogStream[]) {
	if (streams.length === 0) throw ERROR(`joinStreams called with empty array`) // ? EmptyStream
	if (streams.length === 1) return streams[0]
	const fullJoin = () =>
		sortApplogsByTs(
			removeDuplicateAppLogs(streams.flatMap(s => { // ? perf improve
				const logs = s.applogs
				if (!logs) {
					ERROR(`falsy applogs of stream`, s)
					throw new Error(`falsy applogs of stream`)
				}
				return logs
			})),
		)
	const initialMergeResult = untracked(() => fullJoin())
	const eventMapper: ApplogEventMapper = action(function(event, sourceStream) {
		if (isInitEvent(event)) {
			return { init: untracked(() => fullJoin()) } // HACK: if this would become a common thing, think of a better solution
		} else {
			return {
				// TODO: test this stuff
				added: event.added.filter(addedLog => !this.hasApplog(addedLog, true)),
				removed: event.added.filter(addedLog =>
					!this.parents.some(parent => {
						if (parent === sourceStream) return false
						return parent.hasApplog(addedLog, true)
					})
				),
			}
		}
	})
	// TODO @manu check this ['?'] biz
	return new MappedApplogStream(streams, ['?'], initialMergeResult, eventMapper, `join(${streams.map(s => s.name).join(', ')})`)
}

export const removeDuplicateAppLogs = (appLogArray: Applog[]) => {
	const logMap = new Map()
	for (const eachLog of appLogArray) {
		if (!eachLog) {
			ERROR(`falsy entry in applogs`, appLogArray)
			throw new Error(`falsy entry in applogs`)
		}
		logMap.set(stringify(eachLog), eachLog)
	}
	return Array.from(logMap.values())
}

// export const removeDuplicateAndMaybeDeletedAppLogs = (ds: ApplogStream, appLogArray: Applog[], removeDeletedEntities = true) => {
// 	const logMap = new Map()
// 	for (const eachLog of appLogArray) {
// 		if (!removeDeletedEntities || ds.entityIsDeleted(eachLog.en))
// 			logMap.set(stringify(eachLog), eachLog)
// 	}
// 	return Array.from(logMap.values())
// }

export const getHashID = (stringifiable: any, lngth = 8) => cyrb53hash(stringify(stringifiable), 31, lngth) as string

export function isVariable(x: any): x is string {
	return typeof x === 'string' && x.startsWith('?')
}
export function variableNameWithoutQuestionmark(str: string) {
	return str.slice(1)
}
// export function isMatcher(x: any): x is string {
// 	return
// }
export function isStaticPattern(x: any): x is ApplogValue {
	if (!['string', 'boolean', 'number', 'function'].includes(typeof x)) WARN(`Unhandled pattern value type:`, typeof x, x)
	return !isVariable(x) && ['string', 'boolean', 'number'].includes(typeof x)
}
// export function isIgnorePattern(x: any): boolean {
// 	return x === '_'
// }

/*
 * In a pattern from a Query:
 * - variables that don't have a value in the search context:
 *   - remove from pattern
 *   - add to variableToFill as: { en: 'movieID' } (useful for mapTo)
 * - variables that have a value set:
 *   - replace placeholder with actual value
 */
export function resolveOrRemoveVariables(pattern: DatalogQueryPattern, candidate: SearchContext) {
	let variablesToFill = {} as Partial<{ [key in keyof Applog]: string }>
	const newPattern = Object.entries(pattern).reduce((acc, [patternKey, patternValue]) => {
		if (isVariable(patternValue)) {
			const varName = variableNameWithoutQuestionmark(patternValue)
			const candidateValue = candidate[varName]
			if (candidateValue) {
				acc[patternKey] = candidateValue
			} else {
				variablesToFill[patternKey] = varName
				// do nothing to acc = remove field from pattern
			}
		} else {
			acc[patternKey] = patternValue // keep static value
		}
		return acc
	}, {})

	return [newPattern, variablesToFill]
}

function matchVariable(variable: string, triplePart: ApplogValue, context: SearchContext): SearchContext {
	if (context.hasOwnProperty(variable)) {
		// TODO: fix lint error with: if (Object.hasOwnProperty.call(context, variable)) {
		const bound = context[variable]
		const match = matchPart(bound, triplePart, context)
		VERBOSE('[matchVariable] match?', variable, bound, match)
		return match
	}
	VERBOSE('[matchVariable] initializing variable', variable, 'to', triplePart)
	return { ...context, [variable]: triplePart }
}

export function matchPartStatic(field: keyof Applog, patternPart: ValueOrMatcher<ApplogValue>, atomPart: ApplogValue): boolean {
	VERBOSE('[matchPartStatic]', field, patternPart, patternPart === atomPart ? '===' : '!==', atomPart)

	const result = (() => {
		if (typeof patternPart === 'function') {
			return patternPart(atomPart)
		}
		if (Array.isArray(patternPart) && !Array.isArray(atomPart) /* ? how to handle array values */) {
			return patternPart.includes(atomPart)
		}
		// if (field === 'at' && typeof patternPart === 'string' && patternPart.endsWith('*')) {
		// 	return typeof atomPart === 'string' && atomPart.startsWith(patternPart.slice(0, -1))
		// }
		return patternPart === atomPart
	})()

	VERBOSE('[matchPartStatic] =>', field.startsWith('!') ? '!' : '', result)
	if (field.startsWith('!')) {
		return !result
	} else {
		return result
	}
}
export function matchPart(patternPart: ValueOrMatcher<ApplogValue>, atomPart: ApplogValue, context: SearchContext): ResultContext {
	if (!context) {
		VERBOSE('[matchPart] no context')
		return null
	}
	if (typeof patternPart === 'string') {
		if (isVariable(patternPart)) {
			return matchVariable(patternPart, atomPart, context)
		} /* TODO: else if (isIgnorePattern(patternPart)) {
			return matchVariable(patternPart, atomPart, context)
		} */
	}
	VERBOSE('[matchPart]', patternPart, patternPart === atomPart ? '===' : '!==', atomPart)
	if (typeof patternPart === 'function') {
		return patternPart(atomPart) ? context : null
	}
	return patternPart === atomPart ? context : null
}

/**
 * Check if pattern matches triple with context substitutions
 */
export function matchPattern(pattern: DatalogQueryPattern, applog: Applog, context: SearchContext): ResultContext {
	return Object.entries(pattern).reduce((context, [field, patternValue]) => {
		const applogValue = applog[field]
		return matchPart(patternValue, applogValue, context)
	}, context)
}

export function actualize<SELECT extends string>(context: ResultContext, find: readonly SELECT[]): DatalogQueryResultEntry<SELECT> {
	return Object.fromEntries(find.map((findField) => {
		if (context === null) {
			throw new Error(`actualize context is null ${find}`)
		}
		return [
			isVariable(findField) ? findField.replace(/^\?/, '') : findField,
			isVariable(findField) ? context[findField] : findField,
		]
	})) as DatalogQueryResultEntry<SELECT>
}
const sum = function sum(array: number[]) {
	var num = 0
	for (var i = 0, l = array.length; i < l; i++) num += array[i]
	return num
}
const mean = function mean(array: number[]) {
	return sum(array) / array.length
}
export const arrStats = {
	max: function(array: number[]) {
		return Math.max.apply(null, array)
	},

	min: function(array: number[]) {
		return Math.min.apply(null, array)
	},

	range: function(array: number[]) {
		return arrStats.max(array) - arrStats.min(array)
	},

	midrange: function(array: number[]) {
		return arrStats.range(array) / 2
	},

	sum,

	mean,

	average: mean,

	median: function(array: number[]) {
		array.sort(function(a, b) {
			return a - b
		})
		var mid = array.length / 2
		return mid % 1 ? array[mid - 0.5] : (array[mid - 1] + array[mid]) / 2
	},

	modes: function(array: number[]) {
		if (!array.length) return []
		var modeMap = {},
			maxCount = 0,
			modes = []

		array.forEach(function(val) {
			if (!modeMap[val]) modeMap[val] = 1
			else modeMap[val]++

			if (modeMap[val] > maxCount) {
				modes = [val]
				maxCount = modeMap[val]
			} else if (modeMap[val] === maxCount) {
				modes.push(val)
				maxCount = modeMap[val]
			}
		})
		return modes
	},

	variance: function(array: number[]) {
		var mean = arrStats.mean(array)
		return arrStats.mean(array.map(function(num) {
			return Math.pow(num - mean, 2)
		}))
	},

	standardDeviation: function(array: number[]) {
		return Math.sqrt(arrStats.variance(array))
	},

	meanAbsoluteDeviation: function(array: number[]) {
		var mean = arrStats.mean(array)
		return arrStats.mean(array.map(function(num) {
			return Math.abs(num - mean)
		}))
	},

	zScores: function(array: number[]) {
		var mean = arrStats.mean(array)
		var standardDeviation = arrStats.standardDeviation(array)
		return array.map(function(num) {
			return (num - mean) / standardDeviation
		})
	},
}

// Function aliases:
arrStats.average = arrStats.mean

export const tsNearlySame = (timeA: string, timeB: string) => timeB.startsWith(timeA.slice(0, timeA.length - 4)) // HACK: to quickly check if same second
