import { Applog, SearchContextWithLog } from './datom-types'
import { actualize } from './utils'

export interface ResultFields<T extends SearchContextWithLog[]> {
	nodes: T
	subscribe: (callback: (value: Result<T>) => void) => () => void
}

export class Result<T extends SearchContextWithLog[]> implements ResultFields<T> {
	nodes: T
	subscribe: (callback: (value: Result<T>) => void) => () => void

	constructor({ nodes, subscribe }: ResultFields<T>) {
		this.nodes = nodes
		this.subscribe = subscribe
	}

	get values() {
		return this.nodes.map(({ context }) => actualize(context, Object.keys(context) /* HACK: select? */))
	}
}

export class Datum<T> {
	value: T
	applog: Applog
	// subscribe: (callback: ((value: Result<T>) => void)) => (() => void)

	constructor({ value, applog }: Pick<Datum<T>, 'value' | 'applog'>) {
		this.value = value
		this.applog = applog
	}
}
