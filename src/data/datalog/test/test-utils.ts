import { omit } from 'lodash-es'
import { afterEach, beforeEach, expect } from 'vitest'
import { Applog } from '../datom-types'
import { ApplogForInsertOptionalAgent } from './../datom-types'

import { _resetGlobalState, autorun, configure } from 'mobx'
import type { Assertion, AsymmetricMatchersContaining } from 'vitest'
import { testMovieAtoms } from '../../test-applogs'
import { ApplogStream, ApplogStreamInMemory, WriteableApplogStream } from '../query/engine'
import { sortApplogsByTs } from '../utils'

configure({ disableErrorBoundaries: true }) // requires _resetGlobalState() in afterEach

interface CustomMatchers<R = unknown> {
	toBeApplogs(expected: ApplogForInsertOptionalAgent[]): R
}

declare module 'vitest' {
	interface Assertion<T = any> extends CustomMatchers<T> {}
	interface AsymmetricMatchersContaining extends CustomMatchers {}

	export interface TestContext {
		db: WriteableApplogStream
		autorunAndReturn: <R>(fn: () => R, opts?: Parameters<typeof autorun>[1]) => () => R
		insert: (applogs: ApplogForInsertOptionalAgent[], db?: WriteableApplogStream) => void
		disposers: Array<() => void>
	}
}

// interface MyFixtures {
//     db: ApplogStream
//     autorun: typeof autorun
//     disposers: Array<() => void>
// }
// const testExtended = test.extend<MyFixtures>({
//     db: null,
//     autorun: null,
//     disposers: [],
// })

beforeEach(async (context) => {
	context.disposers = []
	const testLogs = [...testMovieAtoms]
	sortApplogsByTs(testLogs)
	// context.db = new ApplogStreamIDB([...testMovieAtoms], 'test-movielogs')
	context.db = new ApplogStreamInMemory(testLogs, 'test-movielogs')
	context.autorunAndReturn = <R>(fn, opts) => {
		let ret: R
		const disposer = autorun(() => {
			ret = fn()
		}, opts)
		context.disposers.push(disposer)
		return () => ret
	}
	context.insert = (applogs, db = context.db) => {
		db.insert(applogs.map(applog => ({ ag: 'demoAgent', ...applog })))
	}
})
afterEach((context) => {
	_resetGlobalState()

	context.disposers.forEach(d => d())
	context.disposers = []
})

//////////////
// MATCHERS //
//////////////

expect.extend({
	toBeApplogs(received: Applog[] | ApplogStream, expected: ApplogForInsertOptionalAgent[]) {
		const { isNot } = this
		if (received instanceof ApplogStream) received = received.applogs
		expect(received).to.be.an('array')
		const sanitizedReceived = received.map(receivedObj => {
			let omitFields = []
			if (receivedObj.ag === 'demoAgent') {
				omitFields.push('ag')
			}
			// If the expected object doesn't have a 'ts' field, remove it from the received object
			if (!expected.some(expectedObj => 'ts' in expectedObj)) {
				omitFields.push('ts')
			}
			return omitFields.length ? omit(receivedObj, omitFields) : receivedObj
		})
		const sanitizedExpected = expected.map(expectedObj => {
			let omitFields = []
			if (expectedObj.ag === 'demoAgent') {
				omitFields.push('ag')
			}
			return omitFields.length ? omit(expectedObj, omitFields) : expectedObj
		})
		const pass = expect(sanitizedReceived).to.have.deep.members(sanitizedExpected) // HACK: this is chai syntax but we should return a boolean
		return {
			// do not alter your "pass" based on isNot. Vitest does it for you
			pass: pass,
			message: () => `Applogs are${isNot ? ' not' : ''} matching`,
		}
	},
})

// export function matchApplog = (receivedObj: Applog, expectedObj: ApplogForInsert) => {
//     if (!expectedObj.ts) {
//         receivedObj = omit(receivedObj, 'ts')
//     }
//     expect(receivedObj).to.have.deep.members([
//         expectedObj
//     ])
// };
