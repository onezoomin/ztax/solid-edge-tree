import type DatalogDB from './datalog-utils'
import type { DatalogQuery, DatalogStateIdentifier } from './datom-types'

export function subscribeToDatalogQuery<SELECT extends string>(
	db: DatalogDB,
	query: DatalogQuery<SELECT>,
	onUpdate: () => void,
	since?: DatalogStateIdentifier,
) {
	return db.subscribe(function subcriptionInvalidateCaller() {
		// VERBOSE('[subscribe] invalidating', { query, since, invalidate })
		onUpdate()
	})
}
