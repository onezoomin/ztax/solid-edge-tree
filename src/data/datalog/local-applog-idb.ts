import Dexie from 'dexie'
import type { Applog } from './datom-types'

export class Note3_AppLogDB extends Dexie {
	applogs!: Dexie.Table<Applog, string>
	// agents!: Dexie.Table<IEmailAddress, number>
	// config!: Dexie.Table<IPhoneNumber, number>

	constructor() {
		super('Note3_AppLogDB')

		//
		// Define tables and indexes
		// (Here's where the implicit table props are dynamically created)
		//
		this.version(2).stores({
			applogs: 'cid, en, at, vl, ag, ts',
		})
	}
}

export const appLogIDB = new Note3_AppLogDB()
