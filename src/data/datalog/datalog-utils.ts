import { Logger } from 'besonders-logger/src'
import type { Applog, ApplogForInsert, ApplogForInsertOptionalAgent, Timestamp } from './datom-types'
import { WriteableApplogStream } from './query/engine'
import { filterAndMap, withoutHistory } from './query/queries'
const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars
export const sortApplogByTs = (p, n) => (p.ts < n.ts) ? -1 : ((p.ts > n.ts) ? 1 : 0)
export const sortApplogByTsDec = (p, n) => (p.ts < n.ts) ? 1 : ((p.ts > n.ts) ? -1 : 0)

/* Fetch applogs for pattern & filter */
export function filterOnlyLatest(logs: Applog[], onlyLatest: boolean): Applog[] {
	if (!onlyLatest || !logs?.length) {
		return logs
	}
	logs = [...logs].sort((a, b) => a.ts < b.ts ? -1 : (a.ts == b.ts ? 0 : 1)) // TODO: optimize (e.g. by sorting applogs by default)
	const mapped = new Map()
	for (const log of logs) {
		if (!log) {
			continue
		}
		mapped.set(JSON.stringify([log.en, log.at]), log)
	}
	// VERBOSE('[filterOnlyLatest]', logs, mapped)
	return [...mapped.values()]
}

export function hasAg(log: ApplogForInsertOptionalAgent): log is ApplogForInsert {
	return !!log.ag
}
export function hasTs(log: ApplogForInsert): log is ApplogForInsert & { ts: Timestamp } {
	return !!log.ts
}
export function hasPv(log: ApplogForInsert): log is ApplogForInsert & { pv: string } {
	return !!log.pv
}

export function withTs(log: ApplogForInsert, ts: Timestamp) {
	return hasTs(log) ? log : { ...log, ts }
}
export function withPv(log: ApplogForInsert, ds: WriteableApplogStream) {
	const { en, at } = log
	const pvs = filterAndMap(withoutHistory(ds), { en, at }, 'cid')
	if (pvs.length > 1) WARN(`[withPv] unexpected result count:`, pvs.length)
	let pv = pvs.length ? pvs[0] : null
	const isMatchingPv = !!(pv === log.pv)
	if (log.pv && !isMatchingPv) WARN(`[withPv] different than pre-set pv:`, { queriedPv: pv, logPv: log.pv }) // TODO: this is actually a normal thing when e.g. pulling
	pv = log.pv ?? pv
	return { ...log, pv: pv ?? null }
}
