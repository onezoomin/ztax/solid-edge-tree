import { Logger } from 'besonders-logger/src'
import {
	_getGlobalState,
	_isComputingDerivation,
	comparer,
	computed,
	configure,
	getDependencyTree,
	getObserverTree,
	IComputedValue,
	IObservableArray,
	isAction,
	observable,
	onBecomeUnobserved,
	Reaction,
	runInAction,
	spy,
	trace,
	untracked,
} from 'mobx'
import { IComputedFnOptions } from 'mobx-utils/lib/computedFn'
import stringify from 'safe-stable-stringify'
import { DatalogQueryPattern } from './datom-types'
import { ApplogStream } from './query/engine'
import { QueryNodes } from './query/queries'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

// mobx linting - https://mobx.js.org/configuration.html#linting-options
configure({
	enforceActions: 'always',
	// disableErrorBoundaries: true // (i) this can be useful in case of exceptions to see where they were caused - https://mobx.js.org/configuration.html#disableerrorboundaries-boolean
	// computedRequiresReaction: true,
	// reactionRequiresObservable: true,
	// observableRequiresReaction: true,
})
// https://mobx.js.org/installation.html#use-spec-compliant-transpilation-for-class-properties
if (
	!new class {
		xyz
	}().hasOwnProperty('xyz')
) throw new Error('Transpiler is not configured correctly')

if (typeof window === 'object') {
	// @ts-expect-error window.
	window.getDependencyTree = getDependencyTree
	// @ts-expect-error window.
	window.getObserverTree = getObserverTree
}

VERBOSE.isDisabled || spy(event => {
	VERBOSE(`[mobx]`, event)
})

export const createDebugName = ({ caller, stream, pattern: args }: {
	caller?: string
	stream?: ApplogStream
	pattern?: DatalogQueryPattern | string // ? | any
}) => {
	const str = `${stream?.name ? stream.name + ' | ' : ''}${caller ?? 'caller?'}${
		args ? `{${typeof args === 'string' ? args : stringify(args)}}` : ''
	}`
	return str
}
export const createDebugNameObj = (args: Parameters<typeof createDebugName>[0]) => {
	return { name: createDebugName(args) } as const
}

export function computedStructuralComparer<T>(a: IComputedValue<T>, b: IComputedValue<T>) {
	return untracked(() => comparer.structural(a.get(), b.get()))
}
export function applogStreamComparer(a: ApplogStream, b: ApplogStream) {
	return comparer.shallow(a.applogs, b.applogs)
}
export function queryNodesComparer(a: QueryNodes, b: QueryNodes) {
	if (a.size !== b.size) return false
	for (let i = 0; i < a.size; i++) {
		const nodeA = a.nodes[i]
		const nodeB = b.nodes[i]
		VERBOSE(`queryNodesComparer`, i, nodeA, nodeB)
		if (!comparer.structural(nodeA.variables, nodeB.variables)) {
			return false
		}
		if (!applogStreamComparer(nodeA.logsOfThisNode, nodeB.logsOfThisNode)) {
			return false
		}
	}
	return true
}

let observableArrayMapID = 0
export function observableArrayMap<T>(fn: () => T[], { name, equals }: {
	name?: string
	equals?: typeof comparer.structural
} = {}) {
	// if (!name) name = `${fn.name}_${fn.pattern}`
	VERBOSE(`[observableArrayMap] create`, { fnname: fn.name, fn, name, equals })
	const debugName = `observableArrayMap@${++observableArrayMapID}{${name}}`

	let observableArr: IObservableArray<T>
	let items

	function runAndTrack() {
		reaction.track(() => {
			DEBUG(`[${debugName}] runAndTrack:`, { name: name ?? fn.name, fn })
			items = fn()
			VERBOSE(`[${debugName}] runAndTrack =>`, items)
		})
	}

	const reaction = new Reaction(debugName, (...args) => {
		VERBOSE(`[${debugName}] reaction.invalidate`, args)
		runAndTrack()
		runInAction(() => {
			if (!(equals ?? comparer.structural)(observableArr, items)) {
				observableArr.replace(items)
			}
		})
	})
	runAndTrack() // initial run

	observableArr = observable.array(items, /* ['NEVER'] as T[] */ { deep: false, name })
	VERBOSE(`[${debugName}] deps of reaction`, getDependencyTree(reaction), 'array:', getDependencyTree(observableArr), {
		reaction,
		observableArr,
	})

	// THIS STRATEGIES DON'T WORK BECAUSE:
	// - Affected reactions run by default immediately (synchronously) if an observable is changed.
	// However, they won't run before the end of the current outermost (trans)action.
	// (https://mobx.js.org/reactions.html#rules)

	// const disposer = reaction(
	//     () => {
	//         const items = fn();
	//         VERBOSE(`[observableArrayMap] reaction.check`, items)
	//         return items
	//     }, // This function defines what to track
	//     (items, reaction) => {
	//         VERBOSE(`[observableArrayMap] reaction.react`, items, reaction)
	//         runInAction(() => observableArr.replace(items))
	//     },
	//     {
	//         fireImmediately: true // This ensures the reaction runs immediately upon creation
	//     }
	// );

	// const disposer = autorun(() => {
	//     const newItems = fn();
	//     VERBOSE(`[observableArrayMap] items:`, newItems)
	//     runInAction(() => observableArr.replace(newItems))
	// })

	const unobserveUnobserving = onBecomeUnobserved(observableArr, () => {
		VERBOSE(`[observableArrayMap] dispose`)
		// disposer()
		reaction.dispose()
		unobserveUnobserving()
	})
	return observableArr
}

/**
 * ADAPTED from https://github.com/mobxjs/mobx-utils/blob/362cbbfb384820d416f253eddc532ebecba89bcb/src/computedFn.ts#L50 to
 * - deep-compare arguments
 * - allow dynamic/optional args
 * - not be so friggin complicated... DeepMap... closest... why?!
 *
 * // TODO: PR upstream
 *
 * computedFnDeepCompare takes a function with an arbitrary amount of arguments,
 * and memoizes the output of the function based on the arguments passed in.
 *
 * computedFnDeepCompare(fn) returns a function with the very same signature. There is no limit on the amount of arguments
 * that is accepted.
 *
 * By default the output of a function call will only be memoized as long as the
 * output is being observed.
 *
 * The function passes into `computedFnDeepCompare` should be pure, not be an action and only be relying on
 * observables.
 *
 * Setting `keepAlive` to `true` will cause the output to be forcefully cached forever.
 * Note that this might introduce memory leaks!
 *
 * @example
 * const store = observable({
    a: 1,
    b: 2,
    c: 3,
    m: computedFnDeepCompare(function(x) {
      return this.a * this.b * x
    })
  })

  const d = autorun(() => {
    // store.m(3) will be cached as long as this autorun is running
    console.log(store.m(3) * store.c)
  })
 *
 * @param fn
 * @param keepAliveOrOptions
 */
export function computedFnDeepCompare<T extends (...args: any[]) => any>(
	fn: T,
	keepAliveOrOptions: (IComputedFnOptions<T> & { name?: string; argsDebugName?: (...args: Parameters<T>) => string }) | boolean = false,
): T {
	if (isAction(fn)) throw new Error("computedFnDeepCompare shouldn't be used on actions")

	let memoWarned = false
	let i = 0
	const opts = typeof keepAliveOrOptions === 'boolean'
		? { keepAlive: keepAliveOrOptions }
		: keepAliveOrOptions
	const map = new Map<Parameters<T>, IComputedValue<any>>()

	return function(this: any, ...args: Parameters<T>): ReturnType<T> {
		let existing
		const untrackedArgs = untracked(() => args)
		const debugName = `computedFnDeepCompare(${opts.name || fn.name}#${++i})${opts.argsDebugName ? `{${opts.argsDebugName(...args)}}` : ''}`

		for (let [existingArgs, computation] of map.entries()) {
			if (comparer.structural(existingArgs, untrackedArgs)) {
				existing = computation
				break
			}
		}
		// cache hit, return
		if (existing) {
			const cachedResult = existing.get()
			VERBOSE(`[${debugName}] cache hit`, { untrackedArgs, cachedResult })
			return cachedResult
		}

		// if function is invoked, and its a cache miss without reactive, there is no point in caching...
		if (!opts.keepAlive && !_isComputingDerivation()) {
			if (!memoWarned && _getGlobalState().computedRequiresReaction) {
				console.warn(
					"invoking a computedFn from outside an reactive context won't be memoized, unless keepAlive is set",
				)
				memoWarned = true
			}
			return fn.apply(this, args)
		}

		// create new entry
		VERBOSE(`[${debugName}] new computation`, { untrackedArgs })
		let latestValue: ReturnType<T> | undefined
		const computation = computed(
			() => {
				VERBOSE(`[${debugName}] update->rerun`, untracked(() => ({ args })), fn)
				VERBOSE.isDisabled || trace()
				return (latestValue = fn.apply(this, args))
			},
			{
				...opts,
				name: debugName,
			},
		)
		VERBOSE(`[${debugName}] deps`, getDependencyTree(computation), { untrackedArgs })
		map.set(untrackedArgs, computation)

		// clean up if no longer observed
		if (!opts.keepAlive) {
			const unobserveUnobserving = onBecomeUnobserved(computation, () => {
				VERBOSE(`[${debugName}] dispose`, computation)
				map.delete(untrackedArgs)
				if (opts.onCleanup) opts.onCleanup(latestValue, ...args)
				latestValue = undefined
				unobserveUnobserving()
			})
		}
		// return current val
		return computation.get()
	} as any
}

export function prettifyStreamName(input: string): string {
	let depth = 0
	let result = ''
	let insideCurlyBraces = 0

	for (let i = 0; i < input.length; i++) {
		const char = input[i]

		if (char === '(') {
			result += char + '\n' + '\t'.repeat(++depth)
		} else if (char === ')') {
			result += '\n' + '\t'.repeat(--depth) + char
		} else if (char === ',' && insideCurlyBraces === 0) {
			result += char + '\n' + '\t'.repeat(depth)
		} else if (char === '{' && insideCurlyBraces === 0) {
			insideCurlyBraces++
			result += char + '\n' + '\t'.repeat(depth + 1)
		} else if (char === '}' && insideCurlyBraces === 1) {
			insideCurlyBraces--
			result += '\n' + '\t'.repeat(depth) + char
		} else if (char === '{' && insideCurlyBraces > 0) {
			insideCurlyBraces++
			result += char
		} else if (char === '}' && insideCurlyBraces > 1) {
			insideCurlyBraces--
			result += char
		} else {
			result += char
		}
	}

	return result
}
