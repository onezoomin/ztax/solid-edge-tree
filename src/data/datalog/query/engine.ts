import {
    action,
    computed,
    getDependencyTree,
    IObservableArray,
    makeObservable,
    observable,
    ObservableMap,
    ObservableSet,
    onBecomeUnobserved,
    reaction,
    toJS,
    untracked,
} from 'mobx'

import { Logger } from 'besonders-logger/src'
import { defer, sortedIndexBy } from 'lodash-es'
import stringify from 'safe-stable-stringify'
import { encodeApplog, encodeApplogAndGetCid } from '../../../ipfs/ipfs-utils'
import { arraysContainSameElements, dateNowIso } from '../../Utils'
import { withPv, withTs } from '../datalog-utils'
import { Applog, ApplogForInsert, DatalogQueryPattern, getApplogTypeErrors, isValidApplog } from '../datom-types'
import { appLogIDB } from '../local-applog-idb'
import { applogStreamComparer, computedFnDeepCompare, prettifyStreamName } from '../mobx-utils'
import { areApplogsEqual, isTsBefore, matchPartStatic, removeDuplicateAppLogs, sortApplogsByTs } from '../utils'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO, { prefix: '[db]' }) // eslint-disable-line no-unused-vars

export type ApplogStreamEvent = { added: readonly Applog[]; removed: readonly Applog[] | null } | { init: readonly Applog[] }
export type ApplogEventMapper = (this: MappedApplogStream, event: ApplogStreamEvent, sourceStream: ApplogStream) => ApplogStreamEvent
export function isInitEvent(event: ApplogStreamEvent): event is { init: readonly Applog[] } {
	return event['init'] !== undefined
}

export abstract class ApplogStream {
	readonly filters: string[]
	readonly parents: ApplogStream[] | null
	protected _subscribers: ((event: ApplogStreamEvent) => void)[] = []

	constructor(
		parents: ApplogStream | ApplogStream[] | null,
		filters: string[],
		protected _applogs: Applog[] = [],
		readonly name: string, /* = null */
	) {
		this.parents = parents && !Array.isArray(parents) ? [parents] : (parents as ApplogStream[])
		this.filters = filters // ? uniq([...parents?.map(p => p.filters), filters])
		if (this.parents?.length === 0) throw new Error(`Unhandled: empty parents`)
		makeObservable(this, {
			// applogs: computed, //observable.shallow,
			// applogsSorted: computed,
			size: computed,
			isEmpty: computed,
			firstLog: computed,
			latestLog: computed,
		}, { name: `Stream{${name}}` })
	}

	subscribe(callback: (event: ApplogStreamEvent) => void) {
		this._subscribers.push(callback)
		return this.unsubscribe.bind(this, callback)
	}

	unsubscribe(callback: (event: ApplogStreamEvent) => void) {
		const index = this._subscribers.indexOf(callback)
		if (index !== -1) {
			this._subscribers.splice(index, 1)
		} else WARN(`unsubscribe called for non-existent`, callback)
	}

	protected notifySubscribers(event: ApplogStreamEvent) {
		DEBUG(`[stream: ${this.name}] notifying`, this._subscribers.length, 'subscribers of', { ...event, subs: this._subscribers })
		for (const subscriber of this._subscribers) {
			subscriber(event)
		}
	}

	get applogs(): readonly Applog[] /* (i) only type hint, not actually immutable */ {
		// VERBOSE.isDisabled || trace()
		return this._applogs
	}

	public map<R>(fn: (applog: Applog) => R) {
		if (!this.applogs.map) ERROR(`applogs arr?!`, this.applogs)
		return this.applogs.map(fn)
	}

	get firstLog() {
		return this.applogs[0]
	}
	get latestLog() {
		return this.applogs[this.applogs.length - 1]
	}
	public hasApplog(applog: Applog, byRef: boolean) {
		if (byRef) {
			return this.applogs.includes(applog)
		} else {
			const keySet = Object.keys(applog) // HACK: sanity check to catch bugs
			return !!this.applogs.find(log => {
				if (!arraysContainSameElements(keySet, Object.keys(log))) {
					throw ERROR(`[hasApplog] field set mismatch:`, { applog, log })
				}
				return areApplogsEqual(log, applog)
			})
		}
	}

	public hasApplogWithDiffTs(applog: ApplogForInsert) {
		// HACK this is basically as inefficient as it gets
		return this.applogs.find(existing => (
			existing.en === applog.en
			&& existing.at === applog.at
			&& existing.vl === applog.vl
			&& existing.ag === applog.ag
		))
	}

	get isEmpty() {
		return this.size === 0
	}
	get size() {
		return this.applogs.length
	}
	get untrackedSize() {
		return untracked(() => this.size)
	}
	get nameAndSizeUntracked() {
		return untracked(() => `${this.name} (${this.size})`)
	}
	get prettyName() {
		return prettifyStreamName(this.name)
	}
}

export abstract class WriteableApplogStream extends ApplogStream {
	constructor(
		parents: ApplogStream | ApplogStream[] | null,
		filters: string[],
		applogs: Applog[] = [],
		name: string,
	) {
		super(parents, filters, applogs, name)

		makeObservable(this, {
			insert: action, // ? is there an advantage to do this here instead of wrapping the fx in action below?
		})
	}

	public insert(appLogsToInsert: ApplogForInsert[]) {
		const ts = dateNowIso()

		const mapped = appLogsToInsert.map(log => {
			const logWithTs = withTs(log, ts)
			if (!isValidApplog(logWithTs)) throw ERROR(`Bogus Applog ${JSON.stringify(logWithTs)}`, getApplogTypeErrors(logWithTs))
			const logWithPv = withPv(logWithTs, this)
			const cid = encodeApplogAndGetCid(logWithPv).toString()
			const logWithCid = { ...logWithPv, cid } satisfies Applog
			return Object.freeze(logWithCid)
		})
		const mappedLogs = removeDuplicateAppLogs(mapped)
		if (appLogsToInsert.length !== mappedLogs.length) {
			WARN('request to insert duplicate log, inserting mappedLogs:', { appLogsToInsert, mappedLogs })
		} else if (!appLogsToInsert.length) {
			WARN('request to insert empty logs array')
		} else {
			LOG('Inserting:', mappedLogs.length === 1 ? mappedLogs[0] : mappedLogs, { ds: this })
		}
		if (!mappedLogs.length) return []

		sortApplogsByTs(mappedLogs)
		const sortNeeded = this._applogs.length && isTsBefore(mappedLogs[0], this._applogs[this._applogs.length - 1])
		this._applogs.push(...mappedLogs)
		if (sortNeeded) {
			sortApplogsByTs(this._applogs)
		}
		this.notifySubscribers({ added: mappedLogs, removed: null })

		// ? persist sync
		void this.persist(mappedLogs)
		return mappedLogs as Applog[]
	}

	protected abstract persist(logs: Applog[])
}
export class ApplogStreamInMemory extends WriteableApplogStream {
	constructor(
		applogs: Applog[],
		filters: string[],
		name: string,
		parents: ApplogStream | ApplogStream[] = null,
	) {
		super(parents, filters, applogs, name)

		makeObservable(this, {
			// @ts-expect-error bc it's private
			_applogs: observable.shallow,
		})
	}

	protected persist(logs: Applog[]) {
		VERBOSE(`[InMem.persist] no persist for`, logs)
	}
}

export class ApplogStreamIDB extends WriteableApplogStream {
	constructor(
		applogs: Applog[] = [],
		filters: string[],
		name: string,
	) {
		super(null, filters, applogs, name)

		makeObservable(this, {
			// @ts-expect-error bc it's private
			_applogs: observable.shallow,
		})
	}

	protected async persist(logs: Applog[]) {
		// when we get the cid, do we want to drop the pv and cid props
		// i mean what content do we want to address with this cid?
		defer(async () => {
			DEBUG(`[IDB.persist] persisting`, logs)
			if (localStorage.getItem('note3.dev.noPersist')) {
				return DEBUG(`[IDB.persist] noPersist`)
			}
			appLogIDB.applogs?.bulkPut(
				await Promise.all(logs.map(async (eachLog) => {
					const cid = (encodeApplogAndGetCid(eachLog)).toString() // TODO: skip if existing - if we're sure this ain't happening:
					if (eachLog.cid && eachLog.cid !== cid) {
						ERROR(`In-mem CID is different than when persisting`, { eachLog, cid, encoded: encodeApplog(eachLog) })
					}
					return ({ ...eachLog, cid })
				})),
			)
		})
	}
}

export class MappedApplogStream extends ApplogStream {
	private _parentSubscriptions: Map<ApplogStream, ((event: ApplogStreamEvent) => void)> = new Map()

	constructor(
		parents: ApplogStream | ApplogStream[],
		filters: string[],
		readonly _initialLogs: readonly Applog[],
		private _eventMapper: ApplogEventMapper,
		readonly name: string,
	) {
		// if (!isObservableArray(_initialLogs)) WARN(`[MappedApplogStream: ${name}] initialized with non-observable array`, _initialLogs)
		super(
			parents,
			filters,
			observable.array([..._initialLogs], { deep: false, name: `${name}.array` }), // ? not sure if array clone is totally necessary - but I also don't want to risk weird bugs
			name,
		)

		makeObservable(this, {
			// @ts-expect-error bc it's private
			onParentUpdate: action,
		})

		this.subscribeToParent()
	}

	private subscribeToParent() {
		if (!this.parents.length) WARN(`MappedStream has no parents`, this)
		if (this._parentSubscriptions.size) throw ERROR(`parents subs must not be called twice`, this)
		VERBOSE(`[MappedStream: ${this.name}] subscribing to parents:`, this.parents.map(p => p.name))
		this.parents.forEach(p => {
			const sub = this.onParentUpdate.bind(this, p)
			p.subscribe(sub)
			this._parentSubscriptions.set(p, sub)
		})

		// TODO: how to check when to dispose this?
		// onBecomeUnobserved(this as unknown as IObservable, '_applogs', () => {
		//     DEBUG(`[MappedStream: ${this.name}] disposed - unsubscribing from parents:`, { parents: this.parents, subs: this._parentSubscriptions })
		//     if (this._subscribers.length) WARN(`[MappedStream: ${this.name}] disposed but got subscriptions:`, this._subscribers)
		//     this.parents.forEach(p => {
		//         const sub = this.onParentUpdate.bind(this, p)
		//         p.unsubscribe(sub)
		//         this._parentSubscriptions.delete(p)
		//     })
		// })
	}

	/**
	 * // HACK to trigger remap on pattern change in rollingFilter
	 * should not be used lightly
	 */
	triggerRemap() {
		VERBOSE(`MappedStream{${this.nameAndSizeUntracked}} triggerRemap`)
		this.parents.forEach(p => {
			this.onParentUpdate(p, { init: untracked(() => toJS(p.applogs)) })
		})
	}

	protected onParentUpdate(stream: ApplogStream, event: ApplogStreamEvent) {
		VERBOSE(`MappedStream{${this.nameAndSizeUntracked}} parentUpdate`, event)
		const mapResult = this._eventMapper(event, stream)
		VERBOSE(`MappedStream{${this.nameAndSizeUntracked}} parentUpdate => mapped`, mapResult)
		if (isInitEvent(mapResult)) {
			;(this._applogs as IObservableArray<Applog>).replace([...mapResult.init])
		} else {
			for (const log of mapResult.added) {
				// insert at right location to maintain sort order
				this._applogs.splice(sortedIndexBy(this._applogs, log, 'ts'), 0, log)
			}
			if (mapResult.removed) {
				for (const toRemove of mapResult.removed) {
					if (!(this._applogs as IObservableArray<Applog>).remove(toRemove)) {
						if (!isInitEvent(event) && event.removed?.includes(toRemove)) {
							DEBUG(`Ignoring remove event for non-existent because it was part of parent event's removed`, toRemove, event) // ? convenience? - or is this a code smell?
						} else {
							throw ERROR(`MappedStream{${this.name}} toRemove contained log that doesn't exist`, toRemove, {
								stream: this,
								event,
								mapResult,
							})
						}
					}
				}
			}
		}
		VERBOSE(`MappedStream{${this.nameAndSizeUntracked}} parentUpdate => deps?`, getDependencyTree(this._applogs))
		this.notifySubscribers(mapResult)
	}
}

export const rollingFilter = computedFnDeepCompare(function rollingFilter(
	stream: ApplogStream,
	pattern: DatalogQueryPattern,
	opts: { name?: string; extraFilterName?: string } = {},
) {
	let untrackedPattern = getUntrackedPattern(pattern) // updated in reaction
	let filter = makeFilter(untrackedPattern) // updated in reaction

	// we are not tracking the filter results here as we are subscribing explicitly to the stream below
	const observableArr = observable.array(
		untracked(() => filter(stream.applogs)),
		{ deep: false, name: `${stream.name} | ${opts.name || `rollingFilter.array{${stringify(untrackedPattern)}}`}` },
	)
	const filterAdded: ApplogEventMapper = (event) => {
		let mappedEvent: ApplogStreamEvent
		if (isInitEvent(event)) {
			mappedEvent = { init: filter(event.init) }
		} else {
			mappedEvent = {
				added: filter(event.added),
				removed: event.removed, // whatever's removed shall be removed
			}
		}
		VERBOSE(
			`rollingFilter{${stream.nameAndSizeUntracked} | ${opts.name ? ` '${opts.name}'}` : ''} parentUpdate`,
			untrackedPattern,
			event,
			'=>',
			mappedEvent,
		)
		// console.trace('rollingFilter.addFiltered', getObserverTree(observableArr))
		// trace()
		return mappedEvent
	}
	const mappedStream = new MappedApplogStream(
		stream,
		[...stream.filters, ...(opts.extraFilterName ? [opts.extraFilterName] : [])],
		observableArr,
		filterAdded,
		`${stream.name} | ${opts.name || `rollingFilter{${stringify(untrackedPattern)}}`}`,
	)

	// observe pattern manually and invalidate stream in case it changes
	const cancelPatternReaction = reaction(() => JSON.parse(JSON.stringify(pattern)), (pat) => {
		VERBOSE(
			`rollingFilter<${stream.nameAndSizeUntracked}>${opts.name ? ` '${opts.name}'` : ''} patternUpdate`,
			untrackedPattern,
			'=>',
			pat, /* , getDependencyTree(pat) */
		)
		untrackedPattern = pat
		filter = makeFilter(untrackedPattern)
		mappedStream.triggerRemap() // HACK
	})

	const unobserveUnobserving = onBecomeUnobserved(observableArr, () => {
		VERBOSE(`rollingFilter<${stream.nameAndSizeUntracked}>${opts.name ? ` '${opts.name}'` : ''} unobserve`, untrackedPattern)
		cancelPatternReaction()
		unobserveUnobserving()
	})

	return mappedStream
}, { equals: applogStreamComparer, argsDebugName: (_, pattern, opts) => `${stringify(pattern)}, ${stringify(opts)}` })

export const rollingMapper = computedFnDeepCompare(function rollingMapper(
	stream: ApplogStream,
	eventMapper: ApplogEventMapper,
	opts: { name?: string; extraFilterName?: string } = {},
) {
	const initialMapResult = untracked(() => eventMapper.call(null, { init: stream.applogs }, stream))
	if (!isInitEvent(initialMapResult)) throw ERROR('Initial run must return init event')
	const initialLogs = initialMapResult.init

	return new MappedApplogStream(
		stream,
		[...stream.filters, ...(opts.extraFilterName ? [opts.extraFilterName] : [])],
		initialLogs,
		eventMapper,
		`${stream.name} | ${opts.name || `rollingMapper`}`,
	)
}, { equals: applogStreamComparer, argsDebugName: (_, pattern, opts) => `${stringify(pattern)}, ${stringify(opts)}` })

export const rollingAcc = computedFnDeepCompare(function rollingAcc<ACC extends IObservableArray | ObservableMap | ObservableSet>(
	stream: ApplogStream,
	acc: ACC,
	eventMapper: (event: ApplogStreamEvent, acc: ACC) => void,
) {
	eventMapper = action(eventMapper)
	eventMapper({ init: stream.applogs }, acc)
	stream.subscribe(event => {
		eventMapper(event, acc)
	})

	return acc
}, { argsDebugName: (stream) => `rollingAcc{${stream.nameAndSizeUntracked}}` })

// function rollingFilter(
//     db: ApplogStreamIDB,
//     filter: (logs: Applog[]) => Applog[],
//     name: string,
// ) {
//     const filtered = observable.array(filter(untracked(() => db.applogs)), { deep: false });

//     const observableArr = rollingFilter(filter, db)
//     const addFiltered = /* action( */(added: Applog[]) => {
//         VERBOSE(`rollingFilter<${db.nameAndSize}>.update`, pattern, added)
//         // filter(added).forEach(log => result.push(log)) //? no bulk insert?
//         observableArr.push(...added)
//     }/* ) */;

//     db.subscribe(addFiltered);
//     onBecomeUnobserved(filtered, () => {
//         VERBOSE(`rollingFilter<${db.nameAndSize}>.unobserve`, filter)
//         db.unsubscribe(addFiltered);
//     });
//     return new ApplogStream(filtered, `${db.name} | ${name}`)
// }

export const getUntrackedPattern = function getUntrackedPattern(
	pattern: DatalogQueryPattern,
) {
	const untrackedPattern = untracked(() => Object.fromEntries(Object.entries(toJS(pattern)).map(([k, v]) => [toJS(k), toJS(v)])))
	if (!Object.entries(untrackedPattern).length) throw new Error(`Pattern is empty`)
	return untrackedPattern
}
export function makeFilter(
	pattern: DatalogQueryPattern,
) {
	return (logs: readonly Applog[]) =>
		logs.filter(applog => {
			for (const [field, patternValue] of Object.entries(pattern)) {
				const applogValue = applog[field.startsWith('!') ? field.slice(1) : field]
				if (!matchPartStatic(field as keyof Applog, patternValue, applogValue)) {
					return false
				}
			}
			return true
		})
}

/**
 * // ! think twice before using
 */
export const getUntrackedFilterResults = function getUntrackedFilterResults(
	stream: ApplogStream,
	pattern: DatalogQueryPattern,
	opts: { name?: string } = {},
) {
	const untrackedPattern = getUntrackedPattern(pattern)
	const filter = makeFilter(untrackedPattern)
	return untracked(() => filter(stream.applogs))
}

///////////////////////////
// FILTERED STREAM TYPES //
///////////////////////////
export type ApplogStreamWithFilter<T extends string> = ApplogStream & {
	filters: T[]
}
export function hasFilter<T extends string>(stream: ApplogStream, filter: T): stream is ApplogStreamWithFilter<T> {
	return stream.filters.includes(filter)
}
export function assertRaw(stream: ApplogStream) /* : stream is ApplogStreamWithoutFilters */ {
	if (stream.filters.length) throw ERROR(`should be unfiltered stream, but is:`, stream.filters)
	return stream as ApplogStreamWithoutFilters
}
export function assertOnlyCurrent(stream: ApplogStream) /* : stream is ApplogStreamOnlyCurrent */ {
	if (
		!hasFilter(stream, 'withoutHistory') ||
		!hasFilter(stream, 'withoutDeleted')
	) throw ERROR(`should be filtered stream, but is:`, stream.filters)
	return stream
}
export type ApplogStreamOnlyCurrent = ApplogStreamWithFilter<'withoutHistory' | 'withoutDeleted'>
export type ApplogStreamWithoutFilters = ApplogStream & { filters: [] }
