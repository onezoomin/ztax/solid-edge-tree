import { Logger } from 'besonders-logger/src'
import { autorun, comparer, toJS } from 'mobx'
import { Applog } from '../datom-types'
import { computedFnDeepCompare, createDebugName, observableArrayMap } from '../mobx-utils'
import { ApplogStream, ApplogStreamInMemory } from './engine'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

export interface DivergenceLeaf {
	log: Applog
	stream: ApplogStream
}

export const queryDivergencesByPrev = computedFnDeepCompare(function queryConflictingByPrev(
	sourceStream: ApplogStream,
) {
	DEBUG(`queryDivergencesByPrev<${sourceStream.nameAndSizeUntracked}>`)
	if (sourceStream.filters.includes('withoutHistory')) WARN(`queryDivergencesByPrev on stream withoutHistory`, sourceStream)

	const divergences = observableArrayMap(() => {
		const logsForNode = new Map<string, Applog[]>()
		const leafs = new Set<string>()
		VERBOSE('all applogs:', sourceStream.applogs)
		for (const log of sourceStream.applogs) {
			let prevLogs
			if (log.pv) {
				prevLogs = log.pv && logsForNode.get(log.pv.toString())
				leafs.delete(log.pv.toString())
			}
			VERBOSE('traversing log', { log, prevLogs, leafs: Array.from(leafs) })
			logsForNode.set(log.cid, prevLogs ? [...prevLogs, log] : [log])
			leafs.add(log.cid)
		}
		return Array.from(leafs).map(leafID => {
			const stream = new ApplogStreamInMemory(
				logsForNode.get(leafID),
				sourceStream.filters,
				createDebugName({
					caller: 'DivergenceLeaf',
					stream: sourceStream,
					pattern: `leaf: ${leafID}`,
				}),
				sourceStream,
			)
			return ({ log: stream.latestLog, stream })
		})
	}, { name: createDebugName({ caller: 'queryDivergencesByPrev', stream: sourceStream }) })
	VERBOSE.isDisabled || autorun(() => VERBOSE(`[queryDivergencesByPrev] result:`, toJS(divergences)))
	return divergences
}, { equals: comparer.structural })
