import { Applog, ApplogValue, DatalogQueryPattern, EntityID, SearchContext } from '../datom-types'

import { Logger } from 'besonders-logger/src'
import { autorun, comparer, computed, makeObservable, observable, onBecomeObserved, toJS, untracked } from 'mobx'

import stringify from 'safe-stable-stringify'
import {
	applogStreamComparer,
	computedFnDeepCompare,
	computedStructuralComparer,
	createDebugName,
	observableArrayMap,
	queryNodesComparer,
} from '../mobx-utils'
import { joinStreams, resolveOrRemoveVariables, sortApplogsByTs } from '../utils'
import { ApplogStream, ApplogStreamEvent, ApplogStreamInMemory, isInitEvent, rollingFilter, rollingMapper } from './engine'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO, { prefix: '[q]' }) // eslint-disable-line no-unused-vars

// util.inspect.defaultOptions.depth = 5;

// export interface QueryExecutorArguments {
//     db: ApplogStream
//     // applogs: AppLog[]
//     nodes: SearchContextWithLog[]
// }
// export interface QueryExecutorResult {
//     // applogs: AppLog[]
//     nodes: SearchContextWithLog[]
// }
// export type QueryExecutor = (args: QueryExecutorArguments) => QueryExecutorResult

export class QueryNode {
	constructor(
		public logsOfThisNode: ApplogStream,
		public variables: SearchContext,
		public prev: QueryNode | null = null,
	) {
		makeObservable(this, {
			allApplogs: computed, // ? intuitively only put the ones here that felt expensive to compute (join)
		})
	}
	get record() {
		return this.variables // alias for end-user consumption
	}

	get allApplogs() {
		if (!this.prev) return this.logsOfThisNode
		return joinStreams([
			this.logsOfThisNode,
			this.prev.allApplogs,
		])
	}
}
export class QueryNodes {
	constructor(
		public nodes: Array<QueryNode>,
	) {
		makeObservable(this, {
			allApplogs: computed, // ? intuitively only put the ones here that felt expensive to compute (join)
			size: computed, // ... or cheap to cache
			isEmpty: computed,
		})
	}

	get size() {
		return this.records.length
	}
	get isEmpty() {
		return this.records.length === 0
	}
	get untrackedSize() {
		return untracked(() => this.records.length)
	}

	get records() {
		return this.nodes.map(({ variables }) => variables)
	}
	get applogSets() {
		return this.nodes.map(({ logsOfThisNode: stream }) => stream.applogs)
	}
	get applogStreams() {
		return this.nodes.map(({ logsOfThisNode: stream }) => stream)
	}
	get allApplogs() {
		return joinStreams(this.nodes.map(node => node.allApplogs))
	}
}

/////////////
// QUERIES //
/////////////

export const withoutHistory = computedFnDeepCompare(function withoutHistory(
	stream: ApplogStream,
	{ inverseToOnlyReturnFirstLogs, tolerateAlreadyFiltered }: {
		inverseToOnlyReturnFirstLogs?: boolean
		tolerateAlreadyFiltered?: boolean
	} = {},
) {
	VERBOSE(`withoutHistory${inverseToOnlyReturnFirstLogs ? '.inversed' : ''} < ${stream.nameAndSizeUntracked} > initializing`)
	// if (stream.name.includes('withoutHistory')) WARN(`stream already contains withoutHistory:`, stream.name)
	if (stream.filters.includes('withoutHistory')) {
		if (tolerateAlreadyFiltered) {
			DEBUG(`[withoutHistory] already filtered, but tolerateAlreadyFiltered=true, so returning`)
			return stream
		}
		throw ERROR(`stream already filtered withoutHistory:`, stream.filters, { name: stream.name })
	}

	let rollingMap: Map<string, Applog>
	const mappedStream = rollingMapper(stream, function(event, sourceStream) {
		const isInitial = isInitEvent(event)

		let newLogs: readonly Applog[]
		const toAdd = [] as Applog[]
		const toRemove = isInitial ? null : [] as Applog[]
		if (isInitial) {
			rollingMap = new Map()
			newLogs = event.init
		} else {
			newLogs = event.added
		}

		let tsCheck: string
		for (
			let i = inverseToOnlyReturnFirstLogs ? 0 : newLogs.length - 1;
			inverseToOnlyReturnFirstLogs ? i < newLogs.length : i >= 0;
			inverseToOnlyReturnFirstLogs ? i++ : i--
		) {
			const log = newLogs[i]
			const key = stringify([log.en, log.at])

			if (tsCheck && (inverseToOnlyReturnFirstLogs ? tsCheck > log.ts : tsCheck < log.ts)) {
				throw ERROR(`withoutHistory.mapper logs not sorted:`, tsCheck, inverseToOnlyReturnFirstLogs ? '>' : '<', log.ts, {
					log,
					i,
					newLogs,
					inverseToOnlyReturnFirstLogs,
				})
			}
			tsCheck = log.ts

			const existing = rollingMap.get(key)
			if (!existing || (inverseToOnlyReturnFirstLogs ? (existing.ts > log.ts) : (existing.ts < log.ts))) {
				if (existing && !isInitial) toRemove.push(existing)
				toAdd.push(log)
				rollingMap.set(key, log)
			}
		}
		sortApplogsByTs(toAdd) // HACK: find logical solution
		VERBOSE.isDisabled ||
			VERBOSE(
				`withoutHistory${inverseToOnlyReturnFirstLogs ? '.inversed' : ''}<${stream.nameAndSizeUntracked}> mapped event`,
				isInitial ?
					{ ...Object.fromEntries(Object.entries(event).map(([k, v]) => [k, v?.length])), toAdd: toAdd.length, toRemove } :
					{ ...event, toAdd, toRemove },
			)
		return isInitial ?
			{ init: toAdd }
			: { added: toAdd, removed: toRemove }
	}, { name: `withoutHistory${inverseToOnlyReturnFirstLogs ? '.inversed' : ''}`, extraFilterName: 'withoutHistory' })
	VERBOSE.isDisabled || autorun(() => {
		VERBOSE(`withoutHistory<${stream.nameAndSizeUntracked}> filtered down to`, mappedStream.applogs.length) // using applogs.length, as size might not change, but we still want a log
	})
	return mappedStream
	// const filtered = observableArrayMap(() => {
	//     VERBOSE(`withoutHistory stream deps:`, getDependencyTree(stream.applogs), stream)
	//     stream.applogs.forEach(applog => {
	//         const key = stringify([applog.en, applog.at])
	//         const existing = mapped.get(key)
	//         if (!existing || existing.ts < applog.ts)
	//             mapped.set(key, applog)
	//     })
	//     VERBOSE(`[withoutHistory] mapped:`, mapped.size)
	//     return Array.from(mapped.values())
	// }, { name: obsArrMapName })
	// VERBOSE(`withoutHistory deps of filteredArr:`, getDependencyTree(filtered))
	// return new MappedApplogStream(stream, filtered, `${stream.name} | withoutHistory`)
}, { equals: applogStreamComparer })

export const withoutDeleted = computedFnDeepCompare(function withoutDeleted(
	stream: ApplogStream,
) {
	VERBOSE(`withoutDeleted<${stream.nameAndSizeUntracked}>`)
	// if (stream.name.includes('withoutDeleted')) WARN(`stream already contains withoutDeleted:`, withoutDeleted)
	if (stream.filters.includes('withoutDeleted')) {
		throw ERROR(`stream already filtered withoutDeleted:`, stream.filters, { name: stream.name })
	}

	const deletionLogs = rollingFilter(stream, { at: ['isDeleted', 'relation/isDeleted', 'block/isDeleted'], vl: true }, {
		name: 'isDeleted',
	})
	VERBOSE(`withoutDeleted<${stream.nameAndSizeUntracked}> deletionLogs:`, untracked(() => [...deletionLogs.applogs]))
	const obsArrMapName = createDebugName({ caller: 'allDeletedEntities', stream })
	const deleted = observableArrayMap(() => deletionLogs.map(log => log.en), { name: obsArrMapName })
	// VERBOSE(`withoutDeleted<${db.nameAndSize}> deleted:`, untracked(() => [...deleted]))
	VERBOSE.isDisabled || autorun(() => {
		VERBOSE(`withoutDeleted<${stream.nameAndSizeUntracked}> deleted:`, [...deleted])
	})

	return rollingFilter(stream, { '!en': deleted }, { name: `withoutDeleted`, extraFilterName: 'withoutDeleted' })
}, { equals: applogStreamComparer })

// export const filterStatic = computedFnDeepCompare(function filterStatic(
//     stream: ApplogStream,
//     pattern: DatalogQueryPattern,
//     opts: { name?: string } = {},
// ) {
//     VERBOSE(`filterStatic<${stream.nameAndSizeUntracked}>:`, pattern)
//     if (!Object.entries(pattern).length) throw new Error(`Pattern is empty`)
//     //TODO: deprecaate in favor of rollingFilter ?
//     return new ApplogStream(stream, stream.applogs.filter(applog => {
//         for (const [field, patternValue] of Object.entries(pattern)) {
//             const applogValue = applog[field.startsWith('!') ? field.slice(1) : field]
//             if (!matchPartStatic(field, patternValue, applogValue))
//                 return false
//         }
//         return true
//     }), `${stream.name} | ${opts.name || `filterStatic{${stringify(pattern)}}`}`)
// }, { equals: applogStreamComparer })

export const query = computedFnDeepCompare(function query(
	stream: ApplogStream,
	patternOrPatterns: DatalogQueryPattern | DatalogQueryPattern[],
	startVariables: SearchContext = {},
	opts: { debug?: boolean } = {},
) {
	DEBUG(`query<${stream.nameAndSizeUntracked}>:`, patternOrPatterns)
	const patterns = (Array.isArray(patternOrPatterns) ? patternOrPatterns : [patternOrPatterns]) as DatalogQueryPattern[]

	let nodes: QueryNodes | null
	if (patterns.length === 1) {
		nodes = null // new QueryNodes([/* new QueryNode(stream, startVariables) */])
	} else {
		const pattersExceptLast = patterns.slice(0, -1)
		// recursively call this function to have partial queries cacheable
		nodes = query(stream, pattersExceptLast, startVariables, opts)
	}
	const lastPattern = patterns[patterns.length - 1]
	const stepResult = queryStep(stream, nodes, lastPattern, opts)
	VERBOSE.isDisabled || autorun(() => VERBOSE(`query result:`, toJS(stepResult)))
	return stepResult
}, { equals: queryNodesComparer })

export const queryStep = computedFnDeepCompare(function queryStep(
	stream: ApplogStream,
	nodeSet: QueryNodes | null,
	pattern: DatalogQueryPattern,
	// variables: SearchContext = {},
	opts: { debug?: boolean } = {},
) {
	DEBUG(`queryStep<${stream.nameAndSizeUntracked}> with`, nodeSet?.untrackedSize ?? 'all', 'nodes, pattern:', pattern)
	if (!Object.entries(pattern).length) throw new Error(`Pattern is empty`)

	const observableResultNodes = observableArrayMap(
		() => {
			function doQuery(node: QueryNode | null) {
				const [patternWithResolvedVars, variablesToFill] = resolveOrRemoveVariables(pattern, node?.variables ?? {})
				VERBOSE(`[queryStep] patternWithoutVars: `, patternWithResolvedVars)
				const applogsMatchingStatic = rollingFilter(stream, patternWithResolvedVars)
				const varMapper = mapTo(variablesToFill)
				const newVarsAndTheirLog = applogsMatchingStatic.map(log => ({ log, vars: varMapper(log) }))
				VERBOSE.isDisabled ||
					VERBOSE(
						`[queryStep] step node:`,
						node?.variables,
						' =>',
						newVarsAndTheirLog,
						'from:',
						untracked(() => applogsMatchingStatic.applogs),
					)
				const resultNodes = newVarsAndTheirLog.map(({ log, vars }) => {
					const nodeVars = Object.assign({}, node?.variables, vars)
					return new QueryNode(
						new ApplogStreamInMemory(
							[log],
							stream.filters,
							createDebugName({
								caller: 'QueryNode',
								stream: applogsMatchingStatic,
								pattern: `${stringify(nodeVars)}@${stringify(patternWithResolvedVars)}`,
							}),
							applogsMatchingStatic,
						),
						nodeVars,
						node,
					)
				})
				if (opts.debug) {
					LOG(
						`[queryStep] step result:`,
						untracked(() =>
							resultNodes.map(({ variables, logsOfThisNode: stream }) => ({
								variables,
								stream: /* util.inspect( */ stream.applogs, /* , { showHidden: false, depth: null }) */
							}))
						),
					)
				}
				return resultNodes
			}
			if (nodeSet) {
				return nodeSet.nodes.flatMap(doQuery)
			} else {
				return doQuery(null) // initial query step
			}
		},
		{ name: createDebugName({ caller: 'queryStep', stream, pattern }) },
	)

	VERBOSE(`queryStep result:`, observableResultNodes)
	return new QueryNodes(observableResultNodes)
}, { equals: queryNodesComparer })

export const queryNot = computedFnDeepCompare(function queryNot( // TODO: update old-style query
	stream: ApplogStream,
	startNodes: QueryNodes,
	patternOrPatterns: DatalogQueryPattern | DatalogQueryPattern[],
	opts: { debug?: boolean } = {},
) {
	let nodes = startNodes.nodes
	DEBUG(`queryNot<${stream.nameAndSizeUntracked}> from: ${nodes.length} nodes`)
	const patterns = (Array.isArray(patternOrPatterns) ? patternOrPatterns : [patternOrPatterns]) as DatalogQueryPattern[]

	for (const pattern of patterns) {
		if (!Object.entries(patternOrPatterns).length) throw new Error(`Pattern is empty`)
		nodes = nodes.filter(({ /* applogs, */ variables }) => {
			const [patternWithResolvedVars, _variablesToFill] = resolveOrRemoveVariables(pattern, variables ?? {})
			VERBOSE(`[queryNot] patternWithoutVars: `, patternWithResolvedVars)
			const newApplogs = rollingFilter(stream, patternWithResolvedVars)
			VERBOSE(`[queryNot] step node:`, variables, ' =>', newApplogs.size, 'applogs')
			VERBOSE.isDisabled || VERBOSE(`[queryNot] step node:`, variables, ' => empty?', untracked(() => newApplogs.applogs))

			if (opts.debug) LOG(`[queryNot] node result:`, variables, '=>', newApplogs.applogs)
			return newApplogs.isEmpty
		})
	}
	return new QueryNodes(nodes)
}, { equals: queryNodesComparer })

// export function or(queries: QueryExecutor[]) {
//     return tagged(
//         `or{${stringify(queries)} } `,
//         function orExecutor(args: QueryExecutorArguments) {
//             const { db, nodes: contexts } = args
//             VERBOSE('[or]', { queries, contexts })
//             let results = []
//             for (const query of queries) {
//                 const res = query(args)
//                 VERBOSE('[or] query', query, 'result =>', res)
//                 results.push(...res.nodes)
//             }
//             return { contexts: results }
//         }
//     )
// }

// export type Tagged<T> = T & { tag: string }
// export function tagged<T>(tag: string, thing: T): Tagged<T> {
//     const e = thing as (T & { tag: string })
//     e.tag = tag
//     return e
// }

//////////////////////
// COMPOSED QUERIES //
//////////////////////
// createDebugName({ caller: 'useKidRelations' }, true)
export const filterAndMap = computedFnDeepCompare(function filterAndMap<R>(
	stream: ApplogStream,
	pattern: DatalogQueryPattern,
	mapper: (keyof Applog) | (Partial<{ [key in keyof Applog]: string }>) | ((applog: Applog) => R),
) {
	DEBUG(`filterAndMap<${stream.nameAndSizeUntracked}>`, pattern)

	const filtered = rollingFilter(stream, pattern)
	VERBOSE(`[filterAndMap] filtered:`, filtered.untrackedSize)
	VERBOSE.isDisabled || autorun(() => VERBOSE(`[filterAndMap] filtered:`, filtered.applogs))

	const mapperFX = function filterAndMapGetterFx() {
		if (typeof mapper === 'function') {
			return filtered.map(mapper)
		} else if (typeof mapper === 'string') {
			return filtered.map(log => log[mapper])
		} else {
			return filtered.map(mapTo(mapper))
		}
	}
	const name = createDebugName({ stream, pattern, caller: 'filterAndMap' })
	const mapped = observableArrayMap<ApplogValue | any>(mapperFX, { name }) // TODO typing:? Record<string, ApplogValue> ?
	VERBOSE.isDisabled || autorun(() => VERBOSE(`[filterAndMap] mapped:`, mapped))
	return mapped
}, { equals: comparer.structural })

export const queryAndMap = computedFnDeepCompare(function queryAndMap<R>(
	stream: ApplogStream,
	patternOrPatterns: DatalogQueryPattern | DatalogQueryPattern[],
	map: string | ((record: SearchContext) => R),
	variables: SearchContext = {},
) {
	DEBUG(`queryAndMap<${stream.nameAndSizeUntracked}>`, { patternOrPatterns, variables, map })
	const debugName = createDebugName({ stream, caller: 'queryAndMap' })

	const filtered = query(stream, patternOrPatterns)
	VERBOSE(`[queryAndMap] filtered count:`, filtered.untrackedSize)
	const mapped = observableArrayMap<ApplogValue | any>(
		() => {
			if (typeof map === 'function') {
				return filtered.records.map(map)
			} else if (typeof map === 'string') {
				return filtered.records.map(log => log[map])
			} else {
				throw new Error("what's this map param about?")
			}
		},
		{ name: debugName },
	)
	VERBOSE.isDisabled || autorun(() => VERBOSE(`[queryAndMap] result:`, toJS(mapped)))
	return mapped
}, { equals: comparer.structural })

export const queryEntity = computedFnDeepCompare(function queryEntity<R>(
	stream: ApplogStream,
	name: string,
	entityID: EntityID,
	attributes: string[],
) {
	DEBUG(`queryEntity<${stream.nameAndSizeUntracked}>`, entityID, name)

	const filtered = rollingFilter(stream, { en: entityID, at: prefixAttrs(name, attributes) })
	VERBOSE(`queryEntity applogs:`, filtered.applogs)
	return computed(() =>
		filtered.isEmpty ? null : Object.fromEntries(
			filtered.map(({ at, vl }) => [at.slice(name.length + 1), vl]),
		)
	)
}, { equals: computedStructuralComparer })

export const agentsOfStream = computedFnDeepCompare(function agentsOfStream<R>(
	stream: ApplogStream,
) {
	LOG(`agentsOfStream<${stream.nameAndSizeUntracked}>`)

	const mapped = observable.map<string, number>()
	function onEvent(event: ApplogStreamEvent) {
		for (const log of (isInitEvent(event) ? event.init : event.added)) {
			const prev = mapped.get(log.ag) ?? 0
			mapped.set(log.ag, prev + 1)
		}
		for (const log of (!isInitEvent(event) && event.removed || [])) {
			const prev = mapped.get(log.ag)
			if (!prev || prev < 1) throw ERROR(`[agentsOfStream] number is now negative`, { log, event, mapped, prev })
			mapped.set(log.ag, prev - 1)
		}
		LOG(`agentsOfStream<${stream.nameAndSizeUntracked}> processed event`, { event, mapped })
	}
	untracked(() => onEvent({ init: stream.applogs }))
	stream.subscribe(onEvent)
	onBecomeObserved(mapped, () => stream.unsubscribe(onEvent))

	return mapped
})

export const entityOverlap = computedFnDeepCompare(function entityOverlapCount(
	streamA: ApplogStream,
	streamB: ApplogStream,
) {
	LOG(`entityOverlap<${streamA.nameAndSizeUntracked}, ${streamB.nameAndSizeUntracked}>`)

	return computed(() => {
		const entitiesA = new Set(streamA.map(log => log.en))
		const entitiesB = new Set(streamB.map(log => log.en))
		return [...entitiesA].filter(en => entitiesB.has(en))
	})
})

export const entityOverlapCount = computedFnDeepCompare(function entityOverlapCount(streamA: ApplogStream, streamB: ApplogStream) {
	return computed(() => entityOverlap(streamA, streamB).get().length)
})

/////////////
// HELPERS //
/////////////

export function mapTo(applogFieldMap: Partial<{ [key in keyof Applog]: string }>) {
	return applog => {
		return Object.entries(applogFieldMap).reduce((acc, [key, value]) => {
			acc[value] = applog[key]
			return acc
		}, {} as SearchContext)
	}
}

export function startsWith(str: string) {
	return (value) => value.startsWith(str)
}

export function prefixAttrs(prefix: string, attrs: string[]) {
	return attrs.map(at => prefixAt(prefix, at))
}
export function prefixAt(prefix: string, attr: string) {
	return `${prefix}/${attr}`
}
