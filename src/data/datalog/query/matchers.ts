import { DatomPart } from '../datom-types'

export function includes(str: string) {
	return (vl: DatomPart) => vl?.includes?.(str)
}
export function includedIn(arr: string[]) {
	return (vl: DatomPart) => arr?.includes?.(vl)
}
