// // 2. querySingleIteration

// import type { Atom, SearchContext } from './datom-types'
// import { isVariable, matchPattern } from './utils'

// export function querySingleIteration(pattern: Atom, db, context: SearchContext) {
// 	return relevantTriples(pattern, db)
// 		.map((triple) => matchPattern(pattern, triple, context))
// 		.filter((x) => x)
// }

// // 3. queryWhere

// export function queryWhere(patterns: Atom[], db, ctx: SearchContext = {}) {
// 	return patterns.reduce(
// 		(contexts, pattern) => {
// 			return contexts.flatMap((context) => {
// 				return querySingleIteration(pattern, db, context)
// 			})
// 		},
// 		[ctx],
// 	)
// }

// // 4. query

// function actualize(context, find) {
// 	return find.map((findPart) => {
// 		return /* isIgnorePattern(findPart) ?  context[findPart] :  */ findPart
// 	})
// }

// export function query({ find, where }: { find: string[]; where: Atom[] }, db) {
// 	const contexts = queryWhere(where, db)
// 	return contexts.map((context) => actualize(context, find))
// }

// // 5. DB

// function relevantTriples(pattern, db) {
// 	const [id, attribute, value] = pattern
// 	if (!isVariable(id)) {
// 		return db.entityIndex[id]
// 	}
// 	if (!isVariable(attribute)) {
// 		return db.attrIndex[attribute]
// 	}
// 	if (!isVariable(value)) {
// 		return db.valueIndex[value]
// 	}
// 	return db.triples
// }

// function indexBy(triples, idx) {
// 	return triples.reduce((index, triple) => {
// 		const k = triple[idx]
// 		index[k] = index[k] || []
// 		index[k].push(triple)
// 		return index
// 	}, {})
// }

// export function createDB(triples) {
// 	return {
// 		triples,
// 		entityIndex: indexBy(triples, 0),
// 		attrIndex: indexBy(triples, 1),
// 		valueIndex: indexBy(triples, 2),
// 	}
// }
