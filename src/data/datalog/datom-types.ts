import type { CID } from '@oddjs/odd'
import { FormatRegistry, Static, TSchema, Type } from '@sinclair/typebox'
import { TypeCompiler } from '@sinclair/typebox/compiler'
import { PartialBy } from '../../types/util-types'

export const Nullable = <T extends TSchema>(schema: T) => Type.Union([schema, Type.Null()])
export const EntityID_LENGTH = 7
const bagu = 'baguqeerav3h4b46j2pyxikqhtm5si5vhzsyrba2duhrtltfutrlmj42anmvq'
const k51q = 'k51qzi5uqu5dhe1bxxjxj144bj2a225o1681yobevns26xlxtsfidjgnpwknfd'
const isCID = /^(k51qz|baguq)[0-9a-z]{56,57}$/
const isShortHash = /^[0-9A-Fa-f]{7,8}$/g // TODO awkward why are some 7 and some 8 long
// engine level: min 6 (lenient within reason)
// note3 TBD... either fixed for all entity types VS 7 for pub/sub, 8 for tags, 9 for blocks, 10 for relations etc...

FormatRegistry.Set('EntityID', (value) => !!value.match(isShortHash) || !!value.match(isCID))
export const EntityID = Type.String({ format: 'EntityID' })
export type EntityID = Static<typeof EntityID>

export type DatomPart = string // TODO refactor
export type CidString = string
export type AgentID = EntityID
export type Attribute = string
export type ApplogValue = string | boolean | Number | null // TODO: use Tagged types
// ? allow objects? or just as serialized strings? Or serialize everything anyways?

export interface Atom {
	en: EntityID
	at: Attribute
	vl: ApplogValue
}

export type AgentHash = string
export type Timestamp = string
export interface Applog extends Atom {
	cid: CidString
	pv?: CidString | CID
	ts: Timestamp
	ag: AgentHash
}
export type ApplogNoCid = Omit<Applog, 'cid'>
export type ApplogOptionalCid = PartialBy<Applog, 'cid'>
export type ApplogForInsert = PartialBy<ApplogNoCid, 'ts'>
export type ApplogForInsertOptionalAgent = PartialBy<ApplogForInsert, 'ag'>

export type AtomPattern = Atom | Applog

export interface DatalogStateIdentifier {
	lastTS: Timestamp
}

// New generic type for fields that can be a value, an array of that, or a function
export type ValueOrMatcher<T> = T | readonly T[] | ((value: T) => boolean)
// Generic type that applies FieldOrFunction to each field of T
export type WithMatchers<T extends Record<string, any>> = {
	[K in keyof T & string as `${K}` | `!${K}`]?: ValueOrMatcher<T[K]>
}

export type DatalogQueryPattern = Partial<WithMatchers<AtomPattern>>
export type DatalogQueryPatternArray = DatalogQueryPattern[]
export interface DatalogQuery<SELECT extends string> {
	find: readonly SELECT[] // see: https://github.com/microsoft/TypeScript/issues/20965#issuecomment-868981458
	where: DatalogQueryPatternArray
	onlyLatest?: boolean
}
export type DatalogQueryResultEntry<SELECT extends string> = Record<
	// SELECT,
	StripPrefix<'?', SELECT>,
	DatomPart
>
export type DatalogQueryResultRows<SELECT extends string> = DatalogQueryResultEntry<SELECT>[]
// export type StripTest = StripPrefix<'?', '?A' | '?B'>
// export type DatalogQueryResultEntryTEST = DatalogQueryResultEntry<'?A' | '?B'>
// export type DatalogQueryResultEntryTESTX = MapKeysStripPrefix<'?A' | '?B', '?'>

export interface SearchContext {
	[key: string]: ApplogValue
}
export interface SearchContextWithLog {
	context: SearchContext
	applog?: Applog
}

export type ResultContext = SearchContext | null

/* https://stackoverflow.com/a/72497461 */
type StripPrefix<
	TPrefix extends string,
	T extends string,
> = T extends `${TPrefix}${infer R}` ? R : never

type MapKeysStripPrefix<SELECT extends string, TPrefix extends string> = {
	[K in SELECT as StripPrefix<TPrefix, K>]: DatomPart
}

FormatRegistry.Set('CID', (value) => !!value.match(isCID))
export const CIDTB = Type.String({ format: 'EntityID' })
export type CIDTB = Static<typeof EntityID>

const isURL = /^http([s]?):\/\/.*\..*/
FormatRegistry.Set('URL', (value) => !!value.match(isURL))
export const URL = Type.String({ format: 'URL' })
export type URL = Static<typeof URL>

export const AppLogTB = Type.Object({
	en: EntityID, // EntityID
	at: Type.String(), // Attribute
	vl: Nullable(Type.Union([Type.String(), Type.Boolean(), Type.Number()])), // TODO refactor to semantic typesafe ApplogValue
	ts: Type.String(), // Timestamp
	ag: Type.String(), // AgentHash
})
export type AppLogTB = Static<typeof AppLogTB> // type T = {

export const AppLogTBC = TypeCompiler.Compile(AppLogTB)
export const getApplogTypeErrors = (obj: any) => Array.from(AppLogTBC.Errors(obj))
export const isValidApplog = AppLogTBC.Check.bind(AppLogTBC)

// maybe useful for defaulting https://github.com/sinclairzx81/typebox#cast
