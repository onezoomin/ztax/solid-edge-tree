import { Logger } from 'besonders-logger/src'
import { untracked } from 'mobx'
import { describe, expect, test } from 'vitest'
import { rollingFilter } from './query/engine.js'
import { filterAndMap, query, queryEntity, withoutDeleted, withoutHistory } from './query/queries.js'

import './test/test-utils.js'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

describe('applog-basic', () => {
	test('filter(at,vl)', async ({ db, autorunAndReturn, insert }) => {
		const movie1992logs = untracked(() => rollingFilter(db, { at: 'movie/year', vl: 1992 }).applogs)
		expect(movie1992logs).toBeApplogs([
			{ ag: 'demoAgent', at: 'movie/year', en: '213', vl: 1992 },
		])
	})

	test('find, insert, find again', async ({ db, autorunAndReturn, insert }) => {
		const movies1992 = autorunAndReturn(() => filterAndMap(db, { at: 'movie/year', vl: 1992 }, { en: 'movieID' }))
		expect(movies1992()).to.have.deep.members([
			{ movieID: '213' },
		])

		insert([
			{ en: '1234', at: 'movie/year', vl: 1992 },
		])
		DEBUG(db.applogs.length)
		expect(movies1992()).to.have.deep.members([
			{ 'movieID': '213' },
			{ 'movieID': '1234' },
		])
	})

	test('filterAndMap', async ({ db, autorunAndReturn, insert }) => {
		const movies1992 = autorunAndReturn(() => {
			let movies1992 = filterAndMap(db, { at: 'movie/year', vl: 1992 }, { en: 'movieID' })
			DEBUG(`mapped movies1992 from:`, movies1992)
			return movies1992
		})
		expect(movies1992()).to.have.deep.members([
			{ movieID: '213' },
		])
	})

	test('director entity', async ({ db, autorunAndReturn, insert }) => {
		const director = untracked(() => queryEntity(db, 'person', '111', ['name', 'born']).get())
		expect(director).toStrictEqual({
			'born': '1930-04-24T00:00:00Z',
			'name': 'Richard Donner',
		})
	})

	test('movies -> directors', async ({ db, autorunAndReturn, insert }) => {
		const directors = autorunAndReturn(() => {
			return query(db, [
				{ en: '?movieID', at: 'movie/year', vl: 1987 },
				{ en: '?movieID', at: 'movie/director', vl: '?directorID' },
				{ en: '?directorID', at: 'person/name', vl: '?directorName' },
			]).records
		})
		expect(directors()).toStrictEqual([
			{ directorID: '108', directorName: 'John McTiernan', movieID: '202' },
			{ directorID: '111', directorName: 'Richard Donner', movieID: '203' },
			{ directorID: '102', directorName: 'Linda Hamilton', movieID: '204' },
			{ directorID: '102', directorName: 'Linda Maria Hamilton', movieID: '204' },
		])
	})

	test('Arnold movies', async ({ db, autorunAndReturn, insert }) => {
		const directors = autorunAndReturn(() => {
			return query(
				db,
				[
					{ en: '?arnoldId', at: 'person/name', vl: 'Arnold Schwarzenegger' },
					{ en: '?movieId', at: 'movie/cast', vl: '?arnoldId' }, // any ?dingdongs, aka vars that are in the previous result/candidate
					{ en: '?movieId', at: 'movie/title', vl: '?movieTitle' },
					{ en: '?movieId', at: 'movie/director', vl: '?directorId' },
					{ en: '?directorId', at: 'person/name', vl: '?directorName' },
				],
				{},
				{ debug: true },
			).records
		})
		DEBUG(`Arnold movies result:`, directors())
		expect(directors()).to.have.deep.members([
			{ arnoldId: '101', directorId: '100', directorName: 'James Cameron', movieId: '200', movieTitle: 'The Terminator' },
			{ arnoldId: '101', directorId: '108', directorName: 'John McTiernan', movieId: '202', movieTitle: 'Predator' },
			{ arnoldId: '101', directorId: '119', directorName: 'Mark L. Lester', movieId: '205', movieTitle: 'Commando' },
			{ arnoldId: '101', directorId: '100', directorName: 'James Cameron', movieId: '207', movieTitle: 'Terminator 2: Judgment Day' },
			{
				arnoldId: '101',
				directorId: '127',
				directorName: 'Jonathan Mostow',
				movieId: '208',
				movieTitle: 'Terminator 3: Rise of the Machines',
			},
		])
	})

	test('changed name (withoutHistory)', async ({ db, autorunAndReturn, insert }) => {
		const allNames = autorunAndReturn(() => filterAndMap(db, { en: '102', at: 'person/name' }, { vl: 'name' }))
		expect(allNames()).to.have.deep.members([
			{ name: 'Linda Hamilton' },
			{ name: 'Linda Maria Hamilton' },
		])

		const latestName = autorunAndReturn(() => filterAndMap(withoutHistory(db), { en: '102', at: 'person/name' }, { vl: 'name' }))
		expect(latestName()).to.have.deep.members([
			{ name: 'Linda Maria Hamilton' },
		])
	})

	test('withoutHistory: only new after change', async ({ db, autorunAndReturn, insert }) => {
		const latestName = autorunAndReturn(() => filterAndMap(withoutHistory(db), { en: '102', at: 'person/name' }, { vl: 'name' }))
		expect(latestName()).to.have.deep.members([
			{ name: 'Linda Maria Hamilton' },
		])
		DEBUG(`\n------------ inserting update ---------------`)
		insert([
			{ en: '102', at: 'person/name', vl: 'Linda Mary Hamilton' },
		])
		expect(latestName()).to.have.deep.members([
			{ name: 'Linda Mary Hamilton' },
		])
	})

	test('not query', async ({ db, autorunAndReturn, insert }) => {
		const person = autorunAndReturn(() => {
			return rollingFilter(db, { en: '111', '!at': 'person/name' }).applogs
		})
		expect(person()).toBeApplogs([
			{ 'at': 'person/born', 'en': '111', 'vl': '1930-04-24T00:00:00Z' },
		])
	})

	test('deleted test', async ({ db, autorunAndReturn, insert }) => {
		insert([
			{ en: '204', at: 'isDeleted', vl: true }, // delete robocop
		])
		const movies = autorunAndReturn(() => {
			let movies = filterAndMap(withoutDeleted(db), { at: 'movie/year', vl: 1987 }, { en: 'movieID' })
			DEBUG(`mapped movies from:`, movies)
			return movies
		})
		expect(movies()).to.have.deep.members([
			{ movieID: '202' },
			{ movieID: '203' },
		])
	})
})
