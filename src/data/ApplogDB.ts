import { Logger } from 'besonders-logger/src'
import { action } from 'mobx'

import { hasAg } from './datalog/datalog-utils'
import type { ApplogForInsertOptionalAgent } from './datalog/datom-types'
import { appLogIDB } from './datalog/local-applog-idb'
import { ApplogStreamIDB } from './datalog/query/engine'
import { sortApplogsByTs } from './datalog/utils'
import { getUseAgent } from './lazy-agent'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

// let applogDB: DatalogDB
let applogDB: ApplogStreamIDB
let useAgent: typeof useAgentT
export async function initApplogDB() {
	if (applogDB) {
		WARN('multiple attempts to initApplogDB')
		return applogDB
	}
	useAgent = await getUseAgent()
	// saturate from IDB
	const appLogsFromIDB = await appLogIDB.applogs.toArray()
	// .map(({ ag, at, en, ts, vl }) => ({ ag, at, en, ts, vl })) // HACK: ignore cid (not anymore)
	// cid will be ignored when encoding the blocks for publishing
	sortApplogsByTs(appLogsFromIDB)

	// @ts-expect
	applogDB = new ApplogStreamIDB(appLogsFromIDB, [], 'note3IDB')

	// @ts-expect-error because window
	window.db = applogDB
	// @ts-expect-error because window
	window.insertApplogs = insertApplogs
	return applogDB
}
export function getApplogDB() {
	if (!applogDB) {
		throw new Error(`Uninitizalized`)
	}
	return applogDB
}

export const prepareSingleApplog = function prepareSingleApplog(log: ApplogForInsertOptionalAgent, ag?: string) {
	ag = ag ?? useAgent().ag
	return hasAg(log) ? log : ({ ...log, ag }) /* , vl: log.vl //? was this a brain fart or still relevant */
}
export const prepareIncompleteApplogs = function prepareIncompleteApplogs(applogs: ApplogForInsertOptionalAgent[]) {
	const ag = useAgent().ag
	return applogs.map(log => prepareSingleApplog(log, ag))
}

export const insertApplogs = action(function insertApplogs(applogs: ApplogForInsertOptionalAgent[]) {
	return getApplogDB().insert(prepareIncompleteApplogs(applogs))
})
