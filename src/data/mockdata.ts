const ts = new Date('2023-06-15T08:57:55.252Z').toISOString() // HACK: for deterministic testing

export const blockApplogs = [
	['roothash', 'block/content', 'ROOT', ts, agentHash],

	['zeroOne', 'block/content', '0 > 1', ts, agentHash],
	['rel1hash', 'relation/parent', 'roothash', ts, agentHash],
	['rel1hash', 'relation/block', 'zeroOne', ts, agentHash],

	['zeroTwo', 'block/content', '0 > 2', ts, agentHash],
	['rel2hash', 'relation/parent', 'roothash', ts, agentHash],
	['rel2hash', 'relation/block', 'zeroTwo', ts, agentHash],
	['rel2hash', 'relation/below', 'zeroOne', ts, agentHash],

	['zeroThree', 'block/content', '0 > 3', ts, agentHash],
	['rel3hash', 'relation/parent', 'roothash', ts, agentHash],
	['rel3hash', 'relation/block', 'zeroThree', ts, agentHash],
	['rel3hash', 'relation/below', 'zeroTwo', ts, agentHash],
	// ['zeroFour', 'block/content', '0 > 4', ts, agentHash],
	// ['rel4hash', 'relation/parent', 'roothash', ts, agentHash],
	// ['rel4hash', 'relation/block', 'zeroFour', ts, agentHash],
	// ['rel4hash', 'relation/below', 'zeroTwo', ts, agentHash],
]
