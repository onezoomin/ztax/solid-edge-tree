import * as odd from '@oddjs/odd' // 592.2k
import { updateAgentState, useAgent } from './AgentStore'
// import {defaultCryptoComponent} from '@oddjs/odd' // 474.7k
import { SymmAlg } from '@oddjs/odd/components/crypto/implementation'
import { Logger } from 'besonders-logger/src'
import { deriveSharedEncryptionKey, importPublicDerivationKey } from '../ipfs/sub-pub'
import { Applog } from './datalog/datom-types'
import { arrayBufferToBase64, base64ToArray, base64ToArrayBuffer } from './Utils'
const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.LOG) // eslint-disable-line no-unused-vars
export const configuration = {
	namespace: { creator: 'ztax', name: 'note3' },
	debug: true,
	fileSystem: {
		loadImmediately: false,
		//   version?: string
	},
	// permissions?: Permissions
	// userMessages?: UserMessages
}
export async function initOddCrypto() {
	const crypto = await odd.defaultCryptoComponent(configuration)
	DEBUG('[odd]', { crypto })

	updateAgentState({ crypto })
}

type Base64String = string
export async function importPublicECDH64(derivationECDH: Base64String) {
	const publicECDHraw = base64ToArrayBuffer(derivationECDH)
	DEBUG({ derivationECDH, publicECDHraw })

	return await window.crypto.subtle.importKey(
		'raw',
		publicECDHraw,
		{ name: 'ECDH', namedCurve: 'P-256' },
		true,
		[],
	)
}

export const decryptWithAesSharedKey = async function decryptWithAesSharedKey(
	encByteArray: Uint8Array,
	aesKey: CryptoKey,
	as: 'array' | 'parsed' | 'string' = 'array',
) {
	// const aesKey = currentPubSub.encryptedWith ?? (currentPubSub as IPublication).sharedKey
	if (!aesKey) {
		throw ERROR('[decrypt] missing aesKey', { encByteArray })
		// throw new Error('[decrypt] missing info') // ERROR from logger is now throwable
	}

	// const fixedIV = enc.encode(currentPubSub.id).slice(-16)
	// this -16 biz is part of the @odd strategy to tag a fixed length iv onto the payload
	// however
	// @expede advises against fixed iv https://github.com/fission-codes/keystore-idb/issues/70#issuecomment-1624377859
	// so lets try to allow @odd to create random ones on its own
	let oddDecrypted: Uint8Array
	const dec = new TextDecoder('utf-8')

	const { crypto } = useAgent()
	DEBUG('decryptWithAesSharedKey', { encByteArray, aesKey, crypto })
	try {
		oddDecrypted = await crypto?.aes.decrypt(encByteArray, aesKey, SymmAlg.AES_GCM)
	} catch (error) {
		ERROR('aes decrypt error', error?.name, error)
	}

	const oddDecoded = dec.decode(oddDecrypted)
	DEBUG('decryptWithAesSharedKey', { oddDecrypted, oddDecoded })
	return as === 'array' ? oddDecrypted as Uint8Array : as === 'parsed' ? JSON.parse(oddDecoded) : oddDecoded
}

export const getAESkeyForEncryptedApplogs = async function getAESkeyForEncryptedApplogs(nonEncryptedLogs: Applog[]) {
	const agent = useAgent()
	const { ag, getDerivationKeypair, getSharedKeyApplogForCurrentAgent } = agent
	const { privateKey: priDerivationKey } = await getDerivationKeypair()
	const sharedKeyApplog = getSharedKeyApplogForCurrentAgent(nonEncryptedLogs)

	if (!sharedKeyApplog) return ERROR('no sharedKey Found for current agent', { nonEncryptedLogs, priDerivationKey, ag })

	const { vl: encryptedSharedKey, ag: publisherAg } = sharedKeyApplog as Applog

	const publicKeyForPublisher = (nonEncryptedLogs as Applog[]).find(eachLog =>
		(eachLog?.at as string) === 'agent/jwkd'
		&& (eachLog?.ag as string) === publisherAg
	)?.vl

	const publicKeyECDHforPublisher = (nonEncryptedLogs as Applog[]).find(eachLog =>
		(eachLog?.at as string) === 'agent/ecdh'
		&& (eachLog?.ag as string) === publisherAg
	)?.vl

	const publicKeyFromApplogs = await importPublicDerivationKey(publicKeyForPublisher as string) // TODO discuss depricate jwkd
	const derivedKeyDecryptionKey = await deriveSharedEncryptionKey(priDerivationKey, publicKeyFromApplogs)
	const encryptedKeyUintArr = base64ToArray(encryptedSharedKey as string)
	DEBUG({ publicKeyFromApplogs, publicKeyECDHforPublisher, derivedKeyDecryptionKey, encryptedSharedKey })

	const decryptedSharedKeyBase64 = arrayBufferToBase64(await decryptWithAesSharedKey(encryptedKeyUintArr, derivedKeyDecryptionKey))
	const decryptedSharedKeyUintArrForImport = base64ToArrayBuffer(decryptedSharedKeyBase64)
	DEBUG({ decryptedSharedKeyBase64, decryptedSharedKeyUintArrForImport })

	const aesKey = await window.crypto.subtle.importKey(
		'raw',
		decryptedSharedKeyUintArrForImport, // this has been converted from b64 to uint8[] then decrypted then converted again to uint and then imported
		{
			name: 'AES-GCM',
			length: 256,
		},
		false,
		['encrypt', 'decrypt'],
	)
	DEBUG('shared enc', { aesKey, decryptedSharedKeyBase64, encryptedSharedKey })
	return { aesKey, publisherAg }
}
