import { hashToPrivateScalar } from '@noble/curves/abstract/modular'
import { p256 } from '@noble/curves/p256'
import { hkdf } from '@noble/hashes/hkdf'
import { sha256 } from '@noble/hashes/sha256'
import { Logger } from 'besonders-logger/src'
import { arrStats } from './datalog/utils'
import { words } from './mnemonic-words'
import { base64ToUrlBase64, bnToB64, buf2hex, cyrb53hash } from './Utils'

// import { secp256k1 } from '@noble/curves/secp256k1'
// import { randomBytes } from '@noble/hashes/utils';

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars
const p = performance

export const keyAlgos = {
	ecdh: {
		p256: { // these are the algorithm options
			name: 'ECDH',
			namedCurve: 'P-256', // can be "P-256", "P-384", or "P-521"
		},
		p384: {
			name: 'ECDH',
			namedCurve: 'P-384',
		},
		p521: {
			name: 'ECDH',
			namedCurve: 'P-521',
		},
	},
}

export const testStats = (x = 1024, y = 10) => {
	const n = p.now()
	const stats = {
		mins: [],
		maxs: [],
		means: [],
		devs: [],
		vars: [],
		mabDevs: [],
	}
	while (y-- > 0) {
		const eachSeries = createRandomSeries(x, 4096)
		stats.mins.push(arrStats.min(eachSeries))
		stats.maxs.push(arrStats.max(eachSeries))
		stats.means.push(arrStats.mean(eachSeries))
		stats.devs.push(arrStats.standardDeviation(eachSeries))
		stats.vars.push(arrStats.variance(eachSeries))
		stats.mabDevs.push(arrStats.meanAbsoluteDeviation(eachSeries))
	}
	LOG('took:', p.now() - n, stats)
}
export const randomish = (multiplier: number, factor = 1) =>
	Math.floor(((p.now() * pseudoRandomDelay(factor)) * Math.random() * multiplier) / (p.now() - 1)) // % multiplier

const pseudoRandomDelay = (factor = 1) => {
	const n = p.now()
	let countDown = Math.random() * 1000 * factor + 1000000 // takes about 1.5sec total for 24 words
	while (countDown > 0) countDown-- // so that the performance.now in randomish has a tiny bit more entropy
	VERBOSE('pseudoRandomDelay', p.now() - n)
	return 1
}
export const createRandomSeries = (length: number, maxIndex: number, factor = 1) => {
	// const crRand = window.crypto.getRandomValues(u8n(length))
	let until = 0
	const returnArray: number[] = []
	while (until++ < length) {
		returnArray.push(randomish(maxIndex, factor))
		VERBOSE('[createRandomSeries]', { returnArray })
	}
	DEBUG('[createRandomSeries]', { returnArray })
	return returnArray
}

export const createMnemonic = (length) => {
	return createRandomSeries(length, 4096, 100000).map(eachIdx => words[eachIdx])
}
export const encHashedMnemonic = (mnem) => {
	const hashedMnemonic = hashMnemonic(mnem)
	return (new TextEncoder()).encode(hashedMnemonic)
}
export const hashMnemonic = (mnu) => {
	if (!mnu || !mnu.length) {
		ERROR('bogus mnu array')
		return ''
	}
	// return b2h(enc.encode(mnu.join(''))) // ?might be just as good

	const arrayToMutate = [...mnu]
	let permutativeHash = ''
	while (arrayToMutate.length) {
		permutativeHash += cyrb53hash(permutativeHash + arrayToMutate.shift(), 31 + arrayToMutate.length, 15)
	}
	// DEBUG({ before: permutativeHash })
	// const enc = new TextEncoder()
	// permutativeHash = b2h(enc.encode(permutativeHash))
	// DEBUG({ after: permutativeHash })

	return permutativeHash
}

export function hashAsArrayToPrivateKey(hashAsUint8: Uint8Array, asJwk = false, nobleCurve = p256) {
	// https://github.com/paulmillr/noble-curves/blob/5609ec7644c517acc3e990582f630f5dd4d4fbd0/README.md?plain=1#L769
	// following is a way to reduce bias when creating a private key deterministically from a hash
	let derived = hkdf(sha256, hashAsUint8, undefined, 'note3', 180) // previously 40 bytes - one hash iteration
	// DEBUG({ derived })

	derived = hkdf(sha256, derived, undefined, 'note3', 90)
	// DEBUG({ derived })

	derived = hkdf(sha256, derived, undefined, 'note3', 40) /* TODO offer password salt to enhance mnemonic with brain key */
	// DEBUG({ derived })

	// (i) totally intuitive triple hashing might actually introduce more bias (for all i know)

	const validPrivateKey = hashToPrivateScalar(derived, nobleCurve.CURVE.n)
	const pointFromPK = nobleCurve.ProjectivePoint.fromPrivateKey(validPrivateKey)
	DEBUG({ validPrivateKey, hashAsUint8, pointFromPK })

	const keyObjBigInts = {
		x: pointFromPK.x,
		y: pointFromPK.y,
		d: validPrivateKey,
	}
	const returnObjJWK = {
		x: base64ToUrlBase64(bnToB64(pointFromPK.x)),
		y: base64ToUrlBase64(bnToB64(pointFromPK.y)),
		d: base64ToUrlBase64(bnToB64(validPrivateKey)),
	}
	DEBUG('[crypto]', { keyObjBigInts, returnObjJWK })

	return asJwk ? returnObjJWK : validPrivateKey
}

export async function getECDHkeypairFromHashArray(encHashedMnu: Uint8Array, keyAlgo = keyAlgos.ecdh.p256) {
	let keypairFromHash: CryptoKeyPair = null
	try {
		const privKeyJWK = hashAsArrayToPrivateKey(encHashedMnu, true) as { x: any; y: any; d: any }
		const privKeyNum = hashAsArrayToPrivateKey(encHashedMnu) as bigint

		const { x, y, d } = privKeyJWK
		DEBUG({ privKeyJWK, privKeyNum })

		const jwkObjPub = { // this is an example jwk key, other key types are Uint8Array objects
			kty: 'EC',
			crv: keyAlgo.namedCurve, // this is actually duplicated within the JWK and in the EcKeyImportParams
			x, // eg: "kgR_PqO07L8sZOBbw6rvv7O_f7clqDeiE3WnMkb5EoI",
			y, // eg: "djI-XqCqSyO9GFk_QT_stROMCAROIvU8KOORBgQUemE",
			// d, // eg: "5aPFSt0UFVXYGu-ZKyC9FQIUOAMmnjzdIwkxCMe3Iok", // public key import works without this private component 'd' and empty array for key usages below
			ext: true,
		}
		const jwkObjPri = { ...jwkObjPub, d }
		VERBOSE({ jwkObjPub, jwkObjPri })

		const privateKey = await window.crypto.subtle.importKey(
			'jwk', // can be "jwk" (public or private), "raw" (public only), "spki" (public only), or "pkcs8" (private only)
			jwkObjPri,
			keyAlgo,
			false, // whether the key is extractable (i.e. can be used in exportKey)
			['deriveKey', 'deriveBits'], // "deriveKey" and/or "deriveBits" for private keys only (just put an empty list if importing a public key)
		)
		// (i) possibly helpful to understand https://github.com/usrz/ec-key/blob/master/src/ec-key.js
		const publicKey = await window.crypto.subtle.importKey(
			'jwk',
			jwkObjPub,
			keyAlgo,
			true, // public key should be extractable
			[], // important: just put an empty list if importing a public key)
		)
		keypairFromHash = {
			publicKey,
			privateKey,
		}
		const exportedPubKeyJWK = await window.crypto.subtle.exportKey('jwk', publicKey)
		const exportedPubKeyHex = buf2hex(await window.crypto.subtle.exportKey('raw', publicKey))
		DEBUG({ keypairFromMnemonic: keypairFromHash, encHashedMnu, exportedPubKeyHex, exportedPubKeyJWK })
	} catch (err) {
		ERROR('[enc ecdh import]', err)
	} finally {
		return keypairFromHash
	}
}
