import { useLocation, useNavigate } from '@solidjs/router'
import { Logger } from 'besonders-logger/src'
import { action } from 'mobx'
import { Accessor, onCleanup, onMount } from 'solid-js'
import { BlockContextType } from '../components/BlockTree'
import { useCurrentDS, useFocus, useParent } from '../ui/reactive'
import { zoomToBlock } from '../ui/ui-utils'
import { useBlockContext } from './block-ui-helpers'
import { addBlock, AddBlockOpts } from './block-utils'
import { EntityID } from './datalog/datom-types'
import { ApplogStreamOnlyCurrent } from './datalog/query/engine'
import { useRel } from './VMs'
import { useBlk } from './VMs/BlockVM'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export function useGlobalKeyhandlers() {
	const focus = useFocus()
	const location = useLocation() // HACK: old router workaround
	const navigate = useNavigate()
	const dataStream = useCurrentDS()

	const handleKeyDown = action((event: KeyboardEvent) => {
		VERBOSE(`[useGlobalKeyhandlers]`, event)

		if (event.ctrlKey) {
			// if (event.key === 'h') { - "history" in chromium
			if (event.key === "'") {
				zoomToBlock({ id: null }, location, navigate)
				return event.stopPropagation()
			}
		}

		// Enter //
		if (event.key === 'Enter' && event.altKey) {
			// HACK do we need this key handler to not override block handler?
			// if (!event.target?.closest?.('[data-block-id]')) {
			event.preventDefault() // prevent tiptap from getting this
			event.stopImmediatePropagation() // prevent tiptap from getting this

			let asChildOf = focus() ? useParent(focus()).get() : null
			if (event.ctrlKey) {
				asChildOf = null
			}
			addBlock({
				dataStream,
				asChildOf: asChildOf,
				focus: true,
				// TODO after: last ?
			})
		}
	})
	document.addEventListener('keydown', handleKeyDown, true)
	onCleanup(() => {
		document.removeEventListener('keydown', handleKeyDown, true)
	})
}

export function useBlockKeyhandlers(refSignal: Accessor<HTMLDivElement>) {
	const focus = useFocus()
	const blockContext = useBlockContext()
	const blockID = blockContext.id // (i) non-reactive, but parent component should never change ID
	const location = useLocation() // HACK: old router workaround
	const [blockVM] = useBlk(blockContext.id)
	const [relToParentVM] = useRel(blockContext.relationToParent)
	const navigate = useNavigate()
	const dataStream = useCurrentDS()

	const handleKeyDown = action((event: KeyboardEvent) => {
		if (event.keyCode < 48 || event.keyCode > 90) { // don't trace normal keys
			DEBUG(`[useBlockKeyhandler#${blockID}]`, event, { focus: focus(), blockContext })
		}
		// Zoom in & out //
		if (event.shiftKey && event.altKey) {
			if (event.key === 'ArrowLeft') {
				zoomOut(focus, blockID, location, navigate)
				return event.stopPropagation()
			}
			if (event.key === 'ArrowRight') {
				zoomIn(blockContext, blockID, location, navigate)
				return event.stopPropagation()
			}
		}
		if (event.altKey) {
			// Create new node above //
			if (event.key === 'Enter') {
				createNodeAbove(dataStream, event, blockID, focus, location, navigate)
				return event.stopPropagation()
			}
		}

		// Enter //
		if (event.key === 'Enter') {
			if (event.shiftKey && !event.altKey && !event.ctrlKey) return // allow linebreaks
			event.preventDefault() // prevent tiptap from getting this
			event.stopImmediatePropagation() // prevent tiptap from getting this

			const isDisplayRoot = !blockContext.parentContext
			const isAtStart = false // TODO: tiptap position detection
			// DEBUG('enter pos', { pos, beforePos, afterPos, isDisplayRoot })

			if (isDisplayRoot && event.altKey && event.ctrlKey) {
				DEBUG('// add root node')
				addBlock({ dataStream, asChildOf: null, /*  relationsOfParent,  */ focus: true })
			} else if (event.ctrlKey || isDisplayRoot) {
				addBlock({ dataStream, asChildOf: blockID, /*  relationsOfParent,  */ focus: true })
			} else if (isAtStart && relToParentVM) {
				// add sibling above
				addBlock({
					dataStream,
					asChildOf: relToParentVM.childOf,
					after: relToParentVM.after,
					focus: true,
				})
			} else {
				//////
				// split or new below
				const kidProps: AddBlockOpts = {
					dataStream,
					asChildOf: relToParentVM?.childOf,
					after: blockID,
					focus: true,
				}
				// TODO: if (isExpanded) {
				//     kidProps.asChildOf = blockID
				//     kidProps.after = null
				//     // ? should enter create kid at start or end?
				//     // ? discuss if this ought to be blockID (that the first kid uses the parent as after)
				// }
				// TODO: splitting
				// if (pos < innerText.length - 1) {
				//     event.currentTarget.innerText = beforePos
				//     kidProps.content = afterPos
				// }
				// block.content = beforePos
				DEBUG('split or add below', { blockVM, kidProps })
				addBlock(kidProps)
			}
		}
	})
	onMount(() => {
		const ref = refSignal()
		ref.addEventListener('keydown', handleKeyDown, true)
		onCleanup(() => {
			ref.removeEventListener('keydown', handleKeyDown, true)
		})
	})
}

function zoomIn(blockContext: BlockContextType, blockID: EntityID, location, navigate) {
	DEBUG(`zoom in`, blockContext)
	zoomToBlock({ id: blockID, focus: blockID }, location, navigate)
}

function zoomOut(focus: Accessor<string>, blockID: string, location, navigate) {
	const parent = useParent(focus()).get()
	DEBUG(`zoom out`, { parent })
	// zoomToBlock({ id: blockContext.parentContext?.id, focus: focus() }, location, navigate)
	zoomToBlock({ id: parent, focus: blockID }, location, navigate)
}

function createNodeAbove(
	dataStream: ApplogStreamOnlyCurrent,
	event: KeyboardEvent,
	blockID: EntityID,
	focus: Accessor<string>,
	location,
	navigate,
) {
	let asChildOf = null
	if (!event.ctrlKey && focus()) {
		asChildOf = useParent(focus()).get()
	}
	const newBlockID = addBlock({ dataStream, asChildOf, after: blockID })
	LOG(`New block created`, newBlockID, { focus: focus() })
	zoomToBlock({ id: newBlockID, focus: newBlockID }, location, navigate)
}
