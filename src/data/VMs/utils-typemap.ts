import { Static } from '@sinclair/typebox'
import { CoerceToString } from '../../types/util-types'
import type { EntityID } from '../datalog/datom-types'
import { ApplogStream } from '../datalog/query/engine'
import { TypeMap } from './TypeMap'

export type TypeMapKeys = keyof typeof TypeMap
export type TypeMapValueTB<VM extends TypeMapKeys = TypeMapKeys> = typeof TypeMap[VM]
export type VMstatic<VM extends TypeMapKeys = TypeMapKeys> = Static<TypeMapValueTB<VM>>

export type EntityNames = `${TypeMapKeys}`
export const KnownAttrs = {} as {
	[EachEntity in EntityNames]: Array<keyof TypeMapValueTB<EachEntity>['properties']>
}
Object.entries(TypeMap).forEach(
	([EachEntity, eachTypeBox]) => KnownAttrs[`${EachEntity}`] = Object.keys(eachTypeBox.properties),
)
export type TypeAttrOptionsAll = {
	[EachEntity in EntityNames]: keyof TypeMapValueTB<EachEntity>['properties']
}
export type TypeAttrOptions<wType extends TypeMapKeys = TypeMapKeys> = keyof TypeMapValueTB<wType>['properties']

export type TypeAttrTB<wType extends TypeMapKeys = TypeMapKeys> = TypeMapValueTB<wType>[`properties`]
export type KnownAtMap = {
	[K in TypeMapKeys]: {
		[P in keyof TypeAttrTB<K> as Uppercase<CoerceToString<P>>]: P
	}
}
export const knownAtMap: KnownAtMap = {} as KnownAtMap
Object.entries(TypeMap).forEach(([entityName, typeBox]) => {
	const entityMapping: Record<string, string> = {}
	for (const key in typeBox.properties) {
		entityMapping[key.toUpperCase()] = key
	}
	;(knownAtMap as any)[entityName] = entityMapping
})

export type VMap<VM extends TypeMapKeys = TypeMapKeys> = Map<
	VM,
	Map<EntityID, VMstatic<VM>>
>
export const UniVMaps = new Map<
	ApplogStream,
	VMap<TypeMapKeys>
>()
