import { Logger } from 'besonders-logger/src'
import { autorun } from 'mobx'
import { useBlockVM, useRawDS } from '../../ui/reactive'
import { getApplogDB } from '../ApplogDB'
import type { EntityID } from '../datalog/datom-types'
import { ApplogStreamWithoutFilters, assertRaw } from '../datalog/query/engine'
import { ObjectBuilder } from './builder'
import { getMappedVMtoExtend, getUseFx } from './MappedVMbase'
import { knownAtMap, VMstatic } from './utils-typemap'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.VERBOSE) // eslint-disable-line no-unused-vars

export const { Relation: REL } = knownAtMap
export const REL_ID_LENGTH = 7
export const RELATION = 'Relation' as const

// export type relT = VMstatic<typeof RELATION>
// export const RelBuilder = class extends ObjectBuilder<relT> { }
export class RelationVM extends getMappedVMtoExtend(RELATION) {
	// initialize = function initializeRelationVM(instanceRef: typeof RelationVM, stream: ApplogStream): void {
	//     // this is meant to mutate the instance and create any side effects and listeners needed
	//     // consider a strict run once strategy if appropriate
	//     DEBUG(this, { instanceRef, en: this.en, stream })

	//     // makeObservable(instanceRef, {
	//     //     after: computed,
	//     // })
	// }
	/*
        // not needed as auto getters and setters are provided out of the box
        // get block() { return useEntityAt<string>(this.en, REL.BLOCK)[0]() }
    */
	get typed() { // TODO generic
		return this as unknown as InstanceType<typeof RelationVM> & VMstatic<typeof RELATION>
	}
	get parentBlock() {
		return useBlockVM(this.typed.childOf)
	}
}

export const RelBuilder = ObjectBuilder<VMstatic<'Relation'>, any> // TODO: runtime/typebox checking is not actually bound here, just TS generics
export const useRel = RelationVM.prototype.use = getUseFx<typeof RelationVM, typeof RelBuilder, 'Relation'>(
	RELATION,
	RelationVM,
	RelBuilder,
)

let commitOnce = false
export const relVMtest = (relationID?: EntityID, ds?: ApplogStreamWithoutFilters) => {
	ds = ds ?? useRawDS() ?? assertRaw(getApplogDB())

	const en = relationID ?? 'aefd2758' // kid: 'b3290703' //parent: 'f7e2bb3f'

	const validRelNoID = {
		after: 'null',
		block: 'b3290703',
		childOf: 'f7e2bb3f',
	} // satisfies relT

	const [relVMempty, newRelB] = useRel({}) // insufficient init for valid object returns a builder
	const updatedApplogsNewB = newRelB
		.update({ block: 'b3290703' })
		.build() // returns [{en: '334e960', at: 'block', vl: 'b3290703', ag: '869a53a'}]
	// .commit() // would commit that applog and in this case bless the new Relation
	DEBUG('relVM builder new', { updatedApplogsNewB, relVMempty, newRelB })

	const [relVMexisting, relBexisting] = useRel(en, ds)
	const updatedApplogsExisting = relBexisting
		.update({ block: 'b3290703', after: 'f7e2bb3f' })
		.build() // returns [{ at: 'block', vl: 'b3290703', ...ag_en}, { at: 'after', vl: 'f7e2bb3f' ...ag_en} }]
	// .commit() // would commit those applogs

	const parentBlockVM = relVMexisting.parentBlock
	DEBUG('relVM builder existing', { updatedApplogsExisting, parentBlockVM, relVMexisting, relVMempty, relBexisting })

	const [relVMnewLoaded, newRelBloaded] = useRel(validRelNoID, ds)
	const updatedApplogsLoaded = newRelBloaded.build()
	DEBUG('relVM builder newLoaded', { updatedApplogsLoaded, relVMnewLoaded, newRelBloaded })
	if (!commitOnce) {
		autorun(() => {
			const updatedApplogsCommit = newRelBloaded.commit() // creates a reactivity loop without this commitOnce trap
			commitOnce = true
			DEBUG('relVM builder aftercommit', { relVMnewLoaded, updatedApplogsCommit, block: relVMnewLoaded.block }) // TODO suss out rx block is null
		})
	}

	// TODO consider
	// const relVMnewVM = useRel(validRel) // full init with valid object returns a new VM
	// const relVMupVM = useRel({ en, block: 'b3290703' }) // does the update and returns the VM
}

export function buildRelation() {
	const rel = ObjectBuilder.create<InstanceType<typeof RelationVM>>()
	return rel
}
