import { BOOL, Obj, Opt, STR } from '../../types/util-types'
import { EntityID, URL } from '../datalog/datom-types'

// only the props that belong in the wovin Applogs belong here
// all other props needed for rendering belong on the VMs as _privates or getters
export const TypeMap = {
	Relation: Obj({
		// en: EntityID,
		block: EntityID,
		childOf: EntityID,
		isExpanded: Opt(BOOL),
		after: Opt(EntityID),
	}),
	Block: Obj({
		// en: EntityID,
		content: STR,
		mirrors: Opt(STR),
		pullUrl: Opt(URL),
	}),
	Publication: Obj({
		// en: EntityID,
		name: STR,
		// TODO ...
	}),
	Subscription: Obj({
		// en: EntityID,
		name: STR,
		// TODO ...
	}),
} as const
