import { Logger } from 'besonders-logger/src'
import { action } from 'mobx'
import { insertApplogs, prepareSingleApplog } from '../ApplogDB'
import { ApplogForInsert, ApplogValue, EntityID } from '../datalog/datom-types'
import { ApplogStream } from '../datalog/query/engine'
import { getHashID } from '../datalog/utils'
import { dateNowIso } from '../Utils'
import { TypeMapKeys } from './utils-typemap'
const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

type Expand<T> = { [K in keyof T]: T[K] }
type ValueBuilder<T extends Record<string, unknown>, K extends string> = {
	value: <V>(value: V) => ObjectBuilder<{ [Key in keyof (T & { [k in K]: V })]: (T & { [k in K]: V })[Key] }>
}

/*
A note about TypeMap and the relation to our CUB builder pattern
This Class is generic, but for our auto mapped VMs we only pass VMstatic<VMname> to ObjectBuilder as the Generic type
This means that the built objects can be checked against the property maps within TypeMap
// TODO check during update and build and warn/error appropriately
 */
/* inspired by: https://medium.hexlabs.io/the-builder-pattern-with-typescript-using-advanced-types-e05a03ffc36e */
export class ObjectBuilder<
	TARGET extends {},
	CURRENT extends Partial<TARGET> = {},
> {
	constructor(
		private jsonObject: CURRENT,
		public en: EntityID,
		private readonly typeTB: TypeMapKeys,
	) {}
	private get atPrefix() {
		return this.typeTB.toLowerCase()
	}

	static create<TARGET>(init: Partial<TARGET> = {} as Partial<TARGET>, en?: EntityID, typeTB?: TypeMapKeys): ObjectBuilder<TARGET> {
		// en = en ?? getHashID({ ...init, ts: dateNowIso() }, 7) // - we should do that as late as possible (when building)
		return new ObjectBuilder<TARGET>(init, en, typeTB)
	}

	update(updateObj: Partial<TARGET>) {
		// TODO: typebox check partial?
		Object.assign(this.jsonObject, updateObj)
		return this as ObjectBuilder<TARGET, CURRENT & typeof updateObj>
		// return new ObjectBuilder({ ...this.jsonObject, ...updateObj }, this.en, this.typeTB)
	}
	build(ds?: ApplogStream): ApplogForInsert[] {
		// ds = ds ?? useCurrentDS() ?? getApplogDB()
		// ds = ds.filters.includes('withoutHistory') ? ds : withoutHistory(ds)
		const en = this.en ?? getHashID({ ...this.jsonObject, ts: dateNowIso() }, 7) // ? id generation
		if (en !== this.en) this.en = en
		// TODO: typebox check

		const applogs = []
		for (const [atName, vl] of Object.entries(this.jsonObject as Record<string, ApplogValue>)) {
			const at = `${this.atPrefix}/${atName}`
			const appLogToAdd = prepareSingleApplog({ en, at, vl })
			// if (!ds.hasApplogWithDiffTs(appLogToAdd)) {
			applogs.push(appLogToAdd)
			// ? i think this should only be added if the most recent applog withoutHistory is not identical
			// - I'd like to avoid the builder needing to know about data
			// }
		}
		return applogs
	}
	commit = action(function commitBuilder() {
		// ? or 'save'
		const applogs = this.build()
		DEBUG('commiting', this, { applogs })
		return {
			en: applogs[0].en, // HACK to get resulting ID
			logs: insertApplogs(applogs),
		}
	})

	/* actually prefer to exclude these, as update can cover it just fine
        // add vs set - add feels more buildy set more familiar
        set<K extends keyof TARGET>(key: K, value: TARGET[K])
        ///*  : ObjectBuilder<{ [Key in keyof (CURRENT & { [k in K]: TARGET[K] })]: (CURRENT & { [k in K]: TARGET[K] })[Key] }>
        {
            const nextPart = { [key]: value } as { [k in K]: TARGET[K] };
            return new ObjectBuilder({ ...this.jsonObject, ...nextPart }, this.en);
        }
        key<K extends keyof TARGET>(key: K)
        //* : ValueBuilder<CURRENT, TARGET[K]>
        {
            return { value: (value: TARGET[K]) => this.set(key, value) };
        }
     */
}
