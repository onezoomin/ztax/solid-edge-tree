import { Logger } from 'besonders-logger/src'
import { debounce } from 'lodash-es'
import { action, autorun, computed, makeObservable, observable, Reaction, runInAction } from 'mobx'
import { notNull } from 'unocss'
import { useBlockAt, useCurrentDS, useKidRelations, useKidVMs, usePlacementRelations, withDS } from '../../ui/reactive'
import { getApplogDB } from '../ApplogDB'
import { parseBlockContentValue, persistBlockContent, persistBlockisDeleted, tiptapToPlaintext } from '../block-utils'
import { compareBlockContent } from './../block-utils'
import { BLOCK_DEF, REL_DEF } from '../data-types'
import { Applog, EntityID } from '../datalog/datom-types'
import { queryDivergencesByPrev } from '../datalog/query/divergences'
import { ApplogStream } from '../datalog/query/engine'
import { withoutHistory } from '../datalog/query/queries'
import { query } from './../datalog/query/queries'
import { joinStreams } from '../datalog/utils'
import { ObjectBuilder } from './builder'
import { getMappedVMtoExtend, getUseFx } from './MappedVMbase'
import { knownAtMap, VMstatic } from './utils-typemap'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

export const { Block: BLK } = knownAtMap
const debounceWait = 1500
export const REL_ID_LENGTH = 7

export type TiptapContent = Record<string, any>

// export type blkT = VMstatic<'Block'>
// export class BlkBuilder extends ObjectBuilder<blkT> {}

const BLOCK = 'Block'
export class BlockVM extends getMappedVMtoExtend(BLOCK, undefined, ['content']) {
	// get typed() { // TODO generic
	// 	return this as unknown as InstanceType<typeof BlockVM> & blkT
	// }
	public editingPos: number = null
	private editingBasedOn: Applog = null // ? Applog? or rather pv/cid ?

	// observable
	_content: TiptapContent

	get exists() {
		return !this.entityStream.isEmpty // HACK: use a query that's less expensive
	}

	get kidRelations() {
		return withDS(this.stream, () => useKidRelations(this.en))
	}
	get kidVMs() {
		return withDS(this.stream, () => useKidVMs(this.en))
	}
	get parentRelations() {
		return withDS(this.stream, () => usePlacementRelations(this.en))
	}

	persistContent = async (newContent: TiptapContent) => {
		return persistBlockContent(this.stream, this.en, newContent, this.editingBasedOn?.cid)
	}
	markNotEditing = () => {
		this.editingBasedOn = this.editingPos = null
	}
	debouncedSetContent = debounce(this.persistContent.bind(this), debounceWait, { 'leading': false /* 'maxWait': 2000  */ })
	debouncedMarkNoEdit = debounce(this.markNotEditing.bind(this), debounceWait + 100) // same but without maxWait

	get isEditing() {
		return !!this.editingBasedOn
	}

	get contentApplog() {
		return this.entityStream?.applogs?.findLast(l => l.at === BLOCK_DEF.content) // HACK: use divergence handling
	}
	get contentFromApplog() {
		return this.contentApplog?.vl
	}
	get content() {
		if (!this._content) WARN('Empty _content?!', this)
		return this._content // || withDS(this.stream, () => useBlockAt(this.en, 'content').get()) // HACK: unify with contentFromApplog
	}
	set content(newContent: TiptapContent) {
		// if (`${newContent}` === 'undefined') WARN(`Setting undefined as content`, newContent) // HA CK: trying to catch a weird bug
		// we are assuming that the only time this setter will be called is during editing
		// thus we set the editingBasedOn prop
		if (this._content === newContent) {
			VERBOSE('BlockVM noop setting content', newContent)
		} else {
			this._content = newContent // optimistic ui, BlockVM debounces persist
			this.debouncedSetContent(newContent) // lazy atom writing - if you need to avoid optimistic ui and rely on observables use setContent directly
			this.editingBasedOn = this.contentApplog
			this.debouncedMarkNoEdit()
		}
	}
	get contentDivergences() {
		const divergences = queryDivergencesByPrev(this.entityStream)
		if (divergences.length <= 1) return null
		LOG('Found content divergence:', divergences)
		return divergences
	}
	get contentPlaintext() {
		DEBUG(`[contentPlaintext] input:`, this.content)
		const text = tiptapToPlaintext(this.content)
		DEBUG(`[contentPlaintext] output:`, text)
		return text
	}

	async setDeleted(newisDeleted) {
		persistBlockisDeleted(this.stream, this.en, newisDeleted)
	}
	get isDeleted() {
		return this.entityStream.applogs.findLast(l => ['isDeleted', 'block/isDeleted'].includes(l.at)).vl // HACK: handle old isDeleted properly
	}
	set isDeleted(newIsDeleted) {
		if (this.isDeleted === newIsDeleted) {
			VERBOSE('BlockVM noop setting isDeleted', newIsDeleted)
		} else {
			void this.setDeleted(newIsDeleted) // lazy atom writing - if you need to avoid optimistic ui and rely on observables use setisDeleted directly
		}
	}

	get recursiveKidCount() {
		let trace = []
		const currentDS = withoutHistory(this.stream)
		const recurseKid = (en: EntityID, recursionGuard: number) => {
			if (recursionGuard > 42) {
				ERROR(`Recursion guard`, trace)
				throw new Error(`[recursiveKidsCount.maxDepth] How deep can you go?`) // ? is 42 the answers.length)
			}
			let count = 0
			const kidsQuery = query(currentDS, [
				{ en: '?rel', at: 'relation/childOf', vl: en },
				{ en: '?rel', at: 'relation/block', vl: '?kid' },
			])
			if (!kidsQuery.isEmpty) {
				const kids = kidsQuery.records.map(({ kid }) => kid) as string[]
				trace.push({ en, kids })
				for (const kid of kids) {
					count += 1 + recurseKid(kid, recursionGuard + 1)
				}
			}
			return count
		}
		return recurseKid(this.en, 0)!
	}
	get logsOfRecursiveKids() {
		let trace = []
		const currentDS = withoutHistory(this.stream)
		const recurseKid = (en: EntityID, recursionGuard: number) => {
			if (recursionGuard > 42) {
				ERROR(`Recursion guard`, trace)
				throw new Error(`[recursiveKids.maxDepth] How deep can you go?`) // ? is 42 the answers.length)
			}
			const blockLogsWithHistory = query(this.stream, [
				{ en: en, at: BLOCK_DEF._attrsFull },
			])
			if (blockLogsWithHistory.isEmpty) return null
			const currentKidsQuery = query(currentDS, [
				{ en: '?rel', at: 'relation/childOf', vl: en },
				{ en: '?rel', at: 'relation/block', vl: '?kid' },
			])
			let allLogs = blockLogsWithHistory.allApplogs
			let relations: string[] = []
			let kids: string[] = []
			if (!currentKidsQuery.isEmpty) {
				relations = currentKidsQuery.records.map(({ rel }) => rel) as string[]
				kids = currentKidsQuery.records.map(({ kid }) => kid) as string[]
				const relLogsWithHistory = query(this.stream, [
					{ en: relations, at: REL_DEF._attrsFull },
				])
				allLogs = joinStreams([allLogs, relLogsWithHistory.allApplogs, currentKidsQuery.allApplogs])
				if (kids.length) {
					trace.push({ en, kids })
					const kidStreams = kids.map(kid => recurseKid(kid, recursionGuard++)).filter(notNull)
					if (kidStreams.length) {
						const joinedStreams = joinStreams(kidStreams)
						allLogs = joinStreams([allLogs, joinedStreams])
					}
				}
			}

			DEBUG('All logs related to immediate kids', { myAttrs: blockLogsWithHistory, kidsQuery: currentKidsQuery, kids, allLogs })
			return allLogs
		}
		return recurseKid(this.en, 0)!
	}
	initialize = action((instanceRef: InstanceType<typeof BlockVM>, stream: ApplogStream) => {
		// TODO avoid re-initialization
		VERBOSE(`Init BlockVM`, instanceRef)
		const blockID = instanceRef.en
		if (!blockID) throw ERROR(`Empty BlockID`, instanceRef)
		// assertRaw(stream) - not actually required (only for history)
		// ? warn in functions that requre history?

		// if (instanceRef.entityStream.isEmpty) /* throw ERROR */ WARN(`[BlockVM] created for unknown ID:`, blockID)

		let initialized = false
		const runAndTrack = () => {
			reaction.track(() => {
				const newContent = withDS(stream, () => useBlockAt(blockID, 'content').get() as string)
				if (typeof newContent !== 'string') WARN(`[BlockVM] block/content is not a string:`, newContent, { blockID })
				const parsedNew = parseBlockContentValue(newContent)
				const oldContent = initialized ? instanceRef.content : null
				DEBUG(`[BlockVM#${blockID}] block/content ${initialized ? 'update' : 'init'}:`, parsedNew, {
					editing: instanceRef.editingBasedOn,
					old: oldContent,
				})
				if (!instanceRef.editingBasedOn) {
					if (compareBlockContent(instanceRef._content, parsedNew)) {
						VERBOSE('skipping content update via reaction', {
							newContent,
							oldContent,
							isEditing: instanceRef.editingBasedOn,
						})
					} else {
						runInAction(() => instanceRef._content = parsedNew)
					}
				} else if (!compareBlockContent(instanceRef._content, parsedNew)) {
					WARN(`content update while isEditing`, { newContent, current: oldContent, isEditing: instanceRef.editingBasedOn })
				}
				initialized = true
			})
		}

		// HACK: using a Reaction manually because even with fireImmediately reaction(..) would wait until the current action is done
		const reaction = new Reaction(`BlockVM#${instanceRef.en}.content`, (...args) => {
			VERBOSE(`[BlockVM#${instanceRef.en}] reaction.invalidate`, args)
			runAndTrack()
		})
		runAndTrack() // initial run

		// DEBUG.isDisabled || createEffect(() => DEBUG('[blockRx] updated block logs ', instanceRef.en, instanceRef.blockLogsSig()))
		// DEBUG.isDisabled || createEffect(() => DEBUG('[blockRx] updated relations', instanceRef.en, instanceRef.kidsDefsMemo()))

		if (!VERBOSE.isDisabled) {
			autorun(() => VERBOSE('entityStream appears to have changed', blockID, [...instanceRef.entityStream.applogs]))
			autorun(() => VERBOSE('content appears to have changed', blockID, instanceRef.content))
			autorun(() => VERBOSE('content2 appears to have changed', blockID, [instanceRef.contentFromApplog]))
		}

		// ! hoping we don't need this, but can flag the things explicitly otherwise
		// I, manu, think _content needs to be made observable
		makeObservable(instanceRef, {
			// apparently not parentRelations: observable, // ? is this observable needed when the return value is explicitly observable?

			_content: observable,
			content: computed,
			contentDivergences: computed,
			contentPlaintext: computed,
		})
		// runInAction(() => instanceRef._content = withDS(instanceRef.stream, () => useBlockAt(blockID, 'content')).get() as string) // ? initial _content set
	})
}

export const BlockBuilder = ObjectBuilder<VMstatic<'Block'>, any> // TODO: runtime/typebox checking is not actually bound here, just TS generics
export const useBlk = getUseFx<typeof BlockVM, typeof BlockBuilder, 'Block'>(BLOCK, BlockVM, BlockBuilder)
BlockVM.prototype.use = useBlk

export const blkVMtest = (vl = '5a650ede', ds?: ApplogStream) => {
	ds = ds ?? useCurrentDS() ?? getApplogDB()
	// vl = 'b3290703'  //rel 'aefd2758' // kid: 'b3290703' //parent: 'f7e2bb3f'
	const [bVM, bB] = useBlk(vl)
	const c = bVM.content
	bB.update({ content: bVM.content + 'changed' })
		.commit()
	autorun(() => DEBUG('[BlkVM testing]', { bVM, c, bB }))
	const [bVM2] = useBlk(vl)
}
