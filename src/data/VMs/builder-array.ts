import { EntityID } from '../datalog/datom-types'
/*
bless/reorder/rebless
{at: 'block/tags[*]', vl:'['tagEntityIDa','tagEntityIDb','tagEntityIDc']' ...}

push:
{at: 'block/tags[+]', vl:'['tagenIDd','tagenIDe']' ...}

remove:
{at: 'block/tags[-]', vl:'['tagenIDe']' ...}

prepend?
{at: 'block/tags[<+]', vl:'['tagenIDy,'tagenIDz']' ...}

result:
['tagenIDy','tagenIDz', 'tagEntityIDa','tagEntityIDb','tagEntityIDc', 'tagenIDd']

using 'indexs' to insert into the middle, is not practical nor SADL friendly
so [*] is used to reorder or insert into the middle
generally this pattern will be more appropriate for inherently unordered arrays so that [+/-] can be used simply

 */

/*

alternative ?messy? option:
all arrays are treated like memberships, so that order can be an optional metadata on each membership
this resembles traditional relational db "join tables" https://blog.tadabase.io/using-join-tables/#enter-join-tables
this is how we do relations now, and with the flexible schema that applogs gives,
it offers much flexibility to describe the relations/memberships - also similar to facets / edge attributes in a graph db
eg https://dgraph.io/docs/query-language/facets/

https://neo4j.com/docs/getting-started/appendix/graphdb-concepts/#graphdb-relationship-type
differentiates between a relationship TYPE(only 1) VS properties(0-many) of/on the relationship
 */

const Tag = {
	name: 'displayName',
	aliasArr: ['akaFoo', 'akaBar'],
	en: 'hash(name)', // could be nice if this only used the hash of the name prop
	// and attempts to create a new tag with an existing alias would get the existing ID
}
const TagGroup = {
	name: 'groupDisplayName',
	aliasArr: ['akaFoo', 'akaBar'],
}
const TagEffect = {
	entityType: 'Block',
	tag: 'hash("RED")',
	effect: { bgColor: '#F00' }, // i love that F00 is red
}
const EntityTag = {
	target: EntityID,
	tag: 'TagID',
	after: 'TagID',
	group: 'TagGroupID', // contextual only: sometimes the tag @1zm may semantically
	// belong in the 'mentions' group, sometimes in the 'assignees' group
}
