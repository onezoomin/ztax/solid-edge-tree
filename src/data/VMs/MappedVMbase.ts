import { TypeCompiler } from '@sinclair/typebox/compiler'
import { Logger } from 'besonders-logger/src'
import { useEntityAt, useRawDS, withDS } from '../../ui/reactive'
import { type EntityID, EntityID_LENGTH } from '../datalog/datom-types'
import { type ApplogStream, rollingFilter } from '../datalog/query/engine'
import { getHashID } from '../datalog/utils'
import { dateNowIso } from '../Utils'
import { ObjectBuilder } from './builder'
import { TypeMap } from './TypeMap'
import type { TypeMapKeys, VMap, VMstatic } from './utils-typemap'
import { KnownAttrs, UniVMaps } from './utils-typemap'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO, { prefix: '[MappedVM]' })

export function getInitializedMap<VMNameT extends TypeMapKeys>(VMname: VMNameT, stream: ApplogStream) {
	let mapForStream = UniVMaps.get(stream)
	if (!mapForStream) {
		mapForStream = new Map() // empty map for the actual VMs
		DEBUG('created new map for stream', stream)
		UniVMaps.set(stream, mapForStream)
	}
	if (mapForStream.get(VMname)) {
		VERBOSE('using existing map', VMname, { UniVMaps, stream, newInstance: this })
	} else {
		mapForStream.set(VMname, new Map())
		DEBUG('created new map for VM', VMname, { UniVMaps, stream, newInstance: this })
	}
	return mapForStream as VMap<typeof VMname>
}
export function getMappedVMtoExtend<whichVM extends TypeMapKeys>(
	VMname: whichVM,
	VMtype = TypeMap[VMname],
	skipGetters = [] as typeof KnownAttrs[whichVM],
) {
	const attrs = KnownAttrs[VMname]
	// interface NamedMapVM<wVM extends whichVM> {
	// 	new(en: EntityID, stream: ApplogStream, initial?: VMstatic<wVM>): VMstatic<wVM> // HACK ts nasty

	// 	// getVMap: (stream: ApplogStream) => VMap<whichVM>
	// 	initialize: (instance: VMstatic<whichVM>, stream: ApplogStream) => void
	// }
	const lowerCaseAttrNamespace = VMname.toLowerCase()
	const getVMap = (stream: ApplogStream) => {
		const mapForStream = getInitializedMap(VMname, stream)
		const thisVMap = mapForStream.get(VMname)
		VERBOSE('get VMap', VMname, { stream, UniVMaps, thisVMap })
		return thisVMap
	}
	const getMappedVM = (en, stream) => {
		const thisVMap = getVMap(stream)
		if (!thisVMap) {
			throw ERROR('missing VMap for', { thisVMap, stream })
		}
		return thisVMap.get(en)
	}

	VERBOSE({ VMname, VMtype })
	const compiledChecker = TypeCompiler.Compile(VMtype)
	type VMTypeT = VMstatic<whichVM>
	// const BuilderClass = class extends ObjectBuilder<relT> { } // doesn't really do anything useful

	const NamedMapVM = class {
		static getMappedVM = getMappedVM
		static get<T extends typeof NamedMapVM>(this: T, en: EntityID, stream: ApplogStream) /* : InstanceType<T> */ {
			VERBOSE(`[getVM<${VMname}>]`, en, this, stream)

			const thisVMap = getVMap(stream)
			const existing = getMappedVM(en, stream)
			if (existing) {
				return existing as InstanceType<T> // HACK: type assumption
			}

			const newVM = new this(en, stream, 'imsure') as InstanceType<T>
			VERBOSE(`[getVM<${VMname}>] newVM:`, { newVM, hasInit: (newVM as any).initialize })
			if ((newVM as any).initialize) {
				;(newVM as any).initialize(newVM, stream)
			}
			thisVMap.set(en, newVM)
			return newVM
			// queueMicrotask(() => {
			//     VERBOSE('oddly enough we need to defer this until the extending class has the chance to overide initialize')
			//     abstractInitialize === this.initialize || this.initialize(this, stream)
			// })
		}
		// static builderClass = BuilderClass

		constructor(
			public en,
			public stream: ApplogStream,
			deprecationWarning: 'imsure',
		) {
			const existing = getMappedVM(en, stream)
			if (deprecationWarning !== 'imsure' || existing) {
				throw ERROR('deprecated: use getVM instead of constructor')
			}

			DEBUG('initalizing', VMname, en, { skipGetters, stream, VMtype, selff: self, thiss: this, init: (this as any).initialize })

			// add getters and setters for eachKnownAttr of the VMtype
			for (const eachAttr of attrs) {
				if (eachAttr === 'en' || skipGetters.includes(eachAttr)) {
					continue
				}
				const [getter, setter] = withDS(stream, () => useEntityAt<string>(this.en, `${lowerCaseAttrNamespace}/${eachAttr.toString()}`))
				Object.defineProperty(this, eachAttr.toString(), {
					get: getter, // () { return getter }, // warning this was not reactive when useEntityAt returned a memo
					set: setter,
					enumerable: true,
					configurable: true,
				})

				// ? also add {at}PvVl and {at}PvCID ?
				VERBOSE('added getter and setter for', { eachAttr, instance: this })
			}
			// // @ts-expect-error
			// return existing as VMstatic<whichVM> & ReturnType<this>
		}

		get entityStream() {
			return rollingFilter(this.stream, { en: this.en })
		}
		get description() {
			return `I am an instance of ${this.constructor.name} with en=${this.en}`
		}
		use: ReturnType<typeof getUseFx>

		static buildNew(init: Partial<VMTypeT> = {}, en?: EntityID) {
			return ObjectBuilder.create<VMTypeT>(init, en, VMname)
		}
		buildUpdate(init: Partial<VMTypeT> = {}) {
			return ObjectBuilder.create<VMTypeT>(init, this.en, VMname)
		}

		check = compiledChecker // TODO try it out
	}

	return NamedMapVM
}

export function getUseFx<
	entityVM extends ReturnType<typeof getMappedVMtoExtend<entityT>>,
	entityB extends typeof ObjectBuilder<VMstatic<entityT>>,
	entityT extends TypeMapKeys = TypeMapKeys,
>(
	VMname: TypeMapKeys,
	VMclass: entityVM,
	Bclass: entityB,
) {
	return function use(prop: string | Partial<VMstatic<entityT>>, ds?: ApplogStream) {
		if (!prop) {
			return [] as unknown as [InstanceType<entityVM> & VMstatic<entityT>, InstanceType<entityB>] // HACK: wrongly makes TS think the result is never empty
		}
		let en: string, initUp: Omit<Partial<VMstatic<entityT>>, 'en'>
		if (typeof prop === 'string') {
			en = prop
			initUp = {}
		} else {
			;({ en, ...initUp } = prop as Partial<VMstatic<entityT>>)
		}
		if (!en) {
			const init = { ...initUp, ts: dateNowIso() }
			en = getHashID(init, EntityID_LENGTH)
			DEBUG('creating new', { en, init })
		}
		ds = ds ?? useRawDS()
		VERBOSE('use', VMname, en, VMclass, Bclass)
		const instanceVM = VMclass.getMappedVM(en, ds) ?? VMclass.get(en, ds) as InstanceType<entityVM> & VMstatic<entityT> // if new, this RelVM will have no data, but should react to relB.commit()
		const instanceB = Bclass.create(initUp, en, VMname) as InstanceType<entityB> // the "restProps" will be already on the Builder

		return [instanceVM as InstanceType<entityVM> & VMstatic<entityT>, instanceB] as [
			InstanceType<entityVM> & VMstatic<entityT>,
			InstanceType<entityB>,
		]
	}
}
export function getObjectBuilderFor<whichVM extends TypeMapKeys = TypeMapKeys>() {
}
