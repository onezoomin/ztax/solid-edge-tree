import { Static, Type } from '@sinclair/typebox'
import { Value } from '@sinclair/typebox/value'
import { Logger } from 'besonders-logger/src'
import stringify from 'safe-stable-stringify'
import { getApplogDB } from './ApplogDB'
import { EntityID } from './datalog/datom-types'
import { ApplogStream, rollingFilter } from './datalog/query/engine'
import { withoutDeleted } from './datalog/query/queries'
const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

const Relation = Type.Object({
	en: EntityID,
	block: EntityID,
	childOf: Type.Union([EntityID, Type.Null()]),
	isDeleted: Type.Optional(Type.Boolean()),
	after: Type.Optional(EntityID),
})
type Relation = Static<typeof Relation>

// const RelationAttrs = Type.Union(Object.keys(Relation.properties).map((k: keyof Relation) => Type.Literal(k)))
const RelationAttrs = Object.keys(Relation.properties)
type RelationAttrs = Array<keyof typeof Relation.properties>

export const checkCheck = (ds?: ApplogStream) => {
	ds = ds ?? getApplogDB()
	const validEntityID = Value.Check(EntityID, '656eb60b')
	const invalidTooShortEntityID = Value.Check(EntityID, '65660b')
	const invalidTooLongEntityID = Value.Check(EntityID, '65660b5660')
	const invalidNonHexEntityID = Value.Check(EntityID, '656xx60b')
	DEBUG({ validEntityID, invalidTooShortEntityID, invalidTooLongEntityID, invalidNonHexEntityID }) // {validEntityID: true, invalidTooShortEntityID: false, invalidTooLongEntityID: false, invalidNonHexEntityID: false}

	const rollingFilterAttIndex = new Map(RelationAttrs.map(
		(at) => [at, rollingFilter(ds, { at: `relation/${at}` })],
	))
	const vl = '6eb99b29'
	const rollingFilterMapForVM = new Map(RelationAttrs.map(
		(at) => [at, rollingFilter(withoutDeleted(ds), { at: `relation/${at}`, vl })], // with or withoutDeleted interesting to consider
	))
	DEBUG({ RelationAttrs, rollingFilterMapForVM, rollingFilterAttIndex, attrs: stringify(RelationAttrs) })
}
export interface RelationModelDef {
	en: EntityID
	isDeleted?: boolean
	isExpanded?: boolean

	/** the ID of "this" block */
	block: EntityID
	/** the ID of the parent of "this" block (in the scope of this relation) */
	childOf: EntityID | null
	/** the ID of the block before this block (in the scope of this parent) */
	after?: EntityID
}
