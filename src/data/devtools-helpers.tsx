import { runInAction } from 'mobx'
import { prettifyStreamName } from './datalog/mobx-utils'
import * as queries from './datalog/query/queries'

if (typeof window === 'object') {
	Object.assign(window, queries)
	Object.assign(window, {
		runInAction,
		prettifyStreamName,
		printStreamName(name: string) {
			console.log(prettifyStreamName(name))
		},
		freezePersist() {
			localStorage.setItem('note3.dev.noPersist', 'nopersist')
			console.log('noPersist ON')
		},
		resumePersist() {
			localStorage.setItem('note3.dev.noPersist', '')
			console.log('noPersist OFF')
		},
	})
}
