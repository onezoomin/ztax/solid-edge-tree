import Dexie from 'dexie'
import { AgentID } from './datalog/datom-types'

export interface IPublication {
	id?: string // string hash of pub (used as unique id in IDB) `W3Name.create().toString()` starts with k51qzi5uqu5d
	createdAt: string // ISO timestamp of creation
	name: string // nick name for the pub
	isDeleted?: boolean

	pk: Uint8Array // exported privatekey - needed to create WritableName for publishing

	autopush: boolean
	lastPush: string | null
	lastCID?: string
	latestLogTs?: string

	publishedBy: string // local user appAgent
	selectors?: string[] // to be used as a filter for which applogs to pub
	encryptedFor?: string | null // short agentHash
	encryptedWith?: CryptoKey | null // AES-GCM derived key from ECDH keys (local private and remote public)

	// HACK WIP #39 - shared encryption
	sharedKey?: CryptoKey | null // AES-GCM derived key from ECDH keys (local private and ipns public)
	sharedAgents?: AgentID[] | null // array of string EntityIDs for the chosen agents (we need public jwkd atoms for each of them)
	sharedKeyMap?: Map<AgentID, string> | null // uses public key from each agent to derive an aes key that is used to encrypt and btoa the sharedKey that is actually used to encrypt and decrypt the applogs
}
export interface ISubscription {
	id: string // string hash of pub (used as unique id in IDB) `W3Name.create().toString()` starts with k51qzi5uqu5d
	createdAt: string // ISO timestamp of creation
	name: string // nick name for the pub
	isDeleted: boolean

	lastPull?: string | null
	lastPullAttempt?: string | null
	autopull: boolean
	lastCID?: string
	publishedBy?: string // remote publisher short agentHash
	encryptedFor?: string | undefined // short agentHash
	encryptedWith?: CryptoKey | undefined // AES-GCM derived key from ECDH keys (local private and remote public)
}
export function isPublication(obj: any): obj is IPublication {
	return obj?.pk !== undefined && obj?.lastPush !== undefined
}
export function isSubscription(obj: any): obj is ISubscription {
	return obj?.lastPull !== undefined
}

export type TSubPub = IPublication | ISubscription
export type TAppAgentCryptoKeypairTypes = 'derivation' | 'signing' | 'encryption'
export interface IAppAgentCryptoKeypair {
	ag: string // compound primary key with typg - string hash of appAgent
	type: TAppAgentCryptoKeypairTypes // compound primary key with ag
	keys: Partial<CryptoKeyPair>
}

export class Note3_StateDB extends Dexie {
	publications!: Dexie.Table<IPublication, string>
	subscriptions!: Dexie.Table<ISubscription, string>
	cryptokeys!: Dexie.Table<IAppAgentCryptoKeypair, [string, string]>
	// agents!: Dexie.Table<IEmailAddress, number>
	// config!: Dexie.Table<IPhoneNumber, number>

	constructor() {
		super('Note3_StateDB')

		this.version(13).stores({
			publications: 'id, name, createdAt, appAgent, encryptedFor, publishedBy, lastPush, latestLogTs, isDeleted',
			subscriptions: 'id, name, createdAt, lastPull, lastCID, appAgent, encryptedFor, isDeleted',
			cryptokeys: '[ag+type]',
		}).upgrade(async tx => {
			await tx.table('publications').toCollection().modify(pub => {
				pub.lastCID = pub.lastCid
				pub.createdAt = pub.ts
				delete pub.ts
				delete pub.lastCid
			})
			await tx.table('subscriptions').toCollection().modify(sub => {
				sub.lastCID = sub.lastCid
				sub.createdAt = new Date()
				delete sub.lastCid
			})
		})
		this.version(14).upgrade(async tx => {
			await tx.table('publications').toCollection().modify(pub => {
				pub.lastPush = pub.lastPush || null
				delete pub.ts
			})
			await tx.table('subscriptions').toCollection().modify(sub => {
				sub.lastPull = sub.lastPull || null
				delete sub.lastCid
			})
		})
		this.version(15).upgrade(async tx => {
			await tx.table('publications').toCollection().modify(pub => {
				pub.rootBlockIDs = pub.rootBlockID ? [pub.rootBlockID] : null
				delete pub.rootBlockID
			})
		})

		this.version(16).upgrade(async tx => {
			await tx.table('publications').toCollection().modify(pub => {
				delete pub.rootBlockIDs // replaced by selectors
			})
		})
	}
}

export const stateDB = new Note3_StateDB()
