import { Logger } from 'besonders-logger/src'
import { XMLBuilder } from 'fast-xml-parser'
import { action, comparer, untracked } from 'mobx'
import stringify from 'safe-stable-stringify'
import TurndownService from 'turndown'
import { DefaultFalse, DefaultTrue } from '../types/util-types'
import { useBlockAt, useRawDS, useRelationVM, withDS } from '../ui/reactive'
import { focusBlockAsInput as focusUserInputOnBlock } from '../ui/ui-utils'
import { insertApplogs } from './ApplogDB'
import { BLOCK_DEF, ENTITY_DEF, REL_DEF } from './data-types'
import { ApplogForInsertOptionalAgent, CidString, EntityID } from './datalog/datom-types'
import { ApplogStream, ApplogStreamOnlyCurrent } from './datalog/query/engine'
import { filterAndMap, queryAndMap, withoutHistory } from './datalog/query/queries'
import { getHashID, joinStreams } from './datalog/utils'
import { IPublication } from './local-state'
import { RelationModelDef } from './Relations'
import { dateNowIso, notNull } from './Utils'
import { BlockVM, TiptapContent, useBlk } from './VMs/BlockVM'
import { RelationVM, RelBuilder, useRel } from './VMs/RelationVM'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export const parseBlockContentValue = (value: string) => {
	if (value === undefined || value === null) return value
	if (!value.startsWith('{')) {
		// HACK too lazy to do migrations yet
		return {
			'type': 'doc',
			'content': [
				{
					'type': 'paragraph',
					'content': [
						{
							'type': 'text',
							'text': value,
						},
					],
				},
			],
		}
	}
	return JSON.parse(value)
}
export const compareBlockContent = (contentA: TiptapContent, contentB) => {
	return comparer.structural(contentA, contentB)
}
export const tiptapToPlaintext = (tiptap: TiptapContent) => {
	return tiptap?.content.map(c => c.content).flatMap(c => c.map(c => c.text)).join('')
}

export const notDeletedFilter = en => !en.isDeleted
// const debounceWait = 1500

export function persistBlockContent(dataStream: ApplogStream, blockID: EntityID, newContent: TiptapContent, pv: CidString = null) {
	const currentContent = withDS(dataStream, () => useBlockAt(blockID, 'content').get())
	// if (currentContent === null) return // not loaded yet
	if (!compareBlockContent(newContent, currentContent)) {
		DEBUG('set content', { currentContent, newContent, blockID })
		insertApplogs([{ en: blockID, at: BLOCK_DEF.content, vl: stringify(newContent), pv }] /* , { focus: true, id: blockID, pos } */) // TODO: which stream?
	} else VERBOSE('set content noop', { newContent, blockVM: this })
}

export function persistBlockisDeleted(dataStream: ApplogStream, blockID: EntityID, newisDeleted: boolean) {
	const currentisDeleted = untracked(() => withDS(dataStream, () => useBlockAt(blockID, 'isDeleted').get())) // ? not needed I think
	// if (currentisDeleted === null) return // not loaded yet
	if (newisDeleted != currentisDeleted) {
		DEBUG('set isDeleted', { currentisDeleted, newisDeleted, blockID })
		insertApplogs([{ en: blockID, at: 'block/isDeleted', vl: newisDeleted }])
	} else VERBOSE('set isDeleted noop', { newisDeleted, blockVM: this })
}

export const outdentBlk = (dataStream: ApplogStream, relation: RelationModelDef, grandParentID = null) => {
	// const appLogDB = getApplogDB()
	const blockID = relation.block
	const previousParent = relation.childOf
	if (!grandParentID) DEBUG('outdenting up to a root node', { relation })

	DEBUG('move block for outdent', { dataStream, relation, previousParent, grandParentID })
	moveBlock({ dataStream, relationID: relation.en, blockID, asChildOf: grandParentID, after: previousParent })
}

export const indentBlk = (dataStream: ApplogStream, relation: RelationModelDef) => {
	// check if its possible to indent (i am not the first or only kid -  i must have childof and after)
	if (!relation.after || !relation.childOf) return WARN('indent not possible', { relation })
	// TODO deal with it when more than one kid has the same after (actually legit if the kids were created by differnt agents)
	const moveOptions = { dataStream, relationID: relation.en, blockID: relation.block, asChildOf: relation.after }
	DEBUG('indent via', moveOptions)
	moveBlock(moveOptions)
}
export const getRecursiveKidsOPML = (rootBlockID, includeRoot = DefaultTrue) => {
	const rawRS = useRawDS()
	const rootVM = BlockVM.get(rootBlockID, rawRS)
	const opmlObj = {
		'?xml': { _version: '1.0' },
		opml: {
			'head': {
				'ownerEmail': 'gotjoshua@gmail.com',
			},
			'body': {},
			'_version': '2.0',
		},
	}
	const addContent = (bVM: BlockVM, objToMutate) => {
		objToMutate.outline = { _text: bVM.content }
		if (bVM.kidVMs.length) {
			const isSingleKid = bVM.kidVMs.length === 1
			for (const eachBlkVM of bVM.kidVMs) {
				objToMutate.outline.outline = isSingleKid
					? { _text: eachBlkVM.content }
					: bVM.kidVMs.map(eachBlkVM => ({ _text: eachBlkVM.content }))
				if (eachBlkVM.kidVMs.length) addContent(eachBlkVM, objToMutate.outline.outline)
			}
		}
		DEBUG({ opmlObj, bVM })
	}
	addContent(rootVM, opmlObj.opml.body)
	const opml = new XMLBuilder({
		ignoreAttributes: false,
		attributeNamePrefix: '_',
		format: true,
	})
	const opmlResult = opml.build(opmlObj)
	DEBUG('opml', { opml, opmlResult, opmlObj })
	return opmlResult
}
export const getRecursiveKidsJSON = (rootBlockID, format: 'md' | 'html') => {
	const rawRS = useRawDS()
	const rootVM = BlockVM.get(rootBlockID, rawRS)
	const rootUL = []
	const baseObj = { ul: rootUL }

	const addContent = (bVM: BlockVM, objToMutate) => {
		const thisRootLI = [{
			'#text': bVM.content as string | Record<string, any>,
		}]
		objToMutate.push({ li: thisRootLI })
		if (bVM.kidVMs.length) {
			const kidsLIs = []
			const kidsUL = { ul: kidsLIs }
			for (const eachBlkVM of bVM.kidVMs) {
				if (eachBlkVM.kidVMs.length) {
					addContent(eachBlkVM, kidsLIs)
				} else kidsLIs.push({ li: eachBlkVM.content })
			}
			thisRootLI.push(kidsUL)
		}
		VERBOSE({ baseObj, bVM })
	}
	addContent(rootVM, rootUL)
	const opml = new XMLBuilder({
		ignoreAttributes: false,
		attributeNamePrefix: '_',
		format: true,
		// preserveOrder: true,
		oneListGroup: true,
	})
	let htmlResult, mdResult
	if (format) {
		htmlResult = opml.build(baseObj)
		if (format == 'html') return htmlResult
		var turndownService = new TurndownService({ option: 'value' }) // https://github.com/mixmark-io/turndown
		mdResult = turndownService.turndown(htmlResult)
		return mdResult
	}
	DEBUG('copy node content', rootBlockID, { opml, htmlResult, baseObj, mdResult })
	return baseObj
}

export const RE_SELECTOR_BLOCKS = /^blocks\(([^)]+)\)/
export const RE_SELECTOR_WITHOUT_HISTORY = /(\| )?withoutHistory/
export const makeBlocksSelector = (blocks: string[], withoutHistory: boolean = false) =>
	`blocks(${blocks.join(',')})${withoutHistory ? ` | withoutHistory` : ''}`
export const toggleWithoutHistorySelector = (selector: string, withoutHistory: boolean) => {
	const match = selector.match(RE_SELECTOR_WITHOUT_HISTORY)
	if (withoutHistory) {
		return match ? selector : `${selector} | withoutHistory`
	} else {
		return match ? selector.replace(match[0], '').trim() : selector
	}
}
export function getPublicationData(rootStream: ApplogStream, publication: IPublication) {
	if (!publication.selectors?.length) {
		return rootStream
	}
	const streams = publication.selectors.map(selector => {
		return filterStreamBySelector(rootStream, selector)
	}).filter(notNull)
	DEBUG(`[getPublicationData]`, { publication, streams, rootStream })
	if (!streams.length) return null // ? EmptyStream
	return joinStreams(streams)
}
function filterStreamBySelector(rootStream: ApplogStream, selector: string): ApplogStream {
	const filters = selector.split('|').map(s => s.trim())
	const blocksMatch = filters[0].match(RE_SELECTOR_BLOCKS)
	let stream = rootStream
	if (blocksMatch) {
		filters.splice(0) // remove first filter
		const streams = blocksMatch[1].split(',').map(blockID => {
			const rootVM = BlockVM.get(blockID, rootStream) // HACK: extract function from BlockVM
			DEBUG(`[filterStreamBySelector] block`, blockID, { rootVM, logs: rootVM.logsOfRecursiveKids })
			if (!rootVM.exists) {
				return null
			}
			return rootVM.logsOfRecursiveKids
		}).filter(notNull)
		if (!streams.length) return null // ? EmptyStream
		stream = joinStreams(streams)
	}
	if (filters[filters.length - 1] === 'withoutHistory') {
		filters.splice(filters.length - 1) // remove last filter
		stream = withoutHistory(stream)
	}
	DEBUG(`[filterStreamBySelector]`, { rootStream, selector, stream })
	return stream
}

type AddBlockRelationOptions = {
	dataStream: ApplogStream
	asChildOf: EntityID | null
	after?: EntityID | null
	blockID: EntityID | null
	focus?: boolean
	bottom?: boolean
}
type MoveBlockOptions = AddBlockRelationOptions & { relationID: EntityID | null }

export async function moveBlock({ dataStream, asChildOf, after, blockID, relationID, focus = true, bottom = true }: MoveBlockOptions) {
	const newApplogs = [
		...getAddBlockRelationLogs({ dataStream, asChildOf, after, blockID, focus: true, bottom }),
	]
	if (relationID) newApplogs.push(...getDeleteRelationAtoms(relationID))
	// TODO update after situations of leftovers
	insertAndMaybeFocus(newApplogs, { focus, id: blockID, end: true })
}
export function getAddBlockRelationLogs({ dataStream, asChildOf, after, blockID, focus = true, bottom = true }: AddBlockRelationOptions) {
	const newRelationID = getHashID([asChildOf, blockID, dateNowIso()])
	const blockToMoveVM = BlockVM.get(blockID, dataStream)
	const newParentBlockVM = asChildOf && BlockVM.get(asChildOf, dataStream)
	let newApplogs: ApplogForInsertOptionalAgent[]
	if (!after) {
		if (bottom) {
			const currentBottomBlockID = after = (newParentBlockVM.kidRelations.slice(-1)[0] ?? {}).block ?? null // explicit null if there are no kids to be at the bottom of
			VERBOSE('using bottom as after', { currentBottomBlockID, newParentBlockVM, blockToMoveVM })
		} else {
			WARN('a bit odd to explicitly disable bottom and not pass after')
		}
	}
	newApplogs = getAppLogsForNewRelation({ newRelationID, asChildOf, after, blockID })
	VERBOSE('getAddBlockRelationLogs returning', { newApplogs })
	return newApplogs
}
export function getAppLogsForNewRelation({ asChildOf, after, newRelationID, blockID }: {
	newRelationID: EntityID | null
	asChildOf: EntityID | null
	blockID: EntityID | null
	after?: EntityID | null
}) {
	const newApplogs = []
	if (asChildOf) {
		newApplogs.push(
			{ en: newRelationID, at: 'relation/childOf', vl: asChildOf },
			{ en: newRelationID, at: 'relation/block', vl: blockID },
			{ en: newRelationID, at: 'relation/after', vl: after ?? null },
		)
		// TODO get parentRelation
		// TODO if(!parentRelation.isExpanded) {newApplogs.push({ en: parentRelation.id, at: REL_DEF.isExpanded, vl: true })}
	}
	return newApplogs as ApplogForInsertOptionalAgent[]
}
export type AddBlockOpts = {
	dataStream: ApplogStreamOnlyCurrent
	asChildOf: EntityID | null
	after?: EntityID | null
	content?: string
	focus?: boolean
}
export const addBlock = action(
	function addBlock({ dataStream, asChildOf, after, content = '', focus = false }: AddBlockOpts) {
		DEBUG('Adding block', { asChildOf, after, content, focus })

		const newBlockID = getHashID([asChildOf, dateNowIso()])
		const newRelationID = getHashID([asChildOf, newBlockID, dateNowIso()])

		const newApplogs = getAppLogsForNewRelation({ newRelationID, asChildOf, after: after ?? null, blockID: newBlockID })
		newApplogs.push(
			{ en: newBlockID, at: BLOCK_DEF.content, vl: content },
		)
		// if (focus) newApplogs.push(
		// 	{ en: newRelationID, at: REL_DEF.isExpanded, vl: true }, // (i) default isExpanded is true anyways
		// )
		insertApplogs(newApplogs)

		if (asChildOf) {
			insertBlockInRelChain(dataStream, newBlockID, asChildOf, after ?? null, newRelationID)
		}

		maybeFocus({ focus, id: newBlockID, end: false })
		DEBUG('Block created:', newBlockID, { newRelationID })
		return newBlockID
	},
)
function insertAndMaybeFocus(newApplogs, { id = '', focus = DefaultFalse, start = DefaultFalse, end = DefaultFalse, pos = 0 } = {}) {
	const _insertResult = insertApplogs(newApplogs)
	maybeFocus({ focus, id, end, pos, start })
}
function maybeFocus({ id = '', focus = DefaultFalse, start = DefaultFalse, end = DefaultFalse, pos = 0 } = {}) {
	if (focus && id) {
		queueMicrotask(() => {
			focusUserInputOnBlock({ id, end, pos, start })
		})
	}
}

export function getDeleteRelationAtoms(en: EntityID) {
	return [{ at: 'isDeleted', en, vl: true }]
}
// export function deleteRelation(en: EntityID) {
// 	const atoms = getDeleteRelationAtoms(en)
// 	VERBOSE('deleting relation', en, { atoms })
// 	insertApplogs(atoms)
// }
export function removeBlockRelAndMaybeDelete(dataStream: ApplogStream, blockID: EntityID, relationID: EntityID) {
	const [blockVM] = useBlk(blockID, dataStream)
	const appLogsToInsert = relationID ? getDeleteRelationAtoms(relationID) : []
	if (!relationID || blockVM.parentRelations.length <= 1) {
		appLogsToInsert.push({ en: blockID, at: 'isDeleted', vl: true })
	}
	DEBUG(`[removeBlockRelAndMaybeDelete]`, blockID, { relationID, parents: blockVM.parentRelations })
	if (!appLogsToInsert.length) WARN('somethings off, removeBlockRelAndMaybeDelete did nothing')
	else insertApplogs(appLogsToInsert)
}

export function deleteAndReplaceBlock(relationID: EntityID, newBlockID: string) {
	const relation = useRelationVM(relationID)
	// TODO what if its not the only reference of the block??
	// i think only after and isExpanded ought to be changable...
	// otherwise (i thought we already agreed that) deleting the relation and making a new one is better
	insertApplogs([
		// delete block
		{ en: relation.block, at: ENTITY_DEF.isDeleted, vl: true },
		// update relation to point at pasted block
		{ en: relation.en, at: REL_DEF.block, vl: newBlockID },
	])
}

export function removeBlockFromRelChain(dataStream: ApplogStreamOnlyCurrent, blockID: EntityID, relationToParentID: EntityID) {
	DEBUG(`[removeBlockFromRelChain]`, { blockID, relationToParentID })
	const [relationToParent] = useRel(relationToParentID, dataStream)
	const wasAfterRelID = relationToParent.after
	const relationsAfterMe = filterAndMap(dataStream, { at: REL_DEF.after, vl: blockID }, 'en')
	DEBUG(`[removeBlockFromRelChain] data:`, { wasAfterRelID, relationsAfterMe })
	if (relationsAfterMe.length) {
		insertApplogs([
			// update relations referring to me
			...relationsAfterMe.map(relID => (
				{ en: relID, at: REL_DEF.after, vl: wasAfterRelID }
			)),
		])
	}
}

export function insertBlockInRelChain(
	ds: ApplogStreamOnlyCurrent,
	blockID: EntityID,
	parentID: EntityID,
	after: EntityID | null,
	relationToParentID?: EntityID,
) {
	DEBUG(`[insertBlockInRelChain]`, { blockID, parentID, after, relationToParentID })

	let relBuilder: InstanceType<typeof RelBuilder>
	if (relationToParentID) {
		let [relToParentVM] = useRel(relationToParentID, ds)
		relBuilder = relToParentVM.buildUpdate()
		if (relToParentVM.childOf !== parentID) {
			relBuilder.update({ childOf: parentID })
		}
	} else {
		relBuilder = RelationVM.buildNew({ block: blockID, childOf: parentID })
	}
	relBuilder.update({ after })
	relationToParentID = relBuilder.commit().en

	// update relations to refer to me
	const relationsAfterTarget = queryAndMap(ds, [
		{ en: '?rel', at: REL_DEF.childOf, vl: parentID },
		{ en: '?rel', at: REL_DEF.after, vl: after },
	], 'rel')
		.filter(rel => rel !== relationToParentID) // except my relation
	if (relationsAfterTarget.length) {
		DEBUG(
			`[insertBlockInRelChain] relationsAfterTarget:`,
			relationToParentID,
			blockID,
			relationsAfterTarget, /* , { relationsAfterTargetQuery, relationsOfParent } */
		)
		insertApplogs([
			...relationsAfterTarget.map(rel => (
				{ en: rel, at: REL_DEF.after, vl: blockID ?? null }
			)),
		])
	}
}
