import { Static, Type } from '@sinclair/typebox'

const Publication = Type.Object({
	id: Type.String(),
	name: Type.String(),
	timestamp: Type.Integer(),
})

type Publication = Static<typeof Publication>
