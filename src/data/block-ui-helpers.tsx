import { type Accessor, type Setter, useContext } from 'solid-js'

import { Logger } from 'besonders-logger/src'
import { XMLParser } from 'fast-xml-parser'
import { action, flow, untracked } from 'mobx'
import { BlockContext } from '../components/BlockTree'
import { useCurrentDS, useRawDS } from '../ui/reactive'
import { focusBlockAsInput, tryParseNote3URL } from '../ui/ui-utils'
import { getSubOrPub } from './AgentStore'
import { insertApplogs } from './ApplogDB'
import {
	deleteAndReplaceBlock,
	indentBlk,
	insertBlockInRelChain,
	outdentBlk,
	removeBlockFromRelChain,
	removeBlockRelAndMaybeDelete,
} from './block-utils'
import { ApplogForInsertOptionalAgent, EntityID } from './datalog/datom-types'
import { ApplogStreamOnlyCurrent } from './datalog/query/engine'
import { reorderRelation } from './relation-utils'
import { BlockVM, useBlk } from './VMs/BlockVM'
import { RelationVM, useRel } from './VMs/RelationVM'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export function useBlockContext() {
	return useContext(BlockContext)
}

export const handleBlockDrag = action(function handleBlockDrag(
	dataStream: ApplogStreamOnlyCurrent,
	blockID: EntityID,
	sourceRelationID: EntityID | null,
	newParentID: EntityID,
	newAfterID: EntityID | null,
) {
	DEBUG(`[handleBlockDrag]`, { blockID, sourceRelationID, newParentID, newAfterID, dataStream })

	if (sourceRelationID) {
		removeBlockFromRelChain(dataStream, blockID, sourceRelationID)
	}
	insertBlockInRelChain(dataStream, blockID, newParentID, newAfterID, sourceRelationID)
})

export function createTreeItemKeydownHandler(
	blockID: EntityID,
	relationID: EntityID,
	isExpandedSignal: Accessor<boolean>,
	setExpandedSignal: Setter<boolean> | ((newVal: boolean) => any[]),
	ag: string,
) {
	VERBOSE('Creating Keydown for', { blockID, relationID })
	const currentDS = useCurrentDS()
	const rawDS = useRawDS()
	const blockContext = useBlockContext()
	if (blockContext.id !== blockID) WARN(`block context != args`, blockID, blockContext)
	if (blockContext.relationToParent !== relationID) WARN(`rel context != args`, relationID, blockContext)
	const [blockVM] = useBlk(blockID)

	return flow(function* treeItemKeydownHandler(evt: KeyboardEvent & { currentTarget: HTMLSpanElement; target: Element }) {
		if (blockContext.id !== blockID) WARN(`block context != args`, blockID, blockContext)
		if (blockContext.relationToParent !== relationID) WARN(`rel context != args`, relationID, blockContext)

		const [relToParentVM, _relToParentB] = useRel(relationID, rawDS)
		const parentID = relToParentVM?.childOf
		const [parentVM] = useBlk(parentID, rawDS)

		const siblingAboveID = relToParentVM?.after
		// const siblingAboveVM = siblingAbove ? BlockVM.get(siblingAbove, rawDS) : null
		const [siblingAboveVM] = useBlk(siblingAboveID, rawDS)
		const siblingRelations = parentVM?.kidRelations ?? []
		const siblingAboveRelID = (siblingRelations?.find(rel => rel.block === siblingAboveID))?.en
		const [siblingAboveRelVM] = useRel(siblingAboveRelID, rawDS)
		const isSiblingAboveExpanded = siblingAboveRelVM?.isExpanded

		const grandParentRelID = blockContext.parentContext?.relationToParent ?? null
		// const grandParentVM = grandParentID ? BlockVM.get(grandParentID, rawDS) : null
		const [grandParentRelVM] = useRel(grandParentRelID, rawDS)
		const grandParentID = grandParentRelVM?.childOf
		const [grandParentVM] = useBlk(grandParentID, rawDS)

		// const focus = useFocus()
		const { target, currentTarget: { innerText } } = evt

		const isExpanded = relToParentVM?.isExpanded // !!(target.parentElement.attributes['aria-expanded']?.nodeValue === 'true') // ? is there a more elegant way to get this?

		const myTopKidID = blockVM.kidRelations[0]?.block

		const siblingBelowID = (siblingRelations?.find(rel => rel.after === blockID))?.block
		// const mySiblingIndex = siblingRelations.findIndex(rel => rel.block === blockID)
		// const isBottomSibling = !!(!siblingRelations.length || mySiblingIndex == siblingRelations.length - 1)

		const siblingAboveRelations = siblingAboveVM?.kidRelations ?? []
		const siblingAbovesLastKid = siblingAboveRelations.length ? siblingAboveRelations[siblingAboveRelations.length - 1].block : null // TODO -> BlockVM

		const parentSiblingRelations = grandParentVM?.kidRelations ?? []
		const parentSiblingIndex = parentSiblingRelations.findIndex(rel => rel.block === parentID)
		const belowMyParent = parentSiblingRelations[parentSiblingIndex + 1]?.block
		const parentsBottomKid = parentSiblingRelations.length ? parentSiblingRelations[parentSiblingRelations.length - 1].block : null
		const belowMe = isExpandedSignal() ? myTopKidID ?? siblingBelowID ?? belowMyParent : siblingBelowID ?? belowMyParent
		// const siblingAboveHTML = target.parentElement.previousElementSibling // HACK well... its better than jquery right?
		// const isSiblingAboveExpanded = !!(siblingAboveHTML?.attributes['aria-expanded']?.nodeValue === 'true') // ? is there a more elegant way to get this?
		const aboveMe = isSiblingAboveExpanded
			? siblingAbovesLastKid ?? relToParentVM?.after ?? relToParentVM?.childOf
			: relToParentVM?.after ?? relToParentVM?.childOf

		const selection = getSelectionInContentEditable(target)
		const pos = getCursorPositionInContentEditable(target)
		const beforePos = innerText.slice(0, pos)
		const afterPos = innerText.slice(pos)
		const isAtStart = !!(pos === 0)
		const isAtEnd = !!(pos === innerText.length && !selection.length)
		VERBOSE('[EditableTreeItem] onKeyDown', {
			evt,
			relationToParent: relToParentVM,
			pos,
			innerText,
			isAtEnd,
			selection,
			blockContext,
			blockVM,
		})
		const isContentEmpty = innerText.length === 0
		if (evt.key === 'Escape') {
			evt.preventDefault()
			const prev = '//TODO undo'
			evt.currentTarget.innerText = prev
			return DEBUG('escape TODO - revert changes', { evt })
		} else if (evt.key === 'Tab') {
			evt.preventDefault()
			if (innerText != blockVM.content) {
				VERBOSE('[EditableTreeItem] onBlur via tab', { evt, innerText })
				blockVM.content = innerText
			}
			if (evt.shiftKey) {
				outdentBlk(rawDS, relToParentVM, grandParentID)
			} else {
				if (!isSiblingAboveExpanded) siblingAboveRelVM.isExpanded = true
				indentBlk(rawDS, relToParentVM)
			}
			return evt.shiftKey
				? DEBUG('rev tab - outdent', { relationToParent: relToParentVM, relationsOfParent: siblingRelations })
				: DEBUG('tab - indent', { relationToParent: relToParentVM, relationsOfParent: siblingRelations })
		} else if (evt.key === 'Backspace') {
			if ((evt.ctrlKey && evt.shiftKey) || (isAtStart && isContentEmpty)) {
				removeBlockRelAndMaybeDelete(currentDS, blockID, relToParentVM?.en)
				removeBlockFromRelChain(currentDS, blockID, relToParentVM?.en)
				evt.preventDefault()
				if (relToParentVM) {
					focusBlockAsInput({ id: relToParentVM.after ?? relToParentVM.childOf, end: true })
				}
			} else if (isAtStart && !isContentEmpty) {
				evt.preventDefault()
				if (relToParentVM) {
					const aboveMe = siblingAboveID ?? parentID
					const aboveMeVM = siblingAboveVM ?? parentVM
					const mergedContent = `${aboveMeVM.content}${afterPos}`
					const originalLengthAbove = untracked(() => aboveMeVM.content.length)
					focusBlockAsInput({ id: aboveMe, /* end: true  */ pos: originalLengthAbove })
					queueMicrotask(() => {
						aboveMeVM.persistContent(mergedContent)
						DEBUG('merging into aboveMe', { aboveMe, afterPos, aboveMeVM, mergedContent })
						removeBlockRelAndMaybeDelete(currentDS, blockID, relToParentVM.en)
					})
				}
				// TODO add old content up to end of upper block
			}
			evt.shiftKey ? DEBUG('back shift') : DEBUG(' back no shift')
			evt.ctrlKey ? DEBUG('backg ctrl') : DEBUG(' back no ctrl')
			return
		} else {
			VERBOSE('RELOFPARENT', {
				relationsOfParent: siblingRelations,
				belowMe,
				aboveMe,
				belowMyParent,
				siblingAbovesLastKid,
			})
			switch (evt.key) {
				case 'ArrowUp':
					if (evt.ctrlKey) {
						evt.preventDefault()
						reorderRelation(relToParentVM, { up: 1 }, siblingRelations, ag)
						focusBlockAsInput({ id: blockID, end: true })
						break
					} else {
						if (relToParentVM) {
							evt.preventDefault()
							// HACK not using aboveMe until the isSiblingAboveExpanded debacle isSolved
							// (so upArrow works if not expanded - leftArrow works when expanded)
							focusBlockAsInput({ id: relToParentVM.after ?? relToParentVM.childOf, /* aboveMe  */ end: true })
						}
						break
					}
				case 'ArrowDown':
					if (evt.ctrlKey) {
						evt.preventDefault()
						reorderRelation(relToParentVM, { down: 1 }, siblingRelations, ag)
						focusBlockAsInput({ id: blockID, end: true })
						break
					} else {
						DEBUG({ relationToParent: relToParentVM, relationsOfParent: siblingRelations })
						if (belowMe) {
							evt.preventDefault()
							focusBlockAsInput({ id: belowMe, select: true })
						}
						break
					}

				case 'ArrowLeft':
					if (evt.ctrlKey) {
						evt.preventDefault()
						setExpandedSignal(false)
						// ? consider if cursor is on a kid with no kids - collapse and focus on parent?
						break
					}

					if (relToParentVM && isAtStart) {
						evt.preventDefault()
						focusBlockAsInput({ id: aboveMe, end: true })
					}
					break
				case 'ArrowRight':
					if (evt.ctrlKey) {
						evt.preventDefault()
						setExpandedSignal(true)
						break
					}
					if (belowMe && isAtEnd) {
						evt.preventDefault()
						focusBlockAsInput({ id: belowMe, end: false, start: true })
					}
					break
				default:
					break
			}
		}
	})
}

export async function spyClipboard() {
	let asParsedXML, allApplogs
	try {
		const permission = await navigator.permissions.query({
			name: 'clipboard-read',
		})
		if (permission.state === 'denied') {
			throw new Error('Not allowed to read clipboard.')
		}
		const clipboardContents = await navigator.clipboard.read()
		for (const item of clipboardContents) {
			//   if (!item.types.includes("image/png")) {
			// 	throw new Error("Clipboard contains non-image data.");
			//   }
			//   const blob = await item.getType("image/png");
			//   destinationImage.src = URL.createObjectURL(blob);
			DEBUG('clip', { item, clipboardContents })
			for (const eachType of item.types) {
				const eachContent = await (await item.getType(eachType)).text()
				DEBUG(eachType, eachContent)
				const options = {
					ignoreAttributes: false,
					attributeNamePrefix: '',
					allowBooleanAttributes: true,
				}
				const parser = new XMLParser(options)
				asParsedXML = parser.parse(eachContent)
				LOG({ asParsedXML })
			}
		}
	} catch (error) {
		console.error(error.message)
	}
	const rootOutline = asParsedXML?.opml?.body?.outline
	type OutlineNode = { text: string; _note?: string; outline?: OutlineNode }
	const getContentWithNote = (outlineNode: OutlineNode) => `${outlineNode.text}\n${outlineNode._note ?? ''}`
	const addApplogs = (logArray: ApplogForInsertOptionalAgent[], outlineNode: OutlineNode | OutlineNode[], parentEn: EntityID) => {
		if (!outlineNode[0]) outlineNode = [outlineNode as OutlineNode]
		let after = null
		for (const eachNode of outlineNode as OutlineNode[]) {
			const eachBuilder = BlockVM.buildNew({ content: getContentWithNote(eachNode) })
			logArray.push(...eachBuilder.build())
			logArray.push(...(RelationVM.buildNew({ childOf: parentEn, block: eachBuilder.en, after })).build())
			after = eachBuilder.en
			if (eachNode.outline) addApplogs(logArray, eachNode.outline, eachBuilder.en)
		}

		return logArray
	}
	if (rootOutline) {
		const rootBuilder = BlockVM.buildNew({ content: getContentWithNote(rootOutline) })
		allApplogs = rootBuilder.build()
		LOG('beforeLoop', { rootOutline, allApplogs })
		if (rootOutline.outline) {
			allApplogs.push()
			addApplogs(allApplogs, rootOutline.outline, rootBuilder.en)
		}
		LOG('afterLoop', { allApplogs })
		return allApplogs?.length ? { allApplogs, rootID: rootBuilder.en } : null
	} else VERBOSE('no root outline node found')
}
export function createTreeItemPasteHandler(
	blockID: EntityID,
	relationVM: InstanceType<typeof RelationVM> | null,
) {
	// const block = useBlockVM(blockID)
	const { setPanel } = useBlockContext()
	// DEBUG(relationVM.after)

	return async (evt: ClipboardEvent) => {
		const opmlParsedApplogs = await spyClipboard()
		DEBUG('[pasteHandler] onPaste', { evt, relation: relationVM, opmlParsedApplogs })
		if (opmlParsedApplogs) {
			const replaceBlockLogs = relationVM.buildUpdate({ isExpanded: true, block: opmlParsedApplogs.rootID }).build()
			insertApplogs([...opmlParsedApplogs.allApplogs, ...replaceBlockLogs])
			// TODO delete block if it has no other non deleted placements
			// deleteAndReplaceBlock(relationVM, opmlParsedApplogs.rootID)
			return DEBUG('added', opmlParsedApplogs)
		}
		setTimeout(async () => { // HACK: otherwise pasted value is not there yet
			const elem = evt.target as HTMLElement
			const value = elem.innerText

			let url = tryParseNote3URL(value)
			if (!url) {
				VERBOSE('Pasted is not a note3 URL:', value)
				return
			}
			DEBUG(`[pasteHandler] found note3 url:`, url)

			if (!url.focus) {
				console.error('TODO: Not sure what to do without root:', url)
				return
			}

			if (url.publication && !getSubOrPub(url.publication)) {
				setPanel({ type: 'paste', publication: url.publication.toString(), block: url.focus })
			}
			// otherwise, we have everything we need
			await deleteAndReplaceBlock(relationVM.en, url.focus)
		}, 300)
	}
}

export async function copyToClipboard(text) {
	try {
		await navigator.clipboard.writeText(text)
		/* ✅ Copied successfully */
	} catch (e) {
		DEBUG(`Failed to copy`, e)
		/* ❌ Failed to copy (insufficient permissions) */
	}
}

function getSelectionInContentEditable(element) {
	return window?.getSelection?.()?.toString()
}
export function getCursorPositionInContentEditable(element) {
	var range, preCaretRange, textContent, caretOffset = 0

	if (window.getSelection) {
		range = window?.getSelection()?.getRangeAt(0)
		if (!range) return undefined
		preCaretRange = range.cloneRange()
		preCaretRange.selectNodeContents(element)
		preCaretRange.setEnd(range.endContainer, range.endOffset)
		textContent = preCaretRange.toString()
		caretOffset = textContent.length
	}

	return caretOffset
}

const wfExampleText = `
Setup your agent #UX 
#NEEDS-DESIGNER
Login to web3
Create encryption keys #UX 
Push public hello world stream 
Send us your link with stream ID 
`
const wfExampleHTML = `
<ul style="-webkit-tap-highlight-color: transparent; margin: 15px 0px 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent; list-style: disc; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; white-space: normal; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
<li style="-webkit-tap-highlight-color: transparent; margin: 4px 0px 4px 20px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent;">
<span class="name" data-wfid="f58371f148c7:3fbecf7eeeea" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent; white-space: pre-wrap;">
<span class="innerContentContainer" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent;">Setup your agent <span class="contentTag" title="Filter #UX" data-val="#ux" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(119, 59, 59) !important; color: rgb(255, 255, 255) !important;">#<span class="contentTagText" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: none; text-decoration: none;">UX</span>
<span class="contentTagNub" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: none;">
</span>
</span> </span>
</span>
<br style="-webkit-tap-highlight-color: transparent;">
<span class="note" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 12px; vertical-align: baseline; background: transparent; white-space: pre-wrap; color: rgb(102, 102, 102);">
<span class="innerContentContainer" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 12px; vertical-align: baseline; background: transparent;">
<span class="contentTag" title="Filter #NEEDS-DESIGNER" data-val="#needs-designer" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 12px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(64, 128, 107) !important; color: rgb(255, 255, 255) !important;">
<span class="colored bc-green" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 2px 0px; border: 0px; outline: 0px; font-size: 12px; vertical-align: baseline; background: none; color: rgb(255, 255, 255);">#</span>
<span class="contentTagText" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 12px; vertical-align: baseline; background: none; text-decoration: none;">
<span class="colored bc-green" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 2px 0px; border: 0px; outline: 0px; font-size: 12px; vertical-align: baseline; background: none; color: rgb(255, 255, 255);">
</span>NEEDS-DESIGNER</span>
<span class="contentTagNub" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 12px; vertical-align: baseline; background: none;">
</span>
</span>
</span>
</span>
<ul style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent; list-style: disc;">
<li style="-webkit-tap-highlight-color: transparent; margin: 4px 0px 4px 20px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent;">
<span class="name" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent; white-space: pre-wrap;">
<span class="innerContentContainer" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent;">Login to web3</span>
</span>
</li>
<li style="-webkit-tap-highlight-color: transparent; margin: 4px 0px 4px 20px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent;">
<span class="name" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent; white-space: pre-wrap;">
<span class="innerContentContainer" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent;">Create encryption keys <span class="contentTag" title="Filter #UX" data-val="#ux" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(119, 59, 59) !important; color: rgb(255, 255, 255) !important;">#<span class="contentTagText" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: none; text-decoration: none;">UX</span>
<span class="contentTagNub" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: none;">
</span>
</span> </span>
</span>
</li>
<li style="-webkit-tap-highlight-color: transparent; margin: 4px 0px 4px 20px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent;">
<span class="name" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent; white-space: pre-wrap;">
<span class="innerContentContainer" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent;">Push public hello world stream </span>
</span>
</li>
<li style="-webkit-tap-highlight-color: transparent; margin: 4px 0px 4px 20px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent;">
<span class="name" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent; white-space: pre-wrap;">
<span class="innerContentContainer" style="-webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background: transparent;">Send us your link with stream ID </span>
</span>
</li>
</ul>
</li>
</ul>
`
