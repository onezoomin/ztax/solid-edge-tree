import '@shoelace-style/shoelace/dist/themes/dark.css'
import { A, Route, Routes, useIsRouting, useLocation, useNavigate, useSearchParams } from '@solidjs/router'
import { Logger } from 'besonders-logger/src'
import { Component, createResource, createSignal, lazy, Match, onCleanup, Show, Suspense, Switch } from 'solid-js'
import { BackLink } from './components/BackLink'
import { Help } from './Help'

// import MainPage from './MainPage'
// import SettingsPage from './SettingsPage'
// import TestPage from './TestPage'
const MainPage = lazy(() => import('./MainPage'))
const SettingsPage = lazy(() => import('./SettingsPage'))
const TestPage = lazy(() => import('./TestPage'))

import { appInit } from './appInit'
import { Spinner } from './components/mini-components'
import { RadialMenu } from './components/RadialMenu'
import { SearchBar } from './components/SearchBar'
import { SyncButton } from './components/SyncControls'
import './data/devtools-helpers'
import './ui/shoelace'
import { devMode, setDevMode } from './ui/ui-utils'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO, { prefix: '[App]' }) // eslint-disable-line no-unused-vars

let renderCount = 0
export const App: Component = () => {
	;(renderCount++ > 0 ? WARN : DEBUG)('!!! APP RENDER !!!', renderCount)
	// trace(true)

	// const applogStream = getApplogDB() /*  as ApplogStream */
	const [{ pub: pubFromUrl }, _setSearchParams] = useSearchParams()

	const location = useLocation()
	const navigate = useNavigate()
	const isRouting = useIsRouting()
	let pathname = location.pathname
	// console.trace('init', { pathname, pubFromUrl, focus: focus() })

	const [initialized, setInitialized] = createSignal(false)
	const [searchOpen, setSearchOpen] = createSignal(false) // NOCOMMIT

	const cleanupMap = new Map()
	void (async () => {
		const cleanUp = await appInit({ pubFromUrl })
		cleanupMap.set(cleanUp, cleanUp)
		setInitialized(true)
		return true
	})()
	onCleanup(() => {
		for (const eachCleanup of Array.from(cleanupMap.keys())) {
			eachCleanup()
		}
	})

	const [versionStr] = createResource(async () => {
		if (import.meta.env.DEV) return 'dev'
		const response = await fetch('/commit.txt')
		if (!response.ok) throw ERROR(`Response ${response.statusText} for /commit.txt`)
		const text = await response.text()
		return text // .substring(7)
	})

	return (
		<>
			<SearchBar open={searchOpen} setOpen={setSearchOpen} />
			<RadialMenu />
			<Show when={isRouting()}>
				<div absolute inset-0 bg='black opacity-40' flex='~ items-center justify-center' z-1000>
					{/* (i) workaround for https://github.com/solidjs/solid-router/discussions/296 */}
					<Spinner size='3rem' color='red' />
				</div>
			</Show>

			<div
				id='main-container'
				flex='~ col items-center justify-start'
				class='bg-#282c34 min-h-100vh color-white'
				px-2
				border-box
				h-100vh
				overflow-auto
			>
				<div flex='~ col items-center justify-start' class='bg-#282c34 min-h-100vh w-full max-w-4xl color-white' gap-2>
					<header id={`${pathname}`} w-full flex='~ items-center justify-between' p-2 hidden={searchOpen()}>
						{/* <Show when={!searchOpen()} fallback={<SearchBar open={searchOpen} setOpen={setSearchOpen} />}> */}
						<div
							flex='~ items-center'
							h-10
							font-900
							pr-2
							gap-2
						>
							<Switch>
								<Match when={['settings', 'help'].includes(location.pathname.split('/')[1])}>
									<h2 m-0 flex='inline items-center gap-4'>
										<BackLink text='' />
										{location.pathname.startsWith('/settings') ? 'Settings' : 'Help'}
									</h2>
								</Match>
								<Match when={true}>
									<div flex='~ items-center' gap-2 cursor-pointer onclick={() => navigate('/')}>
										<div w-10 h-10 class='i-custom:ipld' />
										<div flex='~ items-end' gap-2>
											Note3
											<Show when={versionStr()}>
												<a
													href={`https://gitlab.com/onezoomin/note3/-/commit/${versionStr()}`}
													text-xs
													opacity-30
													color='inherit'
													no-underline
													target='_blank'
													font-mono
												>
													{versionStr()?.substring(0, 7)}
												</a>
											</Show>
										</div>
									</div>
								</Match>
							</Switch>
						</div>
						<div flex='~ items-center justify-end'>
							<Show when={!['settings', 'help'].includes(location.pathname.split('/')[1])}>
								<sl-button
									variant='text'
									onclick={() => setSearchOpen(true)}
									// size='small'
									mr--2
								>
									<div w-6 h-6 class='text-white i-ph:magnifying-glass-bold' />
								</sl-button>
							</Show>
							<Show when={location.pathname.startsWith('settings')}>
								<A
									text='white'
									href='/help'
								>
									<div w-6 h-6 class='white i-ph:seal-question-light' />
								</A>
							</Show>
							<SyncButton />
							<Show when={location.pathname !== '/settings'}>
								<sl-button
									onclick={() => navigate('/settings')}
									circle
									onAuxClick={e => {
										if (e.button === 1) {
											e.preventDefault()
											setDevMode(!devMode())
										}
									}}
								>
									<div w-6 h-6 class='white i-ph:gear' />
								</sl-button>
							</Show>
						</div>
						{/* </Show> */}
					</header>

					<Show
						when={initialized()}
						fallback={
							<div flex='~ justify-center' w-full>
								{/* <pre>innit? {JSON.stringify(initialized())}</pre> */}
								<Spinner size='3rem' color='purple' />
							</div>
						}
					>
						<Suspense fallback={<Spinner size='3rem' color='green' />}>
							<Routes>
								<Route
									path={'/settings/*section'}
									element={
										<Suspense fallback={<Spinner size='3rem' color='green' />}>
											<SettingsPage />
										</Suspense>
									}
								/>
								<Route
									path='/test'
									element={
										<Suspense fallback={<Spinner size='3rem' color='green' />}>
											<TestPage />
										</Suspense>
									}
								/>

								<Route
									path='/help'
									element={
										<Suspense fallback={<Spinner size='3rem' color='green' />}>
											<Help />
										</Suspense>
									}
								/>

								<Route
									path={['/', '/block/:blockID']}
									element={
										<Suspense fallback={<Spinner size='3rem' color='green' />}>
											<MainPage />
										</Suspense>
									}
								/>

								<Route
									path='*'
									element={
										<div w-full>
											<BackLink />
											<h1>Unknown location</h1>
										</div>
									}
								/>
							</Routes>
						</Suspense>
					</Show>
				</div>
			</div>
		</>
	)
}
