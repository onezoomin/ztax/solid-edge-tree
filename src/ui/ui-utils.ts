import { Location, Navigator } from '@solidjs/router'
import { Logger } from 'besonders-logger/src'
import { createEffect, createMemo, createSignal, JSX, onCleanup, onMount } from 'solid-js'
import { Name } from 'w3name'
import { editorMap } from '../components/BlockContent'
import { RE_SELECTOR_BLOCKS } from '../data/block-utils'
import type { EntityID } from '../data/datalog/datom-types'
import { IPublication } from '../data/local-state'
import { parseW3Name } from '../ipfs/web3storage'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

/** Origins listed here are considered Note3 */
const KNOWN_ORIGINS = [
	'https://note3.app',
	'https://note3.fission.app',
	window.location.origin,
]

export const [devMode, setDevMode] = createSignal(import.meta.env.DEV)

export function zoomToBlock(
	{ id, focus = null, end = true }: {
		id: EntityID | null
		focus?: EntityID
		end?: boolean
	},
	location: Location,
	navigate: Navigator,
) {
	const search = window.location.hash.split('?', 2)[1]
	DEBUG('[zoomToBlock]:', id, { end, location, navigate, search })
	navigate('/' + (id ? ('block/' + id) : '') + (search ? `?${search}` : ''))
	if (focus) focusBlockAsInput({ id: focus, end })
}

export function onMountWithRefs(callback: Function) {
	onMount(() => {
		let discard = false
		queueMicrotask(() => !discard && callback())
		onCleanup(() => discard = true)
	})
}
export function onMountFxDeprecated(callback: Function) {
	// HACK: shouldn't be needed actually, use solid-js onMount
	return () =>
		onMount(() => {
			let discard = false
			queueMicrotask(() => !discard && callback())
			onCleanup(() => discard = true)
		})
}

export function focusBlockAsInput(
	args: { id: EntityID; end?: boolean; start?: boolean; select?: boolean; pos?: number },
) {
	const { id, start = false, end = !start, select = false, pos } = args
	// const [blkVM] = useBlk(id)
	VERBOSE('Focusing:', id, { pos })
	// HACK the whole focus debacle is dependent on successful rerendering whenever solid gets around to it...
	let tries = 0
	let successes = 0
	function tryFocus(args) {
		const { id, start = false, end = !start, select = false, pos = null } = args
		const elem = document.getElementById('block-' + id)
		const editor = editorMap.get(elem as HTMLDivElement)
		DEBUG(`Focusing attempt ${tries} (${successes} successes) for`, { id, elem, pos, editor })
		tries++
		if (!elem) {
			if (tries < 10) setTimeout(tryFocus.bind(null, args), 100)
		} else {
			if (successes == 0 || elem !== document.activeElement) {
				editor.commands.focus()
				// TODO: focus at position - https://tiptap.dev/api/commands/focus

				// elem.focus()
				// // from: https://stackoverflow.com/a/59437681/1633985
				// if (elem.hasChildNodes()) { // if the element is not empty
				// 	let s = window.getSelection()
				// 	let r = document.createRange()
				// 	let e = elem.lastChild ?? elem
				// 	if (select) {
				// 		VERBOSE('select', { elem, e, s })
				// 		r.setStart(e, 0)
				// 		r.setEnd(e, 1)
				// 		s.removeAllRanges()
				// 		s.addRange(r)
				// 	} else if (pos) {
				// 		DEBUG('set to pos', { pos, r, e })
				// 		r.setStart(e, pos)
				// 		// r.setEnd(e, pos)
				// 		s.removeAllRanges()
				// 		s.addRange(r)
				// 	} else if (end) {
				// 		r.setStart(e, 1)
				// 		r.setEnd(e, 1)
				// 		s.removeAllRanges()
				// 		s.addRange(r)
				// 	} else if (start) {
				// 		r.setStart(e, 0)
				// 		// r.setEnd(e, 1)
				// 		s.removeAllRanges()
				// 		s.addRange(r)
				// 	}
				// }
			}

			successes++
			if (successes < 5) setTimeout(() => tryFocus(args), 100) // HACK because db invalidation replaced DOM and focus is discarded.... FML
		}
	}
	tryFocus(args)
}
export function singleBlockOfPublication(pub: IPublication) {
	const selector = pub.selectors?.find(s => s.match(RE_SELECTOR_BLOCKS))
	if (!selector) return null

	const blocksMatch = selector.match(RE_SELECTOR_BLOCKS)
	if (!blocksMatch) return null
	const blocks = blocksMatch[1].split(',')
	return blocks.length === 1 ? blocks[0] : null
}

export function makeNote3Url(
	{ pub, block, previewPub = false, relative = false }: { pub?: string; block?: string; previewPub?: boolean; relative?: boolean },
) {
	if (!pub && !block) throw new Error('Neither pub nor block - what kind of URL would that be?')
	const baseUrl = relative ? '' : `${window.origin}/#` // ? on which domain - window.origin vs canonical note3.app
	if (previewPub) {
		return `${baseUrl}/${block ? `block/${block}` : ''}${pub ? `?preview=${pub}` : ''}`
	} else {
		return `${baseUrl}/${block ? `block/${block}` : ''}${pub ? `?pub=${pub}` : ''}`
	}
}

export function explorerUrl(cid: string) {
	return `https://explore.ipld.io/#/explore/${cid}`
}

const RE_NOTE3_URL_PATH = /\/publication\/([a-zA-Z0-9]+)/
export function tryParseNote3URL(value: string) {
	const url = tryParseURL(value)
	if (!url) return null

	const result = {} as {
		publication?: Name
		preview?: boolean
		// root?: EntityID
		focus?: EntityID
	}
	if (url.protocol === 'note3:') {
		const match = url.pathname.match(RE_NOTE3_URL_PATH)
		if (match) {
			result.publication = parseW3Name(match[1])
		}
	} else if (url.hash && KNOWN_ORIGINS.includes(url.origin)) {
		const blockMatch = url.hash.match(/^#\/block\/([a-z0-9]+)/)
		if (blockMatch) {
			result.focus = blockMatch[1]
		}

		const params = searchParamsFromHash(url.hash)
		if (params?.get('pub')) result.publication = parseW3Name(params?.get('pub'))
		else if (params?.get('preview')) {
			result.publication = parseW3Name(params?.get('preview'))
			result.preview = true
		}
	} else {
		return null
	}
	VERBOSE('Parsed Note3 url:', value, result)
	return result
}

export function searchParamsFromHash(hashPartOfUrl?: string) {
	const split = (hashPartOfUrl ?? window.location.hash).split('?', 2)
	return split.length >= 2 ? new URLSearchParams(split[1]) : null
}

export function tryParseURL(value: string) {
	try {
		return new URL(value.trim())
	} catch (error) {
		return null
	}
}

export const stopPropagation = (event, slotFilter?: string) => {
	VERBOSE('StopPropagation of', { event, slotFilter })
	if (!slotFilter || event.target.slot === slotFilter) {
		event.stopPropagation() // this will stop propagation in capturing phase
	}
}

export function createAsyncMemo<T>(effect: () => Promise<T>) {
	const [value, setValue] = createSignal<T>()
	createEffect(async () => {
		// @ts-expect-error weird Awaited type
		setValue(await effect())
	})
	return value
}

export function createAsyncButtonHandler(handler: () => any | Promise<any>) {
	const [loading, setLoading] = createSignal(false)
	return createMemo(() => ({
		onclick: async (e: MouseEvent) => {
			setLoading(true)
			try {
				e.stopPropagation()
				await handler()
			} catch (err) {
				ERROR(err)
				notifyToast('Error: ' + (err.message || JSON.stringify(err, undefined, 2)), 'danger')
			} finally {
				setLoading(false)
			}
		},
		loading: loading(),
	}))
}

function escapeHtml(html) {
	const div = document.createElement('div')
	div.textContent = html
	return div.innerHTML
}
export const variantMap = {
	'primary': 'i-ph:info-bold',
	'success': 'i-ph:check-circle',
	'neutral': 'i-ph:info-bold',
	'warning': 'i-ph:warning-bold',
	'danger': 'i-ph:x-circle',
}
type ToastVariants = keyof typeof variantMap
export function notifyToast(
	message: string,
	variant: ToastVariants = 'primary',
	duration = 5000,
	icon = '',
	iconStyles = '',
	iconClasses = '',
) {
	icon = (icon || variantMap[variant]) ?? 'i-ph:info-bold'
	const escapedMessage = escapeHtml(message)
	DEBUG({ message, escapedMessage })
	iconStyles = iconStyles || `width:1.5em;height:1.5em;margin-right:1em;` // HACKY style stuff = penalty for not using sl-icon : /
	const alert = Object.assign(document.createElement('sl-alert'), {
		variant,
		closable: true,
		duration: duration,
		innerHTML: `
			<div slot="icon" class="${icon} ${iconClasses}" style="${iconStyles}"></div>
			${escapedMessage}
      `, //
		//   innerHTML: `
		//     <span flex="~ row" style="align-items:center" >
		// 		<div style="${iconStyles}" class="${icon}" slot="icon"></div>
		// 		${escapedMessage}
		// 	</span>
		//   `, // shadow dom stuff, uno won't work unless the // also https://shoelace.style/getting-started/usage#dont-use-self-closing-tags
	})
	// TODO do this in a more solidjs way without the style shenanigans
	document.body.append(alert)
	return (alert as typeof alert & { toast: () => void }).toast()
}

/* from: https://stackoverflow.com/a/16348977/ */
export function stringToColor(str: string) {
	var hash = 0
	for (var i = 0; i < str.length; i++) {
		hash = str.charCodeAt(i) + ((hash << 5) - hash)
	}
	var colour = '#'
	for (var i = 0; i < 3; i++) {
		var value = (hash >> (i * 8)) & 0xFF
		colour += ('00' + value.toString(16)).substr(-2)
	}
	return colour
}

/**
 * isColorDark calculates the HSP value for a color, to determine its brightness.
 * A value of 127.5 and below means dark, and above 127.5 means bright
 * You can adjust this parameter to find really bright colors (maxHsp = 223)
 *
 * @param color
 * @param maxHsp - default 127.5 -
 * @returns {boolean|undefined}
 */
export function isColorDark(color, maxHsp = 127.5) {
	const rgb = rgbFromColor(color)
	if (!rgb) {
		VERBOSE(`${color} no RGB??`)
		return undefined
	}
	const { r, g, b } = rgb

	// HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
	const hsp = Math.sqrt(0.299 * (r * r) + 0.587 * (g * g) + 0.114 * (b * b))
	VERBOSE(`${color} isColorDark? ${hsp}`)
	// Using the HSP value, determine whether the color is light or dark
	return hsp <= maxHsp
}

/**
 * returns either [r,g,b] from the color, or undefined if color is undefined
 * @param color
 */
export function rgbFromColor(color) {
	if (!color) return undefined
	// Variables for red, green, blue values
	let r, g, b

	// Check the format of the color, HEX or RGB?
	if (color.match(/^rgb/)) {
		// If HEX --> store the red, green, blue values in separate variables
		color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/)

		r = color[1]
		g = color[2]
		b = color[3]
	} else {
		// If RGB --> Convert it to HEX: http://gist.github.com/983661
		color = +`0x${color.slice(1).replace(color.length < 5 && /./g, '$&$&')}`

		r = color >> 16
		g = (color >> 8) & 255
		b = color & 255
	}

	return { r, g, b }
}

export function mergeStyles(propStyles: string | object | null, ourStyles: object) {
	if (propStyles === 'string') WARN(`string style cannot be merged, dropped:`, propStyles)
	return {
		...(typeof propStyles !== 'string' ? propStyles : null),
		...ourStyles,
	}
}

type GlobalHoverSignal = { type: string; id?: string }
export function makeGlobalHover<ELEM = HTMLDivElement>(
	makeAttributes: (active: boolean, myID: boolean) => Partial<JSX.HTMLAttributes<ELEM>>,
) {
	const [active, setActive] = createSignal<GlobalHoverSignal | null>(null)
	return [
		(type: string) =>
			active()
			&& (active().id || true), // we want to be able to just truthy-check, but also get the specific value, if one is given
		(type: string, id?: string) => {
			const attrs = {
				onMouseEnter() {
					setActive({ type, id })
				},
				onMouseLeave() {
					setActive(null)
				},
				...makeAttributes(active()?.type === type, active()?.id && active().id === id),
			}
			return attrs
		},
	] as const
}

/** https://stackoverflow.com/a/4819886/1633985 */
export function isTouchDevice() {
	return (('ontouchstart' in window) ||
		(navigator.maxTouchPoints > 0) ||
		// @ts-expect-error
		(navigator.msMaxTouchPoints > 0))
}
