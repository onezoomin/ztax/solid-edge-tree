import { Reaction } from 'mobx'
import { enableExternalSource } from 'solid-js'

// register MobX as an external source
let id = 0
enableExternalSource((fn, trigger) => {
	const reaction = new Reaction(`solid-js@${++id}`, trigger)
	return {
		track: (x) => {
			let next
			reaction.track(() => {
				// console.debug("MOBX", reaction, trigger)
				return (next = fn(x))
			})
			return next
		},
		dispose: () => reaction.dispose(),
	}
})
