import { useParams } from '@solidjs/router'
import { Logger } from 'besonders-logger/src'
import { autorun, computed, observable, toJS } from 'mobx'
import { Accessor, createContext, createMemo, useContext } from 'solid-js'
import { getApplogDB, insertApplogs } from '../data/ApplogDB'
import { BLOCK, BLOCK_DEF, REL_DEF } from '../data/data-types'
import { ApplogValue, EntityID } from '../data/datalog/datom-types'
import { createDebugName, observableArrayMap } from '../data/datalog/mobx-utils'
import {
	ApplogStream,
	ApplogStreamOnlyCurrent,
	ApplogStreamWithoutFilters,
	assertRaw,
	isInitEvent,
	rollingAcc,
} from '../data/datalog/query/engine'
import { filterAndMap, query, queryAndMap, queryEntity, queryNot, withoutDeleted, withoutHistory } from './../data/datalog/query/queries'
import { isoDateStrCompare } from '../data/datalog/utils'
import { orderBlockRelations } from '../data/relation-utils'
import { RelationModelDef } from '../data/Relations'
import { notNull } from '../data/Utils'
import { RelationVM } from '../data/VMs'
import { BlockVM } from '../data/VMs/BlockVM'
import { TypeAttrOptions, TypeMapKeys } from '../data/VMs/utils-typemap'
import { DefaultFalse } from '../types/util-types'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO, { prefix: '[rx]' }) // eslint-disable-line no-unused-vars

export const DBContext = createContext<Accessor<ApplogStreamWithoutFilters | null>>(null)

export function useFocus() {
	const params = useParams()
	return createMemo(() => params.blockID)
	// return useHash() // previously const [focus, setFocus] = createSignal(focusFromSearchParams)
	// const location = useLocation()
	// DEBUG(`useFocus`, location)
	// return createMemo(() => {
	// 	if (location.pathname === '/block') return location.
	// 	else return null
	// })
}

let tmpDS: ApplogStream | null = null
export function withDS<R>(
	ds: ApplogStream,
	fn: () => R,
): R {
	const dsBefore = tmpDS
	tmpDS = ds
	const result = fn()
	tmpDS = dsBefore
	return result
}

export function useDSFromContext() {
	let ds = tmpDS ?? useContext(DBContext)?.()
	if (!ds) {
		/* TODO throw */
		WARN(`Empty DBContext and tmpDS - reverting to root`, { tmpDS, ds })
		ds = getApplogDB()
	}
	return ds
}
export function useRawDS() {
	return assertRaw(useDSFromContext())
}
export function useDS({ withHistory, withDeleted }: { withHistory?: DefaultFalse; withDeleted?: DefaultFalse } = {}) {
	let ds = useDSFromContext()
	if (!withHistory && !ds.filters.includes('withoutHistory')) ds = withoutHistory(ds)
	if (!withDeleted && !ds.filters.includes('withoutDeleted')) ds = withoutDeleted(ds)
	return ds
}
export function useCurrentDS() {
	return useDS() as ApplogStreamOnlyCurrent
}

/////////////////
// NEW QUERIES //
/////////////////

export function useBlockVM(blockID: EntityID) {
	const ds = useRawDS()
	DEBUG(`[useBlockVM]`, blockID, ds)
	return BlockVM.get(blockID, ds)
}

export function useBlock(blockID: EntityID) {
	const ds = useCurrentDS()
	DEBUG(`[useBlock]`, blockID, ds)
	return computed(() => {
		const block = queryEntity(ds, 'block', blockID, BLOCK_DEF._attrs).get()
		if (!block) return null
		return ({
			en: blockID,
			...block,
		} as unknown as BLOCK)
	})
}

export function useRelationVM(relationID: EntityID) {
	const ds = useRawDS()
	DEBUG(`[useRelationVM]`, relationID, ds)
	return RelationVM.get(relationID, ds)
}
export function useRelation(relationID: EntityID) {
	const ds = useCurrentDS()
	DEBUG(`[useRelation]`, relationID, ds)
	return computed(() => {
		const relation = queryEntity(ds, 'relation', relationID, REL_DEF._attrs).get()
		if (!relation) return null
		return ({
			en: relationID,
			...relation,
		} as unknown as RelationModelDef)
	})
}

export function useBlockAt<wType extends TypeMapKeys = 'Block'>(blockID: EntityID, at: TypeAttrOptions<wType>) {
	/* TODO: BLOCK._attrs */
	const ds = useCurrentDS()
	const atStr = at.toString()
	DEBUG(`[useBlockAt]`, ds.name, { ds, blockID, at })
	return computed(() => {
		const value = queryEntity(ds, 'block', blockID, [atStr]).get()
		VERBOSE(`[useBlockAt] query result`, value)
		return value?.[atStr]
	})
}

export function useEntityAt<T extends ApplogValue>(id: EntityID, at: string, defaultVal: T = null) {
	const ds = useCurrentDS()
	DEBUG(`[useEntityAt]`, ds.nameAndSizeUntracked, { ds, id, at })
	const resultArr = filterAndMap(ds, { en: id, at }, 'vl')
	VERBOSE.isDisabled || autorun(() => VERBOSE(`[useEntityAt] result`, { ds, id, at }, toJS(resultArr)))
	// autorun(() => LOG(`[useEntityAt] result`, { ds, id, at }, toJS(resultArr)))
	return [
		() => resultArr.length ? resultArr[0] as T : defaultVal, // memo was borking reactivity here
		(newVal: T) =>
			insertApplogs([
				{ en: id, at, vl: newVal },
			]),
	] as const
}

export function useKidRelationIDs(blockID: EntityID) {
	DEBUG(`[useKidRelationIDs]`, blockID)
	const ds = useCurrentDS()
	return filterAndMap(ds, { at: REL_DEF.childOf, vl: blockID }, 'en')
}

export function useKidRelations(blockID: EntityID) {
	DEBUG(`[useKidRelations]`, blockID)
	const ds = useCurrentDS()
	const relationsQuery = useKidRelationIDs(blockID)
	VERBOSE(`[useKidRelations] relationsQuery:`, relationsQuery)
	return observableArrayMap(() => {
		const relations = relationsQuery.map(relID => {
			const rel = queryEntity(ds, 'relation', relID, REL_DEF._attrs).get()
			if (!rel) return null
			if (rel.block === blockID) {
				WARN(`[useKidRelations] block has itself as kid`, { blockID, rel })
				return null
			}
			return ({ ...rel, en: relID } as RelationModelDef)
		}).filter(notNull) // HACK - handle deleted relations properly
		return orderBlockRelations(relations)
	}, { name: createDebugName({ caller: 'useKidRelations' }) })
}

export function useKidVMs(blockID: EntityID) {
	DEBUG(`[useKidVMs]`, blockID)
	const ds = useCurrentDS()
	const relationDefs = useKidRelations(blockID)
	VERBOSE(`[useKidVMs] relationsQuery:`, relationDefs)
	return observableArrayMap(() => {
		const blockVMs = relationDefs.map(({ block }) => {
			return BlockVM.get(block as string, ds)
		})
		return blockVMs
	}, { name: createDebugName({ caller: 'useKidVMs' }) })
}

export function usePlacementRelationIDs(blockID: EntityID) {
	DEBUG(`[useParentRelationIDs]`, blockID, REL_DEF.block)
	const ds = useCurrentDS()
	return filterAndMap(ds, { vl: blockID, at: REL_DEF.block }, 'en')
}

// returns RelationDefs for each Placement of blockID
export function usePlacementRelations(blockID: EntityID) {
	DEBUG(`[useParentRelations]`, blockID)
	const ds = useCurrentDS()
	const relationIDs$arr = usePlacementRelationIDs(blockID)
	return observableArrayMap(() => {
		const relations = relationIDs$arr.map(
			relID => ({ ...queryEntity(ds, 'relation', relID, REL_DEF._attrs).get(), en: relID } as RelationModelDef),
		)
		VERBOSE('useParentRelations', { relationsQuery: relationIDs$arr, relations })
		return orderBlockRelations(relations)
	}, { name: createDebugName({ caller: 'useParentRelations', stream: ds }) })
}

export function useRelAt(relID: EntityID, at: string /* TODO: BLOCK._attrs */) {
	DEBUG(`[useRelAt]`, relID)
	const db = useCurrentDS()
	return computed(() => queryEntity(db, 'relation', relID, [at]).get()?.[at])
}
export function useParent(blockID: EntityID) {
	DEBUG(`[useParent]`, blockID)
	if (!blockID) throw ERROR(`[useParent] empty argument:`, blockID)
	const db = useCurrentDS()
	return computed(() => {
		const parents = queryAndMap(db, [
			{ en: '?relID', at: REL_DEF.block, vl: blockID },
			{ en: '?relID', at: REL_DEF.childOf, vl: '?parentID' },
		], 'parentID') as string[] // HACK: types
		VERBOSE(`[useParent] parents:`, parents)
		if (parents.length > 1) WARN(`Block ${blockID} has multiple parents:`, parents)
		return parents.length ? parents[0] : null
	})
}

// export function useRoots() {
// 	const db = useDB()
// 	const blocks = query(db, [
// 		{ en: '?blockID', at: 'block/content' },
// 		// { en: '?blockID', at: 'block/isDeleted', vl: '?isDeleted' },
// 	])
// 	VERBOSE(`[useRoots] blocks:`, blocks, db.applogs)
// 	const blocksWithoutParent = queryNot(db, blocks, [
// 		{ en: '?relID', at: 'relation/block', vl: '?blockID' },
// 		{ en: '?relID', at: 'relation/childOf', vl: null },
// 		// { en: '?blockID', at: 'block/isDeleted', vl: '?isDeleted' },
// 	])
// 	const records = blocksWithoutParent.records;
// 	const mappedRootIDs = () => records.map(({ blockID }) => blockID) as string[]
// 	VERBOSE(`[useRoots] result:`, db.nameAndSize, records, untracked(mappedRootIDs))
// 	return createMemo(() => mappedRootIDs(), mappedRootIDs(), { equals: areDeduplicatedArraysEqualUnordered })
// }

export function useRoots() {
	// const roots = observable.set([] as EntityID[], { deep: false })
	// autorun(() => {
	// WARN(`[useRoots]`)
	const stream = useCurrentDS()
	const blocks = query(stream, [
		{ en: '?blockID', at: 'block/content' },
	])
	const firstLogsStream = withoutHistory(useDS({ withHistory: true }), {
		inverseToOnlyReturnFirstLogs: true,
		tolerateAlreadyFiltered: true,
	})
	// const blocksFirstLog = query(firstLogsStream, [
	// 	{ en: '?blockID', at: 'block/content' },
	// ])
	const blockCreation = rollingAcc(firstLogsStream, /*blocksFirstLog.allApplogs*/ observable.map<EntityID, string>(), (event, map) => {
		if (!isInitEvent(event)) {
			for (const removed of event.removed) {
				if (removed.at !== 'block/content') continue
				map.delete(removed.en)
			}
		}
		for (const added of (isInitEvent(event) ? event.init : event.added)) {
			if (added.at !== 'block/content') continue
			map.set(added.en, added.ts)
		}
	})
	// autorun(() => WARN(`blocksCreation`, blockCreation, firstLogsStream))

	const rootsObserverFx = function rootsObserverFx() {
		DEBUG(`[useRoots] blocks:`, blocks, stream) // TODO: when queryNot is observable, move this out
		const blocksWithoutParent = queryNot(stream, blocks, [
			{ en: '?relID', at: 'relation/block', vl: '?blockID' },
			{ en: '?relID', at: 'relation/childOf', vl: null },
		])
		// const firstLogs = query(firstLogsStream, [
		// 	{ en: blocksWithoutParent.records.map(({ blockID }) => blockID) as EntityID[], at: 'block/content' },
		// ]).allApplogs.applogs
		// VERBOSE(`[useRoots] firstLogs:`, firstLogs)
		const sortedRecords = blocksWithoutParent.records.sort(({ blockID: blockA }, { blockID: blockB }) => {
			const firstLogA = blockCreation.get(blockA as EntityID)
			const firstLogB = blockCreation.get(blockB as EntityID)
			return isoDateStrCompare(firstLogA, firstLogB, 'desc') // sort by first log time - i.e. creation
			// return blockA.localeCompare(blockB) //old: sort by entity ID
		})
		DEBUG(`[useRoots] result:`, stream.nameAndSizeUntracked, { sortedRecords })
		return sortedRecords.map(({ blockID }) => blockID as string)
	}
	const name = createDebugName({ caller: 'roots', stream })
	const roots = observableArrayMap(rootsObserverFx, { name })
	DEBUG.isDisabled || autorun(() => {
		DEBUG(`[useRoots] updated`, toJS(roots))
	})
	return roots
}
