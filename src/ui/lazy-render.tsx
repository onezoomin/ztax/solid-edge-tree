import { Logger } from 'besonders-logger/src'
import classNames from 'classnames'
import { createEffect, createSignal, For, JSX, on, onCleanup, Show, splitProps } from 'solid-js'
import { Spinner } from '../components/mini-components'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

type LazyRenderProps<T> = {
	items: T[]
	fallback?: JSX.Element
	children?: (item: T, isInitial: boolean) => JSX.Element
} // ? & JSX.HTMLAttributes<HTMLDivElement>

export const LazyRender: <T>(
	props: LazyRenderProps<T>,
) => JSX.Element = (fullProps) => {
	const [props, otherProps] = splitProps(fullProps, ['items', 'children', 'fallback'])
	const [loadedItems, setLoadedItems] = createSignal(props.items.slice(0, 3))
	const [skipAnimationFor, setSkipAnimationFor] = createSignal(props.items.slice(0, 3))
	const [allLoaded, setAllLoaded] = createSignal(props.items.length <= 3)
	const [showLoader, setShowLoader] = createSignal(false)

	let loaderVisible = false
	const onObserverStateChange = (entries, observer) => {
		DEBUG(`loadMoreItems`, entries, observer)
		loaderVisible = false
		for (const entry of entries) {
			if (entry.isIntersecting) {
				loaderVisible = true
				setShowLoader(true)
				loadMore()
			}
		}

		function loadMore() {
			setLoadedItems((prev) => {
				const nextItem = props.items[prev.length]
				return nextItem ? [...prev, nextItem] : prev
			})
			if (loadedItems().length >= props.items.length) {
				// observer.disconnect() // ? Stop observing once all items are loaded - how to reconnect
				setAllLoaded(true)
			}
			setTimeout(() => {
				DEBUG(`timeout. still visible?`, loaderVisible)
				if (loaderVisible) loadMore()
				else setShowLoader(false)
			}, 500)
		}
	}
	createEffect(on(
		() => props.items,
		() => {
			LOG('LazyRender items changed - re-rendering')
			const newItems = props.items.slice(0, 3)
			setLoadedItems(newItems)
			setSkipAnimationFor(newItems)
			setAllLoaded(false)
			setShowLoader(true)
		},
		{ defer: true },
	))

	const observer = new IntersectionObserver(onObserverStateChange, {
		rootMargin: '200px', // Load items when they come within X pixels of the viewport
		root: document.getElementById('main-container'),
	})

	onCleanup(() => {
		observer.disconnect()
	})

	return (
		<>
			<For each={loadedItems()} fallback={props.fallback}>
				{item => {
					DEBUG(`Rendering`, item)
					return props.children(item, skipAnimationFor().includes(item))
					// return <pre style='height: 400px'>{stringify(item)}</pre>
				}}
			</For>
			<Show when={!allLoaded()}>
				<div
					flex='~ justify-center items-center'
					gap-4
					class={classNames(
						showLoader() ? 'opacity-100' : 'opacity-0',
						'transition-property-[opacity] duration-700',
						// doesn't work well: 'animate-fade-in-up animate-duration-[700ms] animate-ease-out',
					)}
					ref={(elem) => {
						DEBUG(`observer ref`, elem)
						observer.observe(elem)
					}}
				>
					<Spinner /> Loading more...
				</div>
			</Show>
		</>
	)
}
