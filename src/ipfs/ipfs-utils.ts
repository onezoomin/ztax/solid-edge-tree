import * as dagJson from '@ipld/dag-json'
import { sha256 } from '@noble/hashes/sha256'
import { Logger } from 'besonders-logger/src'
import { CID, digest as Digest } from 'multiformats'
import { encode as multiformatsEncode } from 'multiformats/block'
// import { encode } from 'multiformats/block';
import { Applog, ApplogForInsert, ApplogNoCid } from '../data/datalog/datom-types'

import { sha256 as sha265Hasher } from 'multiformats/hashes/sha2'

/* THIS FILE SHOULD NOT DEPEND ON UI STUFF, SO THAT TESTS CAN RUN WITH MINIMAL DEPENDENCIES */

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

export function prepareForPub(log: Applog | ApplogForInsert, without: string[] = ['cid']) {
	const logWithout = {}
	for (let [key, val] of Object.entries(log)) {
		if (val === undefined) {
			WARN(`log.${key} is undefined, which is not allowed - encoding as null`, log)
			val = null
		}
		if (!without.includes(key)) {
			logWithout[key] = val && key === 'pv' ? CID.parse(val) : val
		} else {
			VERBOSE('excluding app log', { key, val })
		}
	}
	return logWithout as Applog
}

export function encodeApplogAndGetCid(log: ApplogNoCid) {
	return getCidSync(encodeApplog(log).bytes)
}
export function encodeApplog(log: ApplogNoCid) {
	return encodeBlock(prepareForPub(log))
}

export function getCidSync(bytes: dagJson.ByteView<any>) {
	// Hacky way to use a sync sha265 lib to create a CID - code inspired by https://github.com/multiformats/js-multiformats#multihash-hashers
	const hash = sha256(bytes)
	const digest = Digest.create(sha265Hasher.code, hash)
	const cid = CID.create(1, dagJson.code, digest)
	VERBOSE(`[getCidSync]`, { bytes, hash, digest, cid })
	return cid
}
/** encode the json object into an IPLD block */
export function encodeBlock(jsonObject: any) {
	DEBUG('[encodeBlock]', jsonObject)
	const byteView = dagJson.encode(jsonObject)
	return { bytes: byteView, cid: getCidSync(byteView) }
}

export async function encodeBlockOriginal(jsonObject: any) {
	// HACK re-added this to verify the sync variant is sane
	const encoded = await multiformatsEncode({ value: jsonObject, codec: dagJson, hasher: sha265Hasher })
	const syncVariant = encodeBlock(jsonObject)
	if (syncVariant.cid.toString() !== encoded.cid.toString()) {
		ERROR(`[encodeBlockOriginal] sync cid mismatch`, { jsonObject, encoded, syncVariant })
	}
	return encoded
}
