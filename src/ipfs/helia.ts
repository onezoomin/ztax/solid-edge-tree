// const HeliaState = {
//   };

// import { noise } from '@chainsafe/libp2p-noise'
// import { yamux } from '@chainsafe/libp2p-yamux'
import type { Helia } from '@helia/interface'
// import { bootstrap } from '@libp2p/bootstrap'
// import { webSockets } from '@libp2p/websockets'
import { createHelia } from 'helia'
// import { createLibp2p } from 'libp2p'
// import { unixfs } from "@helia/unixfs";
// import { MemoryBlockstore } from 'blockstore-core'
// import { MemoryDatastore } from 'datastore-core'
import type { DAGJSON } from '@helia/dag-json'
import { dagJson } from '@helia/dag-json'
import { CID } from 'multiformats/cid'

let helia: Helia | null = null
let dag: DAGJSON | null = null

export function getDag() {
	if (!dag) {
		throw new Error(`dag not initialized`)
	}
	return dag
}

export async function startHelia() {
	try {
		if (helia) {
			console.info('helia already started')
			// @ts-expect-error window.helia
		} else if (window.helia) {
			console.info('found a windowed instance of helia, populating ...')
			// @ts-expect-error window.helia
			helia = window.helia
			// setFs(unixfs(helia));
			// setStarting(false);
		} else {
			// // the blockstore is where we store the blocks that make up files
			// const blockstore = new MemoryBlockstore()

			// // application-specific data lives in the datastore
			// const datastore = new MemoryDatastore()

			// // libp2p is the networking layer that underpins Helia
			// // Make sure to stick libp2p here when running react in strict mode
			// const libp2p = await createLibp2p({
			// 	datastore,
			// 	transports: [webSockets()],
			// 	connectionEncryption: [noise()],
			// 	streamMuxers: [yamux()],
			// 	peerDiscovery: [
			// 		bootstrap({
			// 			list: [
			// 				'/dnsaddr/bootstrap.libp2p.io/p2p/QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN',
			// 				'/dnsaddr/bootstrap.libp2p.io/p2p/QmQCU2EcMqAqQPR2i9bChDtGNJchTbq5TbXJJ16u19uLTa',
			// 				'/dnsaddr/bootstrap.libp2p.io/p2p/QmbLHAnMoJPWSCR5Zhtx6BHJX9KiKNN6tpvbUcqanj75Nb',
			// 				'/dnsaddr/bootstrap.libp2p.io/p2p/QmcZf59bWwK5XFi76CZX8cbJ4BhTzzA3gU1ZjYZcYW3dwt',
			// 			],
			// 		}),
			// 	],
			// })
			console.info('Starting Helia')
			helia = await createHelia({
				// datastore,
				// blockstore,
				// libp2p,
			})
			// setHelia(helia);
			// setFs(unixfs(helia));
			// setStarting(false);
			console.info('started Helia')
		}
		// @ts-expect-error window.helia
		window.helia = helia

		dag = dagJson(helia!)
		// @ts-expect-error window.helia
		window.dag = dagJson(helia)
	} catch (e) {
		console.error(e)
		// setError(true);
	}
}

// @ts-expect-error window.helia
window.CID = CID
