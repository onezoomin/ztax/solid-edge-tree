import { Logger } from 'besonders-logger/src'
import { observable, reaction, runInAction } from 'mobx'
import { agentState } from '../data/AgentStore'
import { getApplogDB } from '../data/ApplogDB'
import { ApplogStreamIDB } from '../data/datalog/query/engine'
import { publishDatalog, retrieveDatalog } from './store-sync'
import { useW3Uploader } from './w3ui'
import { parseW3Name } from './web3storage'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export const syncState = observable({
	syncing: false,
	syncInterval: localStorage.getItem('note3.sync.interval') ? Number.parseInt(localStorage.getItem('note3.sync.interval')) : 60,
	errors: [],
})
reaction(() => syncState.syncInterval, newInterval => {
	LOG('Setting sync interval to', newInterval, 'seconds')
	localStorage.setItem('note3.sync.interval', newInterval.toString())
	if (syncState.syncInterval > 0) {
		startSyncService()
	} else {
		stopSyncService()
	}
})

let timer
export function startSyncService(startWithSync = false) {
	LOG(`Starting sync service`, syncState.syncInterval, startWithSync)
	if (timer) clearTimeout(timer)
	if (syncState.syncInterval > 0) {
		timer = setTimeout(doSync, 1000 * (startWithSync ? 1 : syncState.syncInterval)) // first sync 1s after load //? good UX
	}
}
export function stopSyncService() {
	LOG(`Stopping sync service`)
	if (timer) clearTimeout(timer)
}

export function triggerSync({ pub, sub }: { pub?: string; sub?: string } = {}) {
	LOG(`[triggerSync]`, { pub, sub })
	clearTimeout(timer)
	timer = null
	void doSync()
}

async function doSync() {
	DEBUG('[Sync] Starting')
	if (syncState.syncing) throw ERROR(`Already syncing`, syncState)
	if (!agentState.hasStorageSetup) DEBUG(`No storage setup - skipping sync`)
	runInAction(() => {
		syncState.syncing = true
		syncState.errors = []
	})
	try {
		const db = getApplogDB()
		if (!db) throw ERROR(`No DB!`, db)
		await doPull(db)
		await doPush(db)
	} catch (error) {
		runInAction(() =>
			syncState.errors.push(
				ERROR('[Sync] failed to sync', error), // logs and returns error object
			)
		)
	} finally {
		runInAction(() => syncState.syncing = false)
		if (syncState.syncInterval > 0) {
			timer = setTimeout(doSync, 1000 * syncState.syncInterval)
		}
	}
}
async function doPull(db: ApplogStreamIDB) {
	for (const sub of agentState.subscriptions) {
		if (!sub.autopull) continue
		DEBUG('[Sync] Pulling', sub)
		try {
			await retrieveDatalog(db, parseW3Name(sub.id), sub)
		} catch (error) {
			runInAction(() =>
				syncState.errors.push(
					ERROR('[Sync] failed to retrieve', sub, error), // logs and returns error object
				)
			)
		}
	}
}
async function doPush(db: ApplogStreamIDB) {
	const [_, uploader] = useW3Uploader()
	for (const pub of agentState.publications) {
		if (!pub.autopush) continue
		DEBUG('[Sync] Pushing', pub)
		try {
			await publishDatalog(db, uploader, pub)
		} catch (error) {
			runInAction(() =>
				syncState.errors.push(
					ERROR('[Sync] failed to publish', pub, error), // logs and returns error object
				)
			)
		}
	}
}
