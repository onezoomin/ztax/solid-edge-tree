import { CarReader, CarWriter } from '@ipld/car'
import * as json from '@ipld/dag-json'
import { importRsaKey } from '@oddjs/odd/components/crypto/implementation/browser'
// import * as W3Up from '@web3-storage/w3up-client'
import { CID } from '@oddjs/odd'
import type { Name, WritableName } from 'w3name'
import * as W3Name from 'w3name'
import type { CIDString } from 'web3.storage'
import { Web3Storage } from 'web3.storage'
import { stateDB } from '../data/local-state'
// import { Web3Storage } from 'web3.storage/dist/src/lib.d.ts'
import { Logger } from 'besonders-logger/src'
import { action, flow } from 'mobx'
import { useAgent } from '../data/AgentStore'
import { notifyToast } from '../ui/ui-utils'
import { initW3Keyring, initW3Uploader, UploaderContextValue } from './w3ui'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

let w3sReadonly: Web3Storage
// let w3up: W3Up.Client
let name: WritableName

// @ts-expect-error window.
window.Web3Storage = Web3Storage
// @ts-expect-error window.
window.w3 = window.Web3Storage = w3sReadonly
// @ts-expect-error window.
window.w3name = name

type CIDForCar = Exclude<Parameters<(typeof CarWriter)['create']>[0], void>
type BlockForCar = Parameters<CarWriter['put']>[0]

export async function initWeb3Storage() {
	DEBUG('Initializing Web3Storage...')
	w3sReadonly = new Web3Storage({ token: 'FAKE' /* import.meta.env.VITE_WEB3STORAGE_TOKEN */ })
	// w3up = await W3Up.create()
	await initW3Keyring()
	await initW3Uploader()
}

export function createHandleAuthFx({ w3, email, setRegisteringSpace, tryRegisterSpace, setSubmitted, keyring }) {
	return async e => {
		DEBUG('Submit:', e, email())
		e.preventDefault()
		setSubmitted(true)
		try {
			await w3.authorize(email() as `${string}@${string}`)
			DEBUG('Authorized!', keyring, w3)
			if (!keyring.spaces.length) {
				DEBUG('Creating space...')
				const did = await w3.createSpace('note3-dev')
				DEBUG('Created space', did)
				// await setCurrentSpace(did)
			}
			if (!keyring.space.registered()) {
				setRegisteringSpace(true)
				DEBUG('Space is unregistered, registering', keyring.space)
				let tries = 0
				// HACK to work around https://github.com/web3-storage/w3ui/issues/257
				const tryRegisterInLoop = async () => {
					try {
						await tryRegisterSpace()
					} catch (error) {
						console.error(`failed to register space with ${email()}:`, error)
						if (tries < 10) {
							tries++
							await new Promise((resolve, reject) => setTimeout(resolve, 500 * (tries)))
							await tryRegisterInLoop()
						} else {
							alert("Failed to register space - you can try using the 'TryFix' button in settings")
						}
					}
				}
				await tryRegisterInLoop()
			}
			DEBUG('Got a space!', { keyring, space: keyring.space })
		} catch (err) {
			// throw new Error('failed to authorize', { cause: err })
			ERROR(`failed to authorize ${email()}`, err)
			notifyToast('Failed to authorize web3storage: ' + err.message, 'danger')
		} finally {
			setRegisteringSpace(false)
			setSubmitted(false)
		}
	}
}

export const initIPNS = flow(function* initIPNS() {
	// TODO get array of my ipns names from localstorage #9
	// TODO check if there is roothash in the URL and check if its mine #5
	// if so use it, otherwise use newest from localstorage, otherwise create new
	const allPublications = yield stateDB.publications.toArray()
	// const pubFromIDB = (publicationThatsFocused && allPublications?.find(p => p.id === publicationThatsFocused)) || allPublications?.[0]
	const agent = useAgent()
	const { appAgent, ag } = agent
	const pubSigningKeyBytes = yield agent.crypto?.keystore?.publicWriteKey()
	const pubEncryptionKeyBytes = yield agent.crypto?.keystore?.publicExchangeKey()
	DEBUG('[odd]', { pubSigningKeyBytes, pubEncryptionKeyBytes })
	const pubKeyCryptoReimported = yield importRsaKey(pubEncryptionKeyBytes, ['encrypt'])
	const enc = new TextEncoder()
	const dec = new TextDecoder('utf-8')
	const testPayload = enc.encode('fission is rather odd')
	const encPayloadKeystore = yield agent.crypto?.rsa.encrypt(testPayload, pubKeyCryptoReimported)
	const deencPayloadKeystore = dec.decode(yield agent.crypto?.keystore.decrypt(encPayloadKeystore))
	DEBUG('[odd]', { pubSigningKeyBytes, pubEncryptionKeyBytes, pubKeyCryptoReimported, encPayloadKeystore, deencPayloadKeystore })

	// const exportedKey = yield state.crypto?.rsa?.exportPublicKey(pubExchangeKey)

	// TODO refactor this to live elsewhere
	// if (pubFromIDB) {
	// 	name = yield W3Name.from(pubFromIDB.pk)
	// 	yield stateDB.publications.update(name.toString(), { ts: dateNowIso(), appAgent })
	// 	DEBUG('Loaded name: ', name.toString())
	// } else {
	// 	name = yield W3Name.create()
	// 	const { key, key: { bytes: pk } } = name
	// 	const nameString = name.toString()
	// 	DEBUG('created new name: ', nameString)
	// 	//TODO: make default encrypted
	// 	yield stateDB.publications.put({ id: nameString, name: 'default', pk, createdAt: dateNowIso(), publishedBy: appAgent, lastPush: null })
	// 	// const signedPubName = yield key.sign(name.bytes)
	// 	// const encodedSig = ab2str(signedPubName)
	// 	// insertApplogs([
	// 	// 	{ en: nameString, at: 'pub/sig', vl: encodedSig, ag },
	// 	// ])
	// }
	// updateAgentState({ w3NamePublic: name })
})

// TODO make uploader a global reactively available instance
// ooo sounds nice
export const storeCar = action(async function storeCar(car: Blob, uploader: UploaderContextValue[1]) {
	try {
		// upload to web3.storage using putCar
		DEBUG('Uploading', car, 'using', uploader)
		const cid = await uploader.uploadCAR(
			car, /* {
				name: 'putCar using dag-json',
				// include the dag-json codec in the decoders field
				decoders: [json],
			} */
		)
		DEBUG('Stored dag-json:', cid.toString())
		return cid
	} catch (err) {
		ERROR(`[storeCar]`, err)
		throw new Error(`Failed to upload: ${err?.message}\ncheck storage settings`)
	}
})

// make out in the car... been a while but also sounds nice
async function makeCarOut(roots: CIDForCar, blocks: BlockForCar[]) {
	const { writer, out } = CarWriter.create(Array.isArray(roots) ? roots : [roots])

	// add the blocks to the CAR and close it
	VERBOSE(`Writing ${blocks.length} blocks to CAR`, { roots, blocks })
	blocks.forEach(b => writer.put(b))
	writer.close()
	// VERBOSE(`Wrote ${blocks.length} blocks to CAR`, writer)
	return out
}

/** create a new CarWriter, with the encoded block as the root */
export async function makeCarReader(roots: CIDForCar, blocks: BlockForCar[]) {
	const out = await makeCarOut(roots, blocks)

	// create a new CarReader we can hand to web3.storage.putCar
	const reader = await CarReader.fromIterable(out)
	VERBOSE(`CAR reader`, reader)
	return reader
}

/** create a new CarWriter, with the encoded block as the root */
export async function makeCarBlob(roots: CIDForCar, blocks: BlockForCar[]) {
	const carOut = await makeCarOut(roots, blocks)
	const chunks = []
	for await (const chunk of carOut) {
		chunks.push(chunk)
	}
	const blob = new Blob(chunks)
	return blob
}

export async function retrieveCar(cid: CIDString) {
	// Fetch the CAR file from web3.storage
	DEBUG('Retrieving:', cid)
	const response = await w3sReadonly.get(cid)
	// const response = await fetch(`https://saturn.ms/ipfs/${cid}?format=car&dag-scope=all`);
	if (!response?.ok || !response?.body) {
		throw new Error(`unexpected response ${response?.statusText}`)
	}

	// The data is an AsyncIterable<Uint8Array>, convert it to an AsyncIterable for the CarReader
	const data: AsyncIterable<Uint8Array> = streamToIterable(response!.body.getReader())

	// return a CarReader from the CAR data
	return await CarReader.fromIterable(data)
}

export async function decodeCar(car: CarReader) {
	const roots = (await car.getRoots()).map(c => c.toV1().toString() as CIDString)
	const blocks = new Map<CIDString, any>()
	for await (const { cid, bytes } of car.blocks()) {
		blocks.set(cid.toV1().toString(), json.decode(bytes)) // HACK: tried using CID as map key, but because it's based on referential equality it's not working
	}
	return { roots, blocks }
}

export async function publishIPNS(cid: CIDString, ipnsWritableName = name) {
	const value = `/ipfs/${cid}`
	// if we don't have a previous revision, we use Name.v0 to create the initial revision
	// TODO handle updating
	const revision = await W3Name.v0(ipnsWritableName!, value)
	await W3Name.publish(revision, ipnsWritableName!.key)
	DEBUG('[w3name] published', cid, 'to', ipnsWritableName!.toString())
	const parsed = CID.parse(cid)
	DEBUG('[w3name] parsed', parsed)
	// const v0 = parsed.toV0().toString() // try to get qm style cid fails
	// DEBUG('[w3name] v0', v0, `https://explore.ipld.io/#/explore/${v0}`)
}

export async function resolveIPNS(name: Name): Promise<CIDString> {
	const revision = await W3Name.resolve(name)
	DEBUG('[w3name] resolved', name.toString(), 'to', revision)
	if (!revision.value.startsWith('/ipfs/')) {
		console.warn('IPNS value is not ipfs:', revision)
	}
	return revision.value.replace('/ipfs/', '')
}

// TODO make some more sexy store or state or signalDepot or something
export const getW3NamePublic = () => useAgent().w3NamePublic?.toString()

export const parseW3Name = W3Name.parse

function streamToIterable(bodyReader: any): AsyncIterable<Uint8Array> {
	return (async function*() {
		while (true) {
			const { done, value } = await bodyReader.read()
			if (done) {
				break
			}
			yield value
		}
	})()
}
