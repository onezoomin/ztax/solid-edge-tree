import { SymmAlg } from '@oddjs/odd/components/crypto/implementation'
import type { Name } from 'w3name'
import * as W3Name from 'w3name'

import { Logger } from 'besonders-logger/src'
import { flow } from 'mobx'
import { useAgent } from '../data/AgentStore'
import { IPublication, ISubscription } from '../data/local-state'
import { arrayBufferToBase64, dateNowIso } from '../data/Utils'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export const upsertSubscription = flow(function* upsertSubscription(
	ipns: Name,
	subFields: /* Pick<IPublication, 'lastPull'> & */
		& Partial<Pick<ISubscription, 'name' | 'lastPull' | 'encryptedFor' | 'lastCID' | 'autopull'>>
		& { encryptedBy?: string; derivationJWKstring?: string } = {},
) {
	LOG('Upserting subscription:', { ipns, subFields })
	const agentState = useAgent()
	const { ag } = agentState

	const subscriptionObj: ISubscription = {
		id: ipns.toString(),
		createdAt: dateNowIso(),
		name: ipns.toString().slice(-5),
		isDeleted: false,
		lastPull: null,
		autopull: false,
		...subFields,
	}
	if (subFields.encryptedBy && subFields.derivationJWKstring) {
		let privateKey, remotePubDerivationKey
		try {
			privateKey = (yield agentState.getDerivationKeypair()).privateKey
		} catch (error) {
			throw new Error('failed to get private key')
		}
		try {
			remotePubDerivationKey = yield importPublicDerivationKey(subFields.derivationJWKstring)
			subscriptionObj.encryptedWith = yield deriveSharedEncryptionKey(privateKey, remotePubDerivationKey, true)

			// doTestEncryptDecrypt(subscriptionObj) // ! This derived key works in the test... but not with the payloads from ipfs
		} catch (error) {
			throw new Error('failed to importPublicDerivationKey')
		}
		subscriptionObj.publishedBy = subFields.encryptedBy
		subscriptionObj.encryptedFor = ag
		subscriptionObj.name = `${subFields.encryptedBy} => ${ag}`
	}
	if (agentState.subscriptions.find(s => s.id === subscriptionObj.id)) {
		yield agentState.updateSub(subscriptionObj.id, subscriptionObj as ISubscription) // update the subscription
	} else {
		yield agentState.addSub(subscriptionObj as ISubscription) // insert the subscription
	}
	DEBUG('[upsertSubscription]', subscriptionObj)

	return subscriptionObj
})

export const addPublication = flow(function* addPublication(
	pubFields: Partial<Pick<IPublication, 'name' | 'encryptedFor' | 'selectors'>> & { derivationJWKstring?: string } = {},
) {
	LOG('Adding publication:', pubFields)
	const newName: W3Name.WritableName = yield W3Name.create()
	const agentState = useAgent()
	const { ag: publishedBy } = agentState
	const { privateKey } = yield agentState.getDerivationKeypair()
	const { name = `pub ${newName.toString().slice(-7)}`, encryptedFor, derivationJWKstring } = pubFields
	const pk = newName.key.bytes
	const newPub: IPublication = { id: newName.toString(), name, pk, publishedBy, createdAt: dateNowIso(), lastPush: null, autopush: false }
	if (encryptedFor && derivationJWKstring) {
		newPub.name = `${newPub.name}=>${encryptedFor}`
		newPub.encryptedFor = encryptedFor
		const remotePubDerivationKey = yield importPublicDerivationKey(derivationJWKstring)

		newPub.encryptedWith = yield deriveSharedEncryptionKey(privateKey, remotePubDerivationKey, true)
	}

	DEBUG('[addPublication]', { newPub, newName })
	yield agentState.addPub(newPub)
	return newPub
})

export const updatePubEncryption = flow(async function* updatePubEncryption(
	pubFields: Partial<Pick<IPublication, 'id' | 'encryptedFor' | 'sharedAgents' | 'selectors'>> & { derivationJWKstring?: string } = {},
) {
	DEBUG('Updating publication:', pubFields)
	const newName: W3Name.WritableName = yield W3Name.create()
	const agentState = useAgent()
	const { ag: publishedBy, getKnownAgents, crypto } = agentState
	const { privateKey } = yield agentState.getDerivationKeypair()
	const { id, encryptedFor, sharedAgents } = pubFields
	const pk = newName.key.bytes
	const pubUpdate: Partial<IPublication> = {}
	const knownAgentsMap = getKnownAgents().get()
	if (sharedAgents) { // multiple agents
		pubUpdate.encryptedFor = null
		pubUpdate.encryptedWith = null
		pubUpdate.sharedAgents = sharedAgents
		const sharedKey = yield createMultiAgentEncryptionKey()
		const exportedKey = yield window.crypto.subtle.exportKey('raw', sharedKey)
		pubUpdate.sharedKey = sharedKey // ? here we could reimport the shared key as not extractable
		// then WebCrypto would hide it (but its futile for reasons:)
		// 1. if its not extractable then we can't export it to reincrypt it for others
		// 2. if thats the case, we might as well not save it in IDB (but only encrypted for ourselves in the applog)
		// TODO solid strategy for key rotation, saving, sharing, managing
		const keyBase64 = arrayBufferToBase64(exportedKey)
		DEBUG('sharedKey', { exportedKey, keyBase64 })
		pubUpdate.sharedKeyMap = new Map()
		for (const eachAgent of sharedAgents) {
			const eachJWK = knownAgentsMap.get(eachAgent).jwkd as string
			if (eachJWK) {
				const eachPubKey = yield importPublicDerivationKey(eachJWK)
				const derivedKeyToEncryptSharedKeyWith = yield deriveSharedEncryptionKey(privateKey, eachPubKey, true)
				const encryptedSharedKey = await crypto?.aes.encrypt(exportedKey, derivedKeyToEncryptSharedKeyWith, SymmAlg.AES_GCM)
				pubUpdate.sharedKeyMap.set(eachAgent, arrayBufferToBase64(encryptedSharedKey))
				DEBUG('midloop', { pubUpdate })
			} else {
				ERROR('unknown JWK - weirdness', { knownAgentsMap, eachAgent })
			}
		}
	} else if (encryptedFor) { // single agent
		pubUpdate.encryptedFor = encryptedFor
		const derivationJWKstring = knownAgentsMap.get(encryptedFor).jwkd as string
		if (derivationJWKstring) {
			const remotePubDerivationKey = yield importPublicDerivationKey(derivationJWKstring)

			pubUpdate.encryptedWith = yield deriveSharedEncryptionKey(privateKey, remotePubDerivationKey, true)
			if (pubUpdate.encryptedWith) {}
		} else {
		}
	} else {
		ERROR('encryptedFor unknown', { encryptedFor, knownAgentsMap })
	}

	yield agentState.updatePub(id, pubUpdate)
	const traceObj = encryptedFor ? { encryptedFor } : { sharedAgents }
	LOG('[updatePublication]', { pubUpdate }, traceObj)
	// yield agentState.updatePub(id, pubUpdate)
	return pubUpdate
})

export async function importPublicDerivationKey(derivationJWKstring: string) {
	const parsedPublicJWK = JSON.parse(derivationJWKstring)
	DEBUG({ derivationJWKstring, parsedPublicJWK })

	return await window.crypto.subtle.importKey(
		'jwk',
		parsedPublicJWK,
		{ name: 'ECDH', namedCurve: 'P-256' },
		true,
		[],
	)
}
export function deriveSharedEncryptionKey(privateKey, publicKey, exportable = false) {
	return window.crypto.subtle.deriveKey(
		{
			name: 'ECDH',
			public: publicKey,
		},
		privateKey,
		{
			name: 'AES-GCM',
			length: 256,
		},
		exportable, // ! for testing/comparison
		['encrypt', 'decrypt'],
	)
}
export async function createMultiAgentEncryptionKey() {
	const newKey = await window.crypto.subtle.generateKey(
		{
			name: 'AES-GCM',
			length: 256,
		},
		true,
		['encrypt', 'decrypt'],
	)
	DEBUG('created new key', { newKey })
	return newKey
}

// export async function doTestEncryptDecrypt(pubSubWithAesKey) {
// 	const { ag: publishedBy, decryptWithAesSharedKey, getDerivationKeypair } = useAgent()
// 	const { privateKey } = await getDerivationKeypair()
// 	const enc = new TextEncoder()
// 	const fixedIV = enc.encode(pubSubWithAesKey.id).slice(-16)
// 	const { encryptedFor, encryptedWith } = pubSubWithAesKey
// 	let sharedKeyHexNewlyDerived

// 	const derivationJWKstring = getApplogDB().getSortedApplogsFromIndex({
// 		whichID: `${encryptedFor}_|_agent/jwkd`,
// 		whichIndex: 'enatIndex',
// 		onlyLatest: true,
// 	})?.[0]?.vl
// 	if (derivationJWKstring) {
// 		const remotePubDerivationKey = await importPublicDerivationKey(derivationJWKstring)
// 		const exportableSharedKey = await deriveSharedEncryptionKey(privateKey, remotePubDerivationKey, true)
// 		sharedKeyHexNewlyDerived = buf2hex(await window.crypto.subtle.exportKey('raw', exportableSharedKey))
// 	}
// 	const sharedKeyHexFromPubSub = buf2hex(await window.crypto.subtle.exportKey('raw', encryptedWith))
// 	const testPayloadStr = 'TESTYtestTEST123'
// 	const testPayload = enc.encode(testPayloadStr)
// 	let encPayload = await state.crypto?.aes.encrypt(testPayload, pubSubWithAesKey.encryptedWith, SymmAlg.AES_GCM /* , fixedIV */)

// 	const decrypted = await decryptWithAesSharedKey(encPayload ?? new Uint8Array(), pubSubWithAesKey)
// 	const success = testPayloadStr === decrypted
// 	const results = { success, encPayload, decrypted, sharedKeyHexFromPubSub, sharedKeyHexNewlyDerived, publishedBy }
// 	VERBOSE('[decrypt] test results:', results)
// 	return results
// }
