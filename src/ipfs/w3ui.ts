import type { Capability, Delegation, DID, Principal } from '@ucanto/interface'
import {
	Abilities,
	Agent,
	authorize as accessAuthorize,
	createAgent,
	CreateDelegationOptions,
	getCurrentSpace as getCurrentSpaceInAgent,
	getSpaces,
	KeyringContextActions,
	KeyringContextState,
	W3UI_ACCOUNT_LOCALSTORAGE_KEY,
} from '@w3ui/keyring-core'
import {
	CARMetadata,
	ProgressStatus,
	uploadCAR,
	uploadDirectory,
	UploaderContextActions,
	UploaderContextState,
	uploadFile,
} from '@w3ui/uploader-core'
import { add as storeAdd } from '@web3-storage/capabilities/store'
import { add as uploadAdd } from '@web3-storage/capabilities/upload'
import { Logger } from 'besonders-logger/src'
import { flow, observable, runInAction } from 'mobx'
import { createSignal } from 'solid-js'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

export { KeyringContextActions, KeyringContextState }

export type KeyringContextValue = [
	state: KeyringContextState,
	actions: KeyringContextActions,
]
const defaultState: KeyringContextState = {
	space: undefined,
	spaces: [],
	agent: undefined,
	account: window.localStorage.getItem(W3UI_ACCOUNT_LOCALSTORAGE_KEY) ?? undefined,
}
let keyringState: KeyringContextState
let keyringActions: KeyringContextActions
export function useW3Keyring() {
	if (!keyringState) throw ERROR(`[useW3Keyring] not initialized`)
	return [keyringState, keyringActions] as const
}

export type UploaderContextValue = [
	state: UploaderContextState,
	actions: UploaderContextActions,
]
let uploaderState: UploaderContextState
let uploaderActions: UploaderContextActions
export function useW3Uploader() {
	if (!uploaderState) throw ERROR(`[useW3Uploader] not initialized`)
	return [uploaderState, uploaderActions] as const
}

export async function initW3Keyring() {
	DEBUG(`[initW3Keyring] initializing`)
	if (keyringState) return ERROR(`[initW3Keyring] Double call`)

	keyringState = observable(defaultState)

	const [agent, setAgent] = createSignal<Agent>(null)
	const getAgent = flow(function*() {
		let a = agent()
		if (a == null) {
			a = yield createAgent({
				servicePrincipal: null, // props.servicePrincipal,
				connection: null, // props.connection
			})
			setAgent(a)
			keyringState.agent = agent().issuer
			keyringState.space = getCurrentSpaceInAgent(agent())
			keyringState.spaces = getSpaces(agent())
		}
		return a
	})

	const [registerAbortController, setRegisterAbortController] = createSignal<AbortController>()

	const authorize = async (email: `${string}@${string}`): Promise<void> => {
		const agent = await getAgent()
		const controller = new AbortController()
		setRegisterAbortController(controller)

		try {
			await accessAuthorize(agent, email, { signal: controller.signal })
			const newSpaces = runInAction(() => {
				keyringState.account = email
				window.localStorage.setItem(W3UI_ACCOUNT_LOCALSTORAGE_KEY, email)
				const newSpaces = getSpaces(agent)
				keyringState.spaces = newSpaces
				return newSpaces
			})
			const newCurrentSpace = getCurrentSpaceInAgent(agent) ?? newSpaces[0]
			if (newCurrentSpace != null) {
				await setCurrentSpace(newCurrentSpace.did() as DID<'key'>)
			}
		} catch (error) {
			if (!controller.signal.aborted) {
				throw error
			}
		}
	}

	const cancelAuthorize = (): void => {
		const controller = registerAbortController()
		if (controller != null) {
			controller.abort()
		}
	}

	const createSpace = async (name?: string): Promise<DID> => {
		const agent = await getAgent()
		const { did } = await agent.createSpace(name)
		await agent.setCurrentSpace(did)
		runInAction(() => {
			keyringState.space = getCurrentSpaceInAgent(agent)
		})
		return did
	}

	const registerSpace = async (email: string): Promise<void> => {
		const agent = await getAgent()
		await agent.registerSpace(email, { /* space: keyring.space, */ provider: 'did:web:web3.storage' })
		runInAction(() => {
			keyringState.space = getCurrentSpaceInAgent(agent)
			keyringState.spaces = getSpaces(agent)
		})
	}

	const setCurrentSpace = async (did: DID): Promise<void> => {
		const agent = await getAgent()
		await agent.setCurrentSpace(did as DID<'key'>)
		runInAction(() => {
			keyringState.space = getCurrentSpaceInAgent(agent)
		})
	}

	const loadAgent = async (): Promise<void> => {
		DEBUG(`[loadAgent] agent?`, agent())
		if (agent() != null) return
		await getAgent()
	}

	const unloadAgent = async (): Promise<void> => {
		DEBUG(`[unloadAgent] agent?`, agent())
		runInAction(() => {
			keyringState.space = undefined
			keyringState.spaces = []
			keyringState.agent = undefined
			keyringState.account = undefined
			setAgent(undefined)
		})
	}

	const resetAgent = async (): Promise<void> => {
		const agent = await getAgent()
		// @ts-expect-error TODO: expose store in access client
		await Promise.all([agent.store.reset(), unloadAgent()])
	}

	const getProofs = async (caps: Capability[]): Promise<Delegation[]> => {
		const agent = await getAgent()
		return agent.proofs(caps)
	}

	const createDelegation = async (
		audience: Principal,
		abilities: Abilities[],
		options: CreateDelegationOptions,
	): Promise<Delegation> => {
		const agent = await getAgent()
		const audienceMeta = options.audienceMeta ?? {
			name: 'agent',
			type: 'device',
		}
		return await agent.delegate({
			...options,
			abilities,
			audience,
			audienceMeta,
		})
	}

	const addSpace = async (proof: Delegation): Promise<void> => {
		const agent = await getAgent()
		await agent.importSpaceFromDelegation(proof)
	}

	keyringActions = {
		loadAgent,
		unloadAgent,
		resetAgent,
		createSpace,
		registerSpace,
		setCurrentSpace,
		getProofs,
		createDelegation,
		addSpace,
		authorize,
		cancelAuthorize,
	}

	DEBUG(`[initW3Keyring] loading agent`)
	await loadAgent()
	DEBUG(`[initW3Keyring] done`, { w3State: keyringState, w3Actions: keyringActions })
	return [keyringState, keyringActions]
}

export async function initW3Uploader() {
	DEBUG(`[initW3Uploader] initializing`)
	const props: any = {}
	if (uploaderState) return ERROR(`[initW3Uploader] Double call`)

	uploaderState = observable({
		storedDAGShards: [],
		uploadProgress: {},
	})

	uploaderActions = {
		async uploadFile(file: Blob) {
			if (keyringState.space == null) throw new Error('missing space')
			if (keyringState.agent == null) throw new Error('missing agent')

			const storedShards: CARMetadata[] = []
			uploaderState.storedDAGShards = storedShards

			const conf = {
				issuer: keyringState.agent,
				with: keyringState.space.did(),
				audience: props.servicePrincipal,
				proofs: await keyringActions.getProofs([
					{ can: storeAdd.can, with: keyringState.space.did() },
					{ can: uploadAdd.can, with: keyringState.space.did() },
				]),
			}

			const result = await uploadFile(conf, file, {
				onShardStored: (meta) => {
					storedShards.push(meta)
					uploaderState.storedDAGShards = [...storedShards]
				},
				onUploadProgress: (status: ProgressStatus) => {
					uploaderState.uploadProgress = { ...uploaderState.uploadProgress, [status.url ?? '']: status }
				},
				connection: props.connection,
			})
			uploaderState.uploadProgress = {}
			return result
		},
		async uploadDirectory(files: File[]) {
			if (keyringState.space == null) throw new Error('missing space')
			if (keyringState.agent == null) throw new Error('missing agent')

			const storedShards: CARMetadata[] = []
			uploaderState.storedDAGShards = storedShards

			const conf = {
				issuer: keyringState.agent,
				with: keyringState.space.did(),
				audience: props.servicePrincipal,
				proofs: await keyringActions.getProofs([
					{ can: storeAdd.can, with: keyringState.space.did() },
					{ can: uploadAdd.can, with: keyringState.space.did() },
				]),
			}

			const result = await uploadDirectory(conf, files, {
				onShardStored: (meta) => {
					storedShards.push(meta)
					uploaderState.storedDAGShards = [...storedShards]
				},
				onUploadProgress: (status: ProgressStatus) => {
					uploaderState.uploadProgress = { ...uploaderState.uploadProgress, [status.url ?? '']: status }
				},
				connection: props.connection,
			})
			uploaderState.uploadProgress = {}
			return result
		},
		async uploadCAR(car: Blob) {
			if (keyringState.space == null) throw new Error('missing space')
			if (keyringState.agent == null) throw new Error('missing agent')

			const storedShards: CARMetadata[] = []
			uploaderState.storedDAGShards = storedShards

			const conf = {
				issuer: keyringState.agent,
				with: keyringState.space.did(),
				audience: props.servicePrincipal,
				proofs: await keyringActions.getProofs([
					{ can: storeAdd.can, with: keyringState.space.did() },
					{ can: uploadAdd.can, with: keyringState.space.did() },
				]),
			}

			const result = await uploadCAR(conf, car, {
				onShardStored: (meta) => {
					storedShards.push(meta)
					uploaderState.storedDAGShards = [...storedShards]
				},
				onUploadProgress: (status: ProgressStatus) => {
					uploaderState.uploadProgress = { ...uploaderState.uploadProgress, [status.url ?? '']: status }
				},
				connection: props.connection,
			})
			uploaderState.uploadProgress = {}
			return result
		},
	}

	DEBUG(`[initW3Uploader] done`, { w3State: uploaderState, w3Actions: uploaderActions })
}
