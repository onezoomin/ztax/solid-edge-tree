import { SymmAlg } from '@oddjs/odd/components/crypto/implementation'
import type { UploaderContextValue } from '@w3ui/solid-uploader'
import { Logger } from 'besonders-logger/src'
import { flow } from 'mobx'
import type { CID } from 'multiformats'
import stringify from 'safe-stable-stringify'
import type { Name } from 'w3name'
import * as W3Name from 'w3name'
import { decryptWithAesSharedKey, getAESkeyForEncryptedApplogs } from '../data/AgentCrypto'
import { agentState, getAgentString, useAgent } from '../data/AgentStore'
import { prepareIncompleteApplogs } from '../data/ApplogDB'
import { getPublicationData } from '../data/block-utils'
import { Applog, ApplogForInsert, getApplogTypeErrors, isValidApplog } from './../data/datalog/datom-types'
import { ApplogStream, ApplogStreamInMemory, rollingFilter, WriteableApplogStream } from '../data/datalog/query/engine'
import { agentsOfStream, entityOverlapCount } from '../data/datalog/query/queries'
import { IPublication, ISubscription } from '../data/local-state'
import { dateNowIso, sleep, unwrapIfSingle } from '../data/Utils'
import { notifyToast } from '../ui/ui-utils'
import { encodeApplogAndGetCid, encodeBlockOriginal, prepareForPub } from './ipfs-utils'
import { decodeCar, makeCarBlob, parseW3Name, publishIPNS, resolveIPNS, retrieveCar, storeCar } from './web3storage'
// import { debounce } from 'lodash-es';

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

// im  port * as Wasm from 'note3-rs'
// let_wasm: any
// async function initWasm() {
// 	if (!_wasm) {
// 		_wasm = await Wasm.default()
// 		DEBUG('WASM', _wasm, Wasm)
// 		await Wasm.init_rs()
// 	}
// }

export const neverEncryptAttrs = [
	'agent/jwkd',
	'agent/appAgent',
	'pub/encryptedFor',
	'pub/sharedKey',
]
export async function publishDatalog(dataStream: ApplogStream, uploader: UploaderContextValue[1], publication: IPublication) {
	// const [_progress, uploader] = useUploader()
	// ? where does the filter logic belong?
	// 1. on the publication as a. rootBlockID, b. queryWhere results, c. ??
	// 2. ??

	DEBUG('[publish]', publication, { dataStream, uploader })
	const agent = useAgent()
	const { appAgent /* getEncryptionKeyCrypto, decryptWithAesSharedKey, */ } = agent
	// await initWasm()
	// const car_data = Wasm.make_car(applogs)
	// DEBUG('Car data:', car_data)

	// const pubSigningKeyBytes = await state.crypto?.keystore?.publicWriteKey()
	let publicationNameString: string, writableNameFromPriKey
	DEBUG('Publishing datalog snapshot via publication:', { ipns: publication.id, publication })
	writableNameFromPriKey = await W3Name.from(publication.pk)
	DEBUG('using:', { writableNameFromPriKey })
	publicationNameString = publication.id
	const publicationDataStream = getPublicationData(dataStream, publication)
	if (!publicationDataStream) {
		ERROR(`Empty publication data`, { publication, dataStream })
		throw ERROR(`Empty publication data`, publication.id)
	}

	let applogs: ApplogForInsert[] = [...publicationDataStream.applogs]

	// TODO prevent publish if there is no new info
	let maybeEncryptedApplogs: Array<Applog | ApplogForInsert | Uint8Array> = []

	const { sharedAgents, sharedKeyMap, sharedKey } = publication ?? {}
	const agentLogs = rollingFilter(dataStream, {
		en: agent.ag,
		at: ['agent/ecdh', 'agent/jwkd', 'agent/appAgent'],
	})
	DEBUG(`[publish] adding agent logs:`, agentLogs)
	applogs.push(...agentLogs.applogs)
	let encPayload
	const encryptAndTestDecrypt = async (applog: ApplogForInsert, keyToUse: CryptoKey) => {
		const eachLog = prepareForPub(applog) // without cid
		const enc = new TextEncoder()
		const stringified = stringify(eachLog)
		const stringifiedEncodedAppLogPayload = enc.encode(stringified) // TODO: consider encodeToDagJson instead
		VERBOSE('[odd]', { eachLog, stringified, stringifiedEncodedAppLogPayload })

		try {
			encPayload = await agent.crypto?.aes.encrypt(stringifiedEncodedAppLogPayload, keyToUse, SymmAlg.AES_GCM)
		} catch (err) {
			throw ERROR('FAILED TO ENC payload length:', stringifiedEncodedAppLogPayload.length, { err })
		}

		const decrypted = await decryptWithAesSharedKey(encPayload, keyToUse, 'string')

		VERBOSE('[odd] encrypted length:', stringifiedEncodedAppLogPayload.length, { encPayload, decrypted })
		return encPayload
	}
	DEBUG('[publish] applogs ', applogs)

	if (sharedAgents) { // encrypt all Applogs
		if (!sharedKey || !sharedKeyMap) {
			DEBUG('weird state, publishing nonEncrypted', { sharedAgents, sharedKeyMap, sharedKey }) // ? could this be a problem
			maybeEncryptedApplogs = applogs
		} else {
			const agentSharedKeyLogs = []
			for (const [eachAgent, eachEncKey] of Array.from(sharedKeyMap.entries())) {
				agentSharedKeyLogs.push({
					en: eachAgent,
					at: 'pub/sharedKey',
					vl: eachEncKey, // these are encrypted with the derived key from the local agent private and remote agent public keys
				})
			}
			// const encryptedForLogs = await insertApplogs(agentSharedKeyLogs)
			// DEBUG(`[publish] adding agentSharedKeyLogs:`, encryptedForLogs)
			applogs.push(...prepareIncompleteApplogs(agentSharedKeyLogs)) // ensure that non encrypted pub log(s) are in the push

			// TODO ensure that all needed keys are in

			for (const eachLog of applogs) {
				VERBOSE('[crypto] encrypting ', eachLog)
				if (neverEncryptAttrs.includes(eachLog.at)) {
					maybeEncryptedApplogs.push(/* prepareForPub( */ eachLog /* ) */) // ? this seemed to double the below one - @gotjoshua?
					continue
				}
				try {
					encPayload = await encryptAndTestDecrypt(eachLog, sharedKey)
				} catch (err) {
					// its already traced in encryptAndTestDecrypt
					continue
				}
				maybeEncryptedApplogs.push(encPayload)
			}
		}
	} else maybeEncryptedApplogs = applogs // publish nonEncrypted

	maybeEncryptedApplogs = maybeEncryptedApplogs.map(log => (log as Applog).en ? prepareForPub(log as Applog) : log)
	const encodedApplogs = await Promise.all(maybeEncryptedApplogs.map(encodeBlockOriginal))
	DEBUG('[publish] encoded root', { appAgent, encodedApplogs })

	const cids = encodedApplogs.map(b => b.cid ?? null) // ? why would it be null
	const root = { applogs: cids }
	const encodedRoot = await encodeBlockOriginal(root)
	DEBUG('[publish] encoded root', { cids, encodedRoot })

	const car = await makeCarBlob(encodedRoot.cid, [encodedRoot, ...encodedApplogs])
	const cid = await storeCar(car, uploader)

	await publishIPNS(cid.toString(), writableNameFromPriKey)

	agentState.updatePub(publication.id, { lastPush: dateNowIso(), lastCID: cid.toString() })
	queueMicrotask(() => void pullCarToEdge(cid))
	return publicationNameString
}
// export const pullCarToEdge = debounce( // probably should debounce and or reconsider auto push when no changes
async function pullCarToEdge(cid: string) {
	await sleep(1500)
	const p = performance.now()
	await retrieveCar(cid)
	DEBUG('pullCar from w3 took:', performance.now() - p, 'ms')
	const dwebURL = `https://${cid}.ipfs.dweb.link/?format=car`
	const nftsURL = `https://${cid}.ipfs.nftstorage.link/?format=car`
	const ipfsURL = `https://ipfs.io/ipfs/${cid}/?format=car`
	const URLs = [/* dwebURL,  nftsURL, */ ipfsURL]
	for (const eachURL of URLs) {
		await sleep(150)
		queueMicrotask(async () => {
			await sleep(250)
			void fetch(eachURL)
		})
	}

	/**** some edge URL info:
	// https://dweb.link/ipfs/baguqeera2gs6pqrzjcolacw6as2lri2erky22yy5g5ymzwcrpzrokkje2nyq/?format=car // redirects to =>
	// https://baguqeera2gs6pqrzjcolacw6as2lri2erky22yy5g5ymzwcrpzrokkje2nyq.ipfs.dweb.link/?format=car

	// https://ipfs.io/ipfs/baguqeera2gs6pqrzjcolacw6as2lri2erky22yy5g5ymzwcrpzrokkje2nyq/?format=car

	// https://nftstorage.link/ipfs/baguqeera2gs6pqrzjcolacw6as2lri2erky22yy5g5ymzwcrpzrokkje2nyq/?format=car // redirects to =>
	// https://baguqeera2gs6pqrzjcolacw6as2lri2erky22yy5g5ymzwcrpzrokkje2nyq.ipfs.nftstorage.link/?format=car

	// not found:
	// https://ipfs.runfission.com/ipfs/baguqeera2gs6pqrzjcolacw6as2lri2erky22yy5g5ymzwcrpzrokkje2nyq/?format=car
	// no car support:
	// https://cloudflare-ipfs.com/ipfs/baguqeera2gs6pqrzjcolacw6as2lri2erky22yy5g5ymzwcrpzrokkje2nyq/?format=car

	https://ipfs.4everland.io/ipfs/baguqeera2gs6pqrzjcolacw6as2lri2erky22yy5g5ymzwcrpzrokkje2nyq/?format=car


	*/
}

export const retrieveDatalog = flow(
	async function*(dataStream: WriteableApplogStream, ipns: Name, subToUpdate?: ISubscription, pinCID?: string) {
		const ipnsNameString = ipns.toString()
		const agent = useAgent()
		if (!subToUpdate) {
			subToUpdate = agentState.subscriptionsMap.get(ipnsNameString)
			if (!subToUpdate) DEBUG('retrieving unknown sub', ipnsNameString)
		}
		const { ag } = agent
		DEBUG('Retrieving Applogs from', { ipnsNameString, subToUpdate, asAg: ag, pinCID, dataStream })

		let cid
		try {
			cid = pinCID || (yield resolveIPNS(ipns))
		} catch (err) {
			throw ERROR(`Failed to resolve IPNS`, ipns.toString(), err)
		}

		const car = yield retrieveCar(cid)

		const { roots, blocks } = yield decodeCar(car)
		if (!roots || !blocks) throw ERROR('decodeCar is funky', { roots, blocks })

		if (roots.length !== 1) {
			WARN('Unexpected roots count:', roots)
		}
		const root = blocks.get(roots[0])
		const applogs: Array<Applog | Uint8Array> = root.applogs.map((cid: CID) => {
			const applog = blocks.get(cid.toV1().toString())
			return {
				...applog,
				cid: cid.toString(),
			}
		})
		const nonEncryptedLogs = applogs.filter(l => !(l instanceof Uint8Array) /* || isValidApplog(l as Applog) */) as Applog[]
		const encryptedLogs = applogs.filter(l => l instanceof Uint8Array) as Uint8Array[]
		DEBUG('Root:', root, { roots, blocks, encryptedLogs, nonEncryptedLogs })

		if (subToUpdate) agentState.updateSub(subToUpdate.id, { lastCID: cid, lastPullAttempt: dateNowIso() })

		// TODO consider what pubAtoms we want to use
		// const publicationAtoms = nonEncryptedLogs.filter(eachLog => (eachLog.at.includes && eachLog.at.includes('pub/') && eachLog.en === ipnsNameString))

		const { aesKey, publisherAg } = yield getAESkeyForEncryptedApplogs(nonEncryptedLogs)
		const encryptedForCurrentAgent = aesKey ? ag : null
		DEBUG('[decrypt] isEncrypted?:', !!encryptedLogs.length, { aesKey, roots, applogs, subToUpdate })

		let maybeDecryptedApplogs = [] as Applog[]
		let isErrored = false, isRetry = false

		const tryToIntegrateApplogs = flow(function*() {
			for (const eachLog of encryptedLogs) {
				if (!aesKey) {
					isErrored || ERROR('encrypted log(s) found but no pubSub Key found', { aesKey, applogs, subToUpdate })
					isErrored = true
					continue
				}
				VERBOSE('[decrypt] attempting decryption:', eachLog)
				// if(subToUpdate.encryptedWith) yield doTestEncryptDecrypt(subToUpdate)
				const decrypted: Applog = yield decryptWithAesSharedKey(eachLog, aesKey, 'parsed')
				const pv = decrypted.pv ? decrypted.pv.toString() : null
				VERBOSE('[decrypt] decrypted:', { decrypted })
				maybeDecryptedApplogs.push({ ...decrypted, pv, cid: encodeApplogAndGetCid(decrypted).toString() }) // locally we want pv as a string
			}
			for (const eachLog of nonEncryptedLogs) {
				if (isValidApplog(eachLog)) {
					const pv = eachLog.pv ? eachLog.pv.toString() : null
					maybeDecryptedApplogs.push({ ...eachLog, pv }) // locally we want pv as a string
				} else {
					const errors = getApplogTypeErrors(eachLog)
					WARN('[pull] unknown log situation', eachLog, unwrapIfSingle(errors))
				}
			}

			const newApplogs = maybeDecryptedApplogs.filter((newLog: any) => !dataStream.hasApplog(newLog as Applog, false))
			DEBUG('New Applogs:', newApplogs, { maybeDecryptedApplogs, db: dataStream })
			if (newApplogs.length) {
				dataStream.insert(newApplogs)
			}
		})
		tryToIntegrateApplogs() // first try

		if (isErrored) {
			WARN('trying to solve encryption situation', { aesKey, nonEncryptedLogs, applogs, subToUpdate, ipnsNameString })
			if (isRetry) {
				ERROR('second round of errors')
			} else {
				isRetry = true
				let derivationJWKstring, isForMe = false
				// try to infer encryption and setup subscription before retrying

				if (!encryptedForCurrentAgent) {
					notifyToast(
						`Encrypted Applogs found from ${getAgentString(publisherAg)} (${publisherAg}) that are not for you.`,
						'warning',
						Infinity,
					)
				}
				DEBUG('[decrypt] Errors found retrying now', { isForMe, publisherAg, derivationJWKstring, subToUpdate })

				isForMe && derivationJWKstring && tryToIntegrateApplogs() // ? only if its for me and i have the remote jwk, i think ?
			}
		}

		if (encryptedForCurrentAgent && encryptedForCurrentAgent !== subToUpdate?.encryptedFor) {
			WARN('encryptedFor has changed', { subToUpdate, encryptedFor: encryptedForCurrentAgent })
		}

		if (isErrored) {
			throw ERROR(`Failed to decrypt (${encryptedLogs.length}/${applogs.length}) ${ipns.toString()}`)
		}
		if (!isErrored) {
			// TODO hunt this down should be full deprecated
			if (subToUpdate) agentState.updateSub(subToUpdate.id, { lastPull: dateNowIso(), encryptedFor: encryptedForCurrentAgent })
		}
		return { cid, applogs, encryptedFor: encryptedForCurrentAgent }
	},
)

export async function retrievePubData(id: string, pinCID?: string) {
	const dataStream = new ApplogStreamInMemory([], [], 'preview-' + id)
	const { cid, applogs: maybeEncryptedApplogs } = await retrieveDatalog(dataStream, parseW3Name(id), null, pinCID)
	return { dataStream, cid, maybeEncryptedApplogs }
}
export async function retrievePubDataWithExtras(baseDS: ApplogStream, id: string, pinCID?: string) {
	const { dataStream, cid, maybeEncryptedApplogs } = await retrievePubData(id, pinCID)
	return {
		dataStream,
		maybeEncryptedApplogs,
		cid,
		entityOverlapCount: entityOverlapCount(baseDS, dataStream),
		agents: Array.from(agentsOfStream(dataStream).keys()),
		newLogs: dataStream.applogs.filter(log => !baseDS.hasApplog(log, false)).length,
	}
}
