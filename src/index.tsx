import { hashIntegration, Route, Router, Routes } from '@solidjs/router'
import { Logger } from 'besonders-logger/src'
import { render } from 'solid-js/web'
import { App } from './App'
import { initApplogDB } from './data/ApplogDB'
import { DBContext } from './ui/reactive'

import 'solid-devtools'
import { createSignal, lazy } from 'solid-js'
import 'virtual:uno.css'
import { PwaReloadPrompt } from './components/PwaReloadPrompt'
import './index.css'
import './ui/solid-mobx'

const TestPage = lazy(() => import('./TestPage'))

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO) // eslint-disable-line no-unused-vars

await (async () => {
	// await new Promise(resolve => setTimeout(resolve, 1000))
	const [applogStream] = createSignal(await initApplogDB())
	// TODO: const [applogStream] = createResource(async () => await initApplogDB())
	LOG('[index] DB init done', applogStream())

	render(() => (
		/* <ErrorBoundary //TODO: This doesn't work with mobx integration and/or lazy components
			fallback={(err, reset) =>
				ERROR(`<App> caught:`, err) && (
					<div flex='~ col' w-full gap-4>
						<sl-alert variant='danger' open>
							<div flex='~ items-center gap-4' text-xl>
								<div class='i-ph:warning-bold' />
								<strong>Unknown error</strong>
							</div>
							<pre mb-0>{err.message ?? JSON.stringify(err, undefined, 4)}</pre>
							<sl-button mt-4 variant='primary' onclick={() => window.location.hash = '#/'} flex-self-end>Back home</sl-button>
							<sl-button mt-4 ml-2 variant='secondary' onclick={reset} flex-self-end>Retry</sl-button>
						</sl-alert>
					</div>
				)}
		> */
		<Router source={hashIntegration()}>
			<DBContext.Provider value={applogStream}>
				{/* <Show fallback={<sl-spinner />}> */}
				<div flex='~ col items-center' bg='#282c34' px-2>
					<div flex='~ col items-stretch' bg='#282c34' w-full max-w-4xl gap-2>
						<PwaReloadPrompt />
					</div>
				</div>
				<Routes>
					<Route path='/test' component={TestPage} />
					<Route path='*' component={App} />
				</Routes>
				{/* </Show> */}
			</DBContext.Provider>

			{
				/* <Routes>
					<Route path='/test' component={TestPage} />
					<Route path='/' component={App} />
				</Routes> */
			}
		</Router>
		/* </ErrorBoundary> */
	), document.getElementById('root') as HTMLElement)
})()

{
	/*
<Router>
	<App />
</Router>
VS
<Router>
	<Routes>
		<Route path='/' element={<App />} />
	</Routes>
</Router>
	*/
}
