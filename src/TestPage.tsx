import { Logger } from 'besonders-logger/src'
import { trace } from 'mobx'
import { createSignal } from 'solid-js'
import { ApplogView } from './components/ApplogView'
import { getApplogDB, insertApplogs } from './data/ApplogDB'
import { withoutHistory } from './data/datalog/query/queries'

import '@shoelace-style/shoelace/dist/themes/dark.css'
import './ui/shoelace'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG) // eslint-disable-line no-unused-vars

export default ({}) => {
	LOG('!!! TESTPAGE RERENDER !!!')
	trace(true)
	const ds = withoutHistory(getApplogDB())
	// const db = untracked(() => getApplogDB())
	// const deletionLogs = rollingFilter(db, { at: ['isDeleted', 'relation/isDeleted', 'block/isDeleted'], vl: true }, {
	// 	name: 'isDeleted',
	// })
	// autorun(() => {
	// 	DEBUG(`deletionLogs: ${JSON.stringify(deletionLogs)}`)
	// })

	// const focus = useFocus()

	// const block = useBlock(focus() /* HACK: reactive to ID? */)
	// const roots = useRoots()

	let [id, setID] = createSignal(null as string | null)
	function testChange() {
		if (!id()) {
			setID(new Date().toTimeString())
		}
		insertApplogs([
			// {'en': id,'at': 'block/content','vl': 'test root ' + new Date() },
			{ 'en': 'blk-' + id().replaceAll(' ', '').toLowerCase(), 'at': 'block/content', 'vl': 'test content ' + new Date() },
		])
	}
	function testDelete() {
		insertApplogs([
			{ 'en': id(), 'at': 'isDeleted', 'vl': true },
		])
		setID(null)
	}

	return (
		<div>
			<h1>Test</h1>
			<button onClick={testChange}>CHANGE</button>
			<button onClick={testDelete}>TOGGLE DEL</button>
			{/* <pre>{JSON.stringify(roots, undefined, 2)}</pre> */}
			{/* <pre>{JSON.stringify(block, undefined, 2)}</pre> */}
			{
				/* <pre>{deletionLogs.name} - {deletionLogs.size}</pre>
			<pre>{deletionLogs.applogs.map(log => JSON.stringify(log/* , undefined, 2 * /)).join('\n')}</pre> */
			}

			<ApplogView mt-4 ds={ds} />
		</div>
	)
}
