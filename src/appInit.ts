import '@shoelace-style/shoelace/dist/themes/dark.css'
import { Logger } from 'besonders-logger/src'
import { flow } from 'mobx'
import { initOddCrypto } from './data/AgentCrypto'
import { agentState } from './data/AgentStore'
import { getApplogDB } from './data/ApplogDB'
import { addBlock } from './data/block-utils'
// import { startSyncService } from './ipfs/sync-service'
import { initIPNS, initWeb3Storage } from './ipfs/web3storage'
import { useCurrentDS, useRoots, withDS } from './ui/reactive'
import './ui/shoelace'
import { focusBlockAsInput } from './ui/ui-utils'

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO, { prefix: '[init]' }) // eslint-disable-line no-unused-vars

export const appInit = flow(function*({ /* setInitialized, */ pubFromUrl }) {
	DEBUG(`appInit start`, { pubFromUrl })
	yield initOddCrypto()
	DEBUG(`appInit web3`)
	yield initWeb3Storage() // ? is this a good place to init? (can the UI init empty first?)

	yield initIPNS()

	yield agentState.loadIDB()
	yield agentState.loadW3()

	const rawDS = getApplogDB()
	const currentDS = withDS(rawDS, () => useCurrentDS())
	const roots = withDS(rawDS, () => useRoots())
	// const focus = useFocus()
	// if (!focus()) {
	if (!roots.length) {
		LOG(`Creating example root`, { roots })
		const newRoot = yield addBlock({
			dataStream: currentDS,
			asChildOf: null,
			content: 'Example note, edit me (or ctrl+enter to create children)',
		})
		// zoomToBlock({ id: newRoot })
		focusBlockAsInput({ id: newRoot })
	} else {
		if (roots.length === 1) {
			focusBlockAsInput({ id: roots[0] })
		}
	}
	// }

	// window.addEventListener('hashchange', onHashChange)

	LOG(`appInit end`)
	// startSyncService(true)
	return () => {} // window.removeEventListener('hashchange', onHashChange)
})
