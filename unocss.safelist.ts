const layout = [
	'max-w-10',
	'-mt-1',
	'flex-wrap-reverse',
	'w-0',
	'w-6',
	'h-6',
	'mb-1',
	'hidden',
	'w-3',
	'h-3',
	'w-2',
	'h-2',
	'gap-1',
]
// TODO width and height array auto magic
const text = [
	'text-ellipsize',
	'text-xs',
]
export const iconNames = [
	'arrows-clockwise',
	'arrows-down-up-fill',
	'caret-right-bold',
	'check-circle',
	'copy-light',
	'dot-bold',
	'dot-duotone',
	'eye-bold',
	'eye',
	'info-bold',
	'info',
	'link',
	'link-bold',
	'lock-key-fill',
	'lock-key-open-fill',
	'lock-simple-open-light',
	'magnifying-glass',
	'pencil-line-duotone',
	'shield-plus',
	'text-b-bold',
	'text-b',
	'text-indent',
	'text-italic-bold',
	'text-italic',
	'text-outdent',
	'text-strikethrough-bold',
	'text-underline-bold',
	'text-underline',
	'trash-duotone',
	'warning-bold',
	'x-circle',
] as const
const icons = iconNames.map(iconName => `i-ph:${iconName}`)
const box = [
	'border-gray-200',
	'bg-gray-200',
	'opacity-40',
	'opacity-70',
]
const animation = [
	'rotate-180',
	'transition-property-[height]',
	'transition-property-[opacity]',
	'duration-300',
	'duration-500',
]

export const safelist = [
	...layout,
	...icons,
	...text,
	...box,
	...animation,
]
