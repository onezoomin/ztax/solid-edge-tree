import { readFileSync } from 'node:fs'
import { defineConfig, presetAttributify, presetIcons, presetUno } from 'unocss'
import { safelist } from './unocss.safelist'
// import ipld from './src/components/ipld-logo-only-plain.svg?raw' // didn't work here
// import { variantMap } from './src/ui/ui-utils'
const ipldFS = readFileSync('./src/icons/ipld-logo-only-plain.svg', 'utf-8')

export default defineConfig({
	shortcuts: [
		{ logo: 'i-logos-solidjs-icon w-2em h-2em transform transition-800 hover:rotate-360' },
		{ 'text-mono': 'font-mono' },
		{ flexcol: 'flex flex-col gap-2' },
		{ flexrow: 'flex flex-row gap-2' },
		{ darkdropdown: 'p-6 bg-gray-900 color-white max-w-2xl box-border' },
		{ 'outline-rounded-white': 'outline-solid outline-1 outline-offset-1 b-rounded-md outline-white' },
	],
	safelist, // HACKY way to avoid shadow dom and dynamically missing icons classes and stuff
	presets: [
		presetUno(),
		presetAttributify(),
		presetIcons({
			extraProperties: {
				'display': 'inline-block',
				'vertical-align': 'middle',
			},
			collections: {
				custom: {
					ipld: ipldFS,
					circle: '<svg viewBox="0 0 120 120"><circle cx="60" cy="60" r="50"></circle></svg>',
					/* ... */
				},
				ph: () => import('@iconify-json/ph/icons.json').then(i => i.default),
			},
		}),
	],
})
