import basicSsl from '@vitejs/plugin-basic-ssl'
import path from 'path'
// import copy from 'rollup-plugin-copy'
import suidPlugin from '@suid/vite-plugin'
import devtools from 'solid-devtools/vite'
import UnoCSS from 'unocss/vite'
import { defineConfig } from 'vite'
import { VitePWA } from 'vite-plugin-pwa'
import solidPlugin from 'vite-plugin-solid'

export default defineConfig({
	plugins: [
		suidPlugin(),
		solidPlugin(),
		UnoCSS(),
		basicSsl(),
		devtools({
			/* features options - all disabled by default */
			autoname: true, // e.g. enable autoname
			// TODO: locator & co - https://github.com/thetarnav/solid-devtools/tree/main/packages/main#options
		}),
		// // Copy Shoelace assets to dist/shoelace - https://github.com/shoelace-style/rollup-example/blob/master/rollup.config.js
		// copy({
		// 	copyOnce: true,
		// 	targets: [
		// 		{
		// 			src: path.resolve(__dirname, 'node_modules/@shoelace-style/shoelace/dist/assets'),
		// 			dest: path.resolve(__dirname, 'dist/shoelace'),
		// 		},
		// 	],
		// }),

		VitePWA({
			registerType: 'prompt',
			workbox: {
				globPatterns: ['**/*.{js,css,html,ico,png,svg,wasm}'],
				maximumFileSizeToCacheInBytes: 5000000,
			},
			// devOptions: { enabled: true }, // doesn't seem to work
			includeAssets: [
				'favicon.ico',
				'apple-touch-icon-180x180.png',
				'maskable-icon-512x512.png',
				'commit.txt', // added by CI
			],
			manifest: {
				name: 'Note3',
				short_name: 'Note3',
				description: 'Note3 - decentralized woven data PoC',
				theme_color: '#282c34',
				icons: [
					{
						src: 'pwa-192x192.png',
						sizes: '192x192',
						type: 'image/png',
					},
					{
						src: 'pwa-512x512.png',
						sizes: '512x512',
						type: 'image/png',
					},
				],

				// https://developer.mozilla.org/en-US/docs/Web/Manifest/display
				'display': 'standalone',

				// Share from native share selector
				'share_target': {
					'action': '/#/share',
					'params': {
						'title': 'title',
						'text': 'text',
						'url': 'url',
					},
				},

				// Protocol handler - https://developer.mozilla.org/en-US/docs/Web/Manifest/protocol_handlers#browser_compatibility
				'protocol_handlers': [
					{
						'protocol': 'note3',
						'url': '/#?protocol_handler=%s',
					},
				],

				'url_handlers': [
					{
						'origin': 'https://note3.app',
					},
				],
			},
		}),
	],
	server: {
		https: true,
		// port: 443,
	},
	build: {
		target: 'esnext',
		outDir: path.resolve(__dirname, 'dist'), // needed for shoelace - https://github.com/shoelace-style/rollup-example/blob/master/rollup.config.js
		sourcemap: true,
		// minify: false
		emptyOutDir: true,

		rollupOptions: {
			external: [
				// workaround for https://github.com/unocss/unocss/discussions/1953#discussioncomment-4298857
				'@iconify/utils/lib/loader/fs',
				'@iconify/utils/lib/loader/install-pkg',
				'@iconify/utils/lib/loader/node-loader',
				'@iconify/utils/lib/loader/node-loaders',
			],
		},
	},
})
